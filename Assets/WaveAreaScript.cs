﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveAreaScript : MonoBehaviour
{
	public AudioSource waves;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider collider)
    {
		waves.Stop();
    }

	void OnTriggerExit(Collider collider)
	{
		waves.Play();
	}

}
