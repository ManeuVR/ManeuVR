﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionHandling : MonoBehaviour
{
    public float minDieVelocityThreshold = 200f;
    public float lowMediumVelocity = 4f;
    public float mediumHighVelocity = 8f;
    public bool isHumanPlayer;
    public Rigidbody rbody;

    float minDieThreshhold;
    float lowMediumThreshold;
    float mediumHighThreshold;

    public delegate void collisionDelegate();
    public event collisionDelegate deathCollisionEvent;
    public event collisionDelegate lowVelocityCollisionEvent;
    public event collisionDelegate mediumVelocityCollisionEvent;
    public event collisionDelegate highVelocityCollisionEvent;

    public delegate void playerDeathDelegate();
    public event playerDeathDelegate playerCollisionEvent;

    public event playerDeathDelegate borderCollisionEvent;
    public event playerDeathDelegate groundCollisionEvent;

    public delegate void triggerExitDelegate();
    public event triggerExitDelegate motorTriggerExitEvent;
    public event triggerExitDelegate obstacleTriggerExitEvent;
    public event triggerExitDelegate trailTriggerExitEvent;
    public event triggerExitDelegate borderTriggerExitEvent;

    private void Start()
    {
        minDieThreshhold = minDieVelocityThreshold * minDieVelocityThreshold;
        lowMediumThreshold = lowMediumVelocity * lowMediumVelocity;
        mediumHighThreshold = mediumHighVelocity * mediumHighVelocity;
    }

    private void OnCollisionEnter(Collision collision)
    {
        //Debug.Log("Collided with " + collision.gameObject.tag);

        if (collision.gameObject.CompareTag("ground"))
        {
            if (groundCollisionEvent != null) groundCollisionEvent.Invoke();
            //Debug.Log("Ground Collision");
        }
        if (collision.gameObject.CompareTag("border"))
        {
            if(borderCollisionEvent != null) borderCollisionEvent.Invoke();
            //Debug.Log("Border Collision");
        }

        float sqrVelocity = rbody.velocity.sqrMagnitude;

        if (sqrVelocity <= lowMediumThreshold)
        {
            if (lowVelocityCollisionEvent != null) lowVelocityCollisionEvent.Invoke();
        }
        else if(sqrVelocity <= mediumHighThreshold)
        {
            if (mediumVelocityCollisionEvent != null) mediumVelocityCollisionEvent.Invoke();
        }
        else
        {
            if (highVelocityCollisionEvent != null) highVelocityCollisionEvent.Invoke();
        }

        if (collision.gameObject.CompareTag("death"))
        {
            if (deathCollisionEvent != null) deathCollisionEvent.Invoke();
            if (isHumanPlayer)
            {
                if (playerCollisionEvent != null) playerCollisionEvent.Invoke();
                //Debug.Log("Player Death Collision");
            }
        }
        else if (collision.gameObject.CompareTag("trail"))
        {
            if (deathCollisionEvent != null) deathCollisionEvent.Invoke();
            if (isHumanPlayer)
            {
                if (playerCollisionEvent != null) playerCollisionEvent.Invoke();
                //Debug.Log("Player Death Collision");
            }
        }
        else if (collision.gameObject.CompareTag("motor") ||
            collision.gameObject.CompareTag("obstacle") ||
            collision.gameObject.CompareTag("border"))
        {
            //Debug.Log("Collision at velocity: " + rbody.velocity.magnitude);
            if (rbody.velocity.sqrMagnitude >= minDieThreshhold)
            {
                //Debug.Log("Death Collision");
                //die
                if (deathCollisionEvent != null) deathCollisionEvent.Invoke();
                if (isHumanPlayer)
                {
                    if(playerCollisionEvent != null) playerCollisionEvent.Invoke();
                    //Debug.Log("Player Death Collision");
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("death"))
        {
            if(deathCollisionEvent != null) deathCollisionEvent.Invoke();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("motor"))
        {
            if (motorTriggerExitEvent != null) motorTriggerExitEvent.Invoke();
        }
        else if (other.gameObject.CompareTag("obstacle"))
        {
            if (obstacleTriggerExitEvent != null) obstacleTriggerExitEvent.Invoke();
        }
        else if (other.gameObject.CompareTag("trail"))
        {
            if (trailTriggerExitEvent != null) trailTriggerExitEvent.Invoke();
        }
        else if (other.gameObject.CompareTag("border"))
        {
            if (borderTriggerExitEvent != null) borderTriggerExitEvent.Invoke();
        }
    }
}
