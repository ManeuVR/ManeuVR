﻿using UnityEngine;

[System.Serializable]
public class KeyboardInputManager : MonoBehaviour, IInputManager
{

    private float steer;

    public void processInput() { }

    public float getAcceleration()
    {
        float acceleration = Input.GetAxis("Vertical");
        return acceleration;
    }

    public float getSteer()
    {
        steer = Mathf.MoveTowards(steer, Input.GetAxis("Horizontal"), RacerController.steerSensitivity * Time.deltaTime);
        return steer;
    }

    public bool getBrake()
    {
        return Input.GetButton("Jump");
    }

    public bool getShift()
    {
        return Input.GetKey(KeyCode.LeftShift) | Input.GetKey(KeyCode.RightShift);
    }

    public bool getTrailToggle()
    {
        return Input.GetKeyDown(KeyCode.Q);
    }
}
