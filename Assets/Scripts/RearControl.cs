﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RearControl : MonoBehaviour
{
    public Transform target;
    public Vector3 position;

    void Update()
    {
        Vector3 dist = transform.localPosition - target.localPosition;
        Quaternion rot = Quaternion.LookRotation(dist + position);
        transform.localRotation = rot;
    }
}
