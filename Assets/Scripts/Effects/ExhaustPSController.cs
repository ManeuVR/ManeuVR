﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExhaustPSController : MonoBehaviour
{
    public RacerController racerController;
    public ParticleSystem exhaustParticles;
    private int emitCounter = 0;

    public void Update()
    {
        ParticleSystem.EmissionModule emissionModule = exhaustParticles.emission;
        ParticleSystem.MainModule mainModule = exhaustParticles.main;
        float racerSpeed = racerController.getSpeed();

        mainModule.startLifetime = 0.5f + racerSpeed / 10;

        ++emitCounter;
        if (racerSpeed > 1.5f && emitCounter > 3 || emitCounter > 9)
        {
            emitCounter = 0;
            exhaustParticles.Emit(1);
        }

    }
}
