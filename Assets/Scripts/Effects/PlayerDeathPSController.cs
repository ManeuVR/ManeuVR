﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDeathPSController : MonoBehaviour
{
    public CollisionHandling playerStatus;
    public GameObject deathEffect;

    private void OnEnable()
    {
        playerStatus.deathCollisionEvent += DeathEffectSpawn;
    }
    private void OnDisable()
    {
        playerStatus.deathCollisionEvent -= DeathEffectSpawn;
    }

    private void DeathEffectSpawn()
    {
        Instantiate(deathEffect).transform.position = transform.position;
    }
}
