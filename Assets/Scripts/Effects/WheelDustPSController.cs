﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WheelDustPSController : MonoBehaviour
{
    public WheelCollisionHandling wheelStatus;
    public RacerController racerController;
    public ParticleSystem dustParticles = new ParticleSystem();

    private System.Random rand = new System.Random();

    public void Update()
    {
        ParticleSystem.EmissionModule emissionModule = dustParticles.emission;
        ParticleSystem.MainModule mainModule = dustParticles.main;
        float racerSpeed = racerController.getSpeed();
        bool enableParticles = wheelStatus.IsGrounded && racerSpeed > 1.5f;

        if (enableParticles)
        {
            emissionModule.enabled = true;
            mainModule.startLifetime = 0.3f + racerSpeed / 10;
            mainModule.startSize = 1 - 1.5f / racerSpeed;
            bool doEmit = true;
            if (racerSpeed < 10)
            {
                doEmit = rand.Next(101) > 50;
            }

            if (doEmit)
            {
                dustParticles.Emit(1);
            }
        }
        else
        {
            emissionModule.enabled = false;
        }
    }
}
