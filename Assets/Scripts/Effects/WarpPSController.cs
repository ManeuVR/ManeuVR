﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarpPSController : MonoBehaviour
{
    public RacerController racerController;
    public ParticleSystem warpParticles;

    void Update()
    {
        ParticleSystem.EmissionModule emissionModule = warpParticles.emission;
        ParticleSystem.MainModule mainModule = warpParticles.main;
        float racerSpeed = racerController.getSpeed();

        if (racerSpeed > 10)
        {
            float alpha = racerSpeed / 20 - 0.9f;

            Color colour = new Color(0, 0, 1, alpha);

            mainModule.startColor = colour;
            emissionModule.rateOverTime = 200;

            warpParticles.Emit(1);
        }

    }
}
