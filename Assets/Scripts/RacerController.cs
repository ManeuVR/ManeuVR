﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RacerController : MonoBehaviour, ControllerInterface
{
    public static float steerSensitivity = 10.0f;

    public delegate void goingBackwardsDelegate();
    public event goingBackwardsDelegate goingBackwardsEvent;

    public delegate void goingForwardsDelegate();
    public event goingForwardsDelegate goingForwardsEvent;

    public delegate void trailToggleDelegate();
    public event trailToggleDelegate trailToggleEvent;

    public delegate void trailResetDelegate();
    public event trailResetDelegate trailResetEvent;

    private bool goingForward = false;

    public Transform mainBody;

    public Transform bikeSteer;

    public Transform fpsCamera;

    public WheelProperties frontWheelProperties;
    public WheelProperties rearWheelProperties;

    [SerializeField]
    private IInputManager inputManager;

    private Wheel frontWheel;
    private Wheel rearWheel;

    private float finalDriveRatio = 4.0f;

    [System.Serializable]
    public class WheelProperties
    {
        public float radius = 0.32f;
        public float weight = 1000.0f;
        public float suspensionDistance = 0.2f;

        public Transform wheelMesh;
        public Transform axleMesh;
    }

    private class Wheel
    {
        public readonly WheelProperties properties;

        public readonly Vector3 initialPosition;

        public WheelCollider collider;

        public Wheel(WheelProperties properties, Vector3 initialPosition, WheelCollider collider)
        {
            this.properties = properties;
            this.initialPosition = initialPosition;
            this.collider = collider;
        }
    }

    private float springStiffness = 35000.0f;
    private float springDamper = 4000.0f;

    private float brakePower = 6000;
    private float slipBrake = 3.0f;
    private float internalFriction = 200;
    private bool brake;
    private float slip;

    private float shiftDownRpm = 3200.0f;
    private float shiftUpRpm = 4300.0f;
    private float idleRpm = 1000.0f;
    private const float maxGoundRpm = 4500;
    private const float maxAirRpm = 5000;
    private float maxRpm = maxGoundRpm;
    private float motorRpm;
    private float soundRpm;

    public float GetMaxRpm { get => maxGoundRpm; }
    public float GetCurrentRpm { get => motorRpm; }

    public float CurrentRPM { get => motorRpm; }
    public float MaxRPM { get => maxRpm; }

    private Vector3 shiftCenterOfMass = new Vector3(0.0f, 0.0f, 0.0f);

    private float wheelStiffness = 1.0f; // Determines slip

    private float rearTorque = 2000;
    private float maxRearSpeed = 5;

    // Efficiency table for certain RPMs. Currently the most efficient RPM is at around 3000.
    private float efficiencyTableStep = 250.0f;
    private float[] efficiencyTable = { 200, 250, 300, 350, 370, 380, 390, 400, 405, 410, 420,
        425, 405, 390, 370, 360 };

    private int currentGear = 0;
    private float[] gearRpmRatios = { 2.0f, 2.5f, 2.0f, 1.5f };
    private float[] gearTorqueRatios = { 0.0f, 3f, 1.5f, 1.5f };

    private float maxLeanAngle = 1.5f;
    private float currentLeanAngle;

    private float maxSteerAngle = 30.0f;
    private float steer = 0;
    private Quaternion steerRotation;

    private const float maxShiftPower = 100;
    private float shiftPower = maxShiftPower;
    private bool shift;
    private bool shiftPossible;
    private bool shifting;
    private float maxWheelieAngle = 30.0f;
    private float speedWheelie = 10.0f;
    private float wheelie;

    private float acceleration; // Between -1.0 .. 1.0

    private float speed;

    private Rigidbody rigidBody;

    private bool backwards;

    bool grounded = true;

    private WheelCollider getWheelCollider(WheelProperties properties)
    {
        GameObject obj = properties.wheelMesh.GetChild(0).gameObject;
        obj.transform.parent = transform;
        obj.transform.position = properties.wheelMesh.position;
        obj.transform.eulerAngles = transform.eulerAngles;
        obj.AddComponent(typeof(WheelCollider));
        WheelCollider collider = obj.GetComponent<WheelCollider>();

        // Set suspension properties
        collider.suspensionDistance = properties.suspensionDistance;
        JointSpring js = collider.suspensionSpring;
        js.spring = springStiffness;
        js.damper = springDamper;
        collider.suspensionSpring = js;

        collider.radius = properties.radius;
        collider.mass = properties.weight;

        // Set curve for forward friction
        WheelFrictionCurve fc = collider.forwardFriction;
        fc.asymptoteValue = 20.0f;
        fc.extremumValue = 30.0f;
        fc.extremumSlip = 3.0f;
        fc.asymptoteSlip = 3.0f;
        fc.stiffness = wheelStiffness;
        collider.forwardFriction = fc;

        // Set curve for sideways friction
        fc = collider.sidewaysFriction;
        fc.asymptoteValue = 1.1f;
        fc.extremumValue = 1.3f;
        fc.extremumSlip = 0.2f;
        fc.asymptoteSlip = 0.2f;
        fc.stiffness = wheelStiffness;
        collider.sidewaysFriction = fc;

        return collider;
    }

    private Wheel wheelSetup(WheelProperties properties)
    {
        Vector3 initialPosition = properties.axleMesh.transform.localPosition;
        WheelCollider wheelCollider = getWheelCollider(properties);
        return new Wheel(properties, initialPosition, wheelCollider);
    }

    void Awake()
    {
        rigidBody = transform.GetComponent<Rigidbody>();
        rigidBody.centerOfMass = shiftCenterOfMass;

        steerRotation = bikeSteer.localRotation;

        frontWheel = wheelSetup(frontWheelProperties);
        rearWheel = wheelSetup(rearWheelProperties);

        inputManager = GetComponent<IInputManager>();
    }

    void Update()
    {
        // Get input
        inputManager.processInput();
        steer = inputManager.getSteer();
        acceleration = inputManager.getAcceleration();
        brake = inputManager.getBrake();
        shift = inputManager.getShift();

        speed = rigidBody.velocity.magnitude;
        backwards = transform.InverseTransformDirection(rigidBody.velocity).z < 0;

        checkTrailToggle();

        motorRpm = Mathf.Max(idleRpm, rearWheel.collider.rpm * finalDriveRatio * gearRpmRatios[currentGear] * 1.1f + idleRpm);
        if (motorRpm > shiftUpRpm && currentGear < gearRpmRatios.Length - 1)
        {
            currentGear++;
        }
        else if (motorRpm < shiftDownRpm && currentGear > 0)
        {
            currentGear--;
        }
        if (currentGear == 0 && acceleration > 0)
        {
            currentGear = 1;
        }
        motorRpm = Mathf.Max(idleRpm, rearWheel.collider.rpm * finalDriveRatio * gearRpmRatios[currentGear] * 1.1f + idleRpm);

        soundRpm -= 1000 * Time.deltaTime;
        soundRpm += acceleration * 1000 * Time.deltaTime;
        soundRpm *= gearRpmRatios[currentGear] * 0.9f;
        soundRpm = Mathf.Clamp(soundRpm, idleRpm, motorRpm);

        rigidBody.angularDrag = 9.0f;
        grounded = frontWheel.collider.isGrounded || rearWheel.collider.isGrounded;
        if (!grounded)
        {
            rigidBody.angularDrag = 9.0f;

            // Controls jump time
            rigidBody.AddForce(0, -1000, 0);
            maxRpm = maxAirRpm;
        }
        else
        {
            maxRpm = maxGoundRpm;
        }

        // Set rear wheel collider properties
        updateRearWheelCollider();

        // Set front wheel collider properties
        updateFrontWheelCollider();

        // Update wheel meshes
        updateWheelMesh(frontWheel);
        updateWheelMesh(rearWheel);
    }

    void FixedUpdate()
    {
        // Do wheelie but only with limited power
        if (shift && shiftPossible)
        {
            shifting = true;
            if (shiftPower == 0)
            {
                shiftPossible = false;
            }
            shiftPower = Mathf.MoveTowards(shiftPower, 0.0f, Time.deltaTime * 20.0f);
        }
        else
        {
            shifting = false;
            if (shiftPower > 20)
            {
                shiftPossible = true;
            }
            shiftPower = Mathf.MoveTowards(shiftPower, maxShiftPower, Time.deltaTime * 5.0f);
        }

        rigidBody.transform.eulerAngles =
            new Vector3(rigidBody.transform.eulerAngles.x, rigidBody.transform.eulerAngles.y, 0.0f);

        // Update lean angle
        currentLeanAngle = Mathf.LerpAngle(currentLeanAngle,
            -steer * 70 * maxLeanAngle * Mathf.Clamp(speed / 80.0f, 0.0f, 100.0f), Time.deltaTime * 5.0f);

        // Raise front wheel if the player is shifting
        if (shifting && speed > 7)
        {
            wheelie += speedWheelie * Time.deltaTime / (speed / 50);
        }
        else
        {
            wheelie = Mathf.SmoothStep(wheelie, -maxWheelieAngle / 2, (speedWheelie) * Time.deltaTime * 0.8f);
        }
        wheelie = Mathf.Clamp(wheelie, 0, maxWheelieAngle);

        bikeSteer.localRotation = steerRotation * Quaternion.Euler(0, frontWheel.collider.steerAngle, 0);
        mainBody.localRotation = Quaternion.Euler(-wheelie, 0, currentLeanAngle);

        double diff = speed - rearWheel.collider.rpm * 2 * 3.14 * frontWheelProperties.radius / 60;
        //Debug.Log("Motor rpm = " + motorRpm + " Gear: " + currentGear + " Speed: " + speed + " Diff: " + diff);
        /*Debug.Log("RW brake torque = " + rearWheel.collider.brakeTorque + "  FW brake torque = " + frontWheel.collider.brakeTorque +
            " RW rpm: " + rearWheel.collider.rpm + " FW rpm: " + frontWheel.collider.rpm + " FW motorTorque: " + frontWheel.collider.motorTorque +
            " RW motorTorque: " + rearWheel.collider.motorTorque + " Speed: " + speed);*/

        checkVelocityDirection();
    }

    private void updateRearWheelCollider()
    {
        WheelCollider collider = rearWheel.collider;

        int rpmIndex = (int)(motorRpm / efficiencyTableStep);
        if (rpmIndex >= efficiencyTable.Length)
        {
            rpmIndex = efficiencyTable.Length - 1;
        }
        collider.motorTorque = efficiencyTable[rpmIndex] *
            gearTorqueRatios[currentGear] * finalDriveRatio * acceleration * 0.9f;

        if (acceleration < 0.0f || brake)
        {
            if (brake && acceleration > 0.0f)
            {
                slip = Mathf.Lerp(slip, slipBrake, acceleration * 0.01f);
            }
            else if (speed > 0.5f)
            {
                slip = Mathf.Lerp(slip, 1.0f, 0.005f);
            }
            else
            {
                slip = Mathf.Lerp(slip, 1.0f, 0.02f);
            }
            collider.motorTorque = 0;
            collider.brakeTorque = brakePower;
        }
        else
        {
            collider.brakeTorque = Mathf.Abs(collider.rpm) > 100 ? internalFriction : 0;
            if (motorRpm > maxRpm)
            {
                collider.motorTorque = 0;
            }
            slip = Mathf.Lerp(slip, 1.0f, 0.02f);
        }

        if (acceleration < 0 && currentGear == 0 && speed < maxRearSpeed && !brake)
        {
            collider.motorTorque = -rearTorque;
            collider.brakeTorque = 0;
        }

        WheelFrictionCurve fc = collider.sidewaysFriction;
        fc.stiffness = wheelStiffness / slip;
        collider.sidewaysFriction = fc;

        rearWheel.collider = collider;
    }

    private void updateFrontWheelCollider()
    {
        WheelCollider collider = frontWheel.collider;

        float relativeSteerAngle = Mathf.Clamp(speed * 2.5f / maxSteerAngle, 1.0f, maxSteerAngle);
        collider.steerAngle = steer * (maxSteerAngle / relativeSteerAngle);

        collider.brakeTorque = 0;
        collider.motorTorque = 0;
        if (!brake && Mathf.Abs(collider.rpm) > 100)
        {
            collider.brakeTorque = internalFriction;
        }
        else if (acceleration < 0 && currentGear == 0 && !brake && (speed < maxRearSpeed || collider.rpm >= 0))
        {
            collider.motorTorque = -rearTorque;
        }

        frontWheel.collider = collider;
    }

    private void updateWheelMesh(Wheel wheel)
    {
        WheelCollider collider = wheel.collider;

        // Calculate rotation based on rmp and time passed
        wheel.properties.wheelMesh.Rotate(Vector3.right, Time.deltaTime * collider.rpm * 6.0f, Space.Self);

        // If collider hit the ground update wheel mesh accordingly
        Vector3 localPos = wheel.properties.axleMesh.localPosition;
        if (collider.GetGroundHit(out WheelHit hit))
        {
            localPos.y -= Vector3.Dot(wheel.properties.wheelMesh.position - hit.point, transform.TransformDirection(0, 1, 0)) - collider.radius;
            localPos.y = Mathf.Clamp(localPos.y, wheel.initialPosition.y -
                wheel.properties.suspensionDistance, wheel.initialPosition.y + wheel.properties.suspensionDistance);
        }
        else
        {
            localPos.y = wheel.initialPosition.y - wheel.properties.suspensionDistance;
        }
        wheel.properties.axleMesh.localPosition = localPos;
    }

    private void checkVelocityDirection()
    {
        Vector3 velocity = rigidBody.velocity;
        Vector3 localVelocity = transform.InverseTransformDirection(velocity);

        //assuming positive z of the rigidbody is pointing in the forward direction
        if (localVelocity.z > 0 && rigidBody.velocity.sqrMagnitude > 1)
        {
            if (!goingForward)
            {
                //Debug.Log("velocity in forward direction");
                goingForward = true;
                if(goingForwardsEvent != null) goingForwardsEvent.Invoke();
            }
        }
        else
        {
            if (goingForward)
            {
                //Debug.Log("velocity in backward direction");
                goingForward = false;
                if(goingBackwardsEvent != null) goingBackwardsEvent.Invoke();
            }
        }
    }

    private void checkTrailToggle()
    {
        if (inputManager.getTrailToggle())
        {
            //Debug.Log("Trail toggled");
            trailToggleEvent.Invoke();
        }
    }

    public float getSpeed()
    {
        return rigidBody.velocity.magnitude;
    }

    public float getRPM()
    {
        return soundRpm;
    }

    public bool isBraking()
    {
        return brake;
    }

    public bool isGrounded()
    {
        return grounded;
    }

    public float getMaxRPM()
    {
        return maxGoundRpm;
    }

}