﻿public interface IInputManager
{
    void processInput();
    float getAcceleration();
    float getSteer();
    bool getBrake();
    bool getShift();
    bool getTrailToggle();
}
