﻿public interface ControllerInterface
{
    float getSpeed();
    float getRPM();
    bool isBraking();
    bool isGrounded();
    float getMaxRPM();
}
