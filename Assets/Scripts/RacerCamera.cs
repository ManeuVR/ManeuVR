using UnityEngine;
public class RacerCamera : MonoBehaviour
{

    public Transform target;

    public float smooth = 0.3f;
    public float distance = 3.0f;
    public float height = 0.5f;
    public float angle = 20;

    public LayerMask lineOfSightMask = 0;

    private float yVelocity = 0.0f;
    private float xVelocity = 0.0f;

    private RacerController racerScript;

    private int currCamera = 1;

    private bool sView = false;

    public Transform firstPerson;

    public Transform sideView;

    void Update()
    {
        // If there is no one to follow dont update camera position
        if (!target) return;

        racerScript = target.GetComponent<RacerController>();

        if (Input.GetKeyDown(KeyCode.C))
        {
            currCamera = currCamera == 0 ? 1 : 0;
        }

        if (Input.GetKeyDown(KeyCode.M))
        {
            sView = !sView;
        }

        if (sView)
        {
            transform.position = sideView.position;
            transform.rotation = sideView.rotation;
        }
        else if (currCamera == 0)
        {
            // Adjust field of view based on speed
            //GetComponent<Camera>().fieldOfView = Mathf.Clamp(racerScript.speed / 10.0f + 60.0f, 60, 90.0f);

            // Damp angles from current towards target
            float xAngle = Mathf.SmoothDampAngle(transform.eulerAngles.x,
                target.eulerAngles.x + angle, ref xVelocity, smooth);

            float yAngle = Mathf.SmoothDampAngle(transform.eulerAngles.y,
                target.eulerAngles.y, ref yVelocity, smooth);

            // Look at the target
            transform.eulerAngles = new Vector3(xAngle, yAngle, 0.0f);
            var direction = transform.rotation * -Vector3.forward;
            transform.position = target.position + new Vector3(0, height, 0) + direction * distance;
        }
        else
        {
            transform.position = firstPerson.position;
            Quaternion rotation = firstPerson.rotation;
            rotation.eulerAngles = new Vector3(rotation.eulerAngles.x, rotation.eulerAngles.y, 0.0f);
            transform.rotation = Quaternion.Lerp(transform.rotation, rotation, Time.deltaTime * 5.0f);
        }
    }

}
