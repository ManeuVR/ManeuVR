﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WheelCollisionHandling : MonoBehaviour
{
    public Rigidbody motorRbody;

    public float mediumLowVelocityThreshold = 4f;
    public float mediumHighVelocityThreshold = 8f;

    private float mediumLow;
    private float mediumHigh;

    public bool IsGrounded { get; set; } = false;

    public delegate void collisionEnterDelegate();
    public event collisionEnterDelegate lowVelocityCollisionEvent;
    public event collisionEnterDelegate mediumVelocityCollisionEvent;
    public event collisionEnterDelegate highVelocityCollisionEvent;

    public delegate void collisionExitDelegate();
    public event collisionExitDelegate collisionExitEvent;

    private WheelCollider wheelCollider;
    private WheelHit hit;
    private bool lastIsGrounded;

    private void Start()
    {
        mediumLow = mediumLowVelocityThreshold * mediumLowVelocityThreshold;
        mediumHigh = mediumHighVelocityThreshold * mediumHighVelocityThreshold;

        wheelCollider = GetComponent<WheelCollider>();
    }

    private void CollisionEnter(WheelHit hit)
    {
        IsGrounded = true;
        float velocity = motorRbody.velocity.sqrMagnitude;

        //Debug.Log("Wheel collision");

        if (velocity < mediumLow)
        {
            if (lowVelocityCollisionEvent != null) lowVelocityCollisionEvent.Invoke();
            //Debug.Log("Low veolcity collision");
        }
        else if (velocity < mediumHigh)
        {
            if (mediumVelocityCollisionEvent != null) mediumVelocityCollisionEvent.Invoke();
            //Debug.Log("Medium veolcity collision");
        }
        else
        {
            if (highVelocityCollisionEvent != null) highVelocityCollisionEvent.Invoke();
            //Debug.Log("High veolcity collision");
        }
    }

    private void CollisionExit(WheelHit hit)
    {
        IsGrounded = false;
        if (collisionExitEvent != null) collisionExitEvent.Invoke();
    }

    private void FixedUpdate()
    {
        bool currentGrounded = wheelCollider.GetGroundHit(out hit);
        IsGrounded = currentGrounded;
        if (currentGrounded)
        {
            if (!lastIsGrounded)
            {
                CollisionEnter(hit);
            }
        }
        else
        {
            if (lastIsGrounded) 
            {
                CollisionExit(hit);
            }
        }
        lastIsGrounded = currentGrounded;
    }
}
