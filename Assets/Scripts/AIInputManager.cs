﻿using UnityEngine;
public class AIInputManager : MonoBehaviour, IInputManager
{
    private float steer;

    private float acceleration;

    private bool brake;

    private bool shift;

    public void processInput() { }

    public void setAcceleration(float acceleration)
    {
        if (acceleration < -1 || acceleration > 1)
        {
            throw new System.ArgumentException("Acceleration value has to be between -1 and 1");
        }
        this.acceleration = acceleration;
    }

    public void setSteer(float steer)
    {
        if (steer < -1 || steer > 1)
        {
            throw new System.ArgumentException("Steer value has to be between -1 and 1");
        }
        this.steer = Mathf.MoveTowards(this.steer, steer, RacerController.steerSensitivity * Time.deltaTime);
    }

    public void setBrake(bool brake)
    {
        this.brake = brake;
    }

    public void setShift(bool shift)
    {
        this.shift = shift;
    }

    public float getAcceleration()
    {
        return acceleration;
    }

    public float getSteer()
    {
        return steer;
    }

    public bool getBrake()
    {
        return brake;
    }

    public bool getShift()
    {
        return shift;
    }

    public bool getTrailToggle()
    {
        return false;
    }
}
