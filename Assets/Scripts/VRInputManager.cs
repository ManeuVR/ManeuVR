﻿using UnityEngine;
using UnityEngine.XR;
using System.Collections.Generic;
using System.Collections;
using Valve.VR;

[System.Serializable]
public class VRInputManager : MonoBehaviour, IInputManager
{
    public SteamVR_Action_Boolean interactWithUI = SteamVR_Input.GetBooleanAction("InteractUI");

    public SteamVR_Input_Sources handType;

    private const float maxZDistance = 0.16f;

    private const float maxAccelerationAngle = 40.0f;

    private const float rightZ = 90;

    private List<XRNodeState> nodeStatesCache = new List<XRNodeState>();

    private float acceleration;
    private float steer;
    private bool shift;

    protected virtual void OnEnable()
    {
        SteamVR.Initialize();
    }

    public void processInput()
    {
        acceleration = 0.0f;
        float rightZ = 0.0f, leftZ = 0.0f;
        float rightY = -1, leftY = -1;
        InputTracking.GetNodeStates(nodeStatesCache);
        for (int i = 0; i < nodeStatesCache.Count; i++)
        {
            XRNodeState nodeState = nodeStatesCache[i];
            if (nodeState.nodeType == XRNode.RightHand)
            {
                calculateAcceleration(nodeState);
                Vector3 position = Vector3.zero;
                nodeState.TryGetPosition(out position);
                rightZ = position.z;
                rightY = position.y;
            }
            else if (nodeState.nodeType == XRNode.LeftHand)
            {
                Vector3 position = Vector3.zero;
                nodeState.TryGetPosition(out position);
                leftZ = position.z;
                leftY = position.y;
            }
        }
        if (rightZ >= leftZ)
        {
            steer = -Mathf.Min(maxZDistance, rightZ - leftZ) / maxZDistance;
        }
        else
        {
            steer = Mathf.Min(maxZDistance, leftZ - rightZ) / maxZDistance;
        }
        shift = rightY > 0 || leftY > 0;
    }

    private void calculateAcceleration(XRNodeState rightControllerState)
    {
        Quaternion rotation = Quaternion.identity;
        rightControllerState.TryGetRotation(out rotation);
        float rotationZ = rotation.eulerAngles.z;
        if (rotationZ > 340)
        {
            rotationZ -= 360;
        }
        if (rotationZ <= rightZ + 90 && rotationZ > -20)
        {
            acceleration = rightZ - rotationZ;
            acceleration = Mathf.Min(maxAccelerationAngle, Mathf.Abs(acceleration)) * Mathf.Sign(acceleration);
        }
        acceleration /= maxAccelerationAngle;
    }

    public float getAcceleration()
    {
        return acceleration;
    }

    public float getSteer()
    {
        return steer;
    }

    public bool getShift()
    {
        return shift;
    }

    public bool getBrake()
    {
        return interactWithUI != null && interactWithUI.GetState(handType);
    }

    // TODO: Implement this
    public bool getTrailToggle()
    {
        return false;
    }
}
