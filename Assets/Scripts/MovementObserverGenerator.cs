﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementObserverGenerator : MonoBehaviour
{
    public GameObject movementObserverPrefab;
    public GameObject killPlayerPrefab;
    public Transform target;
    public float spawnInterval = 1f;
    public float trackingDuration = 5f;
    public float minDistance = 20f;

    void Start()
    {
        InvokeRepeating("GenerateObserver", spawnInterval, spawnInterval);   
    }

    private void GenerateObserver()
    {
        PlayerMovementObserver observer = Instantiate(movementObserverPrefab).GetComponent<PlayerMovementObserver>();

        observer.target = target;
        observer.timer = trackingDuration;
        observer.killPlayerPrefab = killPlayerPrefab;
        observer.minDistance = minDistance;

        observer.Track();
    }
}
