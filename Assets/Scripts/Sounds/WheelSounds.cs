﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WheelSounds : MonoBehaviour
{
    [SerializeField]
    private WheelCollisionHandling wheelCollisions;

    public AudioSource groundCollisionSoft;
    public AudioSource groundCollisionHard;

    private void Start()
    {
        wheelCollisions.lowVelocityCollisionEvent += PlayCollisionSoft;
        wheelCollisions.mediumVelocityCollisionEvent += PlayCollisionSoft;
        wheelCollisions.highVelocityCollisionEvent += PlayCollsionHard;
    }

    private void OnDisable()
    {
        wheelCollisions.lowVelocityCollisionEvent -= PlayCollisionSoft;
        wheelCollisions.mediumVelocityCollisionEvent -= PlayCollisionSoft;
        wheelCollisions.highVelocityCollisionEvent -= PlayCollsionHard;
    }

    public void PlayCollisionSoft()
    {
        groundCollisionSoft.Play();
    }


    public void PlayCollsionHard()
    {
        groundCollisionHard.Play();
    }
}

