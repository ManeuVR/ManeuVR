﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MotorSounds : MonoBehaviour
{
    [SerializeField]
    private CollisionHandling collisionHandler;
    [SerializeField]
    private WheelCollisionHandling wheelCollisions;
  
    public AudioSource collisionSoft;
    public AudioSource collisionMedium;
    public AudioSource collisionHard;
    public AudioSource motorSound;
    public AudioSource startMotor;
    public AudioSource bgm;
    public AudioSource ambientBirds;
    public AudioSource deathSound;
    public AudioSource brakeLong;
    public AudioSource owSound;
    public AudioSource fallQuiet;
    public AudioSource fallLoud;
    public const float maxSpeed = 17.74f;
    [HideInInspector]
    public int strokeIndex = 0;

    private bool isBrakingSound=false;
    private bool collisionDelay = false;

    private ControllerInterface audioInterface;

    void Awake()
    {
        audioInterface = GetComponentInParent<ControllerInterface>();
    }

    private void Start()
    {
        collisionHandler.deathCollisionEvent += PlayDeathSound;
        collisionHandler.lowVelocityCollisionEvent += PlayCollision;
        collisionHandler.mediumVelocityCollisionEvent += PlayCollision;
        collisionHandler.highVelocityCollisionEvent += PlayCollision;
        wheelCollisions.lowVelocityCollisionEvent += PlayFallQuiet;
        wheelCollisions.mediumVelocityCollisionEvent += PlayFallLoud;
        wheelCollisions.highVelocityCollisionEvent += PlayFallLoud;
        StartCoroutine(playStartSound());
    }

    private void Update()
    {
        engineSound();
        brakeSound();
    }

    private void OnDisable()
    {
        collisionHandler.deathCollisionEvent -= PlayDeathSound;
        collisionHandler.lowVelocityCollisionEvent -= PlayCollision;
        collisionHandler.mediumVelocityCollisionEvent -= PlayCollision;
        collisionHandler.highVelocityCollisionEvent -= PlayCollision;
        wheelCollisions.lowVelocityCollisionEvent -= PlayFallQuiet;
        wheelCollisions.mediumVelocityCollisionEvent -= PlayFallLoud;
        wheelCollisions.highVelocityCollisionEvent -= PlayFallLoud;
    }

    public void PlayFallLoud()
    {
        fallLoud.Play();
    }

    public void PlayFallQuiet()
    {
        fallQuiet.Play();
    }

    public void PlayCollision()
    {
        string quality = getSpeedQuality();
        if (quality.Equals("slow"))
        {
            PlayCollisionSoft();
        } else if (quality.Equals("medium"))
        {
            PlayCollisionMedium();
        }
        else
        {
            PlayCollisionHard();
        }
    }

    public void PlayCollisionSoft()
    {
        //Debug.Log("soft");
        float pitchMin = 0.9f;
        float pitchMax = 1.2f;
     
        float speedRatio = getSpeedRatio();
        collisionSoft.volume = speedRatio + 0.1f;
        collisionSoft.pitch = Random.Range(pitchMin, pitchMax);
        collisionSoft.Play();
    }

    public void PlayCollisionMedium()
    {
        //Debug.Log("medium");
        if (!collisionDelay)
        {
            float pitchMin = 0.9f;
            float pitchMax = 1.2f;

            float speedRatio = getSpeedRatio();
            collisionMedium.volume = speedRatio + 0.1f;
            collisionMedium.pitch = 0.85f* Random.Range(pitchMin, pitchMax); ;
            collisionMedium.volume = speedRatio;
            collisionMedium.Play();
            collisionWait();
        }
    }

    public void PlayCollisionHard()
    {
        //Debug.Log("hard");

        if (!collisionDelay)
        {
            float pitchMin = 0.9f;
            float pitchMax = 1.2f;
            float owChance = Random.Range(0, 15);

            float speedRatio = getSpeedRatio();
            collisionHard.volume = speedRatio - 0.3f;
            collisionHard.pitch = Random.Range(pitchMin, pitchMax);
            if (owChance == 1)
            {

                owSound.Play();
            }
            collisionHard.Play();
            collisionWait();
        }
        
    }

    public void PlayDeathSound()
    {
        //Debug.Log("death");
        deathSound.Play();
    }

    IEnumerator playStartSound()
    {
        startMotor.Play();
        yield return new WaitForSeconds(startMotor.clip.length -1f);
        motorSound.Play();
        bgm.Play();
    }

    IEnumerator collisionWait()
    {
        collisionDelay = true;
        yield return new WaitForSeconds(0.3f);
        collisionDelay = false;
    }

    IEnumerator playBrakeSound()
    {
        isBrakingSound = true;
        brakeLong.Play();
        yield return new WaitForSeconds(brakeLong.clip.length + 1.0f);
        isBrakingSound = false;
    }

    void engineSound()
    {
        float pitchMin = 0.98f;
        float pitchMax = 1.02f;
        float[] strokeArray = { 0.05f, -0.05f, 0.0f, -0.05f };
        float rpm = audioInterface.getRPM();
        motorSound.pitch = (2.5f * audioInterface.getRPM() / audioInterface.getMaxRPM()) 
            * Random.Range(pitchMin, pitchMax) + strokeArray[strokeIndex];

        strokeIndex = (strokeIndex + 1) % 4;
    }

    public void brakeSound()
    {
        bool brake = audioInterface.isBraking();
        if (audioInterface.isGrounded() && brake && !isBrakingSound)
        {
            float pitchMin = 0.9f;
            float pitchMax = 1.1f;
            string speedQuality = getSpeedQuality();

            if (!speedQuality.Equals("slow"))
            {
                brakeLong.volume = getSpeedRatio() * getSpeedRatio() / 2f;
                brakeLong.pitch = Random.Range(pitchMin, pitchMax);
                StartCoroutine(playBrakeSound());
            }
        }
    }

    string getSpeedQuality()
    {
        float speed = audioInterface.getSpeed();
        //Debug.Log(speed);
        float lowMediumThreshold = 0.4f;
        float mediumHighThreshold = 0.7f;
        float speedRatio = speed / maxSpeed;
        
        if (speedRatio < lowMediumThreshold)
        {
            return "slow";
        }
        else if(speedRatio < mediumHighThreshold)
        {
            return "medium";
        }
        else
        {
            return "fast";
        }
    }

    float getSpeedRatio()
    {
        float speed = audioInterface.getSpeed();
        return speed / maxSpeed;
    }

}
