﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementObserver : MonoBehaviour
{
    public GameObject killPlayerPrefab;
    public Transform target;
    public float timer;
    public float minDistance;

    private bool isTracking = false;
    private Vector3 startPosition;
    private Vector3 lastPosition;
    private float totalDistance = 0;

    void Update()
    {
        if (isTracking)
        {
            timer -= Time.deltaTime;
            Vector3 currentPosition = target.position;
            //Debug.Log((currentPosition - startPosition).magnitude);
            totalDistance += (currentPosition - lastPosition).magnitude;
            lastPosition = currentPosition;
        }
        if (timer <= 0)
        {
            CheckTarget();
        }
    }

    public void Track()
    {
        isTracking = true;
        startPosition = target.position;
        lastPosition = startPosition;
    }

    private void CheckTarget()
    {
        if (totalDistance <= minDistance) 
        {
            GameObject killPlayerGO = Instantiate(killPlayerPrefab);
            killPlayerGO.transform.position = lastPosition;
        }

        Destroy(gameObject);
    }
}
