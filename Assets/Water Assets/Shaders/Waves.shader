﻿Shader "Custom/Waves"
{
    Properties
    {
        _Colour ("Colour", Color) = (0,0,1,1)
		_Glossiness ("Glossiness", Range(0,1)) = 0.75
        _Metallic ("Metallic", Range(0,1)) = 0.0
		_WaveFoamDir("Wave foam direction", Vector) = (0,0,0,0)

		[Header(Distortions 1)]
		_NormalMap1("Normal map", 2D) = "white" {}
		_NormalMapMoveDir1("Movement direction", Vector) = (0,0,0,0)
		_NormalMapMoveSpeed1("Movement speed", Float) = 0
		[NoScaleOffset] _HeightMap1("Height map", 2D) = "black" {}

		[Header(Distortions 2)]
		_NormalMap2("Normal map", 2D) = "white" {}
		_NormalMapMoveDir2("Movement direction", Vector) = (0,0,0,0)
		_NormalMapMoveSpeed2("Movement speed", Float) = 0
		[NoScaleOffset] _HeightMap2("Height map", 2D) = "black" {}

		[Header(Distortion settings)]
		_NormalMapBias("Normal mapping strength", Range(0.0,1.0)) = 0.5
		_HeightMapStrength("Height mapping strength", Range(0,10)) = 0
		_HeightMapFoamColour("Height map foam colour", Color) = (1,1,1,1)
		_HeightMapFoamStrength("Height map foam strength", Range(0,2)) = 1

		[Header(Water fog)]
		_WaterFogColour("Water fog colour", Color) = (0, 0, 0, 0)
		_WaterFogDensity("Water fog density", Range(0, 10)) = 0.15

		[Header(Refraction)]
		_RefractionStrength("Refraction strength", Range(0, 1)) = 0.25

		[Header(Subsurface scattering)]
		_SSSPower("Power", Float) = 0

		[Header(Wave crest foam)]
		_FoamSpread("Foam scale", Range(0, 3.0)) = 1.5

		[Header(Intersection foam)]
		_IntersectionFoamDensity("Intersection foam range", Range(0, 10)) = 0.15
		_IntersectionFoamMap("Intersection foam map", 2D) = "black" {}

		[Header(Wave 1)]
		_Wavelength1("Wavelength", Float) = 1
		_Steepness1("Steepness", Float) = 1
		_DirectionX1("Direction X", Range(-1, 1)) = 1
		_DirectionY1("Direction Y", Range(-1, 1)) = 1

		[Header(Wave 2)]
		_Wavelength2("Wavelength", Float) = 1
		_Steepness2("Steepness", Float) = 1
		_DirectionX2("Direction X", Range(-1, 1)) = 1
		_DirectionY2("Direction Y", Range(-1, 1)) = 1
			
		[Header(Wave 3)]
		_Wavelength3("Wavelength", Float) = 1
		_Steepness3("Steepness", Float) = 1
		_DirectionX3("Direction X", Range(-1, 1)) = 1
		_DirectionY3("Direction Y", Range(-1, 1)) = 1		
			
		[Header(Wave 4)]
		_Wavelength4("Wavelength", Float) = 1
		_Steepness4("Steepness", Float) = 1
		_DirectionX4("Direction X", Range(-1, 1)) = 1
		_DirectionY4("Direction Y", Range(-1, 1)) = 1

		[Header(Wave 5)]
		_Wavelength5("Wavelength", Float) = 1
		_Steepness5("Steepness", Float) = 1
		_DirectionX5("Direction X", Range(-1, 1)) = 1
		_DirectionY5("Direction Y", Range(-1, 1)) = 1
			
		[Header(Wave 6)]
		_Wavelength6("Wavelength", Float) = 1
		_Steepness6("Steepness", Float) = 1
		_DirectionX6("Direction X", Range(-1, 1)) = 1
		_DirectionY6("Direction Y", Range(-1, 1)) = 1

		[Header(Wave 7)]
		_Wavelength7("Wavelength", Float) = 1
		_Steepness7("Steepness", Float) = 1
		_DirectionX7("Direction X", Range(-1, 1)) = 1
		_DirectionY7("Direction Y", Range(-1, 1)) = 1
			
		[Header(Wave 8)]
		_Wavelength8("Wavelength", Float) = 1
		_Steepness8("Steepness", Float) = 1
		_DirectionX8("Direction X", Range(-1, 1)) = 1
		_DirectionY8("Direction Y", Range(-1, 1)) = 1
    }
    SubShader
    {
		GrabPass{ "_WaterBackground" }
		Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" }
        LOD 200

		ZWrite off

        CGPROGRAM
		
		#pragma surface surf StandardTranslucent vertex:vert alpha finalcolor:reset_alpha

        //#pragma surface surf Standard alpha vertex:vert finalcolor:reset_alpha

        #pragma target 4.0

		#pragma shader_feature WAVE_OBJECT_ENABLED
		
		#include "UnityCG.cginc"
		#include "UnityPBSLighting.cginc"

        struct Input
        {
			float2 uv_NormalMap1;
			float2 uv_NormalMap2;
			float2 uv_IntersectionFoamMap;
			float4 screenPos;
			float3 worldDirNormal;
			float crestFactor;
		};

		sampler2D _NormalMap1, _NormalMap2, _HeightMap1, _HeightMap2, _CameraDepthTexture, _IntersectionFoamMap, _WaterBackground;
		half4 _NormalMapMoveDir1, _NormalMapMoveDir2;
		half _NormalMapMoveSpeed1, _NormalMapMoveSpeed2, _Glossiness, _Metallic, _IntersectionFoamDensity, _SSSPower;
        fixed4 _Colour, _WaveFoamDir, _SSSColour;
		float _HeightMapStrength,
				_DirectionX1, _DirectionX2, _DirectionX3,_DirectionX4, _DirectionX5, _DirectionX6, _DirectionX7, _DirectionX8,
				_DirectionY1, _DirectionY2, _DirectionY3, _DirectionY4, _DirectionY5, _DirectionY6, _DirectionY7, _DirectionY8,
				_Wavelength1, _Wavelength2, _Wavelength3, _Wavelength4, _Wavelength5, _Wavelength6, _Wavelength7, _Wavelength8,
				_Steepness1, _Steepness2, _Steepness3, _Steepness4, _Steepness5, _Steepness6, _Steepness7, _Steepness8,
				_WaveObjectDirectionX, _WaveObjectDirectionZ, _WaveObjectSteepness, _WaveObjectWavelength,
				_WaveObjectRadiusMax, _WaveObjectRadiusMin, _WaveObjectCurvature, _WaveObjectPositionX, _WaveObjectPositionZ,
				_FoamSpread, _NormalMapBias, _HeightMapFoamStrength, _WaterFogDensity, _RefractionStrength, _IntersectionFoamRange;
		float4 _WaterFogColour, _CameraDepthTexture_TexelSize;

		float4 gerstner_wave(float4 wave, float3 p, inout float3 tangent, inout float3 bitangent)
		{
			float steepness = wave.z;
			float wavelength = wave.w;
			float k = 2*UNITY_PI/wavelength;
			float c = sqrt(9.81/k);
			float2 d = normalize(wave.xy);
			float f =  k*(dot(d, p.xz) - c*_Time.y);
			float a = steepness/k; 

			tangent += float3(
				-d.x*d.x*steepness*sin(f),
				d.x*steepness*cos(f),
				-d.x*d.y*steepness*sin(f)
			);

			bitangent += float3(
				-d.x*d.y*steepness*sin(f),
				d.y*steepness*cos(f),
				- d.y*d.y*steepness*sin(f)
			);

			return float4(
				d.x*a*cos(f),
				a*sin(f),
				d.y*a*cos(f),
				sin(f)*saturate(steepness)
			);
		}

		float4 gerstner_wave_directional(float4 wave, float3 p, inout float3 tangent, inout float3 bitangent, float lerp_factor)
		{
			float3 t = tangent;
			float3 b = bitangent;

			float4 position_crest = gerstner_wave(wave, p, t, b);

			tangent += lerp(t, float3(0, 0, 0), lerp_factor);
			bitangent += lerp(b, float3(0, 0, 0), lerp_factor);

			return lerp(position_crest, float4(0, 0, 0, 0), lerp_factor);
		}

		void vert(inout appdata_full vertexData, out Input o)
		{
			UNITY_INITIALIZE_OUTPUT(Input, o);
			float3 world_position = mul(unity_ObjectToWorld, vertexData.vertex).xyz;

			float3 tangent = float3(1, 0, 0);
			float3 bitangent = float3(0, 0, 1);
			float4 position_crest = float4(world_position, 0);

			float4 wave1 = float4(_DirectionX1, _DirectionY1, _Steepness1, _Wavelength1);
			float4 wave2 = float4(_DirectionX2, _DirectionY2, _Steepness2, _Wavelength2);
			float4 wave3 = float4(_DirectionX3, _DirectionY3, _Steepness3, _Wavelength3);
			float4 wave4 = float4(_DirectionX4, _DirectionY4, _Steepness4, _Wavelength4);
			float4 wave5 = float4(_DirectionX5, _DirectionY5, _Steepness5, _Wavelength5);
			float4 wave6 = float4(_DirectionX6, _DirectionY6, _Steepness6, _Wavelength6);
			float4 wave7 = float4(_DirectionX7, _DirectionY7, _Steepness7, _Wavelength7);
			float4 wave8 = float4(_DirectionX8, _DirectionY8, _Steepness8, _Wavelength8);
			position_crest += gerstner_wave(wave1, world_position, tangent, bitangent);
			position_crest += gerstner_wave(wave2, world_position, tangent, bitangent);
			position_crest += gerstner_wave(wave3, world_position, tangent, bitangent);
			position_crest += gerstner_wave(wave4, world_position, tangent, bitangent);
			position_crest += gerstner_wave(wave5, world_position, tangent, bitangent);
			position_crest += gerstner_wave(wave6, world_position, tangent, bitangent);
			position_crest += gerstner_wave(wave7, world_position, tangent, bitangent);
			position_crest += gerstner_wave(wave8, world_position, tangent, bitangent);
			float factor = 8;

			#ifdef WAVE_OBJECT_ENABLED
			factor++;
			float4 wave9 = float4(_WaveObjectDirectionX, _WaveObjectDirectionZ, _WaveObjectSteepness, _WaveObjectWavelength);

			float3 localpos = vertexData.vertex.xyz;
			
			if (_WaveObjectCurvature < 0)
			{
				localpos.x += 1;
			}
			else 
			{
				localpos.x -= 1;
			}

			// TODO: world space?
			localpos.x = lerp(localpos.x, localpos.x*cos(localpos.z*_WaveObjectCurvature*UNITY_PI/360), 1);

			float dist_to_wave_object = length(world_position - float3(_WaveObjectPositionX, world_position.y, _WaveObjectPositionZ));
			float lerp_factor = saturate((dist_to_wave_object - _WaveObjectRadiusMin)/(_WaveObjectRadiusMax - _WaveObjectRadiusMin));
			//position_crest += gerstner_wave_directional(wave9, mul(unity_ObjectToWorld, localpos), tangent, bitangent, 1 - (vertexData.vertex.x + 1)/2);
			position_crest += gerstner_wave_directional(wave9, mul(unity_ObjectToWorld, localpos), tangent, bitangent, lerp_factor);
			#endif

			position_crest.w = max(0.01, position_crest.w/(factor*_FoamSpread));

			float height1 = tex2Dlod(_HeightMap1, float4(vertexData.texcoord.xy + _NormalMapMoveDir1.xy * _NormalMapMoveSpeed1 * _Time.y, 0, 0)).b;
			float height2 = tex2Dlod(_HeightMap2, float4(vertexData.texcoord.xy + _NormalMapMoveDir2.xy * _NormalMapMoveSpeed2 * _Time.y, 0, 0)).b;

			height1 = (height1 - 0.5) * 2;
			height2 = (height2 - 0.5) * 2;

			float height = (height1 + height2) / 2;
			position_crest.y += height*_HeightMapStrength;

			float3 normal = cross(bitangent, tangent);
			vertexData.normal = normalize(normal);
			
			vertexData.vertex.xyz = mul(unity_WorldToObject, float4(position_crest.xyz, 1.0)).xyz;
			o.worldDirNormal = normal;
			o.crestFactor = position_crest.w;
		}

		float2 align_with_grab_texel (float2 uv) 
		{
			#ifdef UNITY_UV_STARTS_AT_TOP
				if (_CameraDepthTexture_TexelSize.y < 0)
				{
					uv.y = 1 - uv.y;
				}
			#endif

			return (floor(uv * _CameraDepthTexture_TexelSize.zw) + 0.5) * abs(_CameraDepthTexture_TexelSize.xy);
		}

		float3 colour_below_water(float4 screen_pos, float3 tangent_space_normal, float2 foam_uv) 
		{
			float2 uv_offset = tangent_space_normal.xy * _RefractionStrength;
			uv_offset.y *= _CameraDepthTexture_TexelSize.z * abs(_CameraDepthTexture_TexelSize.y);
			float2 uv = align_with_grab_texel((screen_pos.xy + uv_offset) / screen_pos.w);
			float background_depth = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, uv));
			float surface_depth = UNITY_Z_0_FAR_FROM_CLIPSPACE(screen_pos.z);
			float depth_difference = background_depth - surface_depth;

			uv_offset *= saturate(depth_difference);
			uv = align_with_grab_texel((screen_pos.xy + uv_offset) / screen_pos.w);
			background_depth = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, uv));
			depth_difference = background_depth - surface_depth;
    
			float3 background_colour = tex2D(_WaterBackground, uv).rgb;
			float fog_factor = saturate(exp2(-_WaterFogDensity * depth_difference));
			float intersection_foam_range = saturate(exp2(-depth_difference));
			float3 colour_under_water = lerp(_WaterFogColour, background_colour, fog_factor); 
			float3 final_colour = colour_under_water + (intersection_foam_range * tex2D(_IntersectionFoamMap, foam_uv) * _IntersectionFoamDensity);

			return final_colour;
		}

		inline fixed4 LightingStandardTranslucent(SurfaceOutputStandard s, fixed3 view_dir, UnityGI gi)
		{
			fixed4 pbr = LightingStandard(s, view_dir, gi);
			float NdotL = 1 - max(0, dot(gi.light.dir, s.Normal));
			float VdotN = max(0, dot(view_dir, s.Normal));
			float VdotL = max(0, dot(normalize(-_WorldSpaceCameraPos.xyz), gi.light.dir));
			float sss = NdotL * VdotN * VdotL * _SSSPower;
			pbr.rgb = pbr.rgb + sss * _Colour;
			return pbr;
		}

		void LightingStandardTranslucent_GI(SurfaceOutputStandard s, UnityGIInput data, inout UnityGI gi)
		{
			LightingStandard_GI(s, data, gi);
		}

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
			float no_foam = saturate(dot(IN.worldDirNormal, _WaveFoamDir));

			float3 water_normal_1 = normalize(UnpackNormal(tex2D(_NormalMap1, IN.uv_NormalMap1 + _NormalMapMoveDir1.xy * _NormalMapMoveSpeed1 * _Time.y)));
			float3 water_normal_2 = normalize(UnpackNormal(tex2D(_NormalMap2, IN.uv_NormalMap2 + _NormalMapMoveDir2.xy * _NormalMapMoveSpeed2 * _Time.y)));
			float3 total_water_normal = normalize(float3(water_normal_1.xy + water_normal_2.xy, water_normal_1.z));
			total_water_normal.xy *= _NormalMapBias;
			o.Normal = total_water_normal;

			float height1 = tex2D(_HeightMap1, IN.uv_NormalMap1 + _NormalMapMoveDir1.xy * _NormalMapMoveSpeed1 * _Time.y).b;
			float height2 = tex2D(_HeightMap2, IN.uv_NormalMap2 + _NormalMapMoveDir2.xy * _NormalMapMoveSpeed2 * _Time.y).b;
			height1 = (height1 - 0.5)*2;
			height2 = (height2 - 0.5)*2;
			float height = ((height1 + height2) / 2)*_HeightMapFoamStrength;
			float3 foam = ((height + IN.crestFactor) / 2) * (1 - no_foam);

			float3 colour_below = colour_below_water(IN.screenPos, o.Normal, IN.uv_IntersectionFoamMap);

			float alpha = saturate(_Colour.a + foam);

			o.Metallic = _Metallic;
            o.Smoothness = saturate( _Glossiness - foam);
			o.Alpha = alpha;
			o.Albedo = _Colour + foam;
			o.Emission = colour_below * (1 - alpha);
        }

		void reset_alpha(Input IN, SurfaceOutputStandard o, inout fixed4 colour) 
		{
			colour.a = 1;
		}

        ENDCG
    }
}
