#if !defined(FLOW_INCLUDED)
#define FLOW_INCLUDED

float3 FlowUVW(float2 uv, float2 flowVector, float2 jump, float flowOffset, float tiling, float time, bool flowB)
{
	float phaseOffset = flowB * 0.5F;
	float progress = frac(time + phaseOffset);
	return float3((uv - flowVector*(progress + flowOffset))*tiling + phaseOffset + (time - progress) * jump, 1 - abs(1-2*progress));
}

float2 DirectionalFlowUV (float2 uv, float3 flowVectorAndSpeed, float tiling, float time, out float2x2 rotation)
{
	float2 dir = normalize(flowVectorAndSpeed.xy);
	rotation = float2x2(dir.y, dir.x, -dir.x, dir.y);
	uv = mul(float2x2(dir.y, -dir.x, dir.x, dir.y), uv);
	uv.y -= time*flowVectorAndSpeed.z;
	return uv*tiling;
}

#endif
