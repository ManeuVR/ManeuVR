﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveObjectScript : MonoBehaviour
{
    public List<GameObject> Tiles;
    public Vector2 Direction;
    public float Curvature;
    public float RadiusMin;
    public float RadiusMax;
    public float Steepness;
    public float Wavelength;

    void Awake()
    {
        
        foreach(var tile in Tiles)
        {
            foreach (var lodMeshRenderer in tile.GetComponentsInChildren<MeshRenderer>())
            {
                lodMeshRenderer.material.EnableKeyword("WAVE_OBJECT_ENABLED");
                lodMeshRenderer.material.SetFloat("_WaveObjectPositionX", transform.position.x);
                lodMeshRenderer.material.SetFloat("_WaveObjectPositionZ", transform.position.z);
                lodMeshRenderer.material.SetFloat("_WaveObjectDirectionX", Direction.x);
                lodMeshRenderer.material.SetFloat("_WaveObjectDirectionZ", Direction.y);
                lodMeshRenderer.material.SetFloat("_WaveObjectRadiusMax", RadiusMax);
                lodMeshRenderer.material.SetFloat("_WaveObjectRadiusMin", RadiusMin);
                lodMeshRenderer.material.SetFloat("_WaveObjectCurvature", Curvature);
                lodMeshRenderer.material.SetFloat("_WaveObjectSteepness", Steepness);
                lodMeshRenderer.material.SetFloat("_WaveObjectWavelength", Wavelength);
            }
        }
    }

    // For testing only
    void Update()
    {
        foreach(var tile in Tiles)
        {
            foreach (var lodMeshRenderer in tile.GetComponentsInChildren<MeshRenderer>())
            {
                lodMeshRenderer.material.EnableKeyword("WAVE_OBJECT_ENABLED");
                lodMeshRenderer.material.SetFloat("_WaveObjectPositionX", transform.position.x);
                lodMeshRenderer.material.SetFloat("_WaveObjectPositionZ", transform.position.z);
                lodMeshRenderer.material.SetFloat("_WaveObjectDirectionX", Direction.x);
                lodMeshRenderer.material.SetFloat("_WaveObjectDirectionZ", Direction.y);
                lodMeshRenderer.material.SetFloat("_WaveObjectRadiusMax", RadiusMax);
                lodMeshRenderer.material.SetFloat("_WaveObjectRadiusMin", RadiusMin);
                lodMeshRenderer.material.SetFloat("_WaveObjectCurvature", Curvature);
                lodMeshRenderer.material.SetFloat("_WaveObjectSteepness", Steepness);
                lodMeshRenderer.material.SetFloat("_WaveObjectWavelength", Wavelength);
            }
        }
    }
}
