﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAgents;

public class MotorAcademy : Academy
{
    public override void InitializeAcademy()
    {
        base.InitializeAcademy();
    }

    public override void AcademyReset()
    {
        base.AcademyReset();
    }

    public override void AcademyStep()
    {
        base.AcademyStep();
    }
}
