﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAgents;
using MLAgents.Sensor;

public class MotorAgent : Agent
{
    public MapManager mapManager;

    private AIInputManager inputManager;
    private Rigidbody rbody;
    private CollisionHandling collisionHandler;
    private RacerController racerController;

    private const float POSITIVE_REWARD_MULTIPLIER = 1f;
    private const float BRAKE_PENALTY_MULTIPLIER = 1.2f;
    //private const float ACCELERATION_NEGATIVE_PENALTY_MULTIPLIER = 0.7f;

    private float maxRpm;

    public override void InitializeAgent()
    {
        base.InitializeAgent();

        inputManager = GetComponent<AIInputManager>();
        rbody = GetComponent<Rigidbody>();
        collisionHandler = GetComponent<CollisionHandling>();
        racerController = GetComponent<RacerController>();
        maxRpm = racerController.GetMaxRpm;

        ResetAIController();

        collisionHandler.deathCollisionEvent += AgentCollision;
        collisionHandler.borderCollisionEvent += AgentBorderCollision;
        collisionHandler.motorTriggerExitEvent += AgentMotorPassBy;
    }
    
    public override void CollectObservations()
    {
        base.CollectObservations();
        //velocity
        AddVectorObs(rbody.transform.InverseTransformDirection(rbody.velocity));
        //inertia
        //AddVectorObs(rbody.transform.InverseTransformDirection(rbody.inertiaTensor));
        //rotation
        //AddVectorObs(rbody.transform.rotation.x);
        //AddVectorObs(rbody.transform.rotation.y);
        //current steer
        AddVectorObs(inputManager.getSteer());
    }

    public void ResetAIController()
    {
        inputManager.setAcceleration(1);
        inputManager.setBrake(false);
        inputManager.setShift(false);
        inputManager.setSteer(0);
    }

    public void MoveAgent(float[] actions)
    {
        var steer = Mathf.FloorToInt(actions[0]);
        var acceleration = Mathf.FloorToInt(actions[1]);
        var brake = Mathf.FloorToInt(actions[2]);

        switch (steer)
        {
            case 0:
                inputManager.setSteer(0);
                break;
            case 1:
                inputManager.setSteer(1);
                break;
            case 2:
                inputManager.setSteer(-1);
                break;
        }
        switch (acceleration)
        {
            case 0:
                inputManager.setAcceleration(1);
                break;
            case 1:
                inputManager.setAcceleration(1);
                break;
            case 2:
                inputManager.setAcceleration(-1);
                break;
        }
        switch (brake)
        {
            case 0:
                inputManager.setBrake(false);
                break;
            case 1:
                inputManager.setBrake(true);
                break;
        }

        Vector3 velocity = rbody.velocity;
        Vector3 localVelocity = transform.InverseTransformDirection(velocity);
        if (localVelocity.z < 0) inputManager.setAcceleration(1);
    }

    public void CalculateReward()
    {
        float reward = POSITIVE_REWARD_MULTIPLIER * GetStepCount() / agentParameters.maxStep;
        //float acceleration = inputManager.getAcceleration();
        float rpmRatio = racerController.GetCurrentRpm / maxRpm;
        bool brake = inputManager.getBrake();
        /*
        if (acceleration < 0)
        {
            reward *= ACCELERATION_NEGATIVE_PENALTY_MULTIPLIER * -acceleration;
        }
        else
        {
            reward *= acceleration;
        }
        */
        if(brake)
        {
            reward *= BRAKE_PENALTY_MULTIPLIER;
        }
        //Debug.Log(reward);

        float steer = inputManager.getSteer();

        AddReward(reward * rpmRatio * (Mathf.Abs(steer) / 5f + 1f));
    }

    public override void AgentAction(float[] vectorAction)
    {
        CalculateReward();
        MoveAgent(vectorAction);
    }

    public override float[] Heuristic()
    {
        float steer = 0;
        float acceleration = 0;
        float brake = 0;

        if(Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
        {
            acceleration = 1;
        }
        if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
        {
            acceleration = 2;
        }

        if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
        {
            steer = 1;
        }
        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
        {
            steer = 2;
        }

        if (Input.GetKey(KeyCode.Space) || Input.GetKey(KeyCode.Q))
        {
            brake = 1;
        }

        return new float[] { steer, acceleration, brake };
    }

    public override void AgentReset()
    {
        //Debug.Log("Agent reset " + transform.name);
        rbody.ResetInertiaTensor();
        rbody.velocity = Vector3.zero;
        rbody.angularVelocity = Vector3.zero;
        rbody.transform.rotation = Quaternion.Euler(Vector3.zero);

        ResetAIController();
        //reset map
        if (mapManager != null) mapManager.ResetMap();
    }

    public override void AgentOnDone()
    {
        //Debug.Log(transform.name + " is done");
    }

    public void AgentCollision()
    {
        //mark as done
        AddReward(-1f);
        Done();
    }

    public void AgentBorderCollision()
    {
        AddReward(-1f);
    }

    public void AgentMotorPassBy()
    {
        //Debug.Log("Motor pass by");
        AddReward(1f);
    }

    private void OnDestroy()
    {
        collisionHandler.deathCollisionEvent -= AgentCollision;
        collisionHandler.borderCollisionEvent -= AgentBorderCollision;
        collisionHandler.motorTriggerExitEvent -= AgentMotorPassBy;
    }
}
