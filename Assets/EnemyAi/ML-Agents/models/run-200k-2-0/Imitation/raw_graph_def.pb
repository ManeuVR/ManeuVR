
C
Variable/initial_valueConst*
valueB
 *  ?*
dtype0
T
Variable
VariableV2*
shape: *
shared_name *
dtype0*
	container 

Variable/AssignAssignVariableVariable/initial_value*
use_locking(*
T0*
_class
loc:@Variable*
validate_shape(
I
Variable/readIdentityVariable*
T0*
_class
loc:@Variable
E
Variable_1/initial_valueConst*
valueB
 *  ?*
dtype0
V

Variable_1
VariableV2*
shape: *
shared_name *
dtype0*
	container 

Variable_1/AssignAssign
Variable_1Variable_1/initial_value*
use_locking(*
T0*
_class
loc:@Variable_1*
validate_shape(
O
Variable_1/readIdentity
Variable_1*
T0*
_class
loc:@Variable_1
9
Assign/valueConst*
valueB
 *    *
dtype0
w
AssignAssignVariableAssign/value*
use_locking( *
T0*
_class
loc:@Variable*
validate_shape(
;
Assign_1/valueConst*
valueB
 *    *
dtype0

Assign_1Assign
Variable_1Assign_1/value*
use_locking( *
T0*
_class
loc:@Variable_1*
validate_shape(
C
global_step/initial_valueConst*
value	B : *
dtype0
W
global_step
VariableV2*
shape: *
shared_name *
dtype0*
	container 

global_step/AssignAssignglobal_stepglobal_step/initial_value*
use_locking(*
T0*
_class
loc:@global_step*
validate_shape(
R
global_step/readIdentityglobal_step*
T0*
_class
loc:@global_step
;
steps_to_incrementPlaceholder*
shape: *
dtype0
9
AddAddglobal_step/readsteps_to_increment*
T0
v
Assign_2Assignglobal_stepAdd*
use_locking(*
T0*
_class
loc:@global_step*
validate_shape(
5

batch_sizePlaceholder*
shape:*
dtype0
:
sequence_lengthPlaceholder*
shape:*
dtype0
;
masksPlaceholder*
shape:’’’’’’’’’*
dtype0
;
CastCastmasks*

SrcT0*
Truncate( *

DstT0
M
#is_continuous_control/initial_valueConst*
value	B : *
dtype0
a
is_continuous_control
VariableV2*
shape: *
shared_name *
dtype0*
	container 
¾
is_continuous_control/AssignAssignis_continuous_control#is_continuous_control/initial_value*
use_locking(*
T0*(
_class
loc:@is_continuous_control*
validate_shape(
p
is_continuous_control/readIdentityis_continuous_control*
T0*(
_class
loc:@is_continuous_control
F
version_number/initial_valueConst*
value	B :*
dtype0
Z
version_number
VariableV2*
shape: *
shared_name *
dtype0*
	container 
¢
version_number/AssignAssignversion_numberversion_number/initial_value*
use_locking(*
T0*!
_class
loc:@version_number*
validate_shape(
[
version_number/readIdentityversion_number*
T0*!
_class
loc:@version_number
C
memory_size/initial_valueConst*
value	B : *
dtype0
W
memory_size
VariableV2*
shape: *
shared_name *
dtype0*
	container 

memory_size/AssignAssignmemory_sizememory_size/initial_value*
use_locking(*
T0*
_class
loc:@memory_size*
validate_shape(
R
memory_size/readIdentitymemory_size*
T0*
_class
loc:@memory_size
K
!action_output_shape/initial_valueConst*
value	B :*
dtype0
_
action_output_shape
VariableV2*
shape: *
shared_name *
dtype0*
	container 
¶
action_output_shape/AssignAssignaction_output_shape!action_output_shape/initial_value*
use_locking(*
T0*&
_class
loc:@action_output_shape*
validate_shape(
j
action_output_shape/readIdentityaction_output_shape*
T0*&
_class
loc:@action_output_shape
E
global_step_1/initial_valueConst*
value	B : *
dtype0
Y
global_step_1
VariableV2*
shape: *
shared_name *
dtype0*
	container 

global_step_1/AssignAssignglobal_step_1global_step_1/initial_value*
use_locking(*
T0* 
_class
loc:@global_step_1*
validate_shape(
X
global_step_1/readIdentityglobal_step_1*
T0* 
_class
loc:@global_step_1
=
steps_to_increment_1Placeholder*
shape: *
dtype0
?
Add_1Addglobal_step_1/readsteps_to_increment_1*
T0
|
Assign_3Assignglobal_step_1Add_1*
use_locking(*
T0* 
_class
loc:@global_step_1*
validate_shape(
7
batch_size_1Placeholder*
shape:*
dtype0
<
sequence_length_1Placeholder*
shape:*
dtype0
=
masks_1Placeholder*
shape:’’’’’’’’’*
dtype0
?
Cast_1Castmasks_1*

SrcT0*
Truncate( *

DstT0
O
%is_continuous_control_1/initial_valueConst*
value	B : *
dtype0
c
is_continuous_control_1
VariableV2*
shape: *
shared_name *
dtype0*
	container 
Ę
is_continuous_control_1/AssignAssignis_continuous_control_1%is_continuous_control_1/initial_value*
use_locking(*
T0**
_class 
loc:@is_continuous_control_1*
validate_shape(
v
is_continuous_control_1/readIdentityis_continuous_control_1*
T0**
_class 
loc:@is_continuous_control_1
H
version_number_1/initial_valueConst*
value	B :*
dtype0
\
version_number_1
VariableV2*
shape: *
shared_name *
dtype0*
	container 
Ŗ
version_number_1/AssignAssignversion_number_1version_number_1/initial_value*
use_locking(*
T0*#
_class
loc:@version_number_1*
validate_shape(
a
version_number_1/readIdentityversion_number_1*
T0*#
_class
loc:@version_number_1
E
memory_size_1/initial_valueConst*
value	B : *
dtype0
Y
memory_size_1
VariableV2*
shape: *
shared_name *
dtype0*
	container 

memory_size_1/AssignAssignmemory_size_1memory_size_1/initial_value*
use_locking(*
T0* 
_class
loc:@memory_size_1*
validate_shape(
X
memory_size_1/readIdentitymemory_size_1*
T0* 
_class
loc:@memory_size_1
M
#action_output_shape_1/initial_valueConst*
value	B :*
dtype0
a
action_output_shape_1
VariableV2*
shape: *
shared_name *
dtype0*
	container 
¾
action_output_shape_1/AssignAssignaction_output_shape_1#action_output_shape_1/initial_value*
use_locking(*
T0*(
_class
loc:@action_output_shape_1*
validate_shape(
p
action_output_shape_1/readIdentityaction_output_shape_1*
T0*(
_class
loc:@action_output_shape_1
M
vector_observationPlaceholder*
shape:’’’’’’’’’ā*
dtype0
F
action_masksPlaceholder*
shape:’’’’’’’’’*
dtype0
©
Apolicy/encoder/hidden_0/kernel/Initializer/truncated_normal/shapeConst*1
_class'
%#loc:@policy/encoder/hidden_0/kernel*
valueB"b     *
dtype0
 
@policy/encoder/hidden_0/kernel/Initializer/truncated_normal/meanConst*1
_class'
%#loc:@policy/encoder/hidden_0/kernel*
valueB
 *    *
dtype0
¢
Bpolicy/encoder/hidden_0/kernel/Initializer/truncated_normal/stddevConst*1
_class'
%#loc:@policy/encoder/hidden_0/kernel*
valueB
 *Ų}w=*
dtype0

Kpolicy/encoder/hidden_0/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormalApolicy/encoder/hidden_0/kernel/Initializer/truncated_normal/shape*
seedĶ%*
T0*1
_class'
%#loc:@policy/encoder/hidden_0/kernel*
dtype0*
seed2G

?policy/encoder/hidden_0/kernel/Initializer/truncated_normal/mulMulKpolicy/encoder/hidden_0/kernel/Initializer/truncated_normal/TruncatedNormalBpolicy/encoder/hidden_0/kernel/Initializer/truncated_normal/stddev*
T0*1
_class'
%#loc:@policy/encoder/hidden_0/kernel

;policy/encoder/hidden_0/kernel/Initializer/truncated_normalAdd?policy/encoder/hidden_0/kernel/Initializer/truncated_normal/mul@policy/encoder/hidden_0/kernel/Initializer/truncated_normal/mean*
T0*1
_class'
%#loc:@policy/encoder/hidden_0/kernel
§
policy/encoder/hidden_0/kernel
VariableV2*
shape:
ā*
shared_name *1
_class'
%#loc:@policy/encoder/hidden_0/kernel*
dtype0*
	container 
ń
%policy/encoder/hidden_0/kernel/AssignAssignpolicy/encoder/hidden_0/kernel;policy/encoder/hidden_0/kernel/Initializer/truncated_normal*
use_locking(*
T0*1
_class'
%#loc:@policy/encoder/hidden_0/kernel*
validate_shape(

#policy/encoder/hidden_0/kernel/readIdentitypolicy/encoder/hidden_0/kernel*
T0*1
_class'
%#loc:@policy/encoder/hidden_0/kernel

.policy/encoder/hidden_0/bias/Initializer/zerosConst*/
_class%
#!loc:@policy/encoder/hidden_0/bias*
valueB*    *
dtype0

policy/encoder/hidden_0/bias
VariableV2*
shape:*
shared_name */
_class%
#!loc:@policy/encoder/hidden_0/bias*
dtype0*
	container 
Ž
#policy/encoder/hidden_0/bias/AssignAssignpolicy/encoder/hidden_0/bias.policy/encoder/hidden_0/bias/Initializer/zeros*
use_locking(*
T0*/
_class%
#!loc:@policy/encoder/hidden_0/bias*
validate_shape(

!policy/encoder/hidden_0/bias/readIdentitypolicy/encoder/hidden_0/bias*
T0*/
_class%
#!loc:@policy/encoder/hidden_0/bias

policy/encoder/hidden_0/MatMulMatMulvector_observation#policy/encoder/hidden_0/kernel/read*
transpose_b( *
T0*
transpose_a( 

policy/encoder/hidden_0/BiasAddBiasAddpolicy/encoder/hidden_0/MatMul!policy/encoder/hidden_0/bias/read*
T0*
data_formatNHWC
T
policy/encoder/hidden_0/SigmoidSigmoidpolicy/encoder/hidden_0/BiasAdd*
T0
m
policy/encoder/hidden_0/MulMulpolicy/encoder/hidden_0/BiasAddpolicy/encoder/hidden_0/Sigmoid*
T0
©
Apolicy/encoder/hidden_1/kernel/Initializer/truncated_normal/shapeConst*1
_class'
%#loc:@policy/encoder/hidden_1/kernel*
valueB"      *
dtype0
 
@policy/encoder/hidden_1/kernel/Initializer/truncated_normal/meanConst*1
_class'
%#loc:@policy/encoder/hidden_1/kernel*
valueB
 *    *
dtype0
¢
Bpolicy/encoder/hidden_1/kernel/Initializer/truncated_normal/stddevConst*1
_class'
%#loc:@policy/encoder/hidden_1/kernel*
valueB
 *6=*
dtype0

Kpolicy/encoder/hidden_1/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormalApolicy/encoder/hidden_1/kernel/Initializer/truncated_normal/shape*
seedĶ%*
T0*1
_class'
%#loc:@policy/encoder/hidden_1/kernel*
dtype0*
seed2X

?policy/encoder/hidden_1/kernel/Initializer/truncated_normal/mulMulKpolicy/encoder/hidden_1/kernel/Initializer/truncated_normal/TruncatedNormalBpolicy/encoder/hidden_1/kernel/Initializer/truncated_normal/stddev*
T0*1
_class'
%#loc:@policy/encoder/hidden_1/kernel

;policy/encoder/hidden_1/kernel/Initializer/truncated_normalAdd?policy/encoder/hidden_1/kernel/Initializer/truncated_normal/mul@policy/encoder/hidden_1/kernel/Initializer/truncated_normal/mean*
T0*1
_class'
%#loc:@policy/encoder/hidden_1/kernel
§
policy/encoder/hidden_1/kernel
VariableV2*
shape:
*
shared_name *1
_class'
%#loc:@policy/encoder/hidden_1/kernel*
dtype0*
	container 
ń
%policy/encoder/hidden_1/kernel/AssignAssignpolicy/encoder/hidden_1/kernel;policy/encoder/hidden_1/kernel/Initializer/truncated_normal*
use_locking(*
T0*1
_class'
%#loc:@policy/encoder/hidden_1/kernel*
validate_shape(

#policy/encoder/hidden_1/kernel/readIdentitypolicy/encoder/hidden_1/kernel*
T0*1
_class'
%#loc:@policy/encoder/hidden_1/kernel

.policy/encoder/hidden_1/bias/Initializer/zerosConst*/
_class%
#!loc:@policy/encoder/hidden_1/bias*
valueB*    *
dtype0

policy/encoder/hidden_1/bias
VariableV2*
shape:*
shared_name */
_class%
#!loc:@policy/encoder/hidden_1/bias*
dtype0*
	container 
Ž
#policy/encoder/hidden_1/bias/AssignAssignpolicy/encoder/hidden_1/bias.policy/encoder/hidden_1/bias/Initializer/zeros*
use_locking(*
T0*/
_class%
#!loc:@policy/encoder/hidden_1/bias*
validate_shape(

!policy/encoder/hidden_1/bias/readIdentitypolicy/encoder/hidden_1/bias*
T0*/
_class%
#!loc:@policy/encoder/hidden_1/bias

policy/encoder/hidden_1/MatMulMatMulpolicy/encoder/hidden_0/Mul#policy/encoder/hidden_1/kernel/read*
transpose_b( *
T0*
transpose_a( 

policy/encoder/hidden_1/BiasAddBiasAddpolicy/encoder/hidden_1/MatMul!policy/encoder/hidden_1/bias/read*
T0*
data_formatNHWC
T
policy/encoder/hidden_1/SigmoidSigmoidpolicy/encoder/hidden_1/BiasAdd*
T0
m
policy/encoder/hidden_1/MulMulpolicy/encoder/hidden_1/BiasAddpolicy/encoder/hidden_1/Sigmoid*
T0

6policy/dense/kernel/Initializer/truncated_normal/shapeConst*&
_class
loc:@policy/dense/kernel*
valueB"      *
dtype0

5policy/dense/kernel/Initializer/truncated_normal/meanConst*&
_class
loc:@policy/dense/kernel*
valueB
 *    *
dtype0

7policy/dense/kernel/Initializer/truncated_normal/stddevConst*&
_class
loc:@policy/dense/kernel*
valueB
 *Óč;*
dtype0
ć
@policy/dense/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormal6policy/dense/kernel/Initializer/truncated_normal/shape*
seedĶ%*
T0*&
_class
loc:@policy/dense/kernel*
dtype0*
seed2i
ē
4policy/dense/kernel/Initializer/truncated_normal/mulMul@policy/dense/kernel/Initializer/truncated_normal/TruncatedNormal7policy/dense/kernel/Initializer/truncated_normal/stddev*
T0*&
_class
loc:@policy/dense/kernel
Õ
0policy/dense/kernel/Initializer/truncated_normalAdd4policy/dense/kernel/Initializer/truncated_normal/mul5policy/dense/kernel/Initializer/truncated_normal/mean*
T0*&
_class
loc:@policy/dense/kernel

policy/dense/kernel
VariableV2*
shape:	*
shared_name *&
_class
loc:@policy/dense/kernel*
dtype0*
	container 
Å
policy/dense/kernel/AssignAssignpolicy/dense/kernel0policy/dense/kernel/Initializer/truncated_normal*
use_locking(*
T0*&
_class
loc:@policy/dense/kernel*
validate_shape(
j
policy/dense/kernel/readIdentitypolicy/dense/kernel*
T0*&
_class
loc:@policy/dense/kernel

policy_1/dense/MatMulMatMulpolicy/encoder/hidden_1/Mulpolicy/dense/kernel/read*
transpose_b( *
T0*
transpose_a( 

8policy/dense_1/kernel/Initializer/truncated_normal/shapeConst*(
_class
loc:@policy/dense_1/kernel*
valueB"      *
dtype0

7policy/dense_1/kernel/Initializer/truncated_normal/meanConst*(
_class
loc:@policy/dense_1/kernel*
valueB
 *    *
dtype0

9policy/dense_1/kernel/Initializer/truncated_normal/stddevConst*(
_class
loc:@policy/dense_1/kernel*
valueB
 *Óč;*
dtype0
é
Bpolicy/dense_1/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormal8policy/dense_1/kernel/Initializer/truncated_normal/shape*
seedĶ%*
T0*(
_class
loc:@policy/dense_1/kernel*
dtype0*
seed2s
ļ
6policy/dense_1/kernel/Initializer/truncated_normal/mulMulBpolicy/dense_1/kernel/Initializer/truncated_normal/TruncatedNormal9policy/dense_1/kernel/Initializer/truncated_normal/stddev*
T0*(
_class
loc:@policy/dense_1/kernel
Ż
2policy/dense_1/kernel/Initializer/truncated_normalAdd6policy/dense_1/kernel/Initializer/truncated_normal/mul7policy/dense_1/kernel/Initializer/truncated_normal/mean*
T0*(
_class
loc:@policy/dense_1/kernel

policy/dense_1/kernel
VariableV2*
shape:	*
shared_name *(
_class
loc:@policy/dense_1/kernel*
dtype0*
	container 
Ķ
policy/dense_1/kernel/AssignAssignpolicy/dense_1/kernel2policy/dense_1/kernel/Initializer/truncated_normal*
use_locking(*
T0*(
_class
loc:@policy/dense_1/kernel*
validate_shape(
p
policy/dense_1/kernel/readIdentitypolicy/dense_1/kernel*
T0*(
_class
loc:@policy/dense_1/kernel

policy_1/dense_1/MatMulMatMulpolicy/encoder/hidden_1/Mulpolicy/dense_1/kernel/read*
transpose_b( *
T0*
transpose_a( 

8policy/dense_2/kernel/Initializer/truncated_normal/shapeConst*(
_class
loc:@policy/dense_2/kernel*
valueB"      *
dtype0

7policy/dense_2/kernel/Initializer/truncated_normal/meanConst*(
_class
loc:@policy/dense_2/kernel*
valueB
 *    *
dtype0

9policy/dense_2/kernel/Initializer/truncated_normal/stddevConst*(
_class
loc:@policy/dense_2/kernel*
valueB
 *Óč;*
dtype0
é
Bpolicy/dense_2/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormal8policy/dense_2/kernel/Initializer/truncated_normal/shape*
seedĶ%*
T0*(
_class
loc:@policy/dense_2/kernel*
dtype0*
seed2}
ļ
6policy/dense_2/kernel/Initializer/truncated_normal/mulMulBpolicy/dense_2/kernel/Initializer/truncated_normal/TruncatedNormal9policy/dense_2/kernel/Initializer/truncated_normal/stddev*
T0*(
_class
loc:@policy/dense_2/kernel
Ż
2policy/dense_2/kernel/Initializer/truncated_normalAdd6policy/dense_2/kernel/Initializer/truncated_normal/mul7policy/dense_2/kernel/Initializer/truncated_normal/mean*
T0*(
_class
loc:@policy/dense_2/kernel

policy/dense_2/kernel
VariableV2*
shape:	*
shared_name *(
_class
loc:@policy/dense_2/kernel*
dtype0*
	container 
Ķ
policy/dense_2/kernel/AssignAssignpolicy/dense_2/kernel2policy/dense_2/kernel/Initializer/truncated_normal*
use_locking(*
T0*(
_class
loc:@policy/dense_2/kernel*
validate_shape(
p
policy/dense_2/kernel/readIdentitypolicy/dense_2/kernel*
T0*(
_class
loc:@policy/dense_2/kernel

policy_1/dense_2/MatMulMatMulpolicy/encoder/hidden_1/Mulpolicy/dense_2/kernel/read*
transpose_b( *
T0*
transpose_a( 
D
policy_1/action_probs/axisConst*
value	B :*
dtype0
¤
policy_1/action_probsConcatV2policy_1/dense/MatMulpolicy_1/dense_1/MatMulpolicy_1/dense_2/MatMulpolicy_1/action_probs/axis*

Tidx0*
T0*
N
Q
policy_1/strided_slice/stackConst*
valueB"        *
dtype0
S
policy_1/strided_slice/stack_1Const*
valueB"       *
dtype0
S
policy_1/strided_slice/stack_2Const*
valueB"      *
dtype0

policy_1/strided_sliceStridedSlicepolicy_1/action_probspolicy_1/strided_slice/stackpolicy_1/strided_slice/stack_1policy_1/strided_slice/stack_2*
T0*
Index0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
S
policy_1/strided_slice_1/stackConst*
valueB"       *
dtype0
U
 policy_1/strided_slice_1/stack_1Const*
valueB"       *
dtype0
U
 policy_1/strided_slice_1/stack_2Const*
valueB"      *
dtype0

policy_1/strided_slice_1StridedSlicepolicy_1/action_probspolicy_1/strided_slice_1/stack policy_1/strided_slice_1/stack_1 policy_1/strided_slice_1/stack_2*
T0*
Index0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
S
policy_1/strided_slice_2/stackConst*
valueB"       *
dtype0
U
 policy_1/strided_slice_2/stack_1Const*
valueB"       *
dtype0
U
 policy_1/strided_slice_2/stack_2Const*
valueB"      *
dtype0

policy_1/strided_slice_2StridedSlicepolicy_1/action_probspolicy_1/strided_slice_2/stack policy_1/strided_slice_2/stack_1 policy_1/strided_slice_2/stack_2*
T0*
Index0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
S
policy_1/strided_slice_3/stackConst*
valueB"        *
dtype0
U
 policy_1/strided_slice_3/stack_1Const*
valueB"       *
dtype0
U
 policy_1/strided_slice_3/stack_2Const*
valueB"      *
dtype0

policy_1/strided_slice_3StridedSliceaction_maskspolicy_1/strided_slice_3/stack policy_1/strided_slice_3/stack_1 policy_1/strided_slice_3/stack_2*
T0*
Index0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
S
policy_1/strided_slice_4/stackConst*
valueB"       *
dtype0
U
 policy_1/strided_slice_4/stack_1Const*
valueB"       *
dtype0
U
 policy_1/strided_slice_4/stack_2Const*
valueB"      *
dtype0

policy_1/strided_slice_4StridedSliceaction_maskspolicy_1/strided_slice_4/stack policy_1/strided_slice_4/stack_1 policy_1/strided_slice_4/stack_2*
T0*
Index0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
S
policy_1/strided_slice_5/stackConst*
valueB"       *
dtype0
U
 policy_1/strided_slice_5/stack_1Const*
valueB"       *
dtype0
U
 policy_1/strided_slice_5/stack_2Const*
valueB"      *
dtype0

policy_1/strided_slice_5StridedSliceaction_maskspolicy_1/strided_slice_5/stack policy_1/strided_slice_5/stack_1 policy_1/strided_slice_5/stack_2*
T0*
Index0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
<
policy_1/SoftmaxSoftmaxpolicy_1/strided_slice*
T0
;
policy_1/add/yConst*
valueB
 *æÖ3*
dtype0
@
policy_1/addAddV2policy_1/Softmaxpolicy_1/add/y*
T0
D
policy_1/MulMulpolicy_1/addpolicy_1/strided_slice_3*
T0
@
policy_1/Softmax_1Softmaxpolicy_1/strided_slice_1*
T0
=
policy_1/add_1/yConst*
valueB
 *æÖ3*
dtype0
F
policy_1/add_1AddV2policy_1/Softmax_1policy_1/add_1/y*
T0
H
policy_1/Mul_1Mulpolicy_1/add_1policy_1/strided_slice_4*
T0
@
policy_1/Softmax_2Softmaxpolicy_1/strided_slice_2*
T0
=
policy_1/add_2/yConst*
valueB
 *æÖ3*
dtype0
F
policy_1/add_2AddV2policy_1/Softmax_2policy_1/add_2/y*
T0
H
policy_1/Mul_2Mulpolicy_1/add_2policy_1/strided_slice_5*
T0
H
policy_1/Sum/reduction_indicesConst*
value	B :*
dtype0
g
policy_1/SumSumpolicy_1/Mulpolicy_1/Sum/reduction_indices*

Tidx0*
	keep_dims(*
T0
@
policy_1/truedivRealDivpolicy_1/Mulpolicy_1/Sum*
T0
J
 policy_1/Sum_1/reduction_indicesConst*
value	B :*
dtype0
m
policy_1/Sum_1Sumpolicy_1/Mul_1 policy_1/Sum_1/reduction_indices*

Tidx0*
	keep_dims(*
T0
F
policy_1/truediv_1RealDivpolicy_1/Mul_1policy_1/Sum_1*
T0
J
 policy_1/Sum_2/reduction_indicesConst*
value	B :*
dtype0
m
policy_1/Sum_2Sumpolicy_1/Mul_2 policy_1/Sum_2/reduction_indices*

Tidx0*
	keep_dims(*
T0
F
policy_1/truediv_2RealDivpolicy_1/Mul_2policy_1/Sum_2*
T0
=
policy_1/add_3/yConst*
valueB
 *æÖ3*
dtype0
D
policy_1/add_3AddV2policy_1/truedivpolicy_1/add_3/y*
T0
,
policy_1/LogLogpolicy_1/add_3*
T0
V
,policy_1/multinomial/Multinomial/num_samplesConst*
value	B :*
dtype0
£
 policy_1/multinomial/MultinomialMultinomialpolicy_1/Log,policy_1/multinomial/Multinomial/num_samples*
seedĶ%*
output_dtype0	*
T0*
seed2¶
=
policy_1/add_4/yConst*
valueB
 *æÖ3*
dtype0
F
policy_1/add_4AddV2policy_1/truediv_1policy_1/add_4/y*
T0
.
policy_1/Log_1Logpolicy_1/add_4*
T0
X
.policy_1/multinomial_1/Multinomial/num_samplesConst*
value	B :*
dtype0
©
"policy_1/multinomial_1/MultinomialMultinomialpolicy_1/Log_1.policy_1/multinomial_1/Multinomial/num_samples*
seedĶ%*
output_dtype0	*
T0*
seed2»
=
policy_1/add_5/yConst*
valueB
 *æÖ3*
dtype0
F
policy_1/add_5AddV2policy_1/truediv_2policy_1/add_5/y*
T0
.
policy_1/Log_2Logpolicy_1/add_5*
T0
X
.policy_1/multinomial_2/Multinomial/num_samplesConst*
value	B :*
dtype0
©
"policy_1/multinomial_2/MultinomialMultinomialpolicy_1/Log_2.policy_1/multinomial_2/Multinomial/num_samples*
seedĶ%*
output_dtype0	*
T0*
seed2Ą
>
policy_1/concat/axisConst*
value	B :*
dtype0
¹
policy_1/concatConcatV2 policy_1/multinomial/Multinomial"policy_1/multinomial_1/Multinomial"policy_1/multinomial_2/Multinomialpolicy_1/concat/axis*

Tidx0*
T0	*
N
@
policy_1/concat_1/axisConst*
value	B :*
dtype0

policy_1/concat_1ConcatV2policy_1/truedivpolicy_1/truediv_1policy_1/truediv_2policy_1/concat_1/axis*

Tidx0*
T0*
N
=
policy_1/add_6/yConst*
valueB
 *æÖ3*
dtype0
D
policy_1/add_6AddV2policy_1/truedivpolicy_1/add_6/y*
T0
.
policy_1/Log_3Logpolicy_1/add_6*
T0
=
policy_1/add_7/yConst*
valueB
 *æÖ3*
dtype0
F
policy_1/add_7AddV2policy_1/truediv_1policy_1/add_7/y*
T0
.
policy_1/Log_4Logpolicy_1/add_7*
T0
=
policy_1/add_8/yConst*
valueB
 *æÖ3*
dtype0
F
policy_1/add_8AddV2policy_1/truediv_2policy_1/add_8/y*
T0
.
policy_1/Log_5Logpolicy_1/add_8*
T0
@
policy_1/concat_2/axisConst*
value	B :*
dtype0

policy_1/concat_2ConcatV2policy_1/Log_3policy_1/Log_4policy_1/Log_5policy_1/concat_2/axis*

Tidx0*
T0*
N
D
policy_1/mul_3Mulpolicy_1/concat_1policy_1/concat_2*
T0
P
policy_1/action_holderPlaceholder*
shape:’’’’’’’’’*
dtype0
S
policy_1/strided_slice_6/stackConst*
valueB"        *
dtype0
U
 policy_1/strided_slice_6/stack_1Const*
valueB"       *
dtype0
U
 policy_1/strided_slice_6/stack_2Const*
valueB"      *
dtype0

policy_1/strided_slice_6StridedSlicepolicy_1/action_holderpolicy_1/strided_slice_6/stack policy_1/strided_slice_6/stack_1 policy_1/strided_slice_6/stack_2*
T0*
Index0*
shrink_axis_mask*

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
F
policy_1/one_hot/on_valueConst*
valueB
 *  ?*
dtype0
G
policy_1/one_hot/off_valueConst*
valueB
 *    *
dtype0
@
policy_1/one_hot/depthConst*
value	B :*
dtype0
«
policy_1/one_hotOneHotpolicy_1/strided_slice_6policy_1/one_hot/depthpolicy_1/one_hot/on_valuepolicy_1/one_hot/off_value*
T0*
TI0*
axis’’’’’’’’’
S
policy_1/strided_slice_7/stackConst*
valueB"       *
dtype0
U
 policy_1/strided_slice_7/stack_1Const*
valueB"       *
dtype0
U
 policy_1/strided_slice_7/stack_2Const*
valueB"      *
dtype0

policy_1/strided_slice_7StridedSlicepolicy_1/action_holderpolicy_1/strided_slice_7/stack policy_1/strided_slice_7/stack_1 policy_1/strided_slice_7/stack_2*
T0*
Index0*
shrink_axis_mask*

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
H
policy_1/one_hot_1/on_valueConst*
valueB
 *  ?*
dtype0
I
policy_1/one_hot_1/off_valueConst*
valueB
 *    *
dtype0
B
policy_1/one_hot_1/depthConst*
value	B :*
dtype0
³
policy_1/one_hot_1OneHotpolicy_1/strided_slice_7policy_1/one_hot_1/depthpolicy_1/one_hot_1/on_valuepolicy_1/one_hot_1/off_value*
T0*
TI0*
axis’’’’’’’’’
S
policy_1/strided_slice_8/stackConst*
valueB"       *
dtype0
U
 policy_1/strided_slice_8/stack_1Const*
valueB"       *
dtype0
U
 policy_1/strided_slice_8/stack_2Const*
valueB"      *
dtype0

policy_1/strided_slice_8StridedSlicepolicy_1/action_holderpolicy_1/strided_slice_8/stack policy_1/strided_slice_8/stack_1 policy_1/strided_slice_8/stack_2*
T0*
Index0*
shrink_axis_mask*

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
H
policy_1/one_hot_2/on_valueConst*
valueB
 *  ?*
dtype0
I
policy_1/one_hot_2/off_valueConst*
valueB
 *    *
dtype0
B
policy_1/one_hot_2/depthConst*
value	B :*
dtype0
³
policy_1/one_hot_2OneHotpolicy_1/strided_slice_8policy_1/one_hot_2/depthpolicy_1/one_hot_2/on_valuepolicy_1/one_hot_2/off_value*
T0*
TI0*
axis’’’’’’’’’
@
policy_1/concat_3/axisConst*
value	B :*
dtype0

policy_1/concat_3ConcatV2policy_1/one_hotpolicy_1/one_hot_1policy_1/one_hot_2policy_1/concat_3/axis*

Tidx0*
T0*
N
A
policy_1/StopGradientStopGradientpolicy_1/concat_3*
T0
S
policy_1/strided_slice_9/stackConst*
valueB"        *
dtype0
U
 policy_1/strided_slice_9/stack_1Const*
valueB"       *
dtype0
U
 policy_1/strided_slice_9/stack_2Const*
valueB"      *
dtype0

policy_1/strided_slice_9StridedSlicepolicy_1/action_holderpolicy_1/strided_slice_9/stack policy_1/strided_slice_9/stack_1 policy_1/strided_slice_9/stack_2*
T0*
Index0*
shrink_axis_mask*

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
H
policy_1/one_hot_3/on_valueConst*
valueB
 *  ?*
dtype0
I
policy_1/one_hot_3/off_valueConst*
valueB
 *    *
dtype0
B
policy_1/one_hot_3/depthConst*
value	B :*
dtype0
³
policy_1/one_hot_3OneHotpolicy_1/strided_slice_9policy_1/one_hot_3/depthpolicy_1/one_hot_3/on_valuepolicy_1/one_hot_3/off_value*
T0*
TI0*
axis’’’’’’’’’
T
policy_1/strided_slice_10/stackConst*
valueB"       *
dtype0
V
!policy_1/strided_slice_10/stack_1Const*
valueB"       *
dtype0
V
!policy_1/strided_slice_10/stack_2Const*
valueB"      *
dtype0
¢
policy_1/strided_slice_10StridedSlicepolicy_1/action_holderpolicy_1/strided_slice_10/stack!policy_1/strided_slice_10/stack_1!policy_1/strided_slice_10/stack_2*
T0*
Index0*
shrink_axis_mask*

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
H
policy_1/one_hot_4/on_valueConst*
valueB
 *  ?*
dtype0
I
policy_1/one_hot_4/off_valueConst*
valueB
 *    *
dtype0
B
policy_1/one_hot_4/depthConst*
value	B :*
dtype0
“
policy_1/one_hot_4OneHotpolicy_1/strided_slice_10policy_1/one_hot_4/depthpolicy_1/one_hot_4/on_valuepolicy_1/one_hot_4/off_value*
T0*
TI0*
axis’’’’’’’’’
T
policy_1/strided_slice_11/stackConst*
valueB"       *
dtype0
V
!policy_1/strided_slice_11/stack_1Const*
valueB"       *
dtype0
V
!policy_1/strided_slice_11/stack_2Const*
valueB"      *
dtype0
¢
policy_1/strided_slice_11StridedSlicepolicy_1/action_holderpolicy_1/strided_slice_11/stack!policy_1/strided_slice_11/stack_1!policy_1/strided_slice_11/stack_2*
T0*
Index0*
shrink_axis_mask*

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
H
policy_1/one_hot_5/on_valueConst*
valueB
 *  ?*
dtype0
I
policy_1/one_hot_5/off_valueConst*
valueB
 *    *
dtype0
B
policy_1/one_hot_5/depthConst*
value	B :*
dtype0
“
policy_1/one_hot_5OneHotpolicy_1/strided_slice_11policy_1/one_hot_5/depthpolicy_1/one_hot_5/on_valuepolicy_1/one_hot_5/off_value*
T0*
TI0*
axis’’’’’’’’’
@
policy_1/concat_4/axisConst*
value	B :*
dtype0

policy_1/concat_4ConcatV2policy_1/one_hot_3policy_1/one_hot_4policy_1/one_hot_5policy_1/concat_4/axis*

Tidx0*
T0*
N
J
 policy_1/Sum_3/reduction_indicesConst*
value	B :*
dtype0
m
policy_1/Sum_3Sumpolicy_1/mul_3 policy_1/Sum_3/reduction_indices*

Tidx0*
	keep_dims( *
T0
=
policy_1/mul_4/xConst*
valueB
 *  æ*
dtype0
@
policy_1/mul_4Mulpolicy_1/mul_4/xpolicy_1/Sum_3*
T0
.
actionIdentitypolicy_1/concat_2*
T0
µ
Gcritic/value/encoder/hidden_0/kernel/Initializer/truncated_normal/shapeConst*7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel*
valueB"b     *
dtype0
¬
Fcritic/value/encoder/hidden_0/kernel/Initializer/truncated_normal/meanConst*7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel*
valueB
 *    *
dtype0
®
Hcritic/value/encoder/hidden_0/kernel/Initializer/truncated_normal/stddevConst*7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel*
valueB
 *Ų}w=*
dtype0

Qcritic/value/encoder/hidden_0/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormalGcritic/value/encoder/hidden_0/kernel/Initializer/truncated_normal/shape*
seedĶ%*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel*
dtype0*
seed2
«
Ecritic/value/encoder/hidden_0/kernel/Initializer/truncated_normal/mulMulQcritic/value/encoder/hidden_0/kernel/Initializer/truncated_normal/TruncatedNormalHcritic/value/encoder/hidden_0/kernel/Initializer/truncated_normal/stddev*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel

Acritic/value/encoder/hidden_0/kernel/Initializer/truncated_normalAddEcritic/value/encoder/hidden_0/kernel/Initializer/truncated_normal/mulFcritic/value/encoder/hidden_0/kernel/Initializer/truncated_normal/mean*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel
³
$critic/value/encoder/hidden_0/kernel
VariableV2*
shape:
ā*
shared_name *7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel*
dtype0*
	container 

+critic/value/encoder/hidden_0/kernel/AssignAssign$critic/value/encoder/hidden_0/kernelAcritic/value/encoder/hidden_0/kernel/Initializer/truncated_normal*
use_locking(*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel*
validate_shape(

)critic/value/encoder/hidden_0/kernel/readIdentity$critic/value/encoder/hidden_0/kernel*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel

4critic/value/encoder/hidden_0/bias/Initializer/zerosConst*5
_class+
)'loc:@critic/value/encoder/hidden_0/bias*
valueB*    *
dtype0
Ŗ
"critic/value/encoder/hidden_0/bias
VariableV2*
shape:*
shared_name *5
_class+
)'loc:@critic/value/encoder/hidden_0/bias*
dtype0*
	container 
ö
)critic/value/encoder/hidden_0/bias/AssignAssign"critic/value/encoder/hidden_0/bias4critic/value/encoder/hidden_0/bias/Initializer/zeros*
use_locking(*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_0/bias*
validate_shape(

'critic/value/encoder/hidden_0/bias/readIdentity"critic/value/encoder/hidden_0/bias*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_0/bias

$critic/value/encoder/hidden_0/MatMulMatMulvector_observation)critic/value/encoder/hidden_0/kernel/read*
transpose_b( *
T0*
transpose_a( 

%critic/value/encoder/hidden_0/BiasAddBiasAdd$critic/value/encoder/hidden_0/MatMul'critic/value/encoder/hidden_0/bias/read*
T0*
data_formatNHWC
`
%critic/value/encoder/hidden_0/SigmoidSigmoid%critic/value/encoder/hidden_0/BiasAdd*
T0

!critic/value/encoder/hidden_0/MulMul%critic/value/encoder/hidden_0/BiasAdd%critic/value/encoder/hidden_0/Sigmoid*
T0
µ
Gcritic/value/encoder/hidden_1/kernel/Initializer/truncated_normal/shapeConst*7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel*
valueB"      *
dtype0
¬
Fcritic/value/encoder/hidden_1/kernel/Initializer/truncated_normal/meanConst*7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel*
valueB
 *    *
dtype0
®
Hcritic/value/encoder/hidden_1/kernel/Initializer/truncated_normal/stddevConst*7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel*
valueB
 *6=*
dtype0

Qcritic/value/encoder/hidden_1/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormalGcritic/value/encoder/hidden_1/kernel/Initializer/truncated_normal/shape*
seedĶ%*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel*
dtype0*
seed2”
«
Ecritic/value/encoder/hidden_1/kernel/Initializer/truncated_normal/mulMulQcritic/value/encoder/hidden_1/kernel/Initializer/truncated_normal/TruncatedNormalHcritic/value/encoder/hidden_1/kernel/Initializer/truncated_normal/stddev*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel

Acritic/value/encoder/hidden_1/kernel/Initializer/truncated_normalAddEcritic/value/encoder/hidden_1/kernel/Initializer/truncated_normal/mulFcritic/value/encoder/hidden_1/kernel/Initializer/truncated_normal/mean*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel
³
$critic/value/encoder/hidden_1/kernel
VariableV2*
shape:
*
shared_name *7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel*
dtype0*
	container 

+critic/value/encoder/hidden_1/kernel/AssignAssign$critic/value/encoder/hidden_1/kernelAcritic/value/encoder/hidden_1/kernel/Initializer/truncated_normal*
use_locking(*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel*
validate_shape(

)critic/value/encoder/hidden_1/kernel/readIdentity$critic/value/encoder/hidden_1/kernel*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel

4critic/value/encoder/hidden_1/bias/Initializer/zerosConst*5
_class+
)'loc:@critic/value/encoder/hidden_1/bias*
valueB*    *
dtype0
Ŗ
"critic/value/encoder/hidden_1/bias
VariableV2*
shape:*
shared_name *5
_class+
)'loc:@critic/value/encoder/hidden_1/bias*
dtype0*
	container 
ö
)critic/value/encoder/hidden_1/bias/AssignAssign"critic/value/encoder/hidden_1/bias4critic/value/encoder/hidden_1/bias/Initializer/zeros*
use_locking(*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_1/bias*
validate_shape(

'critic/value/encoder/hidden_1/bias/readIdentity"critic/value/encoder/hidden_1/bias*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_1/bias
«
$critic/value/encoder/hidden_1/MatMulMatMul!critic/value/encoder/hidden_0/Mul)critic/value/encoder/hidden_1/kernel/read*
transpose_b( *
T0*
transpose_a( 

%critic/value/encoder/hidden_1/BiasAddBiasAdd$critic/value/encoder/hidden_1/MatMul'critic/value/encoder/hidden_1/bias/read*
T0*
data_formatNHWC
`
%critic/value/encoder/hidden_1/SigmoidSigmoid%critic/value/encoder/hidden_1/BiasAdd*
T0

!critic/value/encoder/hidden_1/MulMul%critic/value/encoder/hidden_1/BiasAdd%critic/value/encoder/hidden_1/Sigmoid*
T0
±
Dcritic/value/extrinsic_value/kernel/Initializer/random_uniform/shapeConst*6
_class,
*(loc:@critic/value/extrinsic_value/kernel*
valueB"      *
dtype0
§
Bcritic/value/extrinsic_value/kernel/Initializer/random_uniform/minConst*6
_class,
*(loc:@critic/value/extrinsic_value/kernel*
valueB
 *Iv¾*
dtype0
§
Bcritic/value/extrinsic_value/kernel/Initializer/random_uniform/maxConst*6
_class,
*(loc:@critic/value/extrinsic_value/kernel*
valueB
 *Iv>*
dtype0

Lcritic/value/extrinsic_value/kernel/Initializer/random_uniform/RandomUniformRandomUniformDcritic/value/extrinsic_value/kernel/Initializer/random_uniform/shape*
seedĶ%*
T0*6
_class,
*(loc:@critic/value/extrinsic_value/kernel*
dtype0*
seed2²

Bcritic/value/extrinsic_value/kernel/Initializer/random_uniform/subSubBcritic/value/extrinsic_value/kernel/Initializer/random_uniform/maxBcritic/value/extrinsic_value/kernel/Initializer/random_uniform/min*
T0*6
_class,
*(loc:@critic/value/extrinsic_value/kernel

Bcritic/value/extrinsic_value/kernel/Initializer/random_uniform/mulMulLcritic/value/extrinsic_value/kernel/Initializer/random_uniform/RandomUniformBcritic/value/extrinsic_value/kernel/Initializer/random_uniform/sub*
T0*6
_class,
*(loc:@critic/value/extrinsic_value/kernel

>critic/value/extrinsic_value/kernel/Initializer/random_uniformAddBcritic/value/extrinsic_value/kernel/Initializer/random_uniform/mulBcritic/value/extrinsic_value/kernel/Initializer/random_uniform/min*
T0*6
_class,
*(loc:@critic/value/extrinsic_value/kernel
°
#critic/value/extrinsic_value/kernel
VariableV2*
shape:	*
shared_name *6
_class,
*(loc:@critic/value/extrinsic_value/kernel*
dtype0*
	container 

*critic/value/extrinsic_value/kernel/AssignAssign#critic/value/extrinsic_value/kernel>critic/value/extrinsic_value/kernel/Initializer/random_uniform*
use_locking(*
T0*6
_class,
*(loc:@critic/value/extrinsic_value/kernel*
validate_shape(

(critic/value/extrinsic_value/kernel/readIdentity#critic/value/extrinsic_value/kernel*
T0*6
_class,
*(loc:@critic/value/extrinsic_value/kernel

3critic/value/extrinsic_value/bias/Initializer/zerosConst*4
_class*
(&loc:@critic/value/extrinsic_value/bias*
valueB*    *
dtype0
§
!critic/value/extrinsic_value/bias
VariableV2*
shape:*
shared_name *4
_class*
(&loc:@critic/value/extrinsic_value/bias*
dtype0*
	container 
ņ
(critic/value/extrinsic_value/bias/AssignAssign!critic/value/extrinsic_value/bias3critic/value/extrinsic_value/bias/Initializer/zeros*
use_locking(*
T0*4
_class*
(&loc:@critic/value/extrinsic_value/bias*
validate_shape(

&critic/value/extrinsic_value/bias/readIdentity!critic/value/extrinsic_value/bias*
T0*4
_class*
(&loc:@critic/value/extrinsic_value/bias
©
#critic/value/extrinsic_value/MatMulMatMul!critic/value/encoder/hidden_1/Mul(critic/value/extrinsic_value/kernel/read*
transpose_b( *
T0*
transpose_a( 

$critic/value/extrinsic_value/BiasAddBiasAdd#critic/value/extrinsic_value/MatMul&critic/value/extrinsic_value/bias/read*
T0*
data_formatNHWC
§
?critic/value/gail_value/kernel/Initializer/random_uniform/shapeConst*1
_class'
%#loc:@critic/value/gail_value/kernel*
valueB"      *
dtype0

=critic/value/gail_value/kernel/Initializer/random_uniform/minConst*1
_class'
%#loc:@critic/value/gail_value/kernel*
valueB
 *Iv¾*
dtype0

=critic/value/gail_value/kernel/Initializer/random_uniform/maxConst*1
_class'
%#loc:@critic/value/gail_value/kernel*
valueB
 *Iv>*
dtype0
ż
Gcritic/value/gail_value/kernel/Initializer/random_uniform/RandomUniformRandomUniform?critic/value/gail_value/kernel/Initializer/random_uniform/shape*
seedĶ%*
T0*1
_class'
%#loc:@critic/value/gail_value/kernel*
dtype0*
seed2Ā
ž
=critic/value/gail_value/kernel/Initializer/random_uniform/subSub=critic/value/gail_value/kernel/Initializer/random_uniform/max=critic/value/gail_value/kernel/Initializer/random_uniform/min*
T0*1
_class'
%#loc:@critic/value/gail_value/kernel

=critic/value/gail_value/kernel/Initializer/random_uniform/mulMulGcritic/value/gail_value/kernel/Initializer/random_uniform/RandomUniform=critic/value/gail_value/kernel/Initializer/random_uniform/sub*
T0*1
_class'
%#loc:@critic/value/gail_value/kernel
ś
9critic/value/gail_value/kernel/Initializer/random_uniformAdd=critic/value/gail_value/kernel/Initializer/random_uniform/mul=critic/value/gail_value/kernel/Initializer/random_uniform/min*
T0*1
_class'
%#loc:@critic/value/gail_value/kernel
¦
critic/value/gail_value/kernel
VariableV2*
shape:	*
shared_name *1
_class'
%#loc:@critic/value/gail_value/kernel*
dtype0*
	container 
ļ
%critic/value/gail_value/kernel/AssignAssigncritic/value/gail_value/kernel9critic/value/gail_value/kernel/Initializer/random_uniform*
use_locking(*
T0*1
_class'
%#loc:@critic/value/gail_value/kernel*
validate_shape(

#critic/value/gail_value/kernel/readIdentitycritic/value/gail_value/kernel*
T0*1
_class'
%#loc:@critic/value/gail_value/kernel

.critic/value/gail_value/bias/Initializer/zerosConst*/
_class%
#!loc:@critic/value/gail_value/bias*
valueB*    *
dtype0

critic/value/gail_value/bias
VariableV2*
shape:*
shared_name */
_class%
#!loc:@critic/value/gail_value/bias*
dtype0*
	container 
Ž
#critic/value/gail_value/bias/AssignAssigncritic/value/gail_value/bias.critic/value/gail_value/bias/Initializer/zeros*
use_locking(*
T0*/
_class%
#!loc:@critic/value/gail_value/bias*
validate_shape(

!critic/value/gail_value/bias/readIdentitycritic/value/gail_value/bias*
T0*/
_class%
#!loc:@critic/value/gail_value/bias

critic/value/gail_value/MatMulMatMul!critic/value/encoder/hidden_1/Mul#critic/value/gail_value/kernel/read*
transpose_b( *
T0*
transpose_a( 

critic/value/gail_value/BiasAddBiasAddcritic/value/gail_value/MatMul!critic/value/gail_value/bias/read*
T0*
data_formatNHWC

critic/value/Mean/inputPack$critic/value/extrinsic_value/BiasAddcritic/value/gail_value/BiasAdd*
T0*

axis *
N
M
#critic/value/Mean/reduction_indicesConst*
value	B : *
dtype0
}
critic/value/MeanMeancritic/value/Mean/input#critic/value/Mean/reduction_indices*

Tidx0*
	keep_dims( *
T0
Ė
Rcritic/q/q1_encoding/q1_encoder/hidden_0/kernel/Initializer/truncated_normal/shapeConst*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel*
valueB"b     *
dtype0
Ā
Qcritic/q/q1_encoding/q1_encoder/hidden_0/kernel/Initializer/truncated_normal/meanConst*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel*
valueB
 *    *
dtype0
Ä
Scritic/q/q1_encoding/q1_encoder/hidden_0/kernel/Initializer/truncated_normal/stddevConst*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel*
valueB
 *Ų}w=*
dtype0
ø
\critic/q/q1_encoding/q1_encoder/hidden_0/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormalRcritic/q/q1_encoding/q1_encoder/hidden_0/kernel/Initializer/truncated_normal/shape*
seedĶ%*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel*
dtype0*
seed2Õ
×
Pcritic/q/q1_encoding/q1_encoder/hidden_0/kernel/Initializer/truncated_normal/mulMul\critic/q/q1_encoding/q1_encoder/hidden_0/kernel/Initializer/truncated_normal/TruncatedNormalScritic/q/q1_encoding/q1_encoder/hidden_0/kernel/Initializer/truncated_normal/stddev*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel
Å
Lcritic/q/q1_encoding/q1_encoder/hidden_0/kernel/Initializer/truncated_normalAddPcritic/q/q1_encoding/q1_encoder/hidden_0/kernel/Initializer/truncated_normal/mulQcritic/q/q1_encoding/q1_encoder/hidden_0/kernel/Initializer/truncated_normal/mean*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel
É
/critic/q/q1_encoding/q1_encoder/hidden_0/kernel
VariableV2*
shape:
ā*
shared_name *B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel*
dtype0*
	container 
µ
6critic/q/q1_encoding/q1_encoder/hidden_0/kernel/AssignAssign/critic/q/q1_encoding/q1_encoder/hidden_0/kernelLcritic/q/q1_encoding/q1_encoder/hidden_0/kernel/Initializer/truncated_normal*
use_locking(*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel*
validate_shape(
¾
4critic/q/q1_encoding/q1_encoder/hidden_0/kernel/readIdentity/critic/q/q1_encoding/q1_encoder/hidden_0/kernel*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel
³
?critic/q/q1_encoding/q1_encoder/hidden_0/bias/Initializer/zerosConst*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_0/bias*
valueB*    *
dtype0
Ą
-critic/q/q1_encoding/q1_encoder/hidden_0/bias
VariableV2*
shape:*
shared_name *@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_0/bias*
dtype0*
	container 
¢
4critic/q/q1_encoding/q1_encoder/hidden_0/bias/AssignAssign-critic/q/q1_encoding/q1_encoder/hidden_0/bias?critic/q/q1_encoding/q1_encoder/hidden_0/bias/Initializer/zeros*
use_locking(*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_0/bias*
validate_shape(
ø
2critic/q/q1_encoding/q1_encoder/hidden_0/bias/readIdentity-critic/q/q1_encoding/q1_encoder/hidden_0/bias*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_0/bias
²
/critic/q/q1_encoding/q1_encoder/hidden_0/MatMulMatMulvector_observation4critic/q/q1_encoding/q1_encoder/hidden_0/kernel/read*
transpose_b( *
T0*
transpose_a( 
Ą
0critic/q/q1_encoding/q1_encoder/hidden_0/BiasAddBiasAdd/critic/q/q1_encoding/q1_encoder/hidden_0/MatMul2critic/q/q1_encoding/q1_encoder/hidden_0/bias/read*
T0*
data_formatNHWC
v
0critic/q/q1_encoding/q1_encoder/hidden_0/SigmoidSigmoid0critic/q/q1_encoding/q1_encoder/hidden_0/BiasAdd*
T0
 
,critic/q/q1_encoding/q1_encoder/hidden_0/MulMul0critic/q/q1_encoding/q1_encoder/hidden_0/BiasAdd0critic/q/q1_encoding/q1_encoder/hidden_0/Sigmoid*
T0
Ė
Rcritic/q/q1_encoding/q1_encoder/hidden_1/kernel/Initializer/truncated_normal/shapeConst*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel*
valueB"      *
dtype0
Ā
Qcritic/q/q1_encoding/q1_encoder/hidden_1/kernel/Initializer/truncated_normal/meanConst*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel*
valueB
 *    *
dtype0
Ä
Scritic/q/q1_encoding/q1_encoder/hidden_1/kernel/Initializer/truncated_normal/stddevConst*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel*
valueB
 *6=*
dtype0
ø
\critic/q/q1_encoding/q1_encoder/hidden_1/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormalRcritic/q/q1_encoding/q1_encoder/hidden_1/kernel/Initializer/truncated_normal/shape*
seedĶ%*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel*
dtype0*
seed2ę
×
Pcritic/q/q1_encoding/q1_encoder/hidden_1/kernel/Initializer/truncated_normal/mulMul\critic/q/q1_encoding/q1_encoder/hidden_1/kernel/Initializer/truncated_normal/TruncatedNormalScritic/q/q1_encoding/q1_encoder/hidden_1/kernel/Initializer/truncated_normal/stddev*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel
Å
Lcritic/q/q1_encoding/q1_encoder/hidden_1/kernel/Initializer/truncated_normalAddPcritic/q/q1_encoding/q1_encoder/hidden_1/kernel/Initializer/truncated_normal/mulQcritic/q/q1_encoding/q1_encoder/hidden_1/kernel/Initializer/truncated_normal/mean*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel
É
/critic/q/q1_encoding/q1_encoder/hidden_1/kernel
VariableV2*
shape:
*
shared_name *B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel*
dtype0*
	container 
µ
6critic/q/q1_encoding/q1_encoder/hidden_1/kernel/AssignAssign/critic/q/q1_encoding/q1_encoder/hidden_1/kernelLcritic/q/q1_encoding/q1_encoder/hidden_1/kernel/Initializer/truncated_normal*
use_locking(*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel*
validate_shape(
¾
4critic/q/q1_encoding/q1_encoder/hidden_1/kernel/readIdentity/critic/q/q1_encoding/q1_encoder/hidden_1/kernel*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel
³
?critic/q/q1_encoding/q1_encoder/hidden_1/bias/Initializer/zerosConst*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_1/bias*
valueB*    *
dtype0
Ą
-critic/q/q1_encoding/q1_encoder/hidden_1/bias
VariableV2*
shape:*
shared_name *@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_1/bias*
dtype0*
	container 
¢
4critic/q/q1_encoding/q1_encoder/hidden_1/bias/AssignAssign-critic/q/q1_encoding/q1_encoder/hidden_1/bias?critic/q/q1_encoding/q1_encoder/hidden_1/bias/Initializer/zeros*
use_locking(*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_1/bias*
validate_shape(
ø
2critic/q/q1_encoding/q1_encoder/hidden_1/bias/readIdentity-critic/q/q1_encoding/q1_encoder/hidden_1/bias*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_1/bias
Ģ
/critic/q/q1_encoding/q1_encoder/hidden_1/MatMulMatMul,critic/q/q1_encoding/q1_encoder/hidden_0/Mul4critic/q/q1_encoding/q1_encoder/hidden_1/kernel/read*
transpose_b( *
T0*
transpose_a( 
Ą
0critic/q/q1_encoding/q1_encoder/hidden_1/BiasAddBiasAdd/critic/q/q1_encoding/q1_encoder/hidden_1/MatMul2critic/q/q1_encoding/q1_encoder/hidden_1/bias/read*
T0*
data_formatNHWC
v
0critic/q/q1_encoding/q1_encoder/hidden_1/SigmoidSigmoid0critic/q/q1_encoding/q1_encoder/hidden_1/BiasAdd*
T0
 
,critic/q/q1_encoding/q1_encoder/hidden_1/MulMul0critic/q/q1_encoding/q1_encoder/hidden_1/BiasAdd0critic/q/q1_encoding/q1_encoder/hidden_1/Sigmoid*
T0
»
Icritic/q/q1_encoding/extrinsic_q1/kernel/Initializer/random_uniform/shapeConst*;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel*
valueB"      *
dtype0
±
Gcritic/q/q1_encoding/extrinsic_q1/kernel/Initializer/random_uniform/minConst*;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel*
valueB
 *²_¾*
dtype0
±
Gcritic/q/q1_encoding/extrinsic_q1/kernel/Initializer/random_uniform/maxConst*;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel*
valueB
 *²_>*
dtype0

Qcritic/q/q1_encoding/extrinsic_q1/kernel/Initializer/random_uniform/RandomUniformRandomUniformIcritic/q/q1_encoding/extrinsic_q1/kernel/Initializer/random_uniform/shape*
seedĶ%*
T0*;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel*
dtype0*
seed2÷
¦
Gcritic/q/q1_encoding/extrinsic_q1/kernel/Initializer/random_uniform/subSubGcritic/q/q1_encoding/extrinsic_q1/kernel/Initializer/random_uniform/maxGcritic/q/q1_encoding/extrinsic_q1/kernel/Initializer/random_uniform/min*
T0*;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel
°
Gcritic/q/q1_encoding/extrinsic_q1/kernel/Initializer/random_uniform/mulMulQcritic/q/q1_encoding/extrinsic_q1/kernel/Initializer/random_uniform/RandomUniformGcritic/q/q1_encoding/extrinsic_q1/kernel/Initializer/random_uniform/sub*
T0*;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel
¢
Ccritic/q/q1_encoding/extrinsic_q1/kernel/Initializer/random_uniformAddGcritic/q/q1_encoding/extrinsic_q1/kernel/Initializer/random_uniform/mulGcritic/q/q1_encoding/extrinsic_q1/kernel/Initializer/random_uniform/min*
T0*;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel
ŗ
(critic/q/q1_encoding/extrinsic_q1/kernel
VariableV2*
shape:	*
shared_name *;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel*
dtype0*
	container 

/critic/q/q1_encoding/extrinsic_q1/kernel/AssignAssign(critic/q/q1_encoding/extrinsic_q1/kernelCcritic/q/q1_encoding/extrinsic_q1/kernel/Initializer/random_uniform*
use_locking(*
T0*;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel*
validate_shape(
©
-critic/q/q1_encoding/extrinsic_q1/kernel/readIdentity(critic/q/q1_encoding/extrinsic_q1/kernel*
T0*;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel
¤
8critic/q/q1_encoding/extrinsic_q1/bias/Initializer/zerosConst*9
_class/
-+loc:@critic/q/q1_encoding/extrinsic_q1/bias*
valueB*    *
dtype0
±
&critic/q/q1_encoding/extrinsic_q1/bias
VariableV2*
shape:*
shared_name *9
_class/
-+loc:@critic/q/q1_encoding/extrinsic_q1/bias*
dtype0*
	container 

-critic/q/q1_encoding/extrinsic_q1/bias/AssignAssign&critic/q/q1_encoding/extrinsic_q1/bias8critic/q/q1_encoding/extrinsic_q1/bias/Initializer/zeros*
use_locking(*
T0*9
_class/
-+loc:@critic/q/q1_encoding/extrinsic_q1/bias*
validate_shape(
£
+critic/q/q1_encoding/extrinsic_q1/bias/readIdentity&critic/q/q1_encoding/extrinsic_q1/bias*
T0*9
_class/
-+loc:@critic/q/q1_encoding/extrinsic_q1/bias
¾
(critic/q/q1_encoding/extrinsic_q1/MatMulMatMul,critic/q/q1_encoding/q1_encoder/hidden_1/Mul-critic/q/q1_encoding/extrinsic_q1/kernel/read*
transpose_b( *
T0*
transpose_a( 
«
)critic/q/q1_encoding/extrinsic_q1/BiasAddBiasAdd(critic/q/q1_encoding/extrinsic_q1/MatMul+critic/q/q1_encoding/extrinsic_q1/bias/read*
T0*
data_formatNHWC
±
Dcritic/q/q1_encoding/gail_q1/kernel/Initializer/random_uniform/shapeConst*6
_class,
*(loc:@critic/q/q1_encoding/gail_q1/kernel*
valueB"      *
dtype0
§
Bcritic/q/q1_encoding/gail_q1/kernel/Initializer/random_uniform/minConst*6
_class,
*(loc:@critic/q/q1_encoding/gail_q1/kernel*
valueB
 *²_¾*
dtype0
§
Bcritic/q/q1_encoding/gail_q1/kernel/Initializer/random_uniform/maxConst*6
_class,
*(loc:@critic/q/q1_encoding/gail_q1/kernel*
valueB
 *²_>*
dtype0

Lcritic/q/q1_encoding/gail_q1/kernel/Initializer/random_uniform/RandomUniformRandomUniformDcritic/q/q1_encoding/gail_q1/kernel/Initializer/random_uniform/shape*
seedĶ%*
T0*6
_class,
*(loc:@critic/q/q1_encoding/gail_q1/kernel*
dtype0*
seed2

Bcritic/q/q1_encoding/gail_q1/kernel/Initializer/random_uniform/subSubBcritic/q/q1_encoding/gail_q1/kernel/Initializer/random_uniform/maxBcritic/q/q1_encoding/gail_q1/kernel/Initializer/random_uniform/min*
T0*6
_class,
*(loc:@critic/q/q1_encoding/gail_q1/kernel

Bcritic/q/q1_encoding/gail_q1/kernel/Initializer/random_uniform/mulMulLcritic/q/q1_encoding/gail_q1/kernel/Initializer/random_uniform/RandomUniformBcritic/q/q1_encoding/gail_q1/kernel/Initializer/random_uniform/sub*
T0*6
_class,
*(loc:@critic/q/q1_encoding/gail_q1/kernel

>critic/q/q1_encoding/gail_q1/kernel/Initializer/random_uniformAddBcritic/q/q1_encoding/gail_q1/kernel/Initializer/random_uniform/mulBcritic/q/q1_encoding/gail_q1/kernel/Initializer/random_uniform/min*
T0*6
_class,
*(loc:@critic/q/q1_encoding/gail_q1/kernel
°
#critic/q/q1_encoding/gail_q1/kernel
VariableV2*
shape:	*
shared_name *6
_class,
*(loc:@critic/q/q1_encoding/gail_q1/kernel*
dtype0*
	container 

*critic/q/q1_encoding/gail_q1/kernel/AssignAssign#critic/q/q1_encoding/gail_q1/kernel>critic/q/q1_encoding/gail_q1/kernel/Initializer/random_uniform*
use_locking(*
T0*6
_class,
*(loc:@critic/q/q1_encoding/gail_q1/kernel*
validate_shape(

(critic/q/q1_encoding/gail_q1/kernel/readIdentity#critic/q/q1_encoding/gail_q1/kernel*
T0*6
_class,
*(loc:@critic/q/q1_encoding/gail_q1/kernel

3critic/q/q1_encoding/gail_q1/bias/Initializer/zerosConst*4
_class*
(&loc:@critic/q/q1_encoding/gail_q1/bias*
valueB*    *
dtype0
§
!critic/q/q1_encoding/gail_q1/bias
VariableV2*
shape:*
shared_name *4
_class*
(&loc:@critic/q/q1_encoding/gail_q1/bias*
dtype0*
	container 
ņ
(critic/q/q1_encoding/gail_q1/bias/AssignAssign!critic/q/q1_encoding/gail_q1/bias3critic/q/q1_encoding/gail_q1/bias/Initializer/zeros*
use_locking(*
T0*4
_class*
(&loc:@critic/q/q1_encoding/gail_q1/bias*
validate_shape(

&critic/q/q1_encoding/gail_q1/bias/readIdentity!critic/q/q1_encoding/gail_q1/bias*
T0*4
_class*
(&loc:@critic/q/q1_encoding/gail_q1/bias
“
#critic/q/q1_encoding/gail_q1/MatMulMatMul,critic/q/q1_encoding/q1_encoder/hidden_1/Mul(critic/q/q1_encoding/gail_q1/kernel/read*
transpose_b( *
T0*
transpose_a( 

$critic/q/q1_encoding/gail_q1/BiasAddBiasAdd#critic/q/q1_encoding/gail_q1/MatMul&critic/q/q1_encoding/gail_q1/bias/read*
T0*
data_formatNHWC

critic/q/q1_encoding/Mean/inputPack)critic/q/q1_encoding/extrinsic_q1/BiasAdd$critic/q/q1_encoding/gail_q1/BiasAdd*
T0*

axis *
N
U
+critic/q/q1_encoding/Mean/reduction_indicesConst*
value	B : *
dtype0

critic/q/q1_encoding/MeanMeancritic/q/q1_encoding/Mean/input+critic/q/q1_encoding/Mean/reduction_indices*

Tidx0*
	keep_dims( *
T0
Ė
Rcritic/q/q2_encoding/q2_encoder/hidden_0/kernel/Initializer/truncated_normal/shapeConst*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel*
valueB"b     *
dtype0
Ā
Qcritic/q/q2_encoding/q2_encoder/hidden_0/kernel/Initializer/truncated_normal/meanConst*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel*
valueB
 *    *
dtype0
Ä
Scritic/q/q2_encoding/q2_encoder/hidden_0/kernel/Initializer/truncated_normal/stddevConst*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel*
valueB
 *Ų}w=*
dtype0
ø
\critic/q/q2_encoding/q2_encoder/hidden_0/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormalRcritic/q/q2_encoding/q2_encoder/hidden_0/kernel/Initializer/truncated_normal/shape*
seedĶ%*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel*
dtype0*
seed2
×
Pcritic/q/q2_encoding/q2_encoder/hidden_0/kernel/Initializer/truncated_normal/mulMul\critic/q/q2_encoding/q2_encoder/hidden_0/kernel/Initializer/truncated_normal/TruncatedNormalScritic/q/q2_encoding/q2_encoder/hidden_0/kernel/Initializer/truncated_normal/stddev*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel
Å
Lcritic/q/q2_encoding/q2_encoder/hidden_0/kernel/Initializer/truncated_normalAddPcritic/q/q2_encoding/q2_encoder/hidden_0/kernel/Initializer/truncated_normal/mulQcritic/q/q2_encoding/q2_encoder/hidden_0/kernel/Initializer/truncated_normal/mean*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel
É
/critic/q/q2_encoding/q2_encoder/hidden_0/kernel
VariableV2*
shape:
ā*
shared_name *B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel*
dtype0*
	container 
µ
6critic/q/q2_encoding/q2_encoder/hidden_0/kernel/AssignAssign/critic/q/q2_encoding/q2_encoder/hidden_0/kernelLcritic/q/q2_encoding/q2_encoder/hidden_0/kernel/Initializer/truncated_normal*
use_locking(*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel*
validate_shape(
¾
4critic/q/q2_encoding/q2_encoder/hidden_0/kernel/readIdentity/critic/q/q2_encoding/q2_encoder/hidden_0/kernel*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel
³
?critic/q/q2_encoding/q2_encoder/hidden_0/bias/Initializer/zerosConst*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_0/bias*
valueB*    *
dtype0
Ą
-critic/q/q2_encoding/q2_encoder/hidden_0/bias
VariableV2*
shape:*
shared_name *@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_0/bias*
dtype0*
	container 
¢
4critic/q/q2_encoding/q2_encoder/hidden_0/bias/AssignAssign-critic/q/q2_encoding/q2_encoder/hidden_0/bias?critic/q/q2_encoding/q2_encoder/hidden_0/bias/Initializer/zeros*
use_locking(*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_0/bias*
validate_shape(
ø
2critic/q/q2_encoding/q2_encoder/hidden_0/bias/readIdentity-critic/q/q2_encoding/q2_encoder/hidden_0/bias*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_0/bias
²
/critic/q/q2_encoding/q2_encoder/hidden_0/MatMulMatMulvector_observation4critic/q/q2_encoding/q2_encoder/hidden_0/kernel/read*
transpose_b( *
T0*
transpose_a( 
Ą
0critic/q/q2_encoding/q2_encoder/hidden_0/BiasAddBiasAdd/critic/q/q2_encoding/q2_encoder/hidden_0/MatMul2critic/q/q2_encoding/q2_encoder/hidden_0/bias/read*
T0*
data_formatNHWC
v
0critic/q/q2_encoding/q2_encoder/hidden_0/SigmoidSigmoid0critic/q/q2_encoding/q2_encoder/hidden_0/BiasAdd*
T0
 
,critic/q/q2_encoding/q2_encoder/hidden_0/MulMul0critic/q/q2_encoding/q2_encoder/hidden_0/BiasAdd0critic/q/q2_encoding/q2_encoder/hidden_0/Sigmoid*
T0
Ė
Rcritic/q/q2_encoding/q2_encoder/hidden_1/kernel/Initializer/truncated_normal/shapeConst*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel*
valueB"      *
dtype0
Ā
Qcritic/q/q2_encoding/q2_encoder/hidden_1/kernel/Initializer/truncated_normal/meanConst*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel*
valueB
 *    *
dtype0
Ä
Scritic/q/q2_encoding/q2_encoder/hidden_1/kernel/Initializer/truncated_normal/stddevConst*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel*
valueB
 *6=*
dtype0
ø
\critic/q/q2_encoding/q2_encoder/hidden_1/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormalRcritic/q/q2_encoding/q2_encoder/hidden_1/kernel/Initializer/truncated_normal/shape*
seedĶ%*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel*
dtype0*
seed2«
×
Pcritic/q/q2_encoding/q2_encoder/hidden_1/kernel/Initializer/truncated_normal/mulMul\critic/q/q2_encoding/q2_encoder/hidden_1/kernel/Initializer/truncated_normal/TruncatedNormalScritic/q/q2_encoding/q2_encoder/hidden_1/kernel/Initializer/truncated_normal/stddev*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel
Å
Lcritic/q/q2_encoding/q2_encoder/hidden_1/kernel/Initializer/truncated_normalAddPcritic/q/q2_encoding/q2_encoder/hidden_1/kernel/Initializer/truncated_normal/mulQcritic/q/q2_encoding/q2_encoder/hidden_1/kernel/Initializer/truncated_normal/mean*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel
É
/critic/q/q2_encoding/q2_encoder/hidden_1/kernel
VariableV2*
shape:
*
shared_name *B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel*
dtype0*
	container 
µ
6critic/q/q2_encoding/q2_encoder/hidden_1/kernel/AssignAssign/critic/q/q2_encoding/q2_encoder/hidden_1/kernelLcritic/q/q2_encoding/q2_encoder/hidden_1/kernel/Initializer/truncated_normal*
use_locking(*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel*
validate_shape(
¾
4critic/q/q2_encoding/q2_encoder/hidden_1/kernel/readIdentity/critic/q/q2_encoding/q2_encoder/hidden_1/kernel*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel
³
?critic/q/q2_encoding/q2_encoder/hidden_1/bias/Initializer/zerosConst*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_1/bias*
valueB*    *
dtype0
Ą
-critic/q/q2_encoding/q2_encoder/hidden_1/bias
VariableV2*
shape:*
shared_name *@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_1/bias*
dtype0*
	container 
¢
4critic/q/q2_encoding/q2_encoder/hidden_1/bias/AssignAssign-critic/q/q2_encoding/q2_encoder/hidden_1/bias?critic/q/q2_encoding/q2_encoder/hidden_1/bias/Initializer/zeros*
use_locking(*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_1/bias*
validate_shape(
ø
2critic/q/q2_encoding/q2_encoder/hidden_1/bias/readIdentity-critic/q/q2_encoding/q2_encoder/hidden_1/bias*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_1/bias
Ģ
/critic/q/q2_encoding/q2_encoder/hidden_1/MatMulMatMul,critic/q/q2_encoding/q2_encoder/hidden_0/Mul4critic/q/q2_encoding/q2_encoder/hidden_1/kernel/read*
transpose_b( *
T0*
transpose_a( 
Ą
0critic/q/q2_encoding/q2_encoder/hidden_1/BiasAddBiasAdd/critic/q/q2_encoding/q2_encoder/hidden_1/MatMul2critic/q/q2_encoding/q2_encoder/hidden_1/bias/read*
T0*
data_formatNHWC
v
0critic/q/q2_encoding/q2_encoder/hidden_1/SigmoidSigmoid0critic/q/q2_encoding/q2_encoder/hidden_1/BiasAdd*
T0
 
,critic/q/q2_encoding/q2_encoder/hidden_1/MulMul0critic/q/q2_encoding/q2_encoder/hidden_1/BiasAdd0critic/q/q2_encoding/q2_encoder/hidden_1/Sigmoid*
T0
»
Icritic/q/q2_encoding/extrinsic_q2/kernel/Initializer/random_uniform/shapeConst*;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel*
valueB"      *
dtype0
±
Gcritic/q/q2_encoding/extrinsic_q2/kernel/Initializer/random_uniform/minConst*;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel*
valueB
 *²_¾*
dtype0
±
Gcritic/q/q2_encoding/extrinsic_q2/kernel/Initializer/random_uniform/maxConst*;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel*
valueB
 *²_>*
dtype0

Qcritic/q/q2_encoding/extrinsic_q2/kernel/Initializer/random_uniform/RandomUniformRandomUniformIcritic/q/q2_encoding/extrinsic_q2/kernel/Initializer/random_uniform/shape*
seedĶ%*
T0*;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel*
dtype0*
seed2¼
¦
Gcritic/q/q2_encoding/extrinsic_q2/kernel/Initializer/random_uniform/subSubGcritic/q/q2_encoding/extrinsic_q2/kernel/Initializer/random_uniform/maxGcritic/q/q2_encoding/extrinsic_q2/kernel/Initializer/random_uniform/min*
T0*;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel
°
Gcritic/q/q2_encoding/extrinsic_q2/kernel/Initializer/random_uniform/mulMulQcritic/q/q2_encoding/extrinsic_q2/kernel/Initializer/random_uniform/RandomUniformGcritic/q/q2_encoding/extrinsic_q2/kernel/Initializer/random_uniform/sub*
T0*;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel
¢
Ccritic/q/q2_encoding/extrinsic_q2/kernel/Initializer/random_uniformAddGcritic/q/q2_encoding/extrinsic_q2/kernel/Initializer/random_uniform/mulGcritic/q/q2_encoding/extrinsic_q2/kernel/Initializer/random_uniform/min*
T0*;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel
ŗ
(critic/q/q2_encoding/extrinsic_q2/kernel
VariableV2*
shape:	*
shared_name *;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel*
dtype0*
	container 

/critic/q/q2_encoding/extrinsic_q2/kernel/AssignAssign(critic/q/q2_encoding/extrinsic_q2/kernelCcritic/q/q2_encoding/extrinsic_q2/kernel/Initializer/random_uniform*
use_locking(*
T0*;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel*
validate_shape(
©
-critic/q/q2_encoding/extrinsic_q2/kernel/readIdentity(critic/q/q2_encoding/extrinsic_q2/kernel*
T0*;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel
¤
8critic/q/q2_encoding/extrinsic_q2/bias/Initializer/zerosConst*9
_class/
-+loc:@critic/q/q2_encoding/extrinsic_q2/bias*
valueB*    *
dtype0
±
&critic/q/q2_encoding/extrinsic_q2/bias
VariableV2*
shape:*
shared_name *9
_class/
-+loc:@critic/q/q2_encoding/extrinsic_q2/bias*
dtype0*
	container 

-critic/q/q2_encoding/extrinsic_q2/bias/AssignAssign&critic/q/q2_encoding/extrinsic_q2/bias8critic/q/q2_encoding/extrinsic_q2/bias/Initializer/zeros*
use_locking(*
T0*9
_class/
-+loc:@critic/q/q2_encoding/extrinsic_q2/bias*
validate_shape(
£
+critic/q/q2_encoding/extrinsic_q2/bias/readIdentity&critic/q/q2_encoding/extrinsic_q2/bias*
T0*9
_class/
-+loc:@critic/q/q2_encoding/extrinsic_q2/bias
¾
(critic/q/q2_encoding/extrinsic_q2/MatMulMatMul,critic/q/q2_encoding/q2_encoder/hidden_1/Mul-critic/q/q2_encoding/extrinsic_q2/kernel/read*
transpose_b( *
T0*
transpose_a( 
«
)critic/q/q2_encoding/extrinsic_q2/BiasAddBiasAdd(critic/q/q2_encoding/extrinsic_q2/MatMul+critic/q/q2_encoding/extrinsic_q2/bias/read*
T0*
data_formatNHWC
±
Dcritic/q/q2_encoding/gail_q2/kernel/Initializer/random_uniform/shapeConst*6
_class,
*(loc:@critic/q/q2_encoding/gail_q2/kernel*
valueB"      *
dtype0
§
Bcritic/q/q2_encoding/gail_q2/kernel/Initializer/random_uniform/minConst*6
_class,
*(loc:@critic/q/q2_encoding/gail_q2/kernel*
valueB
 *²_¾*
dtype0
§
Bcritic/q/q2_encoding/gail_q2/kernel/Initializer/random_uniform/maxConst*6
_class,
*(loc:@critic/q/q2_encoding/gail_q2/kernel*
valueB
 *²_>*
dtype0

Lcritic/q/q2_encoding/gail_q2/kernel/Initializer/random_uniform/RandomUniformRandomUniformDcritic/q/q2_encoding/gail_q2/kernel/Initializer/random_uniform/shape*
seedĶ%*
T0*6
_class,
*(loc:@critic/q/q2_encoding/gail_q2/kernel*
dtype0*
seed2Ģ

Bcritic/q/q2_encoding/gail_q2/kernel/Initializer/random_uniform/subSubBcritic/q/q2_encoding/gail_q2/kernel/Initializer/random_uniform/maxBcritic/q/q2_encoding/gail_q2/kernel/Initializer/random_uniform/min*
T0*6
_class,
*(loc:@critic/q/q2_encoding/gail_q2/kernel

Bcritic/q/q2_encoding/gail_q2/kernel/Initializer/random_uniform/mulMulLcritic/q/q2_encoding/gail_q2/kernel/Initializer/random_uniform/RandomUniformBcritic/q/q2_encoding/gail_q2/kernel/Initializer/random_uniform/sub*
T0*6
_class,
*(loc:@critic/q/q2_encoding/gail_q2/kernel

>critic/q/q2_encoding/gail_q2/kernel/Initializer/random_uniformAddBcritic/q/q2_encoding/gail_q2/kernel/Initializer/random_uniform/mulBcritic/q/q2_encoding/gail_q2/kernel/Initializer/random_uniform/min*
T0*6
_class,
*(loc:@critic/q/q2_encoding/gail_q2/kernel
°
#critic/q/q2_encoding/gail_q2/kernel
VariableV2*
shape:	*
shared_name *6
_class,
*(loc:@critic/q/q2_encoding/gail_q2/kernel*
dtype0*
	container 

*critic/q/q2_encoding/gail_q2/kernel/AssignAssign#critic/q/q2_encoding/gail_q2/kernel>critic/q/q2_encoding/gail_q2/kernel/Initializer/random_uniform*
use_locking(*
T0*6
_class,
*(loc:@critic/q/q2_encoding/gail_q2/kernel*
validate_shape(

(critic/q/q2_encoding/gail_q2/kernel/readIdentity#critic/q/q2_encoding/gail_q2/kernel*
T0*6
_class,
*(loc:@critic/q/q2_encoding/gail_q2/kernel

3critic/q/q2_encoding/gail_q2/bias/Initializer/zerosConst*4
_class*
(&loc:@critic/q/q2_encoding/gail_q2/bias*
valueB*    *
dtype0
§
!critic/q/q2_encoding/gail_q2/bias
VariableV2*
shape:*
shared_name *4
_class*
(&loc:@critic/q/q2_encoding/gail_q2/bias*
dtype0*
	container 
ņ
(critic/q/q2_encoding/gail_q2/bias/AssignAssign!critic/q/q2_encoding/gail_q2/bias3critic/q/q2_encoding/gail_q2/bias/Initializer/zeros*
use_locking(*
T0*4
_class*
(&loc:@critic/q/q2_encoding/gail_q2/bias*
validate_shape(

&critic/q/q2_encoding/gail_q2/bias/readIdentity!critic/q/q2_encoding/gail_q2/bias*
T0*4
_class*
(&loc:@critic/q/q2_encoding/gail_q2/bias
“
#critic/q/q2_encoding/gail_q2/MatMulMatMul,critic/q/q2_encoding/q2_encoder/hidden_1/Mul(critic/q/q2_encoding/gail_q2/kernel/read*
transpose_b( *
T0*
transpose_a( 

$critic/q/q2_encoding/gail_q2/BiasAddBiasAdd#critic/q/q2_encoding/gail_q2/MatMul&critic/q/q2_encoding/gail_q2/bias/read*
T0*
data_formatNHWC

critic/q/q2_encoding/Mean/inputPack)critic/q/q2_encoding/extrinsic_q2/BiasAdd$critic/q/q2_encoding/gail_q2/BiasAdd*
T0*

axis *
N
U
+critic/q/q2_encoding/Mean/reduction_indicesConst*
value	B : *
dtype0

critic/q/q2_encoding/MeanMeancritic/q/q2_encoding/Mean/input+critic/q/q2_encoding/Mean/reduction_indices*

Tidx0*
	keep_dims( *
T0
“
1critic/q/q1_encoding_1/q1_encoder/hidden_0/MatMulMatMulvector_observation4critic/q/q1_encoding/q1_encoder/hidden_0/kernel/read*
transpose_b( *
T0*
transpose_a( 
Ä
2critic/q/q1_encoding_1/q1_encoder/hidden_0/BiasAddBiasAdd1critic/q/q1_encoding_1/q1_encoder/hidden_0/MatMul2critic/q/q1_encoding/q1_encoder/hidden_0/bias/read*
T0*
data_formatNHWC
z
2critic/q/q1_encoding_1/q1_encoder/hidden_0/SigmoidSigmoid2critic/q/q1_encoding_1/q1_encoder/hidden_0/BiasAdd*
T0
¦
.critic/q/q1_encoding_1/q1_encoder/hidden_0/MulMul2critic/q/q1_encoding_1/q1_encoder/hidden_0/BiasAdd2critic/q/q1_encoding_1/q1_encoder/hidden_0/Sigmoid*
T0
Š
1critic/q/q1_encoding_1/q1_encoder/hidden_1/MatMulMatMul.critic/q/q1_encoding_1/q1_encoder/hidden_0/Mul4critic/q/q1_encoding/q1_encoder/hidden_1/kernel/read*
transpose_b( *
T0*
transpose_a( 
Ä
2critic/q/q1_encoding_1/q1_encoder/hidden_1/BiasAddBiasAdd1critic/q/q1_encoding_1/q1_encoder/hidden_1/MatMul2critic/q/q1_encoding/q1_encoder/hidden_1/bias/read*
T0*
data_formatNHWC
z
2critic/q/q1_encoding_1/q1_encoder/hidden_1/SigmoidSigmoid2critic/q/q1_encoding_1/q1_encoder/hidden_1/BiasAdd*
T0
¦
.critic/q/q1_encoding_1/q1_encoder/hidden_1/MulMul2critic/q/q1_encoding_1/q1_encoder/hidden_1/BiasAdd2critic/q/q1_encoding_1/q1_encoder/hidden_1/Sigmoid*
T0
Ā
*critic/q/q1_encoding_1/extrinsic_q1/MatMulMatMul.critic/q/q1_encoding_1/q1_encoder/hidden_1/Mul-critic/q/q1_encoding/extrinsic_q1/kernel/read*
transpose_b( *
T0*
transpose_a( 
Æ
+critic/q/q1_encoding_1/extrinsic_q1/BiasAddBiasAdd*critic/q/q1_encoding_1/extrinsic_q1/MatMul+critic/q/q1_encoding/extrinsic_q1/bias/read*
T0*
data_formatNHWC
ø
%critic/q/q1_encoding_1/gail_q1/MatMulMatMul.critic/q/q1_encoding_1/q1_encoder/hidden_1/Mul(critic/q/q1_encoding/gail_q1/kernel/read*
transpose_b( *
T0*
transpose_a( 
 
&critic/q/q1_encoding_1/gail_q1/BiasAddBiasAdd%critic/q/q1_encoding_1/gail_q1/MatMul&critic/q/q1_encoding/gail_q1/bias/read*
T0*
data_formatNHWC

!critic/q/q1_encoding_1/Mean/inputPack+critic/q/q1_encoding_1/extrinsic_q1/BiasAdd&critic/q/q1_encoding_1/gail_q1/BiasAdd*
T0*

axis *
N
W
-critic/q/q1_encoding_1/Mean/reduction_indicesConst*
value	B : *
dtype0

critic/q/q1_encoding_1/MeanMean!critic/q/q1_encoding_1/Mean/input-critic/q/q1_encoding_1/Mean/reduction_indices*

Tidx0*
	keep_dims( *
T0
“
1critic/q/q2_encoding_1/q2_encoder/hidden_0/MatMulMatMulvector_observation4critic/q/q2_encoding/q2_encoder/hidden_0/kernel/read*
transpose_b( *
T0*
transpose_a( 
Ä
2critic/q/q2_encoding_1/q2_encoder/hidden_0/BiasAddBiasAdd1critic/q/q2_encoding_1/q2_encoder/hidden_0/MatMul2critic/q/q2_encoding/q2_encoder/hidden_0/bias/read*
T0*
data_formatNHWC
z
2critic/q/q2_encoding_1/q2_encoder/hidden_0/SigmoidSigmoid2critic/q/q2_encoding_1/q2_encoder/hidden_0/BiasAdd*
T0
¦
.critic/q/q2_encoding_1/q2_encoder/hidden_0/MulMul2critic/q/q2_encoding_1/q2_encoder/hidden_0/BiasAdd2critic/q/q2_encoding_1/q2_encoder/hidden_0/Sigmoid*
T0
Š
1critic/q/q2_encoding_1/q2_encoder/hidden_1/MatMulMatMul.critic/q/q2_encoding_1/q2_encoder/hidden_0/Mul4critic/q/q2_encoding/q2_encoder/hidden_1/kernel/read*
transpose_b( *
T0*
transpose_a( 
Ä
2critic/q/q2_encoding_1/q2_encoder/hidden_1/BiasAddBiasAdd1critic/q/q2_encoding_1/q2_encoder/hidden_1/MatMul2critic/q/q2_encoding/q2_encoder/hidden_1/bias/read*
T0*
data_formatNHWC
z
2critic/q/q2_encoding_1/q2_encoder/hidden_1/SigmoidSigmoid2critic/q/q2_encoding_1/q2_encoder/hidden_1/BiasAdd*
T0
¦
.critic/q/q2_encoding_1/q2_encoder/hidden_1/MulMul2critic/q/q2_encoding_1/q2_encoder/hidden_1/BiasAdd2critic/q/q2_encoding_1/q2_encoder/hidden_1/Sigmoid*
T0
Ā
*critic/q/q2_encoding_1/extrinsic_q2/MatMulMatMul.critic/q/q2_encoding_1/q2_encoder/hidden_1/Mul-critic/q/q2_encoding/extrinsic_q2/kernel/read*
transpose_b( *
T0*
transpose_a( 
Æ
+critic/q/q2_encoding_1/extrinsic_q2/BiasAddBiasAdd*critic/q/q2_encoding_1/extrinsic_q2/MatMul+critic/q/q2_encoding/extrinsic_q2/bias/read*
T0*
data_formatNHWC
ø
%critic/q/q2_encoding_1/gail_q2/MatMulMatMul.critic/q/q2_encoding_1/q2_encoder/hidden_1/Mul(critic/q/q2_encoding/gail_q2/kernel/read*
transpose_b( *
T0*
transpose_a( 
 
&critic/q/q2_encoding_1/gail_q2/BiasAddBiasAdd%critic/q/q2_encoding_1/gail_q2/MatMul&critic/q/q2_encoding/gail_q2/bias/read*
T0*
data_formatNHWC

!critic/q/q2_encoding_1/Mean/inputPack+critic/q/q2_encoding_1/extrinsic_q2/BiasAdd&critic/q/q2_encoding_1/gail_q2/BiasAdd*
T0*

axis *
N
W
-critic/q/q2_encoding_1/Mean/reduction_indicesConst*
value	B : *
dtype0

critic/q/q2_encoding_1/MeanMean!critic/q/q2_encoding_1/Mean/input-critic/q/q2_encoding_1/Mean/reduction_indices*

Tidx0*
	keep_dims( *
T0
E
global_step_2/initial_valueConst*
value	B : *
dtype0
Y
global_step_2
VariableV2*
shape: *
shared_name *
dtype0*
	container 

global_step_2/AssignAssignglobal_step_2global_step_2/initial_value*
use_locking(*
T0* 
_class
loc:@global_step_2*
validate_shape(
X
global_step_2/readIdentityglobal_step_2*
T0* 
_class
loc:@global_step_2
=
steps_to_increment_2Placeholder*
shape: *
dtype0
?
Add_2Addglobal_step_2/readsteps_to_increment_2*
T0
|
Assign_4Assignglobal_step_2Add_2*
use_locking(*
T0* 
_class
loc:@global_step_2*
validate_shape(
7
batch_size_2Placeholder*
shape:*
dtype0
<
sequence_length_2Placeholder*
shape:*
dtype0
=
masks_2Placeholder*
shape:’’’’’’’’’*
dtype0
?
Cast_2Castmasks_2*

SrcT0*
Truncate( *

DstT0
O
%is_continuous_control_2/initial_valueConst*
value	B : *
dtype0
c
is_continuous_control_2
VariableV2*
shape: *
shared_name *
dtype0*
	container 
Ę
is_continuous_control_2/AssignAssignis_continuous_control_2%is_continuous_control_2/initial_value*
use_locking(*
T0**
_class 
loc:@is_continuous_control_2*
validate_shape(
v
is_continuous_control_2/readIdentityis_continuous_control_2*
T0**
_class 
loc:@is_continuous_control_2
H
version_number_2/initial_valueConst*
value	B :*
dtype0
\
version_number_2
VariableV2*
shape: *
shared_name *
dtype0*
	container 
Ŗ
version_number_2/AssignAssignversion_number_2version_number_2/initial_value*
use_locking(*
T0*#
_class
loc:@version_number_2*
validate_shape(
a
version_number_2/readIdentityversion_number_2*
T0*#
_class
loc:@version_number_2
E
memory_size_2/initial_valueConst*
value	B : *
dtype0
Y
memory_size_2
VariableV2*
shape: *
shared_name *
dtype0*
	container 

memory_size_2/AssignAssignmemory_size_2memory_size_2/initial_value*
use_locking(*
T0* 
_class
loc:@memory_size_2*
validate_shape(
X
memory_size_2/readIdentitymemory_size_2*
T0* 
_class
loc:@memory_size_2
M
#action_output_shape_2/initial_valueConst*
value	B :*
dtype0
a
action_output_shape_2
VariableV2*
shape: *
shared_name *
dtype0*
	container 
¾
action_output_shape_2/AssignAssignaction_output_shape_2#action_output_shape_2/initial_value*
use_locking(*
T0*(
_class
loc:@action_output_shape_2*
validate_shape(
p
action_output_shape_2/readIdentityaction_output_shape_2*
T0*(
_class
loc:@action_output_shape_2
\
!target_network/vector_observationPlaceholder*
shape:’’’’’’’’’ā*
dtype0
Ó
Vtarget_network/critic/value/encoder/hidden_0/kernel/Initializer/truncated_normal/shapeConst*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_0/kernel*
valueB"b     *
dtype0
Ź
Utarget_network/critic/value/encoder/hidden_0/kernel/Initializer/truncated_normal/meanConst*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_0/kernel*
valueB
 *    *
dtype0
Ģ
Wtarget_network/critic/value/encoder/hidden_0/kernel/Initializer/truncated_normal/stddevConst*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_0/kernel*
valueB
 *Ų}w=*
dtype0
Ä
`target_network/critic/value/encoder/hidden_0/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormalVtarget_network/critic/value/encoder/hidden_0/kernel/Initializer/truncated_normal/shape*
seedĶ%*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_0/kernel*
dtype0*
seed2
ē
Ttarget_network/critic/value/encoder/hidden_0/kernel/Initializer/truncated_normal/mulMul`target_network/critic/value/encoder/hidden_0/kernel/Initializer/truncated_normal/TruncatedNormalWtarget_network/critic/value/encoder/hidden_0/kernel/Initializer/truncated_normal/stddev*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_0/kernel
Õ
Ptarget_network/critic/value/encoder/hidden_0/kernel/Initializer/truncated_normalAddTtarget_network/critic/value/encoder/hidden_0/kernel/Initializer/truncated_normal/mulUtarget_network/critic/value/encoder/hidden_0/kernel/Initializer/truncated_normal/mean*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_0/kernel
Ń
3target_network/critic/value/encoder/hidden_0/kernel
VariableV2*
shape:
ā*
shared_name *F
_class<
:8loc:@target_network/critic/value/encoder/hidden_0/kernel*
dtype0*
	container 
Å
:target_network/critic/value/encoder/hidden_0/kernel/AssignAssign3target_network/critic/value/encoder/hidden_0/kernelPtarget_network/critic/value/encoder/hidden_0/kernel/Initializer/truncated_normal*
use_locking(*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_0/kernel*
validate_shape(
Ź
8target_network/critic/value/encoder/hidden_0/kernel/readIdentity3target_network/critic/value/encoder/hidden_0/kernel*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_0/kernel
»
Ctarget_network/critic/value/encoder/hidden_0/bias/Initializer/zerosConst*D
_class:
86loc:@target_network/critic/value/encoder/hidden_0/bias*
valueB*    *
dtype0
Č
1target_network/critic/value/encoder/hidden_0/bias
VariableV2*
shape:*
shared_name *D
_class:
86loc:@target_network/critic/value/encoder/hidden_0/bias*
dtype0*
	container 
²
8target_network/critic/value/encoder/hidden_0/bias/AssignAssign1target_network/critic/value/encoder/hidden_0/biasCtarget_network/critic/value/encoder/hidden_0/bias/Initializer/zeros*
use_locking(*
T0*D
_class:
86loc:@target_network/critic/value/encoder/hidden_0/bias*
validate_shape(
Ä
6target_network/critic/value/encoder/hidden_0/bias/readIdentity1target_network/critic/value/encoder/hidden_0/bias*
T0*D
_class:
86loc:@target_network/critic/value/encoder/hidden_0/bias
É
3target_network/critic/value/encoder/hidden_0/MatMulMatMul!target_network/vector_observation8target_network/critic/value/encoder/hidden_0/kernel/read*
transpose_b( *
T0*
transpose_a( 
Ģ
4target_network/critic/value/encoder/hidden_0/BiasAddBiasAdd3target_network/critic/value/encoder/hidden_0/MatMul6target_network/critic/value/encoder/hidden_0/bias/read*
T0*
data_formatNHWC
~
4target_network/critic/value/encoder/hidden_0/SigmoidSigmoid4target_network/critic/value/encoder/hidden_0/BiasAdd*
T0
¬
0target_network/critic/value/encoder/hidden_0/MulMul4target_network/critic/value/encoder/hidden_0/BiasAdd4target_network/critic/value/encoder/hidden_0/Sigmoid*
T0
Ó
Vtarget_network/critic/value/encoder/hidden_1/kernel/Initializer/truncated_normal/shapeConst*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_1/kernel*
valueB"      *
dtype0
Ź
Utarget_network/critic/value/encoder/hidden_1/kernel/Initializer/truncated_normal/meanConst*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_1/kernel*
valueB
 *    *
dtype0
Ģ
Wtarget_network/critic/value/encoder/hidden_1/kernel/Initializer/truncated_normal/stddevConst*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_1/kernel*
valueB
 *6=*
dtype0
Ä
`target_network/critic/value/encoder/hidden_1/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormalVtarget_network/critic/value/encoder/hidden_1/kernel/Initializer/truncated_normal/shape*
seedĶ%*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_1/kernel*
dtype0*
seed2Ŗ
ē
Ttarget_network/critic/value/encoder/hidden_1/kernel/Initializer/truncated_normal/mulMul`target_network/critic/value/encoder/hidden_1/kernel/Initializer/truncated_normal/TruncatedNormalWtarget_network/critic/value/encoder/hidden_1/kernel/Initializer/truncated_normal/stddev*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_1/kernel
Õ
Ptarget_network/critic/value/encoder/hidden_1/kernel/Initializer/truncated_normalAddTtarget_network/critic/value/encoder/hidden_1/kernel/Initializer/truncated_normal/mulUtarget_network/critic/value/encoder/hidden_1/kernel/Initializer/truncated_normal/mean*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_1/kernel
Ń
3target_network/critic/value/encoder/hidden_1/kernel
VariableV2*
shape:
*
shared_name *F
_class<
:8loc:@target_network/critic/value/encoder/hidden_1/kernel*
dtype0*
	container 
Å
:target_network/critic/value/encoder/hidden_1/kernel/AssignAssign3target_network/critic/value/encoder/hidden_1/kernelPtarget_network/critic/value/encoder/hidden_1/kernel/Initializer/truncated_normal*
use_locking(*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_1/kernel*
validate_shape(
Ź
8target_network/critic/value/encoder/hidden_1/kernel/readIdentity3target_network/critic/value/encoder/hidden_1/kernel*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_1/kernel
»
Ctarget_network/critic/value/encoder/hidden_1/bias/Initializer/zerosConst*D
_class:
86loc:@target_network/critic/value/encoder/hidden_1/bias*
valueB*    *
dtype0
Č
1target_network/critic/value/encoder/hidden_1/bias
VariableV2*
shape:*
shared_name *D
_class:
86loc:@target_network/critic/value/encoder/hidden_1/bias*
dtype0*
	container 
²
8target_network/critic/value/encoder/hidden_1/bias/AssignAssign1target_network/critic/value/encoder/hidden_1/biasCtarget_network/critic/value/encoder/hidden_1/bias/Initializer/zeros*
use_locking(*
T0*D
_class:
86loc:@target_network/critic/value/encoder/hidden_1/bias*
validate_shape(
Ä
6target_network/critic/value/encoder/hidden_1/bias/readIdentity1target_network/critic/value/encoder/hidden_1/bias*
T0*D
_class:
86loc:@target_network/critic/value/encoder/hidden_1/bias
Ų
3target_network/critic/value/encoder/hidden_1/MatMulMatMul0target_network/critic/value/encoder/hidden_0/Mul8target_network/critic/value/encoder/hidden_1/kernel/read*
transpose_b( *
T0*
transpose_a( 
Ģ
4target_network/critic/value/encoder/hidden_1/BiasAddBiasAdd3target_network/critic/value/encoder/hidden_1/MatMul6target_network/critic/value/encoder/hidden_1/bias/read*
T0*
data_formatNHWC
~
4target_network/critic/value/encoder/hidden_1/SigmoidSigmoid4target_network/critic/value/encoder/hidden_1/BiasAdd*
T0
¬
0target_network/critic/value/encoder/hidden_1/MulMul4target_network/critic/value/encoder/hidden_1/BiasAdd4target_network/critic/value/encoder/hidden_1/Sigmoid*
T0
Ļ
Starget_network/critic/value/extrinsic_value/kernel/Initializer/random_uniform/shapeConst*E
_class;
97loc:@target_network/critic/value/extrinsic_value/kernel*
valueB"      *
dtype0
Å
Qtarget_network/critic/value/extrinsic_value/kernel/Initializer/random_uniform/minConst*E
_class;
97loc:@target_network/critic/value/extrinsic_value/kernel*
valueB
 *Iv¾*
dtype0
Å
Qtarget_network/critic/value/extrinsic_value/kernel/Initializer/random_uniform/maxConst*E
_class;
97loc:@target_network/critic/value/extrinsic_value/kernel*
valueB
 *Iv>*
dtype0
¹
[target_network/critic/value/extrinsic_value/kernel/Initializer/random_uniform/RandomUniformRandomUniformStarget_network/critic/value/extrinsic_value/kernel/Initializer/random_uniform/shape*
seedĶ%*
T0*E
_class;
97loc:@target_network/critic/value/extrinsic_value/kernel*
dtype0*
seed2»
Ī
Qtarget_network/critic/value/extrinsic_value/kernel/Initializer/random_uniform/subSubQtarget_network/critic/value/extrinsic_value/kernel/Initializer/random_uniform/maxQtarget_network/critic/value/extrinsic_value/kernel/Initializer/random_uniform/min*
T0*E
_class;
97loc:@target_network/critic/value/extrinsic_value/kernel
Ų
Qtarget_network/critic/value/extrinsic_value/kernel/Initializer/random_uniform/mulMul[target_network/critic/value/extrinsic_value/kernel/Initializer/random_uniform/RandomUniformQtarget_network/critic/value/extrinsic_value/kernel/Initializer/random_uniform/sub*
T0*E
_class;
97loc:@target_network/critic/value/extrinsic_value/kernel
Ź
Mtarget_network/critic/value/extrinsic_value/kernel/Initializer/random_uniformAddQtarget_network/critic/value/extrinsic_value/kernel/Initializer/random_uniform/mulQtarget_network/critic/value/extrinsic_value/kernel/Initializer/random_uniform/min*
T0*E
_class;
97loc:@target_network/critic/value/extrinsic_value/kernel
Ī
2target_network/critic/value/extrinsic_value/kernel
VariableV2*
shape:	*
shared_name *E
_class;
97loc:@target_network/critic/value/extrinsic_value/kernel*
dtype0*
	container 
æ
9target_network/critic/value/extrinsic_value/kernel/AssignAssign2target_network/critic/value/extrinsic_value/kernelMtarget_network/critic/value/extrinsic_value/kernel/Initializer/random_uniform*
use_locking(*
T0*E
_class;
97loc:@target_network/critic/value/extrinsic_value/kernel*
validate_shape(
Ē
7target_network/critic/value/extrinsic_value/kernel/readIdentity2target_network/critic/value/extrinsic_value/kernel*
T0*E
_class;
97loc:@target_network/critic/value/extrinsic_value/kernel
ø
Btarget_network/critic/value/extrinsic_value/bias/Initializer/zerosConst*C
_class9
75loc:@target_network/critic/value/extrinsic_value/bias*
valueB*    *
dtype0
Å
0target_network/critic/value/extrinsic_value/bias
VariableV2*
shape:*
shared_name *C
_class9
75loc:@target_network/critic/value/extrinsic_value/bias*
dtype0*
	container 
®
7target_network/critic/value/extrinsic_value/bias/AssignAssign0target_network/critic/value/extrinsic_value/biasBtarget_network/critic/value/extrinsic_value/bias/Initializer/zeros*
use_locking(*
T0*C
_class9
75loc:@target_network/critic/value/extrinsic_value/bias*
validate_shape(
Į
5target_network/critic/value/extrinsic_value/bias/readIdentity0target_network/critic/value/extrinsic_value/bias*
T0*C
_class9
75loc:@target_network/critic/value/extrinsic_value/bias
Ö
2target_network/critic/value/extrinsic_value/MatMulMatMul0target_network/critic/value/encoder/hidden_1/Mul7target_network/critic/value/extrinsic_value/kernel/read*
transpose_b( *
T0*
transpose_a( 
É
3target_network/critic/value/extrinsic_value/BiasAddBiasAdd2target_network/critic/value/extrinsic_value/MatMul5target_network/critic/value/extrinsic_value/bias/read*
T0*
data_formatNHWC
Å
Ntarget_network/critic/value/gail_value/kernel/Initializer/random_uniform/shapeConst*@
_class6
42loc:@target_network/critic/value/gail_value/kernel*
valueB"      *
dtype0
»
Ltarget_network/critic/value/gail_value/kernel/Initializer/random_uniform/minConst*@
_class6
42loc:@target_network/critic/value/gail_value/kernel*
valueB
 *Iv¾*
dtype0
»
Ltarget_network/critic/value/gail_value/kernel/Initializer/random_uniform/maxConst*@
_class6
42loc:@target_network/critic/value/gail_value/kernel*
valueB
 *Iv>*
dtype0
Ŗ
Vtarget_network/critic/value/gail_value/kernel/Initializer/random_uniform/RandomUniformRandomUniformNtarget_network/critic/value/gail_value/kernel/Initializer/random_uniform/shape*
seedĶ%*
T0*@
_class6
42loc:@target_network/critic/value/gail_value/kernel*
dtype0*
seed2Ė
ŗ
Ltarget_network/critic/value/gail_value/kernel/Initializer/random_uniform/subSubLtarget_network/critic/value/gail_value/kernel/Initializer/random_uniform/maxLtarget_network/critic/value/gail_value/kernel/Initializer/random_uniform/min*
T0*@
_class6
42loc:@target_network/critic/value/gail_value/kernel
Ä
Ltarget_network/critic/value/gail_value/kernel/Initializer/random_uniform/mulMulVtarget_network/critic/value/gail_value/kernel/Initializer/random_uniform/RandomUniformLtarget_network/critic/value/gail_value/kernel/Initializer/random_uniform/sub*
T0*@
_class6
42loc:@target_network/critic/value/gail_value/kernel
¶
Htarget_network/critic/value/gail_value/kernel/Initializer/random_uniformAddLtarget_network/critic/value/gail_value/kernel/Initializer/random_uniform/mulLtarget_network/critic/value/gail_value/kernel/Initializer/random_uniform/min*
T0*@
_class6
42loc:@target_network/critic/value/gail_value/kernel
Ä
-target_network/critic/value/gail_value/kernel
VariableV2*
shape:	*
shared_name *@
_class6
42loc:@target_network/critic/value/gail_value/kernel*
dtype0*
	container 
«
4target_network/critic/value/gail_value/kernel/AssignAssign-target_network/critic/value/gail_value/kernelHtarget_network/critic/value/gail_value/kernel/Initializer/random_uniform*
use_locking(*
T0*@
_class6
42loc:@target_network/critic/value/gail_value/kernel*
validate_shape(
ø
2target_network/critic/value/gail_value/kernel/readIdentity-target_network/critic/value/gail_value/kernel*
T0*@
_class6
42loc:@target_network/critic/value/gail_value/kernel
®
=target_network/critic/value/gail_value/bias/Initializer/zerosConst*>
_class4
20loc:@target_network/critic/value/gail_value/bias*
valueB*    *
dtype0
»
+target_network/critic/value/gail_value/bias
VariableV2*
shape:*
shared_name *>
_class4
20loc:@target_network/critic/value/gail_value/bias*
dtype0*
	container 

2target_network/critic/value/gail_value/bias/AssignAssign+target_network/critic/value/gail_value/bias=target_network/critic/value/gail_value/bias/Initializer/zeros*
use_locking(*
T0*>
_class4
20loc:@target_network/critic/value/gail_value/bias*
validate_shape(
²
0target_network/critic/value/gail_value/bias/readIdentity+target_network/critic/value/gail_value/bias*
T0*>
_class4
20loc:@target_network/critic/value/gail_value/bias
Ģ
-target_network/critic/value/gail_value/MatMulMatMul0target_network/critic/value/encoder/hidden_1/Mul2target_network/critic/value/gail_value/kernel/read*
transpose_b( *
T0*
transpose_a( 
ŗ
.target_network/critic/value/gail_value/BiasAddBiasAdd-target_network/critic/value/gail_value/MatMul0target_network/critic/value/gail_value/bias/read*
T0*
data_formatNHWC
±
&target_network/critic/value/Mean/inputPack3target_network/critic/value/extrinsic_value/BiasAdd.target_network/critic/value/gail_value/BiasAdd*
T0*

axis *
N
\
2target_network/critic/value/Mean/reduction_indicesConst*
value	B : *
dtype0
Ŗ
 target_network/critic/value/MeanMean&target_network/critic/value/Mean/input2target_network/critic/value/Mean/reduction_indices*

Tidx0*
	keep_dims( *
T0
=
value_estimate_unusedIdentitycritic/value/Mean*
T0
B
dones_holderPlaceholder*
shape:’’’’’’’’’*
dtype0
A
epsilonPlaceholder*
shape:’’’’’’’’’*
dtype0
E
Variable_2/initial_valueConst*
valueB
 *RI9*
dtype0
V

Variable_2
VariableV2*
shape: *
shared_name *
dtype0*
	container 

Variable_2/AssignAssign
Variable_2Variable_2/initial_value*
use_locking(*
T0*
_class
loc:@Variable_2*
validate_shape(
O
Variable_2/readIdentity
Variable_2*
T0*
_class
loc:@Variable_2
S
mulMul+critic/q/q1_encoding_1/extrinsic_q1/BiasAddpolicy_1/concat_1*
T0
H
strided_slice/stackConst*
valueB"        *
dtype0
J
strided_slice/stack_1Const*
valueB"       *
dtype0
J
strided_slice/stack_2Const*
valueB"      *
dtype0
ß
strided_sliceStridedSlicemulstrided_slice/stackstrided_slice/stack_1strided_slice/stack_2*
T0*
Index0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
J
strided_slice_1/stackConst*
valueB"       *
dtype0
L
strided_slice_1/stack_1Const*
valueB"       *
dtype0
L
strided_slice_1/stack_2Const*
valueB"      *
dtype0
ē
strided_slice_1StridedSlicemulstrided_slice_1/stackstrided_slice_1/stack_1strided_slice_1/stack_2*
T0*
Index0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
J
strided_slice_2/stackConst*
valueB"       *
dtype0
L
strided_slice_2/stack_1Const*
valueB"       *
dtype0
L
strided_slice_2/stack_2Const*
valueB"      *
dtype0
ē
strided_slice_2StridedSlicemulstrided_slice_2/stackstrided_slice_2/stack_1strided_slice_2/stack_2*
T0*
Index0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
?
Sum/reduction_indicesConst*
value	B :*
dtype0
V
SumSumstrided_sliceSum/reduction_indices*

Tidx0*
	keep_dims(*
T0
A
Sum_1/reduction_indicesConst*
value	B :*
dtype0
\
Sum_1Sumstrided_slice_1Sum_1/reduction_indices*

Tidx0*
	keep_dims(*
T0
A
Sum_2/reduction_indicesConst*
value	B :*
dtype0
\
Sum_2Sumstrided_slice_2Sum_2/reduction_indices*

Tidx0*
	keep_dims(*
T0
>
stackPackSumSum_1Sum_2*
T0*

axis *
N
@
Mean/reduction_indicesConst*
value	B : *
dtype0
Q
MeanMeanstackMean/reduction_indices*

Tidx0*
	keep_dims( *
T0
U
mul_1Mul+critic/q/q2_encoding_1/extrinsic_q2/BiasAddpolicy_1/concat_1*
T0
J
strided_slice_3/stackConst*
valueB"        *
dtype0
L
strided_slice_3/stack_1Const*
valueB"       *
dtype0
L
strided_slice_3/stack_2Const*
valueB"      *
dtype0
é
strided_slice_3StridedSlicemul_1strided_slice_3/stackstrided_slice_3/stack_1strided_slice_3/stack_2*
T0*
Index0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
J
strided_slice_4/stackConst*
valueB"       *
dtype0
L
strided_slice_4/stack_1Const*
valueB"       *
dtype0
L
strided_slice_4/stack_2Const*
valueB"      *
dtype0
é
strided_slice_4StridedSlicemul_1strided_slice_4/stackstrided_slice_4/stack_1strided_slice_4/stack_2*
T0*
Index0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
J
strided_slice_5/stackConst*
valueB"       *
dtype0
L
strided_slice_5/stack_1Const*
valueB"       *
dtype0
L
strided_slice_5/stack_2Const*
valueB"      *
dtype0
é
strided_slice_5StridedSlicemul_1strided_slice_5/stackstrided_slice_5/stack_1strided_slice_5/stack_2*
T0*
Index0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
A
Sum_3/reduction_indicesConst*
value	B :*
dtype0
\
Sum_3Sumstrided_slice_3Sum_3/reduction_indices*

Tidx0*
	keep_dims(*
T0
A
Sum_4/reduction_indicesConst*
value	B :*
dtype0
\
Sum_4Sumstrided_slice_4Sum_4/reduction_indices*

Tidx0*
	keep_dims(*
T0
A
Sum_5/reduction_indicesConst*
value	B :*
dtype0
\
Sum_5Sumstrided_slice_5Sum_5/reduction_indices*

Tidx0*
	keep_dims(*
T0
B
stack_1PackSum_3Sum_4Sum_5*
T0*

axis *
N
B
Mean_1/reduction_indicesConst*
value	B : *
dtype0
W
Mean_1Meanstack_1Mean_1/reduction_indices*

Tidx0*
	keep_dims( *
T0
)
MinimumMinimumMeanMean_1*
T0
G
extrinsic_rewardsPlaceholder*
shape:’’’’’’’’’*
dtype0
I
extrinsic_rewards_1Placeholder*
shape:’’’’’’’’’*
dtype0
P
mul_2Mul&critic/q/q1_encoding_1/gail_q1/BiasAddpolicy_1/concat_1*
T0
J
strided_slice_6/stackConst*
valueB"        *
dtype0
L
strided_slice_6/stack_1Const*
valueB"       *
dtype0
L
strided_slice_6/stack_2Const*
valueB"      *
dtype0
é
strided_slice_6StridedSlicemul_2strided_slice_6/stackstrided_slice_6/stack_1strided_slice_6/stack_2*
T0*
Index0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
J
strided_slice_7/stackConst*
valueB"       *
dtype0
L
strided_slice_7/stack_1Const*
valueB"       *
dtype0
L
strided_slice_7/stack_2Const*
valueB"      *
dtype0
é
strided_slice_7StridedSlicemul_2strided_slice_7/stackstrided_slice_7/stack_1strided_slice_7/stack_2*
T0*
Index0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
J
strided_slice_8/stackConst*
valueB"       *
dtype0
L
strided_slice_8/stack_1Const*
valueB"       *
dtype0
L
strided_slice_8/stack_2Const*
valueB"      *
dtype0
é
strided_slice_8StridedSlicemul_2strided_slice_8/stackstrided_slice_8/stack_1strided_slice_8/stack_2*
T0*
Index0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
A
Sum_6/reduction_indicesConst*
value	B :*
dtype0
\
Sum_6Sumstrided_slice_6Sum_6/reduction_indices*

Tidx0*
	keep_dims(*
T0
A
Sum_7/reduction_indicesConst*
value	B :*
dtype0
\
Sum_7Sumstrided_slice_7Sum_7/reduction_indices*

Tidx0*
	keep_dims(*
T0
A
Sum_8/reduction_indicesConst*
value	B :*
dtype0
\
Sum_8Sumstrided_slice_8Sum_8/reduction_indices*

Tidx0*
	keep_dims(*
T0
B
stack_2PackSum_6Sum_7Sum_8*
T0*

axis *
N
B
Mean_2/reduction_indicesConst*
value	B : *
dtype0
W
Mean_2Meanstack_2Mean_2/reduction_indices*

Tidx0*
	keep_dims( *
T0
P
mul_3Mul&critic/q/q2_encoding_1/gail_q2/BiasAddpolicy_1/concat_1*
T0
J
strided_slice_9/stackConst*
valueB"        *
dtype0
L
strided_slice_9/stack_1Const*
valueB"       *
dtype0
L
strided_slice_9/stack_2Const*
valueB"      *
dtype0
é
strided_slice_9StridedSlicemul_3strided_slice_9/stackstrided_slice_9/stack_1strided_slice_9/stack_2*
T0*
Index0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
K
strided_slice_10/stackConst*
valueB"       *
dtype0
M
strided_slice_10/stack_1Const*
valueB"       *
dtype0
M
strided_slice_10/stack_2Const*
valueB"      *
dtype0
ķ
strided_slice_10StridedSlicemul_3strided_slice_10/stackstrided_slice_10/stack_1strided_slice_10/stack_2*
T0*
Index0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
K
strided_slice_11/stackConst*
valueB"       *
dtype0
M
strided_slice_11/stack_1Const*
valueB"       *
dtype0
M
strided_slice_11/stack_2Const*
valueB"      *
dtype0
ķ
strided_slice_11StridedSlicemul_3strided_slice_11/stackstrided_slice_11/stack_1strided_slice_11/stack_2*
T0*
Index0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
A
Sum_9/reduction_indicesConst*
value	B :*
dtype0
\
Sum_9Sumstrided_slice_9Sum_9/reduction_indices*

Tidx0*
	keep_dims(*
T0
B
Sum_10/reduction_indicesConst*
value	B :*
dtype0
_
Sum_10Sumstrided_slice_10Sum_10/reduction_indices*

Tidx0*
	keep_dims(*
T0
B
Sum_11/reduction_indicesConst*
value	B :*
dtype0
_
Sum_11Sumstrided_slice_11Sum_11/reduction_indices*

Tidx0*
	keep_dims(*
T0
D
stack_3PackSum_9Sum_10Sum_11*
T0*

axis *
N
B
Mean_3/reduction_indicesConst*
value	B : *
dtype0
W
Mean_3Meanstack_3Mean_3/reduction_indices*

Tidx0*
	keep_dims( *
T0
-
	Minimum_1MinimumMean_2Mean_3*
T0
B
gail_rewardsPlaceholder*
shape:’’’’’’’’’*
dtype0
D
gail_rewards_1Placeholder*
shape:’’’’’’’’’*
dtype0
A
ExpandDims/dimConst*
valueB :
’’’’’’’’’*
dtype0
K

ExpandDims
ExpandDimsdones_holderExpandDims/dim*

Tdim0*
T0
C
ExpandDims_1/dimConst*
valueB :
’’’’’’’’’*
dtype0
V
ExpandDims_1
ExpandDimsextrinsic_rewards_1ExpandDims_1/dim*

Tdim0*
T0
0
mul_4MulVariable/read
ExpandDims*
T0
2
sub/xConst*
valueB
 *  ?*
dtype0
!
subSubsub/xmul_4*
T0
4
mul_5/yConst*
valueB
 *¤p}?*
dtype0
#
mul_5Mulsubmul_5/y*
T0
Q
mul_6Mulmul_53target_network/critic/value/extrinsic_value/BiasAdd*
T0
,
add_3AddV2ExpandDims_1mul_6*
T0
,
StopGradientStopGradientadd_3*
T0
S
mul_7Mulpolicy_1/concat_4)critic/q/q1_encoding/extrinsic_q1/BiasAdd*
T0
K
strided_slice_12/stackConst*
valueB"        *
dtype0
M
strided_slice_12/stack_1Const*
valueB"       *
dtype0
M
strided_slice_12/stack_2Const*
valueB"      *
dtype0
ķ
strided_slice_12StridedSlicemul_7strided_slice_12/stackstrided_slice_12/stack_1strided_slice_12/stack_2*
T0*
Index0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
K
strided_slice_13/stackConst*
valueB"       *
dtype0
M
strided_slice_13/stack_1Const*
valueB"       *
dtype0
M
strided_slice_13/stack_2Const*
valueB"      *
dtype0
ķ
strided_slice_13StridedSlicemul_7strided_slice_13/stackstrided_slice_13/stack_1strided_slice_13/stack_2*
T0*
Index0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
K
strided_slice_14/stackConst*
valueB"       *
dtype0
M
strided_slice_14/stack_1Const*
valueB"       *
dtype0
M
strided_slice_14/stack_2Const*
valueB"      *
dtype0
ķ
strided_slice_14StridedSlicemul_7strided_slice_14/stackstrided_slice_14/stack_1strided_slice_14/stack_2*
T0*
Index0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
S
mul_8Mulpolicy_1/concat_4)critic/q/q2_encoding/extrinsic_q2/BiasAdd*
T0
K
strided_slice_15/stackConst*
valueB"        *
dtype0
M
strided_slice_15/stack_1Const*
valueB"       *
dtype0
M
strided_slice_15/stack_2Const*
valueB"      *
dtype0
ķ
strided_slice_15StridedSlicemul_8strided_slice_15/stackstrided_slice_15/stack_1strided_slice_15/stack_2*
T0*
Index0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
K
strided_slice_16/stackConst*
valueB"       *
dtype0
M
strided_slice_16/stack_1Const*
valueB"       *
dtype0
M
strided_slice_16/stack_2Const*
valueB"      *
dtype0
ķ
strided_slice_16StridedSlicemul_8strided_slice_16/stackstrided_slice_16/stack_1strided_slice_16/stack_2*
T0*
Index0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
K
strided_slice_17/stackConst*
valueB"       *
dtype0
M
strided_slice_17/stack_1Const*
valueB"       *
dtype0
M
strided_slice_17/stack_2Const*
valueB"      *
dtype0
ķ
strided_slice_17StridedSlicemul_8strided_slice_17/stackstrided_slice_17/stack_1strided_slice_17/stack_2*
T0*
Index0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
B
Sum_12/reduction_indicesConst*
value	B :*
dtype0
_
Sum_12Sumstrided_slice_12Sum_12/reduction_indices*

Tidx0*
	keep_dims(*
T0
B
Sum_13/reduction_indicesConst*
value	B :*
dtype0
_
Sum_13Sumstrided_slice_13Sum_13/reduction_indices*

Tidx0*
	keep_dims(*
T0
B
Sum_14/reduction_indicesConst*
value	B :*
dtype0
_
Sum_14Sumstrided_slice_14Sum_14/reduction_indices*

Tidx0*
	keep_dims(*
T0
B
Sum_15/reduction_indicesConst*
value	B :*
dtype0
_
Sum_15Sumstrided_slice_15Sum_15/reduction_indices*

Tidx0*
	keep_dims(*
T0
B
Sum_16/reduction_indicesConst*
value	B :*
dtype0
_
Sum_16Sumstrided_slice_16Sum_16/reduction_indices*

Tidx0*
	keep_dims(*
T0
B
Sum_17/reduction_indicesConst*
value	B :*
dtype0
_
Sum_17Sumstrided_slice_17Sum_17/reduction_indices*

Tidx0*
	keep_dims(*
T0
J
Mean_4/inputPackSum_12Sum_13Sum_14*
T0*

axis *
N
B
Mean_4/reduction_indicesConst*
value	B : *
dtype0
\
Mean_4MeanMean_4/inputMean_4/reduction_indices*

Tidx0*
	keep_dims( *
T0
J
Mean_5/inputPackSum_15Sum_16Sum_17*
T0*

axis *
N
B
Mean_5/reduction_indicesConst*
value	B : *
dtype0
\
Mean_5MeanMean_5/inputMean_5/reduction_indices*

Tidx0*
	keep_dims( *
T0
=
ToFloatCastCast*

SrcT0*
Truncate( *

DstT0
E
SquaredDifferenceSquaredDifferenceStopGradientMean_4*
T0
1
mul_9MulToFloatSquaredDifference*
T0
:
ConstConst*
valueB"       *
dtype0
B
Mean_6Meanmul_9Const*

Tidx0*
	keep_dims( *
T0
5
mul_10/xConst*
valueB
 *   ?*
dtype0
(
mul_10Mulmul_10/xMean_6*
T0
?
	ToFloat_1CastCast*

SrcT0*
Truncate( *

DstT0
G
SquaredDifference_1SquaredDifferenceStopGradientMean_5*
T0
6
mul_11Mul	ToFloat_1SquaredDifference_1*
T0
<
Const_1Const*
valueB"       *
dtype0
E
Mean_7Meanmul_11Const_1*

Tidx0*
	keep_dims( *
T0
5
mul_12/xConst*
valueB
 *   ?*
dtype0
(
mul_12Mulmul_12/xMean_7*
T0
C
ExpandDims_2/dimConst*
valueB :
’’’’’’’’’*
dtype0
Q
ExpandDims_2
ExpandDimsgail_rewards_1ExpandDims_2/dim*

Tdim0*
T0
3
mul_13MulVariable_1/read
ExpandDims*
T0
4
sub_1/xConst*
valueB
 *  ?*
dtype0
&
sub_1Subsub_1/xmul_13*
T0
5
mul_14/yConst*
valueB
 *¤p}?*
dtype0
'
mul_14Mulsub_1mul_14/y*
T0
N
mul_15Mulmul_14.target_network/critic/value/gail_value/BiasAdd*
T0
-
add_4AddV2ExpandDims_2mul_15*
T0
.
StopGradient_1StopGradientadd_4*
T0
O
mul_16Mulpolicy_1/concat_4$critic/q/q1_encoding/gail_q1/BiasAdd*
T0
K
strided_slice_18/stackConst*
valueB"        *
dtype0
M
strided_slice_18/stack_1Const*
valueB"       *
dtype0
M
strided_slice_18/stack_2Const*
valueB"      *
dtype0
ī
strided_slice_18StridedSlicemul_16strided_slice_18/stackstrided_slice_18/stack_1strided_slice_18/stack_2*
T0*
Index0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
K
strided_slice_19/stackConst*
valueB"       *
dtype0
M
strided_slice_19/stack_1Const*
valueB"       *
dtype0
M
strided_slice_19/stack_2Const*
valueB"      *
dtype0
ī
strided_slice_19StridedSlicemul_16strided_slice_19/stackstrided_slice_19/stack_1strided_slice_19/stack_2*
T0*
Index0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
K
strided_slice_20/stackConst*
valueB"       *
dtype0
M
strided_slice_20/stack_1Const*
valueB"       *
dtype0
M
strided_slice_20/stack_2Const*
valueB"      *
dtype0
ī
strided_slice_20StridedSlicemul_16strided_slice_20/stackstrided_slice_20/stack_1strided_slice_20/stack_2*
T0*
Index0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
O
mul_17Mulpolicy_1/concat_4$critic/q/q2_encoding/gail_q2/BiasAdd*
T0
K
strided_slice_21/stackConst*
valueB"        *
dtype0
M
strided_slice_21/stack_1Const*
valueB"       *
dtype0
M
strided_slice_21/stack_2Const*
valueB"      *
dtype0
ī
strided_slice_21StridedSlicemul_17strided_slice_21/stackstrided_slice_21/stack_1strided_slice_21/stack_2*
T0*
Index0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
K
strided_slice_22/stackConst*
valueB"       *
dtype0
M
strided_slice_22/stack_1Const*
valueB"       *
dtype0
M
strided_slice_22/stack_2Const*
valueB"      *
dtype0
ī
strided_slice_22StridedSlicemul_17strided_slice_22/stackstrided_slice_22/stack_1strided_slice_22/stack_2*
T0*
Index0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
K
strided_slice_23/stackConst*
valueB"       *
dtype0
M
strided_slice_23/stack_1Const*
valueB"       *
dtype0
M
strided_slice_23/stack_2Const*
valueB"      *
dtype0
ī
strided_slice_23StridedSlicemul_17strided_slice_23/stackstrided_slice_23/stack_1strided_slice_23/stack_2*
T0*
Index0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
B
Sum_18/reduction_indicesConst*
value	B :*
dtype0
_
Sum_18Sumstrided_slice_18Sum_18/reduction_indices*

Tidx0*
	keep_dims(*
T0
B
Sum_19/reduction_indicesConst*
value	B :*
dtype0
_
Sum_19Sumstrided_slice_19Sum_19/reduction_indices*

Tidx0*
	keep_dims(*
T0
B
Sum_20/reduction_indicesConst*
value	B :*
dtype0
_
Sum_20Sumstrided_slice_20Sum_20/reduction_indices*

Tidx0*
	keep_dims(*
T0
B
Sum_21/reduction_indicesConst*
value	B :*
dtype0
_
Sum_21Sumstrided_slice_21Sum_21/reduction_indices*

Tidx0*
	keep_dims(*
T0
B
Sum_22/reduction_indicesConst*
value	B :*
dtype0
_
Sum_22Sumstrided_slice_22Sum_22/reduction_indices*

Tidx0*
	keep_dims(*
T0
B
Sum_23/reduction_indicesConst*
value	B :*
dtype0
_
Sum_23Sumstrided_slice_23Sum_23/reduction_indices*

Tidx0*
	keep_dims(*
T0
J
Mean_8/inputPackSum_18Sum_19Sum_20*
T0*

axis *
N
B
Mean_8/reduction_indicesConst*
value	B : *
dtype0
\
Mean_8MeanMean_8/inputMean_8/reduction_indices*

Tidx0*
	keep_dims( *
T0
J
Mean_9/inputPackSum_21Sum_22Sum_23*
T0*

axis *
N
B
Mean_9/reduction_indicesConst*
value	B : *
dtype0
\
Mean_9MeanMean_9/inputMean_9/reduction_indices*

Tidx0*
	keep_dims( *
T0
?
	ToFloat_2CastCast*

SrcT0*
Truncate( *

DstT0
I
SquaredDifference_2SquaredDifferenceStopGradient_1Mean_8*
T0
6
mul_18Mul	ToFloat_2SquaredDifference_2*
T0
<
Const_2Const*
valueB"       *
dtype0
F
Mean_10Meanmul_18Const_2*

Tidx0*
	keep_dims( *
T0
5
mul_19/xConst*
valueB
 *   ?*
dtype0
)
mul_19Mulmul_19/xMean_10*
T0
?
	ToFloat_3CastCast*

SrcT0*
Truncate( *

DstT0
I
SquaredDifference_3SquaredDifferenceStopGradient_1Mean_9*
T0
6
mul_20Mul	ToFloat_3SquaredDifference_3*
T0
<
Const_3Const*
valueB"       *
dtype0
F
Mean_11Meanmul_20Const_3*

Tidx0*
	keep_dims( *
T0
5
mul_21/xConst*
valueB
 *   ?*
dtype0
)
mul_21Mulmul_21/xMean_11*
T0
A
Rank/packedPackmul_10mul_19*
T0*

axis *
N
.
RankConst*
value	B :*
dtype0
5
range/startConst*
value	B : *
dtype0
5
range/deltaConst*
value	B :*
dtype0
:
rangeRangerange/startRankrange/delta*

Tidx0
C
Mean_12/inputPackmul_10mul_19*
T0*

axis *
N
K
Mean_12MeanMean_12/inputrange*

Tidx0*
	keep_dims( *
T0
C
Rank_1/packedPackmul_12mul_21*
T0*

axis *
N
0
Rank_1Const*
value	B :*
dtype0
7
range_1/startConst*
value	B : *
dtype0
7
range_1/deltaConst*
value	B :*
dtype0
B
range_1Rangerange_1/startRank_1range_1/delta*

Tidx0
C
Mean_13/inputPackmul_12mul_21*
T0*

axis *
N
M
Mean_13MeanMean_13/inputrange_1*

Tidx0*
	keep_dims( *
T0
@
Const_4Const*!
valueB"]Ą]Ą]Ą*
dtype0
S
log_ent_coef/initial_valueConst*!
valueB"]Ą]Ą]Ą*
dtype0
\
log_ent_coef
VariableV2*
shape:*
shared_name *
dtype0*
	container 

log_ent_coef/AssignAssignlog_ent_coeflog_ent_coef/initial_value*
use_locking(*
T0*
_class
loc:@log_ent_coef*
validate_shape(
U
log_ent_coef/readIdentitylog_ent_coef*
T0*
_class
loc:@log_ent_coef
&
ExpExplog_ent_coef/read*
T0
K
strided_slice_24/stackConst*
valueB"        *
dtype0
M
strided_slice_24/stack_1Const*
valueB"       *
dtype0
M
strided_slice_24/stack_2Const*
valueB"      *
dtype0
ö
strided_slice_24StridedSlicepolicy_1/mul_3strided_slice_24/stackstrided_slice_24/stack_1strided_slice_24/stack_2*
T0*
Index0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
K
strided_slice_25/stackConst*
valueB"       *
dtype0
M
strided_slice_25/stack_1Const*
valueB"       *
dtype0
M
strided_slice_25/stack_2Const*
valueB"      *
dtype0
ö
strided_slice_25StridedSlicepolicy_1/mul_3strided_slice_25/stackstrided_slice_25/stack_1strided_slice_25/stack_2*
T0*
Index0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
K
strided_slice_26/stackConst*
valueB"       *
dtype0
M
strided_slice_26/stack_1Const*
valueB"       *
dtype0
M
strided_slice_26/stack_2Const*
valueB"      *
dtype0
ö
strided_slice_26StridedSlicepolicy_1/mul_3strided_slice_26/stackstrided_slice_26/stack_1strided_slice_26/stack_2*
T0*
Index0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
B
Sum_24/reduction_indicesConst*
value	B :*
dtype0
_
Sum_24Sumstrided_slice_24Sum_24/reduction_indices*

Tidx0*
	keep_dims(*
T0
4
add_5/yConst*
valueB
 *ķž`>*
dtype0
(
add_5AddV2Sum_24add_5/y*
T0
B
Sum_25/reduction_indicesConst*
value	B :*
dtype0
_
Sum_25Sumstrided_slice_25Sum_25/reduction_indices*

Tidx0*
	keep_dims(*
T0
4
add_6/yConst*
valueB
 *ķž`>*
dtype0
(
add_6AddV2Sum_25add_6/y*
T0
B
Sum_26/reduction_indicesConst*
value	B :*
dtype0
_
Sum_26Sumstrided_slice_26Sum_26/reduction_indices*

Tidx0*
	keep_dims(*
T0
4
add_7/yConst*
valueB
 *ąō>*
dtype0
(
add_7AddV2Sum_26add_7/y*
T0
B
stack_4Packadd_5add_6add_7*
T0*

axis*
N
?
	ToFloat_4CastCast*

SrcT0*
Truncate( *

DstT0
0
StopGradient_2StopGradientstack_4*
T0
B
SqueezeSqueezeStopGradient_2*
squeeze_dims
*
T0
2
mul_22Mullog_ent_coef/readSqueeze*
T0
C
Mean_14/reduction_indicesConst*
value	B :*
dtype0
X
Mean_14Meanmul_22Mean_14/reduction_indices*

Tidx0*
	keep_dims( *
T0
*
mul_23Mul	ToFloat_4Mean_14*
T0
5
Const_5Const*
valueB: *
dtype0
F
Mean_15Meanmul_23Const_5*

Tidx0*
	keep_dims( *
T0

NegNegMean_15*
T0
F
mul_24Mulpolicy_1/concat_1critic/q/q1_encoding_1/Mean*
T0
K
strided_slice_27/stackConst*
valueB"        *
dtype0
M
strided_slice_27/stack_1Const*
valueB"       *
dtype0
M
strided_slice_27/stack_2Const*
valueB"      *
dtype0
ī
strided_slice_27StridedSlicemul_24strided_slice_27/stackstrided_slice_27/stack_1strided_slice_27/stack_2*
T0*
Index0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
K
strided_slice_28/stackConst*
valueB"       *
dtype0
M
strided_slice_28/stack_1Const*
valueB"       *
dtype0
M
strided_slice_28/stack_2Const*
valueB"      *
dtype0
ī
strided_slice_28StridedSlicemul_24strided_slice_28/stackstrided_slice_28/stack_1strided_slice_28/stack_2*
T0*
Index0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
K
strided_slice_29/stackConst*
valueB"       *
dtype0
M
strided_slice_29/stack_1Const*
valueB"       *
dtype0
M
strided_slice_29/stack_2Const*
valueB"      *
dtype0
ī
strided_slice_29StridedSlicemul_24strided_slice_29/stackstrided_slice_29/stack_1strided_slice_29/stack_2*
T0*
Index0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
D
strided_slice_30/stackConst*
valueB: *
dtype0
F
strided_slice_30/stack_1Const*
valueB:*
dtype0
F
strided_slice_30/stack_2Const*
valueB:*
dtype0
ė
strided_slice_30StridedSliceExpstrided_slice_30/stackstrided_slice_30/stack_1strided_slice_30/stack_2*
T0*
Index0*
shrink_axis_mask*

begin_mask *
ellipsis_mask *
new_axis_mask *
end_mask 
:
mul_25Mulstrided_slice_30strided_slice_24*
T0
/
sub_2Submul_25strided_slice_27*
T0
B
Sum_27/reduction_indicesConst*
value	B :*
dtype0
T
Sum_27Sumsub_2Sum_27/reduction_indices*

Tidx0*
	keep_dims(*
T0
D
strided_slice_31/stackConst*
valueB:*
dtype0
F
strided_slice_31/stack_1Const*
valueB:*
dtype0
F
strided_slice_31/stack_2Const*
valueB:*
dtype0
ė
strided_slice_31StridedSliceExpstrided_slice_31/stackstrided_slice_31/stack_1strided_slice_31/stack_2*
T0*
Index0*
shrink_axis_mask*

begin_mask *
ellipsis_mask *
new_axis_mask *
end_mask 
:
mul_26Mulstrided_slice_31strided_slice_25*
T0
/
sub_3Submul_26strided_slice_28*
T0
B
Sum_28/reduction_indicesConst*
value	B :*
dtype0
T
Sum_28Sumsub_3Sum_28/reduction_indices*

Tidx0*
	keep_dims(*
T0
D
strided_slice_32/stackConst*
valueB:*
dtype0
F
strided_slice_32/stack_1Const*
valueB:*
dtype0
F
strided_slice_32/stack_2Const*
valueB:*
dtype0
ė
strided_slice_32StridedSliceExpstrided_slice_32/stackstrided_slice_32/stack_1strided_slice_32/stack_2*
T0*
Index0*
shrink_axis_mask*

begin_mask *
ellipsis_mask *
new_axis_mask *
end_mask 
:
mul_27Mulstrided_slice_32strided_slice_26*
T0
/
sub_4Submul_27strided_slice_29*
T0
B
Sum_29/reduction_indicesConst*
value	B :*
dtype0
T
Sum_29Sumsub_4Sum_29/reduction_indices*

Tidx0*
	keep_dims(*
T0
E
stack_5PackSum_27Sum_28Sum_29*
T0*

axis *
N
?
	ToFloat_5CastCast*

SrcT0*
Truncate( *

DstT0
:
	Squeeze_1Squeezestack_5*
squeeze_dims
 *
T0
,
mul_28Mul	ToFloat_5	Squeeze_1*
T0

Rank_2Rankmul_28*
T0
7
range_2/startConst*
value	B : *
dtype0
7
range_2/deltaConst*
value	B :*
dtype0
B
range_2Rangerange_2/startRank_2range_2/delta*

Tidx0
F
Mean_16Meanmul_28range_2*

Tidx0*
	keep_dims( *
T0
D
strided_slice_33/stackConst*
valueB: *
dtype0
F
strided_slice_33/stack_1Const*
valueB:*
dtype0
F
strided_slice_33/stack_2Const*
valueB:*
dtype0
ė
strided_slice_33StridedSliceExpstrided_slice_33/stackstrided_slice_33/stack_1strided_slice_33/stack_2*
T0*
Index0*
shrink_axis_mask*

begin_mask *
ellipsis_mask *
new_axis_mask *
end_mask 
:
mul_29Mulstrided_slice_33strided_slice_24*
T0
B
Sum_30/reduction_indicesConst*
value	B :*
dtype0
U
Sum_30Summul_29Sum_30/reduction_indices*

Tidx0*
	keep_dims(*
T0
D
strided_slice_34/stackConst*
valueB:*
dtype0
F
strided_slice_34/stack_1Const*
valueB:*
dtype0
F
strided_slice_34/stack_2Const*
valueB:*
dtype0
ė
strided_slice_34StridedSliceExpstrided_slice_34/stackstrided_slice_34/stack_1strided_slice_34/stack_2*
T0*
Index0*
shrink_axis_mask*

begin_mask *
ellipsis_mask *
new_axis_mask *
end_mask 
:
mul_30Mulstrided_slice_34strided_slice_25*
T0
B
Sum_31/reduction_indicesConst*
value	B :*
dtype0
U
Sum_31Summul_30Sum_31/reduction_indices*

Tidx0*
	keep_dims(*
T0
D
strided_slice_35/stackConst*
valueB:*
dtype0
F
strided_slice_35/stack_1Const*
valueB:*
dtype0
F
strided_slice_35/stack_2Const*
valueB:*
dtype0
ė
strided_slice_35StridedSliceExpstrided_slice_35/stackstrided_slice_35/stack_1strided_slice_35/stack_2*
T0*
Index0*
shrink_axis_mask*

begin_mask *
ellipsis_mask *
new_axis_mask *
end_mask 
:
mul_31Mulstrided_slice_35strided_slice_26*
T0
B
Sum_32/reduction_indicesConst*
value	B :*
dtype0
U
Sum_32Summul_31Sum_32/reduction_indices*

Tidx0*
	keep_dims(*
T0
E
stack_6PackSum_30Sum_31Sum_32*
T0*

axis *
N
C
Mean_17/reduction_indicesConst*
value	B : *
dtype0
Y
Mean_17Meanstack_6Mean_17/reduction_indices*

Tidx0*
	keep_dims( *
T0
'
sub_5SubMinimumMean_17*
T0
.
StopGradient_3StopGradientsub_5*
T0
?
	ToFloat_6CastCast*

SrcT0*
Truncate( *

DstT0
g
SquaredDifference_4SquaredDifference$critic/value/extrinsic_value/BiasAddStopGradient_3*
T0
6
mul_32Mul	ToFloat_6SquaredDifference_4*
T0
<
Const_6Const*
valueB"       *
dtype0
F
Mean_18Meanmul_32Const_6*

Tidx0*
	keep_dims( *
T0
5
mul_33/xConst*
valueB
 *   ?*
dtype0
)
mul_33Mulmul_33/xMean_18*
T0
C
Mean_19/reduction_indicesConst*
value	B : *
dtype0
Y
Mean_19Meanstack_6Mean_19/reduction_indices*

Tidx0*
	keep_dims( *
T0
)
sub_6Sub	Minimum_1Mean_19*
T0
.
StopGradient_4StopGradientsub_6*
T0
?
	ToFloat_7CastCast*

SrcT0*
Truncate( *

DstT0
b
SquaredDifference_5SquaredDifferencecritic/value/gail_value/BiasAddStopGradient_4*
T0
6
mul_34Mul	ToFloat_7SquaredDifference_5*
T0
<
Const_7Const*
valueB"       *
dtype0
F
Mean_20Meanmul_34Const_7*

Tidx0*
	keep_dims( *
T0
5
mul_35/xConst*
valueB
 *   ?*
dtype0
)
mul_35Mulmul_35/xMean_20*
T0
C
Rank_3/packedPackmul_33mul_35*
T0*

axis *
N
0
Rank_3Const*
value	B :*
dtype0
7
range_3/startConst*
value	B : *
dtype0
7
range_3/deltaConst*
value	B :*
dtype0
B
range_3Rangerange_3/startRank_3range_3/delta*

Tidx0
C
Mean_21/inputPackmul_33mul_35*
T0*

axis *
N
M
Mean_21MeanMean_21/inputrange_3*

Tidx0*
	keep_dims( *
T0
)
add_8AddV2Mean_12Mean_13*
T0
'
add_9AddV2add_8Mean_21*
T0
5
mul_36/xConst*
valueB
 *¤p}?*
dtype0
Z
mul_36Mulmul_36/x8target_network/critic/value/encoder/hidden_0/kernel/read*
T0
5
mul_37/xConst*
valueB
 *
×#<*
dtype0
K
mul_37Mulmul_37/x)critic/value/encoder/hidden_0/kernel/read*
T0
(
add_10AddV2mul_36mul_37*
T0
É
Assign_5Assign3target_network/critic/value/encoder/hidden_0/kerneladd_10*
use_locking(*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_0/kernel*
validate_shape(
5
mul_38/xConst*
valueB
 *¤p}?*
dtype0
X
mul_38Mulmul_38/x6target_network/critic/value/encoder/hidden_0/bias/read*
T0
5
mul_39/xConst*
valueB
 *
×#<*
dtype0
I
mul_39Mulmul_39/x'critic/value/encoder/hidden_0/bias/read*
T0
(
add_11AddV2mul_38mul_39*
T0
Å
Assign_6Assign1target_network/critic/value/encoder/hidden_0/biasadd_11*
use_locking(*
T0*D
_class:
86loc:@target_network/critic/value/encoder/hidden_0/bias*
validate_shape(
5
mul_40/xConst*
valueB
 *¤p}?*
dtype0
Z
mul_40Mulmul_40/x8target_network/critic/value/encoder/hidden_1/kernel/read*
T0
5
mul_41/xConst*
valueB
 *
×#<*
dtype0
K
mul_41Mulmul_41/x)critic/value/encoder/hidden_1/kernel/read*
T0
(
add_12AddV2mul_40mul_41*
T0
É
Assign_7Assign3target_network/critic/value/encoder/hidden_1/kerneladd_12*
use_locking(*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_1/kernel*
validate_shape(
5
mul_42/xConst*
valueB
 *¤p}?*
dtype0
X
mul_42Mulmul_42/x6target_network/critic/value/encoder/hidden_1/bias/read*
T0
5
mul_43/xConst*
valueB
 *
×#<*
dtype0
I
mul_43Mulmul_43/x'critic/value/encoder/hidden_1/bias/read*
T0
(
add_13AddV2mul_42mul_43*
T0
Å
Assign_8Assign1target_network/critic/value/encoder/hidden_1/biasadd_13*
use_locking(*
T0*D
_class:
86loc:@target_network/critic/value/encoder/hidden_1/bias*
validate_shape(
5
mul_44/xConst*
valueB
 *¤p}?*
dtype0
Y
mul_44Mulmul_44/x7target_network/critic/value/extrinsic_value/kernel/read*
T0
5
mul_45/xConst*
valueB
 *
×#<*
dtype0
J
mul_45Mulmul_45/x(critic/value/extrinsic_value/kernel/read*
T0
(
add_14AddV2mul_44mul_45*
T0
Ē
Assign_9Assign2target_network/critic/value/extrinsic_value/kerneladd_14*
use_locking(*
T0*E
_class;
97loc:@target_network/critic/value/extrinsic_value/kernel*
validate_shape(
5
mul_46/xConst*
valueB
 *¤p}?*
dtype0
W
mul_46Mulmul_46/x5target_network/critic/value/extrinsic_value/bias/read*
T0
5
mul_47/xConst*
valueB
 *
×#<*
dtype0
H
mul_47Mulmul_47/x&critic/value/extrinsic_value/bias/read*
T0
(
add_15AddV2mul_46mul_47*
T0
Ä
	Assign_10Assign0target_network/critic/value/extrinsic_value/biasadd_15*
use_locking(*
T0*C
_class9
75loc:@target_network/critic/value/extrinsic_value/bias*
validate_shape(
5
mul_48/xConst*
valueB
 *¤p}?*
dtype0
T
mul_48Mulmul_48/x2target_network/critic/value/gail_value/kernel/read*
T0
5
mul_49/xConst*
valueB
 *
×#<*
dtype0
E
mul_49Mulmul_49/x#critic/value/gail_value/kernel/read*
T0
(
add_16AddV2mul_48mul_49*
T0
¾
	Assign_11Assign-target_network/critic/value/gail_value/kerneladd_16*
use_locking(*
T0*@
_class6
42loc:@target_network/critic/value/gail_value/kernel*
validate_shape(
5
mul_50/xConst*
valueB
 *¤p}?*
dtype0
R
mul_50Mulmul_50/x0target_network/critic/value/gail_value/bias/read*
T0
5
mul_51/xConst*
valueB
 *
×#<*
dtype0
C
mul_51Mulmul_51/x!critic/value/gail_value/bias/read*
T0
(
add_17AddV2mul_50mul_51*
T0
ŗ
	Assign_12Assign+target_network/critic/value/gail_value/biasadd_17*
use_locking(*
T0*>
_class4
20loc:@target_network/critic/value/gail_value/bias*
validate_shape(
ķ
	Assign_13Assign3target_network/critic/value/encoder/hidden_0/kernel)critic/value/encoder/hidden_0/kernel/read*
use_locking(*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_0/kernel*
validate_shape(
ē
	Assign_14Assign1target_network/critic/value/encoder/hidden_0/bias'critic/value/encoder/hidden_0/bias/read*
use_locking(*
T0*D
_class:
86loc:@target_network/critic/value/encoder/hidden_0/bias*
validate_shape(
ķ
	Assign_15Assign3target_network/critic/value/encoder/hidden_1/kernel)critic/value/encoder/hidden_1/kernel/read*
use_locking(*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_1/kernel*
validate_shape(
ē
	Assign_16Assign1target_network/critic/value/encoder/hidden_1/bias'critic/value/encoder/hidden_1/bias/read*
use_locking(*
T0*D
_class:
86loc:@target_network/critic/value/encoder/hidden_1/bias*
validate_shape(
ź
	Assign_17Assign2target_network/critic/value/extrinsic_value/kernel(critic/value/extrinsic_value/kernel/read*
use_locking(*
T0*E
_class;
97loc:@target_network/critic/value/extrinsic_value/kernel*
validate_shape(
ä
	Assign_18Assign0target_network/critic/value/extrinsic_value/bias&critic/value/extrinsic_value/bias/read*
use_locking(*
T0*C
_class9
75loc:@target_network/critic/value/extrinsic_value/bias*
validate_shape(
Ū
	Assign_19Assign-target_network/critic/value/gail_value/kernel#critic/value/gail_value/kernel/read*
use_locking(*
T0*@
_class6
42loc:@target_network/critic/value/gail_value/kernel*
validate_shape(
Õ
	Assign_20Assign+target_network/critic/value/gail_value/bias!critic/value/gail_value/bias/read*
use_locking(*
T0*>
_class4
20loc:@target_network/critic/value/gail_value/bias*
validate_shape(
8
gradients/ShapeConst*
valueB *
dtype0
@
gradients/grad_ys_0Const*
valueB
 *  ?*
dtype0
W
gradients/FillFillgradients/Shapegradients/grad_ys_0*
T0*

index_type0
F
gradients/Mean_16_grad/ShapeShapemul_28*
T0*
out_type0

gradients/Mean_16_grad/SizeSizegradients/Mean_16_grad/Shape*
T0*/
_class%
#!loc:@gradients/Mean_16_grad/Shape*
out_type0

gradients/Mean_16_grad/addAddV2range_2gradients/Mean_16_grad/Size*
T0*/
_class%
#!loc:@gradients/Mean_16_grad/Shape

gradients/Mean_16_grad/modFloorModgradients/Mean_16_grad/addgradients/Mean_16_grad/Size*
T0*/
_class%
#!loc:@gradients/Mean_16_grad/Shape

gradients/Mean_16_grad/Shape_1Shapegradients/Mean_16_grad/mod*
T0*/
_class%
#!loc:@gradients/Mean_16_grad/Shape*
out_type0
}
"gradients/Mean_16_grad/range/startConst*/
_class%
#!loc:@gradients/Mean_16_grad/Shape*
value	B : *
dtype0
}
"gradients/Mean_16_grad/range/deltaConst*/
_class%
#!loc:@gradients/Mean_16_grad/Shape*
value	B :*
dtype0
Ē
gradients/Mean_16_grad/rangeRange"gradients/Mean_16_grad/range/startgradients/Mean_16_grad/Size"gradients/Mean_16_grad/range/delta*

Tidx0*/
_class%
#!loc:@gradients/Mean_16_grad/Shape
|
!gradients/Mean_16_grad/Fill/valueConst*/
_class%
#!loc:@gradients/Mean_16_grad/Shape*
value	B :*
dtype0
²
gradients/Mean_16_grad/FillFillgradients/Mean_16_grad/Shape_1!gradients/Mean_16_grad/Fill/value*
T0*/
_class%
#!loc:@gradients/Mean_16_grad/Shape*

index_type0
ķ
$gradients/Mean_16_grad/DynamicStitchDynamicStitchgradients/Mean_16_grad/rangegradients/Mean_16_grad/modgradients/Mean_16_grad/Shapegradients/Mean_16_grad/Fill*
T0*/
_class%
#!loc:@gradients/Mean_16_grad/Shape*
N
{
 gradients/Mean_16_grad/Maximum/yConst*/
_class%
#!loc:@gradients/Mean_16_grad/Shape*
value	B :*
dtype0
«
gradients/Mean_16_grad/MaximumMaximum$gradients/Mean_16_grad/DynamicStitch gradients/Mean_16_grad/Maximum/y*
T0*/
_class%
#!loc:@gradients/Mean_16_grad/Shape
£
gradients/Mean_16_grad/floordivFloorDivgradients/Mean_16_grad/Shapegradients/Mean_16_grad/Maximum*
T0*/
_class%
#!loc:@gradients/Mean_16_grad/Shape
v
gradients/Mean_16_grad/ReshapeReshapegradients/Fill$gradients/Mean_16_grad/DynamicStitch*
T0*
Tshape0

gradients/Mean_16_grad/TileTilegradients/Mean_16_grad/Reshapegradients/Mean_16_grad/floordiv*

Tmultiples0*
T0
H
gradients/Mean_16_grad/Shape_2Shapemul_28*
T0*
out_type0
G
gradients/Mean_16_grad/Shape_3Const*
valueB *
dtype0
J
gradients/Mean_16_grad/ConstConst*
valueB: *
dtype0

gradients/Mean_16_grad/ProdProdgradients/Mean_16_grad/Shape_2gradients/Mean_16_grad/Const*

Tidx0*
	keep_dims( *
T0
L
gradients/Mean_16_grad/Const_1Const*
valueB: *
dtype0

gradients/Mean_16_grad/Prod_1Prodgradients/Mean_16_grad/Shape_3gradients/Mean_16_grad/Const_1*

Tidx0*
	keep_dims( *
T0
L
"gradients/Mean_16_grad/Maximum_1/yConst*
value	B :*
dtype0
w
 gradients/Mean_16_grad/Maximum_1Maximumgradients/Mean_16_grad/Prod_1"gradients/Mean_16_grad/Maximum_1/y*
T0
u
!gradients/Mean_16_grad/floordiv_1FloorDivgradients/Mean_16_grad/Prod gradients/Mean_16_grad/Maximum_1*
T0
n
gradients/Mean_16_grad/CastCast!gradients/Mean_16_grad/floordiv_1*

SrcT0*
Truncate( *

DstT0
l
gradients/Mean_16_grad/truedivRealDivgradients/Mean_16_grad/Tilegradients/Mean_16_grad/Cast*
T0
H
gradients/mul_28_grad/ShapeShape	ToFloat_5*
T0*
out_type0
J
gradients/mul_28_grad/Shape_1Shape	Squeeze_1*
T0*
out_type0

+gradients/mul_28_grad/BroadcastGradientArgsBroadcastGradientArgsgradients/mul_28_grad/Shapegradients/mul_28_grad/Shape_1*
T0
T
gradients/mul_28_grad/MulMulgradients/Mean_16_grad/truediv	Squeeze_1*
T0

gradients/mul_28_grad/SumSumgradients/mul_28_grad/Mul+gradients/mul_28_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0
w
gradients/mul_28_grad/ReshapeReshapegradients/mul_28_grad/Sumgradients/mul_28_grad/Shape*
T0*
Tshape0
V
gradients/mul_28_grad/Mul_1Mul	ToFloat_5gradients/Mean_16_grad/truediv*
T0

gradients/mul_28_grad/Sum_1Sumgradients/mul_28_grad/Mul_1-gradients/mul_28_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0
}
gradients/mul_28_grad/Reshape_1Reshapegradients/mul_28_grad/Sum_1gradients/mul_28_grad/Shape_1*
T0*
Tshape0
p
&gradients/mul_28_grad/tuple/group_depsNoOp^gradients/mul_28_grad/Reshape ^gradients/mul_28_grad/Reshape_1
½
.gradients/mul_28_grad/tuple/control_dependencyIdentitygradients/mul_28_grad/Reshape'^gradients/mul_28_grad/tuple/group_deps*
T0*0
_class&
$"loc:@gradients/mul_28_grad/Reshape
Ć
0gradients/mul_28_grad/tuple/control_dependency_1Identitygradients/mul_28_grad/Reshape_1'^gradients/mul_28_grad/tuple/group_deps*
T0*2
_class(
&$loc:@gradients/mul_28_grad/Reshape_1
I
gradients/Squeeze_1_grad/ShapeShapestack_5*
T0*
out_type0

 gradients/Squeeze_1_grad/ReshapeReshape0gradients/mul_28_grad/tuple/control_dependency_1gradients/Squeeze_1_grad/Shape*
T0*
Tshape0
j
gradients/stack_5_grad/unstackUnpack gradients/Squeeze_1_grad/Reshape*
T0*	
num*

axis 
P
'gradients/stack_5_grad/tuple/group_depsNoOp^gradients/stack_5_grad/unstack
Į
/gradients/stack_5_grad/tuple/control_dependencyIdentitygradients/stack_5_grad/unstack(^gradients/stack_5_grad/tuple/group_deps*
T0*1
_class'
%#loc:@gradients/stack_5_grad/unstack
Å
1gradients/stack_5_grad/tuple/control_dependency_1Identity gradients/stack_5_grad/unstack:1(^gradients/stack_5_grad/tuple/group_deps*
T0*1
_class'
%#loc:@gradients/stack_5_grad/unstack
Å
1gradients/stack_5_grad/tuple/control_dependency_2Identity gradients/stack_5_grad/unstack:2(^gradients/stack_5_grad/tuple/group_deps*
T0*1
_class'
%#loc:@gradients/stack_5_grad/unstack
D
gradients/Sum_27_grad/ShapeShapesub_2*
T0*
out_type0
t
gradients/Sum_27_grad/SizeConst*.
_class$
" loc:@gradients/Sum_27_grad/Shape*
value	B :*
dtype0

gradients/Sum_27_grad/addAddV2Sum_27/reduction_indicesgradients/Sum_27_grad/Size*
T0*.
_class$
" loc:@gradients/Sum_27_grad/Shape

gradients/Sum_27_grad/modFloorModgradients/Sum_27_grad/addgradients/Sum_27_grad/Size*
T0*.
_class$
" loc:@gradients/Sum_27_grad/Shape
v
gradients/Sum_27_grad/Shape_1Const*.
_class$
" loc:@gradients/Sum_27_grad/Shape*
valueB *
dtype0
{
!gradients/Sum_27_grad/range/startConst*.
_class$
" loc:@gradients/Sum_27_grad/Shape*
value	B : *
dtype0
{
!gradients/Sum_27_grad/range/deltaConst*.
_class$
" loc:@gradients/Sum_27_grad/Shape*
value	B :*
dtype0
Ā
gradients/Sum_27_grad/rangeRange!gradients/Sum_27_grad/range/startgradients/Sum_27_grad/Size!gradients/Sum_27_grad/range/delta*

Tidx0*.
_class$
" loc:@gradients/Sum_27_grad/Shape
z
 gradients/Sum_27_grad/Fill/valueConst*.
_class$
" loc:@gradients/Sum_27_grad/Shape*
value	B :*
dtype0
®
gradients/Sum_27_grad/FillFillgradients/Sum_27_grad/Shape_1 gradients/Sum_27_grad/Fill/value*
T0*.
_class$
" loc:@gradients/Sum_27_grad/Shape*

index_type0
ē
#gradients/Sum_27_grad/DynamicStitchDynamicStitchgradients/Sum_27_grad/rangegradients/Sum_27_grad/modgradients/Sum_27_grad/Shapegradients/Sum_27_grad/Fill*
T0*.
_class$
" loc:@gradients/Sum_27_grad/Shape*
N
y
gradients/Sum_27_grad/Maximum/yConst*.
_class$
" loc:@gradients/Sum_27_grad/Shape*
value	B :*
dtype0
§
gradients/Sum_27_grad/MaximumMaximum#gradients/Sum_27_grad/DynamicStitchgradients/Sum_27_grad/Maximum/y*
T0*.
_class$
" loc:@gradients/Sum_27_grad/Shape

gradients/Sum_27_grad/floordivFloorDivgradients/Sum_27_grad/Shapegradients/Sum_27_grad/Maximum*
T0*.
_class$
" loc:@gradients/Sum_27_grad/Shape

gradients/Sum_27_grad/ReshapeReshape/gradients/stack_5_grad/tuple/control_dependency#gradients/Sum_27_grad/DynamicStitch*
T0*
Tshape0
|
gradients/Sum_27_grad/TileTilegradients/Sum_27_grad/Reshapegradients/Sum_27_grad/floordiv*

Tmultiples0*
T0
D
gradients/Sum_28_grad/ShapeShapesub_3*
T0*
out_type0
t
gradients/Sum_28_grad/SizeConst*.
_class$
" loc:@gradients/Sum_28_grad/Shape*
value	B :*
dtype0

gradients/Sum_28_grad/addAddV2Sum_28/reduction_indicesgradients/Sum_28_grad/Size*
T0*.
_class$
" loc:@gradients/Sum_28_grad/Shape

gradients/Sum_28_grad/modFloorModgradients/Sum_28_grad/addgradients/Sum_28_grad/Size*
T0*.
_class$
" loc:@gradients/Sum_28_grad/Shape
v
gradients/Sum_28_grad/Shape_1Const*.
_class$
" loc:@gradients/Sum_28_grad/Shape*
valueB *
dtype0
{
!gradients/Sum_28_grad/range/startConst*.
_class$
" loc:@gradients/Sum_28_grad/Shape*
value	B : *
dtype0
{
!gradients/Sum_28_grad/range/deltaConst*.
_class$
" loc:@gradients/Sum_28_grad/Shape*
value	B :*
dtype0
Ā
gradients/Sum_28_grad/rangeRange!gradients/Sum_28_grad/range/startgradients/Sum_28_grad/Size!gradients/Sum_28_grad/range/delta*

Tidx0*.
_class$
" loc:@gradients/Sum_28_grad/Shape
z
 gradients/Sum_28_grad/Fill/valueConst*.
_class$
" loc:@gradients/Sum_28_grad/Shape*
value	B :*
dtype0
®
gradients/Sum_28_grad/FillFillgradients/Sum_28_grad/Shape_1 gradients/Sum_28_grad/Fill/value*
T0*.
_class$
" loc:@gradients/Sum_28_grad/Shape*

index_type0
ē
#gradients/Sum_28_grad/DynamicStitchDynamicStitchgradients/Sum_28_grad/rangegradients/Sum_28_grad/modgradients/Sum_28_grad/Shapegradients/Sum_28_grad/Fill*
T0*.
_class$
" loc:@gradients/Sum_28_grad/Shape*
N
y
gradients/Sum_28_grad/Maximum/yConst*.
_class$
" loc:@gradients/Sum_28_grad/Shape*
value	B :*
dtype0
§
gradients/Sum_28_grad/MaximumMaximum#gradients/Sum_28_grad/DynamicStitchgradients/Sum_28_grad/Maximum/y*
T0*.
_class$
" loc:@gradients/Sum_28_grad/Shape

gradients/Sum_28_grad/floordivFloorDivgradients/Sum_28_grad/Shapegradients/Sum_28_grad/Maximum*
T0*.
_class$
" loc:@gradients/Sum_28_grad/Shape

gradients/Sum_28_grad/ReshapeReshape1gradients/stack_5_grad/tuple/control_dependency_1#gradients/Sum_28_grad/DynamicStitch*
T0*
Tshape0
|
gradients/Sum_28_grad/TileTilegradients/Sum_28_grad/Reshapegradients/Sum_28_grad/floordiv*

Tmultiples0*
T0
D
gradients/Sum_29_grad/ShapeShapesub_4*
T0*
out_type0
t
gradients/Sum_29_grad/SizeConst*.
_class$
" loc:@gradients/Sum_29_grad/Shape*
value	B :*
dtype0

gradients/Sum_29_grad/addAddV2Sum_29/reduction_indicesgradients/Sum_29_grad/Size*
T0*.
_class$
" loc:@gradients/Sum_29_grad/Shape

gradients/Sum_29_grad/modFloorModgradients/Sum_29_grad/addgradients/Sum_29_grad/Size*
T0*.
_class$
" loc:@gradients/Sum_29_grad/Shape
v
gradients/Sum_29_grad/Shape_1Const*.
_class$
" loc:@gradients/Sum_29_grad/Shape*
valueB *
dtype0
{
!gradients/Sum_29_grad/range/startConst*.
_class$
" loc:@gradients/Sum_29_grad/Shape*
value	B : *
dtype0
{
!gradients/Sum_29_grad/range/deltaConst*.
_class$
" loc:@gradients/Sum_29_grad/Shape*
value	B :*
dtype0
Ā
gradients/Sum_29_grad/rangeRange!gradients/Sum_29_grad/range/startgradients/Sum_29_grad/Size!gradients/Sum_29_grad/range/delta*

Tidx0*.
_class$
" loc:@gradients/Sum_29_grad/Shape
z
 gradients/Sum_29_grad/Fill/valueConst*.
_class$
" loc:@gradients/Sum_29_grad/Shape*
value	B :*
dtype0
®
gradients/Sum_29_grad/FillFillgradients/Sum_29_grad/Shape_1 gradients/Sum_29_grad/Fill/value*
T0*.
_class$
" loc:@gradients/Sum_29_grad/Shape*

index_type0
ē
#gradients/Sum_29_grad/DynamicStitchDynamicStitchgradients/Sum_29_grad/rangegradients/Sum_29_grad/modgradients/Sum_29_grad/Shapegradients/Sum_29_grad/Fill*
T0*.
_class$
" loc:@gradients/Sum_29_grad/Shape*
N
y
gradients/Sum_29_grad/Maximum/yConst*.
_class$
" loc:@gradients/Sum_29_grad/Shape*
value	B :*
dtype0
§
gradients/Sum_29_grad/MaximumMaximum#gradients/Sum_29_grad/DynamicStitchgradients/Sum_29_grad/Maximum/y*
T0*.
_class$
" loc:@gradients/Sum_29_grad/Shape

gradients/Sum_29_grad/floordivFloorDivgradients/Sum_29_grad/Shapegradients/Sum_29_grad/Maximum*
T0*.
_class$
" loc:@gradients/Sum_29_grad/Shape

gradients/Sum_29_grad/ReshapeReshape1gradients/stack_5_grad/tuple/control_dependency_2#gradients/Sum_29_grad/DynamicStitch*
T0*
Tshape0
|
gradients/Sum_29_grad/TileTilegradients/Sum_29_grad/Reshapegradients/Sum_29_grad/floordiv*

Tmultiples0*
T0
D
gradients/sub_2_grad/ShapeShapemul_25*
T0*
out_type0
P
gradients/sub_2_grad/Shape_1Shapestrided_slice_27*
T0*
out_type0

*gradients/sub_2_grad/BroadcastGradientArgsBroadcastGradientArgsgradients/sub_2_grad/Shapegradients/sub_2_grad/Shape_1*
T0

gradients/sub_2_grad/SumSumgradients/Sum_27_grad/Tile*gradients/sub_2_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0
t
gradients/sub_2_grad/ReshapeReshapegradients/sub_2_grad/Sumgradients/sub_2_grad/Shape*
T0*
Tshape0
D
gradients/sub_2_grad/NegNeggradients/Sum_27_grad/Tile*
T0

gradients/sub_2_grad/Sum_1Sumgradients/sub_2_grad/Neg,gradients/sub_2_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0
z
gradients/sub_2_grad/Reshape_1Reshapegradients/sub_2_grad/Sum_1gradients/sub_2_grad/Shape_1*
T0*
Tshape0
m
%gradients/sub_2_grad/tuple/group_depsNoOp^gradients/sub_2_grad/Reshape^gradients/sub_2_grad/Reshape_1
¹
-gradients/sub_2_grad/tuple/control_dependencyIdentitygradients/sub_2_grad/Reshape&^gradients/sub_2_grad/tuple/group_deps*
T0*/
_class%
#!loc:@gradients/sub_2_grad/Reshape
æ
/gradients/sub_2_grad/tuple/control_dependency_1Identitygradients/sub_2_grad/Reshape_1&^gradients/sub_2_grad/tuple/group_deps*
T0*1
_class'
%#loc:@gradients/sub_2_grad/Reshape_1
D
gradients/sub_3_grad/ShapeShapemul_26*
T0*
out_type0
P
gradients/sub_3_grad/Shape_1Shapestrided_slice_28*
T0*
out_type0

*gradients/sub_3_grad/BroadcastGradientArgsBroadcastGradientArgsgradients/sub_3_grad/Shapegradients/sub_3_grad/Shape_1*
T0

gradients/sub_3_grad/SumSumgradients/Sum_28_grad/Tile*gradients/sub_3_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0
t
gradients/sub_3_grad/ReshapeReshapegradients/sub_3_grad/Sumgradients/sub_3_grad/Shape*
T0*
Tshape0
D
gradients/sub_3_grad/NegNeggradients/Sum_28_grad/Tile*
T0

gradients/sub_3_grad/Sum_1Sumgradients/sub_3_grad/Neg,gradients/sub_3_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0
z
gradients/sub_3_grad/Reshape_1Reshapegradients/sub_3_grad/Sum_1gradients/sub_3_grad/Shape_1*
T0*
Tshape0
m
%gradients/sub_3_grad/tuple/group_depsNoOp^gradients/sub_3_grad/Reshape^gradients/sub_3_grad/Reshape_1
¹
-gradients/sub_3_grad/tuple/control_dependencyIdentitygradients/sub_3_grad/Reshape&^gradients/sub_3_grad/tuple/group_deps*
T0*/
_class%
#!loc:@gradients/sub_3_grad/Reshape
æ
/gradients/sub_3_grad/tuple/control_dependency_1Identitygradients/sub_3_grad/Reshape_1&^gradients/sub_3_grad/tuple/group_deps*
T0*1
_class'
%#loc:@gradients/sub_3_grad/Reshape_1
D
gradients/sub_4_grad/ShapeShapemul_27*
T0*
out_type0
P
gradients/sub_4_grad/Shape_1Shapestrided_slice_29*
T0*
out_type0

*gradients/sub_4_grad/BroadcastGradientArgsBroadcastGradientArgsgradients/sub_4_grad/Shapegradients/sub_4_grad/Shape_1*
T0

gradients/sub_4_grad/SumSumgradients/Sum_29_grad/Tile*gradients/sub_4_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0
t
gradients/sub_4_grad/ReshapeReshapegradients/sub_4_grad/Sumgradients/sub_4_grad/Shape*
T0*
Tshape0
D
gradients/sub_4_grad/NegNeggradients/Sum_29_grad/Tile*
T0

gradients/sub_4_grad/Sum_1Sumgradients/sub_4_grad/Neg,gradients/sub_4_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0
z
gradients/sub_4_grad/Reshape_1Reshapegradients/sub_4_grad/Sum_1gradients/sub_4_grad/Shape_1*
T0*
Tshape0
m
%gradients/sub_4_grad/tuple/group_depsNoOp^gradients/sub_4_grad/Reshape^gradients/sub_4_grad/Reshape_1
¹
-gradients/sub_4_grad/tuple/control_dependencyIdentitygradients/sub_4_grad/Reshape&^gradients/sub_4_grad/tuple/group_deps*
T0*/
_class%
#!loc:@gradients/sub_4_grad/Reshape
æ
/gradients/sub_4_grad/tuple/control_dependency_1Identitygradients/sub_4_grad/Reshape_1&^gradients/sub_4_grad/tuple/group_deps*
T0*1
_class'
%#loc:@gradients/sub_4_grad/Reshape_1
O
gradients/mul_25_grad/ShapeShapestrided_slice_30*
T0*
out_type0
Q
gradients/mul_25_grad/Shape_1Shapestrided_slice_24*
T0*
out_type0

+gradients/mul_25_grad/BroadcastGradientArgsBroadcastGradientArgsgradients/mul_25_grad/Shapegradients/mul_25_grad/Shape_1*
T0
j
gradients/mul_25_grad/MulMul-gradients/sub_2_grad/tuple/control_dependencystrided_slice_24*
T0

gradients/mul_25_grad/SumSumgradients/mul_25_grad/Mul+gradients/mul_25_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0
w
gradients/mul_25_grad/ReshapeReshapegradients/mul_25_grad/Sumgradients/mul_25_grad/Shape*
T0*
Tshape0
l
gradients/mul_25_grad/Mul_1Mulstrided_slice_30-gradients/sub_2_grad/tuple/control_dependency*
T0

gradients/mul_25_grad/Sum_1Sumgradients/mul_25_grad/Mul_1-gradients/mul_25_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0
}
gradients/mul_25_grad/Reshape_1Reshapegradients/mul_25_grad/Sum_1gradients/mul_25_grad/Shape_1*
T0*
Tshape0
p
&gradients/mul_25_grad/tuple/group_depsNoOp^gradients/mul_25_grad/Reshape ^gradients/mul_25_grad/Reshape_1
½
.gradients/mul_25_grad/tuple/control_dependencyIdentitygradients/mul_25_grad/Reshape'^gradients/mul_25_grad/tuple/group_deps*
T0*0
_class&
$"loc:@gradients/mul_25_grad/Reshape
Ć
0gradients/mul_25_grad/tuple/control_dependency_1Identitygradients/mul_25_grad/Reshape_1'^gradients/mul_25_grad/tuple/group_deps*
T0*2
_class(
&$loc:@gradients/mul_25_grad/Reshape_1
O
%gradients/strided_slice_27_grad/ShapeShapemul_24*
T0*
out_type0
ā
0gradients/strided_slice_27_grad/StridedSliceGradStridedSliceGrad%gradients/strided_slice_27_grad/Shapestrided_slice_27/stackstrided_slice_27/stack_1strided_slice_27/stack_2/gradients/sub_2_grad/tuple/control_dependency_1*
Index0*
T0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
O
gradients/mul_26_grad/ShapeShapestrided_slice_31*
T0*
out_type0
Q
gradients/mul_26_grad/Shape_1Shapestrided_slice_25*
T0*
out_type0

+gradients/mul_26_grad/BroadcastGradientArgsBroadcastGradientArgsgradients/mul_26_grad/Shapegradients/mul_26_grad/Shape_1*
T0
j
gradients/mul_26_grad/MulMul-gradients/sub_3_grad/tuple/control_dependencystrided_slice_25*
T0

gradients/mul_26_grad/SumSumgradients/mul_26_grad/Mul+gradients/mul_26_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0
w
gradients/mul_26_grad/ReshapeReshapegradients/mul_26_grad/Sumgradients/mul_26_grad/Shape*
T0*
Tshape0
l
gradients/mul_26_grad/Mul_1Mulstrided_slice_31-gradients/sub_3_grad/tuple/control_dependency*
T0

gradients/mul_26_grad/Sum_1Sumgradients/mul_26_grad/Mul_1-gradients/mul_26_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0
}
gradients/mul_26_grad/Reshape_1Reshapegradients/mul_26_grad/Sum_1gradients/mul_26_grad/Shape_1*
T0*
Tshape0
p
&gradients/mul_26_grad/tuple/group_depsNoOp^gradients/mul_26_grad/Reshape ^gradients/mul_26_grad/Reshape_1
½
.gradients/mul_26_grad/tuple/control_dependencyIdentitygradients/mul_26_grad/Reshape'^gradients/mul_26_grad/tuple/group_deps*
T0*0
_class&
$"loc:@gradients/mul_26_grad/Reshape
Ć
0gradients/mul_26_grad/tuple/control_dependency_1Identitygradients/mul_26_grad/Reshape_1'^gradients/mul_26_grad/tuple/group_deps*
T0*2
_class(
&$loc:@gradients/mul_26_grad/Reshape_1
O
%gradients/strided_slice_28_grad/ShapeShapemul_24*
T0*
out_type0
ā
0gradients/strided_slice_28_grad/StridedSliceGradStridedSliceGrad%gradients/strided_slice_28_grad/Shapestrided_slice_28/stackstrided_slice_28/stack_1strided_slice_28/stack_2/gradients/sub_3_grad/tuple/control_dependency_1*
Index0*
T0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
O
gradients/mul_27_grad/ShapeShapestrided_slice_32*
T0*
out_type0
Q
gradients/mul_27_grad/Shape_1Shapestrided_slice_26*
T0*
out_type0

+gradients/mul_27_grad/BroadcastGradientArgsBroadcastGradientArgsgradients/mul_27_grad/Shapegradients/mul_27_grad/Shape_1*
T0
j
gradients/mul_27_grad/MulMul-gradients/sub_4_grad/tuple/control_dependencystrided_slice_26*
T0

gradients/mul_27_grad/SumSumgradients/mul_27_grad/Mul+gradients/mul_27_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0
w
gradients/mul_27_grad/ReshapeReshapegradients/mul_27_grad/Sumgradients/mul_27_grad/Shape*
T0*
Tshape0
l
gradients/mul_27_grad/Mul_1Mulstrided_slice_32-gradients/sub_4_grad/tuple/control_dependency*
T0

gradients/mul_27_grad/Sum_1Sumgradients/mul_27_grad/Mul_1-gradients/mul_27_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0
}
gradients/mul_27_grad/Reshape_1Reshapegradients/mul_27_grad/Sum_1gradients/mul_27_grad/Shape_1*
T0*
Tshape0
p
&gradients/mul_27_grad/tuple/group_depsNoOp^gradients/mul_27_grad/Reshape ^gradients/mul_27_grad/Reshape_1
½
.gradients/mul_27_grad/tuple/control_dependencyIdentitygradients/mul_27_grad/Reshape'^gradients/mul_27_grad/tuple/group_deps*
T0*0
_class&
$"loc:@gradients/mul_27_grad/Reshape
Ć
0gradients/mul_27_grad/tuple/control_dependency_1Identitygradients/mul_27_grad/Reshape_1'^gradients/mul_27_grad/tuple/group_deps*
T0*2
_class(
&$loc:@gradients/mul_27_grad/Reshape_1
O
%gradients/strided_slice_29_grad/ShapeShapemul_24*
T0*
out_type0
ā
0gradients/strided_slice_29_grad/StridedSliceGradStridedSliceGrad%gradients/strided_slice_29_grad/Shapestrided_slice_29/stackstrided_slice_29/stack_1strided_slice_29/stack_2/gradients/sub_4_grad/tuple/control_dependency_1*
Index0*
T0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
W
%gradients/strided_slice_24_grad/ShapeShapepolicy_1/mul_3*
T0*
out_type0
ć
0gradients/strided_slice_24_grad/StridedSliceGradStridedSliceGrad%gradients/strided_slice_24_grad/Shapestrided_slice_24/stackstrided_slice_24/stack_1strided_slice_24/stack_20gradients/mul_25_grad/tuple/control_dependency_1*
Index0*
T0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
W
%gradients/strided_slice_25_grad/ShapeShapepolicy_1/mul_3*
T0*
out_type0
ć
0gradients/strided_slice_25_grad/StridedSliceGradStridedSliceGrad%gradients/strided_slice_25_grad/Shapestrided_slice_25/stackstrided_slice_25/stack_1strided_slice_25/stack_20gradients/mul_26_grad/tuple/control_dependency_1*
Index0*
T0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
W
%gradients/strided_slice_26_grad/ShapeShapepolicy_1/mul_3*
T0*
out_type0
ć
0gradients/strided_slice_26_grad/StridedSliceGradStridedSliceGrad%gradients/strided_slice_26_grad/Shapestrided_slice_26/stackstrided_slice_26/stack_1strided_slice_26/stack_20gradients/mul_27_grad/tuple/control_dependency_1*
Index0*
T0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask

gradients/AddNAddN0gradients/strided_slice_27_grad/StridedSliceGrad0gradients/strided_slice_28_grad/StridedSliceGrad0gradients/strided_slice_29_grad/StridedSliceGrad*
T0*C
_class9
75loc:@gradients/strided_slice_27_grad/StridedSliceGrad*
N
P
gradients/mul_24_grad/ShapeShapepolicy_1/concat_1*
T0*
out_type0
\
gradients/mul_24_grad/Shape_1Shapecritic/q/q1_encoding_1/Mean*
T0*
out_type0

+gradients/mul_24_grad/BroadcastGradientArgsBroadcastGradientArgsgradients/mul_24_grad/Shapegradients/mul_24_grad/Shape_1*
T0
V
gradients/mul_24_grad/MulMulgradients/AddNcritic/q/q1_encoding_1/Mean*
T0

gradients/mul_24_grad/SumSumgradients/mul_24_grad/Mul+gradients/mul_24_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0
w
gradients/mul_24_grad/ReshapeReshapegradients/mul_24_grad/Sumgradients/mul_24_grad/Shape*
T0*
Tshape0
N
gradients/mul_24_grad/Mul_1Mulpolicy_1/concat_1gradients/AddN*
T0

gradients/mul_24_grad/Sum_1Sumgradients/mul_24_grad/Mul_1-gradients/mul_24_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0
}
gradients/mul_24_grad/Reshape_1Reshapegradients/mul_24_grad/Sum_1gradients/mul_24_grad/Shape_1*
T0*
Tshape0
p
&gradients/mul_24_grad/tuple/group_depsNoOp^gradients/mul_24_grad/Reshape ^gradients/mul_24_grad/Reshape_1
½
.gradients/mul_24_grad/tuple/control_dependencyIdentitygradients/mul_24_grad/Reshape'^gradients/mul_24_grad/tuple/group_deps*
T0*0
_class&
$"loc:@gradients/mul_24_grad/Reshape
Ć
0gradients/mul_24_grad/tuple/control_dependency_1Identitygradients/mul_24_grad/Reshape_1'^gradients/mul_24_grad/tuple/group_deps*
T0*2
_class(
&$loc:@gradients/mul_24_grad/Reshape_1

gradients/AddN_1AddN0gradients/strided_slice_24_grad/StridedSliceGrad0gradients/strided_slice_25_grad/StridedSliceGrad0gradients/strided_slice_26_grad/StridedSliceGrad*
T0*C
_class9
75loc:@gradients/strided_slice_24_grad/StridedSliceGrad*
N
X
#gradients/policy_1/mul_3_grad/ShapeShapepolicy_1/concat_1*
T0*
out_type0
Z
%gradients/policy_1/mul_3_grad/Shape_1Shapepolicy_1/concat_2*
T0*
out_type0
”
3gradients/policy_1/mul_3_grad/BroadcastGradientArgsBroadcastGradientArgs#gradients/policy_1/mul_3_grad/Shape%gradients/policy_1/mul_3_grad/Shape_1*
T0
V
!gradients/policy_1/mul_3_grad/MulMulgradients/AddN_1policy_1/concat_2*
T0
¦
!gradients/policy_1/mul_3_grad/SumSum!gradients/policy_1/mul_3_grad/Mul3gradients/policy_1/mul_3_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0

%gradients/policy_1/mul_3_grad/ReshapeReshape!gradients/policy_1/mul_3_grad/Sum#gradients/policy_1/mul_3_grad/Shape*
T0*
Tshape0
X
#gradients/policy_1/mul_3_grad/Mul_1Mulpolicy_1/concat_1gradients/AddN_1*
T0
¬
#gradients/policy_1/mul_3_grad/Sum_1Sum#gradients/policy_1/mul_3_grad/Mul_15gradients/policy_1/mul_3_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0

'gradients/policy_1/mul_3_grad/Reshape_1Reshape#gradients/policy_1/mul_3_grad/Sum_1%gradients/policy_1/mul_3_grad/Shape_1*
T0*
Tshape0

.gradients/policy_1/mul_3_grad/tuple/group_depsNoOp&^gradients/policy_1/mul_3_grad/Reshape(^gradients/policy_1/mul_3_grad/Reshape_1
Ż
6gradients/policy_1/mul_3_grad/tuple/control_dependencyIdentity%gradients/policy_1/mul_3_grad/Reshape/^gradients/policy_1/mul_3_grad/tuple/group_deps*
T0*8
_class.
,*loc:@gradients/policy_1/mul_3_grad/Reshape
ć
8gradients/policy_1/mul_3_grad/tuple/control_dependency_1Identity'gradients/policy_1/mul_3_grad/Reshape_1/^gradients/policy_1/mul_3_grad/tuple/group_deps*
T0*:
_class0
.,loc:@gradients/policy_1/mul_3_grad/Reshape_1
Ä
gradients/AddN_2AddN.gradients/mul_24_grad/tuple/control_dependency6gradients/policy_1/mul_3_grad/tuple/control_dependency*
T0*0
_class&
$"loc:@gradients/mul_24_grad/Reshape*
N
O
%gradients/policy_1/concat_1_grad/RankConst*
value	B :*
dtype0
x
$gradients/policy_1/concat_1_grad/modFloorModpolicy_1/concat_1/axis%gradients/policy_1/concat_1_grad/Rank*
T0
Z
&gradients/policy_1/concat_1_grad/ShapeShapepolicy_1/truediv*
T0*
out_type0

'gradients/policy_1/concat_1_grad/ShapeNShapeNpolicy_1/truedivpolicy_1/truediv_1policy_1/truediv_2*
T0*
out_type0*
N
ė
-gradients/policy_1/concat_1_grad/ConcatOffsetConcatOffset$gradients/policy_1/concat_1_grad/mod'gradients/policy_1/concat_1_grad/ShapeN)gradients/policy_1/concat_1_grad/ShapeN:1)gradients/policy_1/concat_1_grad/ShapeN:2*
N
Æ
&gradients/policy_1/concat_1_grad/SliceSlicegradients/AddN_2-gradients/policy_1/concat_1_grad/ConcatOffset'gradients/policy_1/concat_1_grad/ShapeN*
T0*
Index0
µ
(gradients/policy_1/concat_1_grad/Slice_1Slicegradients/AddN_2/gradients/policy_1/concat_1_grad/ConcatOffset:1)gradients/policy_1/concat_1_grad/ShapeN:1*
T0*
Index0
µ
(gradients/policy_1/concat_1_grad/Slice_2Slicegradients/AddN_2/gradients/policy_1/concat_1_grad/ConcatOffset:2)gradients/policy_1/concat_1_grad/ShapeN:2*
T0*
Index0
ø
1gradients/policy_1/concat_1_grad/tuple/group_depsNoOp'^gradients/policy_1/concat_1_grad/Slice)^gradients/policy_1/concat_1_grad/Slice_1)^gradients/policy_1/concat_1_grad/Slice_2
å
9gradients/policy_1/concat_1_grad/tuple/control_dependencyIdentity&gradients/policy_1/concat_1_grad/Slice2^gradients/policy_1/concat_1_grad/tuple/group_deps*
T0*9
_class/
-+loc:@gradients/policy_1/concat_1_grad/Slice
ė
;gradients/policy_1/concat_1_grad/tuple/control_dependency_1Identity(gradients/policy_1/concat_1_grad/Slice_12^gradients/policy_1/concat_1_grad/tuple/group_deps*
T0*;
_class1
/-loc:@gradients/policy_1/concat_1_grad/Slice_1
ė
;gradients/policy_1/concat_1_grad/tuple/control_dependency_2Identity(gradients/policy_1/concat_1_grad/Slice_22^gradients/policy_1/concat_1_grad/tuple/group_deps*
T0*;
_class1
/-loc:@gradients/policy_1/concat_1_grad/Slice_2
O
%gradients/policy_1/concat_2_grad/RankConst*
value	B :*
dtype0
x
$gradients/policy_1/concat_2_grad/modFloorModpolicy_1/concat_2/axis%gradients/policy_1/concat_2_grad/Rank*
T0
X
&gradients/policy_1/concat_2_grad/ShapeShapepolicy_1/Log_3*
T0*
out_type0

'gradients/policy_1/concat_2_grad/ShapeNShapeNpolicy_1/Log_3policy_1/Log_4policy_1/Log_5*
T0*
out_type0*
N
ė
-gradients/policy_1/concat_2_grad/ConcatOffsetConcatOffset$gradients/policy_1/concat_2_grad/mod'gradients/policy_1/concat_2_grad/ShapeN)gradients/policy_1/concat_2_grad/ShapeN:1)gradients/policy_1/concat_2_grad/ShapeN:2*
N
×
&gradients/policy_1/concat_2_grad/SliceSlice8gradients/policy_1/mul_3_grad/tuple/control_dependency_1-gradients/policy_1/concat_2_grad/ConcatOffset'gradients/policy_1/concat_2_grad/ShapeN*
T0*
Index0
Ż
(gradients/policy_1/concat_2_grad/Slice_1Slice8gradients/policy_1/mul_3_grad/tuple/control_dependency_1/gradients/policy_1/concat_2_grad/ConcatOffset:1)gradients/policy_1/concat_2_grad/ShapeN:1*
T0*
Index0
Ż
(gradients/policy_1/concat_2_grad/Slice_2Slice8gradients/policy_1/mul_3_grad/tuple/control_dependency_1/gradients/policy_1/concat_2_grad/ConcatOffset:2)gradients/policy_1/concat_2_grad/ShapeN:2*
T0*
Index0
ø
1gradients/policy_1/concat_2_grad/tuple/group_depsNoOp'^gradients/policy_1/concat_2_grad/Slice)^gradients/policy_1/concat_2_grad/Slice_1)^gradients/policy_1/concat_2_grad/Slice_2
å
9gradients/policy_1/concat_2_grad/tuple/control_dependencyIdentity&gradients/policy_1/concat_2_grad/Slice2^gradients/policy_1/concat_2_grad/tuple/group_deps*
T0*9
_class/
-+loc:@gradients/policy_1/concat_2_grad/Slice
ė
;gradients/policy_1/concat_2_grad/tuple/control_dependency_1Identity(gradients/policy_1/concat_2_grad/Slice_12^gradients/policy_1/concat_2_grad/tuple/group_deps*
T0*;
_class1
/-loc:@gradients/policy_1/concat_2_grad/Slice_1
ė
;gradients/policy_1/concat_2_grad/tuple/control_dependency_2Identity(gradients/policy_1/concat_2_grad/Slice_22^gradients/policy_1/concat_2_grad/tuple/group_deps*
T0*;
_class1
/-loc:@gradients/policy_1/concat_2_grad/Slice_2

(gradients/policy_1/Log_3_grad/Reciprocal
Reciprocalpolicy_1/add_6:^gradients/policy_1/concat_2_grad/tuple/control_dependency*
T0

!gradients/policy_1/Log_3_grad/mulMul9gradients/policy_1/concat_2_grad/tuple/control_dependency(gradients/policy_1/Log_3_grad/Reciprocal*
T0

(gradients/policy_1/Log_4_grad/Reciprocal
Reciprocalpolicy_1/add_7<^gradients/policy_1/concat_2_grad/tuple/control_dependency_1*
T0

!gradients/policy_1/Log_4_grad/mulMul;gradients/policy_1/concat_2_grad/tuple/control_dependency_1(gradients/policy_1/Log_4_grad/Reciprocal*
T0

(gradients/policy_1/Log_5_grad/Reciprocal
Reciprocalpolicy_1/add_8<^gradients/policy_1/concat_2_grad/tuple/control_dependency_2*
T0

!gradients/policy_1/Log_5_grad/mulMul;gradients/policy_1/concat_2_grad/tuple/control_dependency_2(gradients/policy_1/Log_5_grad/Reciprocal*
T0
W
#gradients/policy_1/add_6_grad/ShapeShapepolicy_1/truediv*
T0*
out_type0
Y
%gradients/policy_1/add_6_grad/Shape_1Shapepolicy_1/add_6/y*
T0*
out_type0
”
3gradients/policy_1/add_6_grad/BroadcastGradientArgsBroadcastGradientArgs#gradients/policy_1/add_6_grad/Shape%gradients/policy_1/add_6_grad/Shape_1*
T0
¦
!gradients/policy_1/add_6_grad/SumSum!gradients/policy_1/Log_3_grad/mul3gradients/policy_1/add_6_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0

%gradients/policy_1/add_6_grad/ReshapeReshape!gradients/policy_1/add_6_grad/Sum#gradients/policy_1/add_6_grad/Shape*
T0*
Tshape0
Ŗ
#gradients/policy_1/add_6_grad/Sum_1Sum!gradients/policy_1/Log_3_grad/mul5gradients/policy_1/add_6_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0

'gradients/policy_1/add_6_grad/Reshape_1Reshape#gradients/policy_1/add_6_grad/Sum_1%gradients/policy_1/add_6_grad/Shape_1*
T0*
Tshape0

.gradients/policy_1/add_6_grad/tuple/group_depsNoOp&^gradients/policy_1/add_6_grad/Reshape(^gradients/policy_1/add_6_grad/Reshape_1
Ż
6gradients/policy_1/add_6_grad/tuple/control_dependencyIdentity%gradients/policy_1/add_6_grad/Reshape/^gradients/policy_1/add_6_grad/tuple/group_deps*
T0*8
_class.
,*loc:@gradients/policy_1/add_6_grad/Reshape
ć
8gradients/policy_1/add_6_grad/tuple/control_dependency_1Identity'gradients/policy_1/add_6_grad/Reshape_1/^gradients/policy_1/add_6_grad/tuple/group_deps*
T0*:
_class0
.,loc:@gradients/policy_1/add_6_grad/Reshape_1
Y
#gradients/policy_1/add_7_grad/ShapeShapepolicy_1/truediv_1*
T0*
out_type0
Y
%gradients/policy_1/add_7_grad/Shape_1Shapepolicy_1/add_7/y*
T0*
out_type0
”
3gradients/policy_1/add_7_grad/BroadcastGradientArgsBroadcastGradientArgs#gradients/policy_1/add_7_grad/Shape%gradients/policy_1/add_7_grad/Shape_1*
T0
¦
!gradients/policy_1/add_7_grad/SumSum!gradients/policy_1/Log_4_grad/mul3gradients/policy_1/add_7_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0

%gradients/policy_1/add_7_grad/ReshapeReshape!gradients/policy_1/add_7_grad/Sum#gradients/policy_1/add_7_grad/Shape*
T0*
Tshape0
Ŗ
#gradients/policy_1/add_7_grad/Sum_1Sum!gradients/policy_1/Log_4_grad/mul5gradients/policy_1/add_7_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0

'gradients/policy_1/add_7_grad/Reshape_1Reshape#gradients/policy_1/add_7_grad/Sum_1%gradients/policy_1/add_7_grad/Shape_1*
T0*
Tshape0

.gradients/policy_1/add_7_grad/tuple/group_depsNoOp&^gradients/policy_1/add_7_grad/Reshape(^gradients/policy_1/add_7_grad/Reshape_1
Ż
6gradients/policy_1/add_7_grad/tuple/control_dependencyIdentity%gradients/policy_1/add_7_grad/Reshape/^gradients/policy_1/add_7_grad/tuple/group_deps*
T0*8
_class.
,*loc:@gradients/policy_1/add_7_grad/Reshape
ć
8gradients/policy_1/add_7_grad/tuple/control_dependency_1Identity'gradients/policy_1/add_7_grad/Reshape_1/^gradients/policy_1/add_7_grad/tuple/group_deps*
T0*:
_class0
.,loc:@gradients/policy_1/add_7_grad/Reshape_1
Y
#gradients/policy_1/add_8_grad/ShapeShapepolicy_1/truediv_2*
T0*
out_type0
Y
%gradients/policy_1/add_8_grad/Shape_1Shapepolicy_1/add_8/y*
T0*
out_type0
”
3gradients/policy_1/add_8_grad/BroadcastGradientArgsBroadcastGradientArgs#gradients/policy_1/add_8_grad/Shape%gradients/policy_1/add_8_grad/Shape_1*
T0
¦
!gradients/policy_1/add_8_grad/SumSum!gradients/policy_1/Log_5_grad/mul3gradients/policy_1/add_8_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0

%gradients/policy_1/add_8_grad/ReshapeReshape!gradients/policy_1/add_8_grad/Sum#gradients/policy_1/add_8_grad/Shape*
T0*
Tshape0
Ŗ
#gradients/policy_1/add_8_grad/Sum_1Sum!gradients/policy_1/Log_5_grad/mul5gradients/policy_1/add_8_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0

'gradients/policy_1/add_8_grad/Reshape_1Reshape#gradients/policy_1/add_8_grad/Sum_1%gradients/policy_1/add_8_grad/Shape_1*
T0*
Tshape0

.gradients/policy_1/add_8_grad/tuple/group_depsNoOp&^gradients/policy_1/add_8_grad/Reshape(^gradients/policy_1/add_8_grad/Reshape_1
Ż
6gradients/policy_1/add_8_grad/tuple/control_dependencyIdentity%gradients/policy_1/add_8_grad/Reshape/^gradients/policy_1/add_8_grad/tuple/group_deps*
T0*8
_class.
,*loc:@gradients/policy_1/add_8_grad/Reshape
ć
8gradients/policy_1/add_8_grad/tuple/control_dependency_1Identity'gradients/policy_1/add_8_grad/Reshape_1/^gradients/policy_1/add_8_grad/tuple/group_deps*
T0*:
_class0
.,loc:@gradients/policy_1/add_8_grad/Reshape_1
Ų
gradients/AddN_3AddN9gradients/policy_1/concat_1_grad/tuple/control_dependency6gradients/policy_1/add_6_grad/tuple/control_dependency*
T0*9
_class/
-+loc:@gradients/policy_1/concat_1_grad/Slice*
N
U
%gradients/policy_1/truediv_grad/ShapeShapepolicy_1/Mul*
T0*
out_type0
W
'gradients/policy_1/truediv_grad/Shape_1Shapepolicy_1/Sum*
T0*
out_type0
§
5gradients/policy_1/truediv_grad/BroadcastGradientArgsBroadcastGradientArgs%gradients/policy_1/truediv_grad/Shape'gradients/policy_1/truediv_grad/Shape_1*
T0
[
'gradients/policy_1/truediv_grad/RealDivRealDivgradients/AddN_3policy_1/Sum*
T0
°
#gradients/policy_1/truediv_grad/SumSum'gradients/policy_1/truediv_grad/RealDiv5gradients/policy_1/truediv_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0

'gradients/policy_1/truediv_grad/ReshapeReshape#gradients/policy_1/truediv_grad/Sum%gradients/policy_1/truediv_grad/Shape*
T0*
Tshape0
A
#gradients/policy_1/truediv_grad/NegNegpolicy_1/Mul*
T0
p
)gradients/policy_1/truediv_grad/RealDiv_1RealDiv#gradients/policy_1/truediv_grad/Negpolicy_1/Sum*
T0
v
)gradients/policy_1/truediv_grad/RealDiv_2RealDiv)gradients/policy_1/truediv_grad/RealDiv_1policy_1/Sum*
T0
p
#gradients/policy_1/truediv_grad/mulMulgradients/AddN_3)gradients/policy_1/truediv_grad/RealDiv_2*
T0
°
%gradients/policy_1/truediv_grad/Sum_1Sum#gradients/policy_1/truediv_grad/mul7gradients/policy_1/truediv_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0

)gradients/policy_1/truediv_grad/Reshape_1Reshape%gradients/policy_1/truediv_grad/Sum_1'gradients/policy_1/truediv_grad/Shape_1*
T0*
Tshape0

0gradients/policy_1/truediv_grad/tuple/group_depsNoOp(^gradients/policy_1/truediv_grad/Reshape*^gradients/policy_1/truediv_grad/Reshape_1
å
8gradients/policy_1/truediv_grad/tuple/control_dependencyIdentity'gradients/policy_1/truediv_grad/Reshape1^gradients/policy_1/truediv_grad/tuple/group_deps*
T0*:
_class0
.,loc:@gradients/policy_1/truediv_grad/Reshape
ė
:gradients/policy_1/truediv_grad/tuple/control_dependency_1Identity)gradients/policy_1/truediv_grad/Reshape_11^gradients/policy_1/truediv_grad/tuple/group_deps*
T0*<
_class2
0.loc:@gradients/policy_1/truediv_grad/Reshape_1
Ü
gradients/AddN_4AddN;gradients/policy_1/concat_1_grad/tuple/control_dependency_16gradients/policy_1/add_7_grad/tuple/control_dependency*
T0*;
_class1
/-loc:@gradients/policy_1/concat_1_grad/Slice_1*
N
Y
'gradients/policy_1/truediv_1_grad/ShapeShapepolicy_1/Mul_1*
T0*
out_type0
[
)gradients/policy_1/truediv_1_grad/Shape_1Shapepolicy_1/Sum_1*
T0*
out_type0
­
7gradients/policy_1/truediv_1_grad/BroadcastGradientArgsBroadcastGradientArgs'gradients/policy_1/truediv_1_grad/Shape)gradients/policy_1/truediv_1_grad/Shape_1*
T0
_
)gradients/policy_1/truediv_1_grad/RealDivRealDivgradients/AddN_4policy_1/Sum_1*
T0
¶
%gradients/policy_1/truediv_1_grad/SumSum)gradients/policy_1/truediv_1_grad/RealDiv7gradients/policy_1/truediv_1_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0

)gradients/policy_1/truediv_1_grad/ReshapeReshape%gradients/policy_1/truediv_1_grad/Sum'gradients/policy_1/truediv_1_grad/Shape*
T0*
Tshape0
E
%gradients/policy_1/truediv_1_grad/NegNegpolicy_1/Mul_1*
T0
v
+gradients/policy_1/truediv_1_grad/RealDiv_1RealDiv%gradients/policy_1/truediv_1_grad/Negpolicy_1/Sum_1*
T0
|
+gradients/policy_1/truediv_1_grad/RealDiv_2RealDiv+gradients/policy_1/truediv_1_grad/RealDiv_1policy_1/Sum_1*
T0
t
%gradients/policy_1/truediv_1_grad/mulMulgradients/AddN_4+gradients/policy_1/truediv_1_grad/RealDiv_2*
T0
¶
'gradients/policy_1/truediv_1_grad/Sum_1Sum%gradients/policy_1/truediv_1_grad/mul9gradients/policy_1/truediv_1_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0
”
+gradients/policy_1/truediv_1_grad/Reshape_1Reshape'gradients/policy_1/truediv_1_grad/Sum_1)gradients/policy_1/truediv_1_grad/Shape_1*
T0*
Tshape0

2gradients/policy_1/truediv_1_grad/tuple/group_depsNoOp*^gradients/policy_1/truediv_1_grad/Reshape,^gradients/policy_1/truediv_1_grad/Reshape_1
ķ
:gradients/policy_1/truediv_1_grad/tuple/control_dependencyIdentity)gradients/policy_1/truediv_1_grad/Reshape3^gradients/policy_1/truediv_1_grad/tuple/group_deps*
T0*<
_class2
0.loc:@gradients/policy_1/truediv_1_grad/Reshape
ó
<gradients/policy_1/truediv_1_grad/tuple/control_dependency_1Identity+gradients/policy_1/truediv_1_grad/Reshape_13^gradients/policy_1/truediv_1_grad/tuple/group_deps*
T0*>
_class4
20loc:@gradients/policy_1/truediv_1_grad/Reshape_1
Ü
gradients/AddN_5AddN;gradients/policy_1/concat_1_grad/tuple/control_dependency_26gradients/policy_1/add_8_grad/tuple/control_dependency*
T0*;
_class1
/-loc:@gradients/policy_1/concat_1_grad/Slice_2*
N
Y
'gradients/policy_1/truediv_2_grad/ShapeShapepolicy_1/Mul_2*
T0*
out_type0
[
)gradients/policy_1/truediv_2_grad/Shape_1Shapepolicy_1/Sum_2*
T0*
out_type0
­
7gradients/policy_1/truediv_2_grad/BroadcastGradientArgsBroadcastGradientArgs'gradients/policy_1/truediv_2_grad/Shape)gradients/policy_1/truediv_2_grad/Shape_1*
T0
_
)gradients/policy_1/truediv_2_grad/RealDivRealDivgradients/AddN_5policy_1/Sum_2*
T0
¶
%gradients/policy_1/truediv_2_grad/SumSum)gradients/policy_1/truediv_2_grad/RealDiv7gradients/policy_1/truediv_2_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0

)gradients/policy_1/truediv_2_grad/ReshapeReshape%gradients/policy_1/truediv_2_grad/Sum'gradients/policy_1/truediv_2_grad/Shape*
T0*
Tshape0
E
%gradients/policy_1/truediv_2_grad/NegNegpolicy_1/Mul_2*
T0
v
+gradients/policy_1/truediv_2_grad/RealDiv_1RealDiv%gradients/policy_1/truediv_2_grad/Negpolicy_1/Sum_2*
T0
|
+gradients/policy_1/truediv_2_grad/RealDiv_2RealDiv+gradients/policy_1/truediv_2_grad/RealDiv_1policy_1/Sum_2*
T0
t
%gradients/policy_1/truediv_2_grad/mulMulgradients/AddN_5+gradients/policy_1/truediv_2_grad/RealDiv_2*
T0
¶
'gradients/policy_1/truediv_2_grad/Sum_1Sum%gradients/policy_1/truediv_2_grad/mul9gradients/policy_1/truediv_2_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0
”
+gradients/policy_1/truediv_2_grad/Reshape_1Reshape'gradients/policy_1/truediv_2_grad/Sum_1)gradients/policy_1/truediv_2_grad/Shape_1*
T0*
Tshape0

2gradients/policy_1/truediv_2_grad/tuple/group_depsNoOp*^gradients/policy_1/truediv_2_grad/Reshape,^gradients/policy_1/truediv_2_grad/Reshape_1
ķ
:gradients/policy_1/truediv_2_grad/tuple/control_dependencyIdentity)gradients/policy_1/truediv_2_grad/Reshape3^gradients/policy_1/truediv_2_grad/tuple/group_deps*
T0*<
_class2
0.loc:@gradients/policy_1/truediv_2_grad/Reshape
ó
<gradients/policy_1/truediv_2_grad/tuple/control_dependency_1Identity+gradients/policy_1/truediv_2_grad/Reshape_13^gradients/policy_1/truediv_2_grad/tuple/group_deps*
T0*>
_class4
20loc:@gradients/policy_1/truediv_2_grad/Reshape_1
Q
!gradients/policy_1/Sum_grad/ShapeShapepolicy_1/Mul*
T0*
out_type0

 gradients/policy_1/Sum_grad/SizeConst*4
_class*
(&loc:@gradients/policy_1/Sum_grad/Shape*
value	B :*
dtype0
©
gradients/policy_1/Sum_grad/addAddV2policy_1/Sum/reduction_indices gradients/policy_1/Sum_grad/Size*
T0*4
_class*
(&loc:@gradients/policy_1/Sum_grad/Shape
­
gradients/policy_1/Sum_grad/modFloorModgradients/policy_1/Sum_grad/add gradients/policy_1/Sum_grad/Size*
T0*4
_class*
(&loc:@gradients/policy_1/Sum_grad/Shape

#gradients/policy_1/Sum_grad/Shape_1Const*4
_class*
(&loc:@gradients/policy_1/Sum_grad/Shape*
valueB *
dtype0

'gradients/policy_1/Sum_grad/range/startConst*4
_class*
(&loc:@gradients/policy_1/Sum_grad/Shape*
value	B : *
dtype0

'gradients/policy_1/Sum_grad/range/deltaConst*4
_class*
(&loc:@gradients/policy_1/Sum_grad/Shape*
value	B :*
dtype0
ą
!gradients/policy_1/Sum_grad/rangeRange'gradients/policy_1/Sum_grad/range/start gradients/policy_1/Sum_grad/Size'gradients/policy_1/Sum_grad/range/delta*

Tidx0*4
_class*
(&loc:@gradients/policy_1/Sum_grad/Shape

&gradients/policy_1/Sum_grad/Fill/valueConst*4
_class*
(&loc:@gradients/policy_1/Sum_grad/Shape*
value	B :*
dtype0
Ę
 gradients/policy_1/Sum_grad/FillFill#gradients/policy_1/Sum_grad/Shape_1&gradients/policy_1/Sum_grad/Fill/value*
T0*4
_class*
(&loc:@gradients/policy_1/Sum_grad/Shape*

index_type0

)gradients/policy_1/Sum_grad/DynamicStitchDynamicStitch!gradients/policy_1/Sum_grad/rangegradients/policy_1/Sum_grad/mod!gradients/policy_1/Sum_grad/Shape gradients/policy_1/Sum_grad/Fill*
T0*4
_class*
(&loc:@gradients/policy_1/Sum_grad/Shape*
N

%gradients/policy_1/Sum_grad/Maximum/yConst*4
_class*
(&loc:@gradients/policy_1/Sum_grad/Shape*
value	B :*
dtype0
æ
#gradients/policy_1/Sum_grad/MaximumMaximum)gradients/policy_1/Sum_grad/DynamicStitch%gradients/policy_1/Sum_grad/Maximum/y*
T0*4
_class*
(&loc:@gradients/policy_1/Sum_grad/Shape
·
$gradients/policy_1/Sum_grad/floordivFloorDiv!gradients/policy_1/Sum_grad/Shape#gradients/policy_1/Sum_grad/Maximum*
T0*4
_class*
(&loc:@gradients/policy_1/Sum_grad/Shape
¬
#gradients/policy_1/Sum_grad/ReshapeReshape:gradients/policy_1/truediv_grad/tuple/control_dependency_1)gradients/policy_1/Sum_grad/DynamicStitch*
T0*
Tshape0

 gradients/policy_1/Sum_grad/TileTile#gradients/policy_1/Sum_grad/Reshape$gradients/policy_1/Sum_grad/floordiv*

Tmultiples0*
T0
U
#gradients/policy_1/Sum_1_grad/ShapeShapepolicy_1/Mul_1*
T0*
out_type0

"gradients/policy_1/Sum_1_grad/SizeConst*6
_class,
*(loc:@gradients/policy_1/Sum_1_grad/Shape*
value	B :*
dtype0
±
!gradients/policy_1/Sum_1_grad/addAddV2 policy_1/Sum_1/reduction_indices"gradients/policy_1/Sum_1_grad/Size*
T0*6
_class,
*(loc:@gradients/policy_1/Sum_1_grad/Shape
µ
!gradients/policy_1/Sum_1_grad/modFloorMod!gradients/policy_1/Sum_1_grad/add"gradients/policy_1/Sum_1_grad/Size*
T0*6
_class,
*(loc:@gradients/policy_1/Sum_1_grad/Shape

%gradients/policy_1/Sum_1_grad/Shape_1Const*6
_class,
*(loc:@gradients/policy_1/Sum_1_grad/Shape*
valueB *
dtype0

)gradients/policy_1/Sum_1_grad/range/startConst*6
_class,
*(loc:@gradients/policy_1/Sum_1_grad/Shape*
value	B : *
dtype0

)gradients/policy_1/Sum_1_grad/range/deltaConst*6
_class,
*(loc:@gradients/policy_1/Sum_1_grad/Shape*
value	B :*
dtype0
ź
#gradients/policy_1/Sum_1_grad/rangeRange)gradients/policy_1/Sum_1_grad/range/start"gradients/policy_1/Sum_1_grad/Size)gradients/policy_1/Sum_1_grad/range/delta*

Tidx0*6
_class,
*(loc:@gradients/policy_1/Sum_1_grad/Shape

(gradients/policy_1/Sum_1_grad/Fill/valueConst*6
_class,
*(loc:@gradients/policy_1/Sum_1_grad/Shape*
value	B :*
dtype0
Ī
"gradients/policy_1/Sum_1_grad/FillFill%gradients/policy_1/Sum_1_grad/Shape_1(gradients/policy_1/Sum_1_grad/Fill/value*
T0*6
_class,
*(loc:@gradients/policy_1/Sum_1_grad/Shape*

index_type0

+gradients/policy_1/Sum_1_grad/DynamicStitchDynamicStitch#gradients/policy_1/Sum_1_grad/range!gradients/policy_1/Sum_1_grad/mod#gradients/policy_1/Sum_1_grad/Shape"gradients/policy_1/Sum_1_grad/Fill*
T0*6
_class,
*(loc:@gradients/policy_1/Sum_1_grad/Shape*
N

'gradients/policy_1/Sum_1_grad/Maximum/yConst*6
_class,
*(loc:@gradients/policy_1/Sum_1_grad/Shape*
value	B :*
dtype0
Ē
%gradients/policy_1/Sum_1_grad/MaximumMaximum+gradients/policy_1/Sum_1_grad/DynamicStitch'gradients/policy_1/Sum_1_grad/Maximum/y*
T0*6
_class,
*(loc:@gradients/policy_1/Sum_1_grad/Shape
æ
&gradients/policy_1/Sum_1_grad/floordivFloorDiv#gradients/policy_1/Sum_1_grad/Shape%gradients/policy_1/Sum_1_grad/Maximum*
T0*6
_class,
*(loc:@gradients/policy_1/Sum_1_grad/Shape
²
%gradients/policy_1/Sum_1_grad/ReshapeReshape<gradients/policy_1/truediv_1_grad/tuple/control_dependency_1+gradients/policy_1/Sum_1_grad/DynamicStitch*
T0*
Tshape0

"gradients/policy_1/Sum_1_grad/TileTile%gradients/policy_1/Sum_1_grad/Reshape&gradients/policy_1/Sum_1_grad/floordiv*

Tmultiples0*
T0
U
#gradients/policy_1/Sum_2_grad/ShapeShapepolicy_1/Mul_2*
T0*
out_type0

"gradients/policy_1/Sum_2_grad/SizeConst*6
_class,
*(loc:@gradients/policy_1/Sum_2_grad/Shape*
value	B :*
dtype0
±
!gradients/policy_1/Sum_2_grad/addAddV2 policy_1/Sum_2/reduction_indices"gradients/policy_1/Sum_2_grad/Size*
T0*6
_class,
*(loc:@gradients/policy_1/Sum_2_grad/Shape
µ
!gradients/policy_1/Sum_2_grad/modFloorMod!gradients/policy_1/Sum_2_grad/add"gradients/policy_1/Sum_2_grad/Size*
T0*6
_class,
*(loc:@gradients/policy_1/Sum_2_grad/Shape

%gradients/policy_1/Sum_2_grad/Shape_1Const*6
_class,
*(loc:@gradients/policy_1/Sum_2_grad/Shape*
valueB *
dtype0

)gradients/policy_1/Sum_2_grad/range/startConst*6
_class,
*(loc:@gradients/policy_1/Sum_2_grad/Shape*
value	B : *
dtype0

)gradients/policy_1/Sum_2_grad/range/deltaConst*6
_class,
*(loc:@gradients/policy_1/Sum_2_grad/Shape*
value	B :*
dtype0
ź
#gradients/policy_1/Sum_2_grad/rangeRange)gradients/policy_1/Sum_2_grad/range/start"gradients/policy_1/Sum_2_grad/Size)gradients/policy_1/Sum_2_grad/range/delta*

Tidx0*6
_class,
*(loc:@gradients/policy_1/Sum_2_grad/Shape

(gradients/policy_1/Sum_2_grad/Fill/valueConst*6
_class,
*(loc:@gradients/policy_1/Sum_2_grad/Shape*
value	B :*
dtype0
Ī
"gradients/policy_1/Sum_2_grad/FillFill%gradients/policy_1/Sum_2_grad/Shape_1(gradients/policy_1/Sum_2_grad/Fill/value*
T0*6
_class,
*(loc:@gradients/policy_1/Sum_2_grad/Shape*

index_type0

+gradients/policy_1/Sum_2_grad/DynamicStitchDynamicStitch#gradients/policy_1/Sum_2_grad/range!gradients/policy_1/Sum_2_grad/mod#gradients/policy_1/Sum_2_grad/Shape"gradients/policy_1/Sum_2_grad/Fill*
T0*6
_class,
*(loc:@gradients/policy_1/Sum_2_grad/Shape*
N

'gradients/policy_1/Sum_2_grad/Maximum/yConst*6
_class,
*(loc:@gradients/policy_1/Sum_2_grad/Shape*
value	B :*
dtype0
Ē
%gradients/policy_1/Sum_2_grad/MaximumMaximum+gradients/policy_1/Sum_2_grad/DynamicStitch'gradients/policy_1/Sum_2_grad/Maximum/y*
T0*6
_class,
*(loc:@gradients/policy_1/Sum_2_grad/Shape
æ
&gradients/policy_1/Sum_2_grad/floordivFloorDiv#gradients/policy_1/Sum_2_grad/Shape%gradients/policy_1/Sum_2_grad/Maximum*
T0*6
_class,
*(loc:@gradients/policy_1/Sum_2_grad/Shape
²
%gradients/policy_1/Sum_2_grad/ReshapeReshape<gradients/policy_1/truediv_2_grad/tuple/control_dependency_1+gradients/policy_1/Sum_2_grad/DynamicStitch*
T0*
Tshape0

"gradients/policy_1/Sum_2_grad/TileTile%gradients/policy_1/Sum_2_grad/Reshape&gradients/policy_1/Sum_2_grad/floordiv*

Tmultiples0*
T0
Ā
gradients/AddN_6AddN8gradients/policy_1/truediv_grad/tuple/control_dependency gradients/policy_1/Sum_grad/Tile*
T0*:
_class0
.,loc:@gradients/policy_1/truediv_grad/Reshape*
N
Q
!gradients/policy_1/Mul_grad/ShapeShapepolicy_1/add*
T0*
out_type0
_
#gradients/policy_1/Mul_grad/Shape_1Shapepolicy_1/strided_slice_3*
T0*
out_type0

1gradients/policy_1/Mul_grad/BroadcastGradientArgsBroadcastGradientArgs!gradients/policy_1/Mul_grad/Shape#gradients/policy_1/Mul_grad/Shape_1*
T0
[
gradients/policy_1/Mul_grad/MulMulgradients/AddN_6policy_1/strided_slice_3*
T0
 
gradients/policy_1/Mul_grad/SumSumgradients/policy_1/Mul_grad/Mul1gradients/policy_1/Mul_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0

#gradients/policy_1/Mul_grad/ReshapeReshapegradients/policy_1/Mul_grad/Sum!gradients/policy_1/Mul_grad/Shape*
T0*
Tshape0
Q
!gradients/policy_1/Mul_grad/Mul_1Mulpolicy_1/addgradients/AddN_6*
T0
¦
!gradients/policy_1/Mul_grad/Sum_1Sum!gradients/policy_1/Mul_grad/Mul_13gradients/policy_1/Mul_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0

%gradients/policy_1/Mul_grad/Reshape_1Reshape!gradients/policy_1/Mul_grad/Sum_1#gradients/policy_1/Mul_grad/Shape_1*
T0*
Tshape0

,gradients/policy_1/Mul_grad/tuple/group_depsNoOp$^gradients/policy_1/Mul_grad/Reshape&^gradients/policy_1/Mul_grad/Reshape_1
Õ
4gradients/policy_1/Mul_grad/tuple/control_dependencyIdentity#gradients/policy_1/Mul_grad/Reshape-^gradients/policy_1/Mul_grad/tuple/group_deps*
T0*6
_class,
*(loc:@gradients/policy_1/Mul_grad/Reshape
Ū
6gradients/policy_1/Mul_grad/tuple/control_dependency_1Identity%gradients/policy_1/Mul_grad/Reshape_1-^gradients/policy_1/Mul_grad/tuple/group_deps*
T0*8
_class.
,*loc:@gradients/policy_1/Mul_grad/Reshape_1
Č
gradients/AddN_7AddN:gradients/policy_1/truediv_1_grad/tuple/control_dependency"gradients/policy_1/Sum_1_grad/Tile*
T0*<
_class2
0.loc:@gradients/policy_1/truediv_1_grad/Reshape*
N
U
#gradients/policy_1/Mul_1_grad/ShapeShapepolicy_1/add_1*
T0*
out_type0
a
%gradients/policy_1/Mul_1_grad/Shape_1Shapepolicy_1/strided_slice_4*
T0*
out_type0
”
3gradients/policy_1/Mul_1_grad/BroadcastGradientArgsBroadcastGradientArgs#gradients/policy_1/Mul_1_grad/Shape%gradients/policy_1/Mul_1_grad/Shape_1*
T0
]
!gradients/policy_1/Mul_1_grad/MulMulgradients/AddN_7policy_1/strided_slice_4*
T0
¦
!gradients/policy_1/Mul_1_grad/SumSum!gradients/policy_1/Mul_1_grad/Mul3gradients/policy_1/Mul_1_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0

%gradients/policy_1/Mul_1_grad/ReshapeReshape!gradients/policy_1/Mul_1_grad/Sum#gradients/policy_1/Mul_1_grad/Shape*
T0*
Tshape0
U
#gradients/policy_1/Mul_1_grad/Mul_1Mulpolicy_1/add_1gradients/AddN_7*
T0
¬
#gradients/policy_1/Mul_1_grad/Sum_1Sum#gradients/policy_1/Mul_1_grad/Mul_15gradients/policy_1/Mul_1_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0

'gradients/policy_1/Mul_1_grad/Reshape_1Reshape#gradients/policy_1/Mul_1_grad/Sum_1%gradients/policy_1/Mul_1_grad/Shape_1*
T0*
Tshape0

.gradients/policy_1/Mul_1_grad/tuple/group_depsNoOp&^gradients/policy_1/Mul_1_grad/Reshape(^gradients/policy_1/Mul_1_grad/Reshape_1
Ż
6gradients/policy_1/Mul_1_grad/tuple/control_dependencyIdentity%gradients/policy_1/Mul_1_grad/Reshape/^gradients/policy_1/Mul_1_grad/tuple/group_deps*
T0*8
_class.
,*loc:@gradients/policy_1/Mul_1_grad/Reshape
ć
8gradients/policy_1/Mul_1_grad/tuple/control_dependency_1Identity'gradients/policy_1/Mul_1_grad/Reshape_1/^gradients/policy_1/Mul_1_grad/tuple/group_deps*
T0*:
_class0
.,loc:@gradients/policy_1/Mul_1_grad/Reshape_1
Č
gradients/AddN_8AddN:gradients/policy_1/truediv_2_grad/tuple/control_dependency"gradients/policy_1/Sum_2_grad/Tile*
T0*<
_class2
0.loc:@gradients/policy_1/truediv_2_grad/Reshape*
N
U
#gradients/policy_1/Mul_2_grad/ShapeShapepolicy_1/add_2*
T0*
out_type0
a
%gradients/policy_1/Mul_2_grad/Shape_1Shapepolicy_1/strided_slice_5*
T0*
out_type0
”
3gradients/policy_1/Mul_2_grad/BroadcastGradientArgsBroadcastGradientArgs#gradients/policy_1/Mul_2_grad/Shape%gradients/policy_1/Mul_2_grad/Shape_1*
T0
]
!gradients/policy_1/Mul_2_grad/MulMulgradients/AddN_8policy_1/strided_slice_5*
T0
¦
!gradients/policy_1/Mul_2_grad/SumSum!gradients/policy_1/Mul_2_grad/Mul3gradients/policy_1/Mul_2_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0

%gradients/policy_1/Mul_2_grad/ReshapeReshape!gradients/policy_1/Mul_2_grad/Sum#gradients/policy_1/Mul_2_grad/Shape*
T0*
Tshape0
U
#gradients/policy_1/Mul_2_grad/Mul_1Mulpolicy_1/add_2gradients/AddN_8*
T0
¬
#gradients/policy_1/Mul_2_grad/Sum_1Sum#gradients/policy_1/Mul_2_grad/Mul_15gradients/policy_1/Mul_2_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0

'gradients/policy_1/Mul_2_grad/Reshape_1Reshape#gradients/policy_1/Mul_2_grad/Sum_1%gradients/policy_1/Mul_2_grad/Shape_1*
T0*
Tshape0

.gradients/policy_1/Mul_2_grad/tuple/group_depsNoOp&^gradients/policy_1/Mul_2_grad/Reshape(^gradients/policy_1/Mul_2_grad/Reshape_1
Ż
6gradients/policy_1/Mul_2_grad/tuple/control_dependencyIdentity%gradients/policy_1/Mul_2_grad/Reshape/^gradients/policy_1/Mul_2_grad/tuple/group_deps*
T0*8
_class.
,*loc:@gradients/policy_1/Mul_2_grad/Reshape
ć
8gradients/policy_1/Mul_2_grad/tuple/control_dependency_1Identity'gradients/policy_1/Mul_2_grad/Reshape_1/^gradients/policy_1/Mul_2_grad/tuple/group_deps*
T0*:
_class0
.,loc:@gradients/policy_1/Mul_2_grad/Reshape_1
U
!gradients/policy_1/add_grad/ShapeShapepolicy_1/Softmax*
T0*
out_type0
U
#gradients/policy_1/add_grad/Shape_1Shapepolicy_1/add/y*
T0*
out_type0

1gradients/policy_1/add_grad/BroadcastGradientArgsBroadcastGradientArgs!gradients/policy_1/add_grad/Shape#gradients/policy_1/add_grad/Shape_1*
T0
µ
gradients/policy_1/add_grad/SumSum4gradients/policy_1/Mul_grad/tuple/control_dependency1gradients/policy_1/add_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0

#gradients/policy_1/add_grad/ReshapeReshapegradients/policy_1/add_grad/Sum!gradients/policy_1/add_grad/Shape*
T0*
Tshape0
¹
!gradients/policy_1/add_grad/Sum_1Sum4gradients/policy_1/Mul_grad/tuple/control_dependency3gradients/policy_1/add_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0

%gradients/policy_1/add_grad/Reshape_1Reshape!gradients/policy_1/add_grad/Sum_1#gradients/policy_1/add_grad/Shape_1*
T0*
Tshape0

,gradients/policy_1/add_grad/tuple/group_depsNoOp$^gradients/policy_1/add_grad/Reshape&^gradients/policy_1/add_grad/Reshape_1
Õ
4gradients/policy_1/add_grad/tuple/control_dependencyIdentity#gradients/policy_1/add_grad/Reshape-^gradients/policy_1/add_grad/tuple/group_deps*
T0*6
_class,
*(loc:@gradients/policy_1/add_grad/Reshape
Ū
6gradients/policy_1/add_grad/tuple/control_dependency_1Identity%gradients/policy_1/add_grad/Reshape_1-^gradients/policy_1/add_grad/tuple/group_deps*
T0*8
_class.
,*loc:@gradients/policy_1/add_grad/Reshape_1
Y
#gradients/policy_1/add_1_grad/ShapeShapepolicy_1/Softmax_1*
T0*
out_type0
Y
%gradients/policy_1/add_1_grad/Shape_1Shapepolicy_1/add_1/y*
T0*
out_type0
”
3gradients/policy_1/add_1_grad/BroadcastGradientArgsBroadcastGradientArgs#gradients/policy_1/add_1_grad/Shape%gradients/policy_1/add_1_grad/Shape_1*
T0
»
!gradients/policy_1/add_1_grad/SumSum6gradients/policy_1/Mul_1_grad/tuple/control_dependency3gradients/policy_1/add_1_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0

%gradients/policy_1/add_1_grad/ReshapeReshape!gradients/policy_1/add_1_grad/Sum#gradients/policy_1/add_1_grad/Shape*
T0*
Tshape0
æ
#gradients/policy_1/add_1_grad/Sum_1Sum6gradients/policy_1/Mul_1_grad/tuple/control_dependency5gradients/policy_1/add_1_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0

'gradients/policy_1/add_1_grad/Reshape_1Reshape#gradients/policy_1/add_1_grad/Sum_1%gradients/policy_1/add_1_grad/Shape_1*
T0*
Tshape0

.gradients/policy_1/add_1_grad/tuple/group_depsNoOp&^gradients/policy_1/add_1_grad/Reshape(^gradients/policy_1/add_1_grad/Reshape_1
Ż
6gradients/policy_1/add_1_grad/tuple/control_dependencyIdentity%gradients/policy_1/add_1_grad/Reshape/^gradients/policy_1/add_1_grad/tuple/group_deps*
T0*8
_class.
,*loc:@gradients/policy_1/add_1_grad/Reshape
ć
8gradients/policy_1/add_1_grad/tuple/control_dependency_1Identity'gradients/policy_1/add_1_grad/Reshape_1/^gradients/policy_1/add_1_grad/tuple/group_deps*
T0*:
_class0
.,loc:@gradients/policy_1/add_1_grad/Reshape_1
Y
#gradients/policy_1/add_2_grad/ShapeShapepolicy_1/Softmax_2*
T0*
out_type0
Y
%gradients/policy_1/add_2_grad/Shape_1Shapepolicy_1/add_2/y*
T0*
out_type0
”
3gradients/policy_1/add_2_grad/BroadcastGradientArgsBroadcastGradientArgs#gradients/policy_1/add_2_grad/Shape%gradients/policy_1/add_2_grad/Shape_1*
T0
»
!gradients/policy_1/add_2_grad/SumSum6gradients/policy_1/Mul_2_grad/tuple/control_dependency3gradients/policy_1/add_2_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0

%gradients/policy_1/add_2_grad/ReshapeReshape!gradients/policy_1/add_2_grad/Sum#gradients/policy_1/add_2_grad/Shape*
T0*
Tshape0
æ
#gradients/policy_1/add_2_grad/Sum_1Sum6gradients/policy_1/Mul_2_grad/tuple/control_dependency5gradients/policy_1/add_2_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0

'gradients/policy_1/add_2_grad/Reshape_1Reshape#gradients/policy_1/add_2_grad/Sum_1%gradients/policy_1/add_2_grad/Shape_1*
T0*
Tshape0

.gradients/policy_1/add_2_grad/tuple/group_depsNoOp&^gradients/policy_1/add_2_grad/Reshape(^gradients/policy_1/add_2_grad/Reshape_1
Ż
6gradients/policy_1/add_2_grad/tuple/control_dependencyIdentity%gradients/policy_1/add_2_grad/Reshape/^gradients/policy_1/add_2_grad/tuple/group_deps*
T0*8
_class.
,*loc:@gradients/policy_1/add_2_grad/Reshape
ć
8gradients/policy_1/add_2_grad/tuple/control_dependency_1Identity'gradients/policy_1/add_2_grad/Reshape_1/^gradients/policy_1/add_2_grad/tuple/group_deps*
T0*:
_class0
.,loc:@gradients/policy_1/add_2_grad/Reshape_1
{
#gradients/policy_1/Softmax_grad/mulMul4gradients/policy_1/add_grad/tuple/control_dependencypolicy_1/Softmax*
T0
h
5gradients/policy_1/Softmax_grad/Sum/reduction_indicesConst*
valueB :
’’’’’’’’’*
dtype0
¬
#gradients/policy_1/Softmax_grad/SumSum#gradients/policy_1/Softmax_grad/mul5gradients/policy_1/Softmax_grad/Sum/reduction_indices*

Tidx0*
	keep_dims(*
T0

#gradients/policy_1/Softmax_grad/subSub4gradients/policy_1/add_grad/tuple/control_dependency#gradients/policy_1/Softmax_grad/Sum*
T0
l
%gradients/policy_1/Softmax_grad/mul_1Mul#gradients/policy_1/Softmax_grad/subpolicy_1/Softmax*
T0

%gradients/policy_1/Softmax_1_grad/mulMul6gradients/policy_1/add_1_grad/tuple/control_dependencypolicy_1/Softmax_1*
T0
j
7gradients/policy_1/Softmax_1_grad/Sum/reduction_indicesConst*
valueB :
’’’’’’’’’*
dtype0
²
%gradients/policy_1/Softmax_1_grad/SumSum%gradients/policy_1/Softmax_1_grad/mul7gradients/policy_1/Softmax_1_grad/Sum/reduction_indices*

Tidx0*
	keep_dims(*
T0

%gradients/policy_1/Softmax_1_grad/subSub6gradients/policy_1/add_1_grad/tuple/control_dependency%gradients/policy_1/Softmax_1_grad/Sum*
T0
r
'gradients/policy_1/Softmax_1_grad/mul_1Mul%gradients/policy_1/Softmax_1_grad/subpolicy_1/Softmax_1*
T0

%gradients/policy_1/Softmax_2_grad/mulMul6gradients/policy_1/add_2_grad/tuple/control_dependencypolicy_1/Softmax_2*
T0
j
7gradients/policy_1/Softmax_2_grad/Sum/reduction_indicesConst*
valueB :
’’’’’’’’’*
dtype0
²
%gradients/policy_1/Softmax_2_grad/SumSum%gradients/policy_1/Softmax_2_grad/mul7gradients/policy_1/Softmax_2_grad/Sum/reduction_indices*

Tidx0*
	keep_dims(*
T0

%gradients/policy_1/Softmax_2_grad/subSub6gradients/policy_1/add_2_grad/tuple/control_dependency%gradients/policy_1/Softmax_2_grad/Sum*
T0
r
'gradients/policy_1/Softmax_2_grad/mul_1Mul%gradients/policy_1/Softmax_2_grad/subpolicy_1/Softmax_2*
T0
d
+gradients/policy_1/strided_slice_grad/ShapeShapepolicy_1/action_probs*
T0*
out_type0
ö
6gradients/policy_1/strided_slice_grad/StridedSliceGradStridedSliceGrad+gradients/policy_1/strided_slice_grad/Shapepolicy_1/strided_slice/stackpolicy_1/strided_slice/stack_1policy_1/strided_slice/stack_2%gradients/policy_1/Softmax_grad/mul_1*
Index0*
T0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
f
-gradients/policy_1/strided_slice_1_grad/ShapeShapepolicy_1/action_probs*
T0*
out_type0

8gradients/policy_1/strided_slice_1_grad/StridedSliceGradStridedSliceGrad-gradients/policy_1/strided_slice_1_grad/Shapepolicy_1/strided_slice_1/stack policy_1/strided_slice_1/stack_1 policy_1/strided_slice_1/stack_2'gradients/policy_1/Softmax_1_grad/mul_1*
Index0*
T0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
f
-gradients/policy_1/strided_slice_2_grad/ShapeShapepolicy_1/action_probs*
T0*
out_type0

8gradients/policy_1/strided_slice_2_grad/StridedSliceGradStridedSliceGrad-gradients/policy_1/strided_slice_2_grad/Shapepolicy_1/strided_slice_2/stack policy_1/strided_slice_2/stack_1 policy_1/strided_slice_2/stack_2'gradients/policy_1/Softmax_2_grad/mul_1*
Index0*
T0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
”
gradients/AddN_9AddN6gradients/policy_1/strided_slice_grad/StridedSliceGrad8gradients/policy_1/strided_slice_1_grad/StridedSliceGrad8gradients/policy_1/strided_slice_2_grad/StridedSliceGrad*
T0*I
_class?
=;loc:@gradients/policy_1/strided_slice_grad/StridedSliceGrad*
N
S
)gradients/policy_1/action_probs_grad/RankConst*
value	B :*
dtype0

(gradients/policy_1/action_probs_grad/modFloorModpolicy_1/action_probs/axis)gradients/policy_1/action_probs_grad/Rank*
T0
c
*gradients/policy_1/action_probs_grad/ShapeShapepolicy_1/dense/MatMul*
T0*
out_type0
 
+gradients/policy_1/action_probs_grad/ShapeNShapeNpolicy_1/dense/MatMulpolicy_1/dense_1/MatMulpolicy_1/dense_2/MatMul*
T0*
out_type0*
N
’
1gradients/policy_1/action_probs_grad/ConcatOffsetConcatOffset(gradients/policy_1/action_probs_grad/mod+gradients/policy_1/action_probs_grad/ShapeN-gradients/policy_1/action_probs_grad/ShapeN:1-gradients/policy_1/action_probs_grad/ShapeN:2*
N
»
*gradients/policy_1/action_probs_grad/SliceSlicegradients/AddN_91gradients/policy_1/action_probs_grad/ConcatOffset+gradients/policy_1/action_probs_grad/ShapeN*
T0*
Index0
Į
,gradients/policy_1/action_probs_grad/Slice_1Slicegradients/AddN_93gradients/policy_1/action_probs_grad/ConcatOffset:1-gradients/policy_1/action_probs_grad/ShapeN:1*
T0*
Index0
Į
,gradients/policy_1/action_probs_grad/Slice_2Slicegradients/AddN_93gradients/policy_1/action_probs_grad/ConcatOffset:2-gradients/policy_1/action_probs_grad/ShapeN:2*
T0*
Index0
Č
5gradients/policy_1/action_probs_grad/tuple/group_depsNoOp+^gradients/policy_1/action_probs_grad/Slice-^gradients/policy_1/action_probs_grad/Slice_1-^gradients/policy_1/action_probs_grad/Slice_2
õ
=gradients/policy_1/action_probs_grad/tuple/control_dependencyIdentity*gradients/policy_1/action_probs_grad/Slice6^gradients/policy_1/action_probs_grad/tuple/group_deps*
T0*=
_class3
1/loc:@gradients/policy_1/action_probs_grad/Slice
ū
?gradients/policy_1/action_probs_grad/tuple/control_dependency_1Identity,gradients/policy_1/action_probs_grad/Slice_16^gradients/policy_1/action_probs_grad/tuple/group_deps*
T0*?
_class5
31loc:@gradients/policy_1/action_probs_grad/Slice_1
ū
?gradients/policy_1/action_probs_grad/tuple/control_dependency_2Identity,gradients/policy_1/action_probs_grad/Slice_26^gradients/policy_1/action_probs_grad/tuple/group_deps*
T0*?
_class5
31loc:@gradients/policy_1/action_probs_grad/Slice_2
½
+gradients/policy_1/dense/MatMul_grad/MatMulMatMul=gradients/policy_1/action_probs_grad/tuple/control_dependencypolicy/dense/kernel/read*
transpose_b(*
T0*
transpose_a( 
Ā
-gradients/policy_1/dense/MatMul_grad/MatMul_1MatMulpolicy/encoder/hidden_1/Mul=gradients/policy_1/action_probs_grad/tuple/control_dependency*
transpose_b( *
T0*
transpose_a(

5gradients/policy_1/dense/MatMul_grad/tuple/group_depsNoOp,^gradients/policy_1/dense/MatMul_grad/MatMul.^gradients/policy_1/dense/MatMul_grad/MatMul_1
÷
=gradients/policy_1/dense/MatMul_grad/tuple/control_dependencyIdentity+gradients/policy_1/dense/MatMul_grad/MatMul6^gradients/policy_1/dense/MatMul_grad/tuple/group_deps*
T0*>
_class4
20loc:@gradients/policy_1/dense/MatMul_grad/MatMul
ż
?gradients/policy_1/dense/MatMul_grad/tuple/control_dependency_1Identity-gradients/policy_1/dense/MatMul_grad/MatMul_16^gradients/policy_1/dense/MatMul_grad/tuple/group_deps*
T0*@
_class6
42loc:@gradients/policy_1/dense/MatMul_grad/MatMul_1
Ć
-gradients/policy_1/dense_1/MatMul_grad/MatMulMatMul?gradients/policy_1/action_probs_grad/tuple/control_dependency_1policy/dense_1/kernel/read*
transpose_b(*
T0*
transpose_a( 
Ę
/gradients/policy_1/dense_1/MatMul_grad/MatMul_1MatMulpolicy/encoder/hidden_1/Mul?gradients/policy_1/action_probs_grad/tuple/control_dependency_1*
transpose_b( *
T0*
transpose_a(
”
7gradients/policy_1/dense_1/MatMul_grad/tuple/group_depsNoOp.^gradients/policy_1/dense_1/MatMul_grad/MatMul0^gradients/policy_1/dense_1/MatMul_grad/MatMul_1
’
?gradients/policy_1/dense_1/MatMul_grad/tuple/control_dependencyIdentity-gradients/policy_1/dense_1/MatMul_grad/MatMul8^gradients/policy_1/dense_1/MatMul_grad/tuple/group_deps*
T0*@
_class6
42loc:@gradients/policy_1/dense_1/MatMul_grad/MatMul

Agradients/policy_1/dense_1/MatMul_grad/tuple/control_dependency_1Identity/gradients/policy_1/dense_1/MatMul_grad/MatMul_18^gradients/policy_1/dense_1/MatMul_grad/tuple/group_deps*
T0*B
_class8
64loc:@gradients/policy_1/dense_1/MatMul_grad/MatMul_1
Ć
-gradients/policy_1/dense_2/MatMul_grad/MatMulMatMul?gradients/policy_1/action_probs_grad/tuple/control_dependency_2policy/dense_2/kernel/read*
transpose_b(*
T0*
transpose_a( 
Ę
/gradients/policy_1/dense_2/MatMul_grad/MatMul_1MatMulpolicy/encoder/hidden_1/Mul?gradients/policy_1/action_probs_grad/tuple/control_dependency_2*
transpose_b( *
T0*
transpose_a(
”
7gradients/policy_1/dense_2/MatMul_grad/tuple/group_depsNoOp.^gradients/policy_1/dense_2/MatMul_grad/MatMul0^gradients/policy_1/dense_2/MatMul_grad/MatMul_1
’
?gradients/policy_1/dense_2/MatMul_grad/tuple/control_dependencyIdentity-gradients/policy_1/dense_2/MatMul_grad/MatMul8^gradients/policy_1/dense_2/MatMul_grad/tuple/group_deps*
T0*@
_class6
42loc:@gradients/policy_1/dense_2/MatMul_grad/MatMul

Agradients/policy_1/dense_2/MatMul_grad/tuple/control_dependency_1Identity/gradients/policy_1/dense_2/MatMul_grad/MatMul_18^gradients/policy_1/dense_2/MatMul_grad/tuple/group_deps*
T0*B
_class8
64loc:@gradients/policy_1/dense_2/MatMul_grad/MatMul_1
¬
gradients/AddN_10AddN=gradients/policy_1/dense/MatMul_grad/tuple/control_dependency?gradients/policy_1/dense_1/MatMul_grad/tuple/control_dependency?gradients/policy_1/dense_2/MatMul_grad/tuple/control_dependency*
T0*>
_class4
20loc:@gradients/policy_1/dense/MatMul_grad/MatMul*
N
s
0gradients/policy/encoder/hidden_1/Mul_grad/ShapeShapepolicy/encoder/hidden_1/BiasAdd*
T0*
out_type0
u
2gradients/policy/encoder/hidden_1/Mul_grad/Shape_1Shapepolicy/encoder/hidden_1/Sigmoid*
T0*
out_type0
Č
@gradients/policy/encoder/hidden_1/Mul_grad/BroadcastGradientArgsBroadcastGradientArgs0gradients/policy/encoder/hidden_1/Mul_grad/Shape2gradients/policy/encoder/hidden_1/Mul_grad/Shape_1*
T0
r
.gradients/policy/encoder/hidden_1/Mul_grad/MulMulgradients/AddN_10policy/encoder/hidden_1/Sigmoid*
T0
Ķ
.gradients/policy/encoder/hidden_1/Mul_grad/SumSum.gradients/policy/encoder/hidden_1/Mul_grad/Mul@gradients/policy/encoder/hidden_1/Mul_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0
¶
2gradients/policy/encoder/hidden_1/Mul_grad/ReshapeReshape.gradients/policy/encoder/hidden_1/Mul_grad/Sum0gradients/policy/encoder/hidden_1/Mul_grad/Shape*
T0*
Tshape0
t
0gradients/policy/encoder/hidden_1/Mul_grad/Mul_1Mulpolicy/encoder/hidden_1/BiasAddgradients/AddN_10*
T0
Ó
0gradients/policy/encoder/hidden_1/Mul_grad/Sum_1Sum0gradients/policy/encoder/hidden_1/Mul_grad/Mul_1Bgradients/policy/encoder/hidden_1/Mul_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0
¼
4gradients/policy/encoder/hidden_1/Mul_grad/Reshape_1Reshape0gradients/policy/encoder/hidden_1/Mul_grad/Sum_12gradients/policy/encoder/hidden_1/Mul_grad/Shape_1*
T0*
Tshape0
Æ
;gradients/policy/encoder/hidden_1/Mul_grad/tuple/group_depsNoOp3^gradients/policy/encoder/hidden_1/Mul_grad/Reshape5^gradients/policy/encoder/hidden_1/Mul_grad/Reshape_1

Cgradients/policy/encoder/hidden_1/Mul_grad/tuple/control_dependencyIdentity2gradients/policy/encoder/hidden_1/Mul_grad/Reshape<^gradients/policy/encoder/hidden_1/Mul_grad/tuple/group_deps*
T0*E
_class;
97loc:@gradients/policy/encoder/hidden_1/Mul_grad/Reshape

Egradients/policy/encoder/hidden_1/Mul_grad/tuple/control_dependency_1Identity4gradients/policy/encoder/hidden_1/Mul_grad/Reshape_1<^gradients/policy/encoder/hidden_1/Mul_grad/tuple/group_deps*
T0*G
_class=
;9loc:@gradients/policy/encoder/hidden_1/Mul_grad/Reshape_1
ŗ
:gradients/policy/encoder/hidden_1/Sigmoid_grad/SigmoidGradSigmoidGradpolicy/encoder/hidden_1/SigmoidEgradients/policy/encoder/hidden_1/Mul_grad/tuple/control_dependency_1*
T0
ó
gradients/AddN_11AddNCgradients/policy/encoder/hidden_1/Mul_grad/tuple/control_dependency:gradients/policy/encoder/hidden_1/Sigmoid_grad/SigmoidGrad*
T0*E
_class;
97loc:@gradients/policy/encoder/hidden_1/Mul_grad/Reshape*
N
|
:gradients/policy/encoder/hidden_1/BiasAdd_grad/BiasAddGradBiasAddGradgradients/AddN_11*
T0*
data_formatNHWC

?gradients/policy/encoder/hidden_1/BiasAdd_grad/tuple/group_depsNoOp^gradients/AddN_11;^gradients/policy/encoder/hidden_1/BiasAdd_grad/BiasAddGrad
ų
Ggradients/policy/encoder/hidden_1/BiasAdd_grad/tuple/control_dependencyIdentitygradients/AddN_11@^gradients/policy/encoder/hidden_1/BiasAdd_grad/tuple/group_deps*
T0*E
_class;
97loc:@gradients/policy/encoder/hidden_1/Mul_grad/Reshape
«
Igradients/policy/encoder/hidden_1/BiasAdd_grad/tuple/control_dependency_1Identity:gradients/policy/encoder/hidden_1/BiasAdd_grad/BiasAddGrad@^gradients/policy/encoder/hidden_1/BiasAdd_grad/tuple/group_deps*
T0*M
_classC
A?loc:@gradients/policy/encoder/hidden_1/BiasAdd_grad/BiasAddGrad
Ū
4gradients/policy/encoder/hidden_1/MatMul_grad/MatMulMatMulGgradients/policy/encoder/hidden_1/BiasAdd_grad/tuple/control_dependency#policy/encoder/hidden_1/kernel/read*
transpose_b(*
T0*
transpose_a( 
Õ
6gradients/policy/encoder/hidden_1/MatMul_grad/MatMul_1MatMulpolicy/encoder/hidden_0/MulGgradients/policy/encoder/hidden_1/BiasAdd_grad/tuple/control_dependency*
transpose_b( *
T0*
transpose_a(
¶
>gradients/policy/encoder/hidden_1/MatMul_grad/tuple/group_depsNoOp5^gradients/policy/encoder/hidden_1/MatMul_grad/MatMul7^gradients/policy/encoder/hidden_1/MatMul_grad/MatMul_1

Fgradients/policy/encoder/hidden_1/MatMul_grad/tuple/control_dependencyIdentity4gradients/policy/encoder/hidden_1/MatMul_grad/MatMul?^gradients/policy/encoder/hidden_1/MatMul_grad/tuple/group_deps*
T0*G
_class=
;9loc:@gradients/policy/encoder/hidden_1/MatMul_grad/MatMul
”
Hgradients/policy/encoder/hidden_1/MatMul_grad/tuple/control_dependency_1Identity6gradients/policy/encoder/hidden_1/MatMul_grad/MatMul_1?^gradients/policy/encoder/hidden_1/MatMul_grad/tuple/group_deps*
T0*I
_class?
=;loc:@gradients/policy/encoder/hidden_1/MatMul_grad/MatMul_1
s
0gradients/policy/encoder/hidden_0/Mul_grad/ShapeShapepolicy/encoder/hidden_0/BiasAdd*
T0*
out_type0
u
2gradients/policy/encoder/hidden_0/Mul_grad/Shape_1Shapepolicy/encoder/hidden_0/Sigmoid*
T0*
out_type0
Č
@gradients/policy/encoder/hidden_0/Mul_grad/BroadcastGradientArgsBroadcastGradientArgs0gradients/policy/encoder/hidden_0/Mul_grad/Shape2gradients/policy/encoder/hidden_0/Mul_grad/Shape_1*
T0
§
.gradients/policy/encoder/hidden_0/Mul_grad/MulMulFgradients/policy/encoder/hidden_1/MatMul_grad/tuple/control_dependencypolicy/encoder/hidden_0/Sigmoid*
T0
Ķ
.gradients/policy/encoder/hidden_0/Mul_grad/SumSum.gradients/policy/encoder/hidden_0/Mul_grad/Mul@gradients/policy/encoder/hidden_0/Mul_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0
¶
2gradients/policy/encoder/hidden_0/Mul_grad/ReshapeReshape.gradients/policy/encoder/hidden_0/Mul_grad/Sum0gradients/policy/encoder/hidden_0/Mul_grad/Shape*
T0*
Tshape0
©
0gradients/policy/encoder/hidden_0/Mul_grad/Mul_1Mulpolicy/encoder/hidden_0/BiasAddFgradients/policy/encoder/hidden_1/MatMul_grad/tuple/control_dependency*
T0
Ó
0gradients/policy/encoder/hidden_0/Mul_grad/Sum_1Sum0gradients/policy/encoder/hidden_0/Mul_grad/Mul_1Bgradients/policy/encoder/hidden_0/Mul_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0
¼
4gradients/policy/encoder/hidden_0/Mul_grad/Reshape_1Reshape0gradients/policy/encoder/hidden_0/Mul_grad/Sum_12gradients/policy/encoder/hidden_0/Mul_grad/Shape_1*
T0*
Tshape0
Æ
;gradients/policy/encoder/hidden_0/Mul_grad/tuple/group_depsNoOp3^gradients/policy/encoder/hidden_0/Mul_grad/Reshape5^gradients/policy/encoder/hidden_0/Mul_grad/Reshape_1

Cgradients/policy/encoder/hidden_0/Mul_grad/tuple/control_dependencyIdentity2gradients/policy/encoder/hidden_0/Mul_grad/Reshape<^gradients/policy/encoder/hidden_0/Mul_grad/tuple/group_deps*
T0*E
_class;
97loc:@gradients/policy/encoder/hidden_0/Mul_grad/Reshape

Egradients/policy/encoder/hidden_0/Mul_grad/tuple/control_dependency_1Identity4gradients/policy/encoder/hidden_0/Mul_grad/Reshape_1<^gradients/policy/encoder/hidden_0/Mul_grad/tuple/group_deps*
T0*G
_class=
;9loc:@gradients/policy/encoder/hidden_0/Mul_grad/Reshape_1
ŗ
:gradients/policy/encoder/hidden_0/Sigmoid_grad/SigmoidGradSigmoidGradpolicy/encoder/hidden_0/SigmoidEgradients/policy/encoder/hidden_0/Mul_grad/tuple/control_dependency_1*
T0
ó
gradients/AddN_12AddNCgradients/policy/encoder/hidden_0/Mul_grad/tuple/control_dependency:gradients/policy/encoder/hidden_0/Sigmoid_grad/SigmoidGrad*
T0*E
_class;
97loc:@gradients/policy/encoder/hidden_0/Mul_grad/Reshape*
N
|
:gradients/policy/encoder/hidden_0/BiasAdd_grad/BiasAddGradBiasAddGradgradients/AddN_12*
T0*
data_formatNHWC

?gradients/policy/encoder/hidden_0/BiasAdd_grad/tuple/group_depsNoOp^gradients/AddN_12;^gradients/policy/encoder/hidden_0/BiasAdd_grad/BiasAddGrad
ų
Ggradients/policy/encoder/hidden_0/BiasAdd_grad/tuple/control_dependencyIdentitygradients/AddN_12@^gradients/policy/encoder/hidden_0/BiasAdd_grad/tuple/group_deps*
T0*E
_class;
97loc:@gradients/policy/encoder/hidden_0/Mul_grad/Reshape
«
Igradients/policy/encoder/hidden_0/BiasAdd_grad/tuple/control_dependency_1Identity:gradients/policy/encoder/hidden_0/BiasAdd_grad/BiasAddGrad@^gradients/policy/encoder/hidden_0/BiasAdd_grad/tuple/group_deps*
T0*M
_classC
A?loc:@gradients/policy/encoder/hidden_0/BiasAdd_grad/BiasAddGrad
Ū
4gradients/policy/encoder/hidden_0/MatMul_grad/MatMulMatMulGgradients/policy/encoder/hidden_0/BiasAdd_grad/tuple/control_dependency#policy/encoder/hidden_0/kernel/read*
transpose_b(*
T0*
transpose_a( 
Ģ
6gradients/policy/encoder/hidden_0/MatMul_grad/MatMul_1MatMulvector_observationGgradients/policy/encoder/hidden_0/BiasAdd_grad/tuple/control_dependency*
transpose_b( *
T0*
transpose_a(
¶
>gradients/policy/encoder/hidden_0/MatMul_grad/tuple/group_depsNoOp5^gradients/policy/encoder/hidden_0/MatMul_grad/MatMul7^gradients/policy/encoder/hidden_0/MatMul_grad/MatMul_1

Fgradients/policy/encoder/hidden_0/MatMul_grad/tuple/control_dependencyIdentity4gradients/policy/encoder/hidden_0/MatMul_grad/MatMul?^gradients/policy/encoder/hidden_0/MatMul_grad/tuple/group_deps*
T0*G
_class=
;9loc:@gradients/policy/encoder/hidden_0/MatMul_grad/MatMul
”
Hgradients/policy/encoder/hidden_0/MatMul_grad/tuple/control_dependency_1Identity6gradients/policy/encoder/hidden_0/MatMul_grad/MatMul_1?^gradients/policy/encoder/hidden_0/MatMul_grad/tuple/group_deps*
T0*I
_class?
=;loc:@gradients/policy/encoder/hidden_0/MatMul_grad/MatMul_1
n
beta1_power/initial_valueConst*&
_class
loc:@policy/dense/kernel*
valueB
 *fff?*
dtype0

beta1_power
VariableV2*
shape: *
shared_name *&
_class
loc:@policy/dense/kernel*
dtype0*
	container 

beta1_power/AssignAssignbeta1_powerbeta1_power/initial_value*
use_locking(*
T0*&
_class
loc:@policy/dense/kernel*
validate_shape(
Z
beta1_power/readIdentitybeta1_power*
T0*&
_class
loc:@policy/dense/kernel
n
beta2_power/initial_valueConst*&
_class
loc:@policy/dense/kernel*
valueB
 *w¾?*
dtype0

beta2_power
VariableV2*
shape: *
shared_name *&
_class
loc:@policy/dense/kernel*
dtype0*
	container 

beta2_power/AssignAssignbeta2_powerbeta2_power/initial_value*
use_locking(*
T0*&
_class
loc:@policy/dense/kernel*
validate_shape(
Z
beta2_power/readIdentitybeta2_power*
T0*&
_class
loc:@policy/dense/kernel
­
Epolicy/encoder/hidden_0/kernel/Adam/Initializer/zeros/shape_as_tensorConst*
valueB"b     *1
_class'
%#loc:@policy/encoder/hidden_0/kernel*
dtype0

;policy/encoder/hidden_0/kernel/Adam/Initializer/zeros/ConstConst*
valueB
 *    *1
_class'
%#loc:@policy/encoder/hidden_0/kernel*
dtype0

5policy/encoder/hidden_0/kernel/Adam/Initializer/zerosFillEpolicy/encoder/hidden_0/kernel/Adam/Initializer/zeros/shape_as_tensor;policy/encoder/hidden_0/kernel/Adam/Initializer/zeros/Const*
T0*

index_type0*1
_class'
%#loc:@policy/encoder/hidden_0/kernel
¬
#policy/encoder/hidden_0/kernel/Adam
VariableV2*
shape:
ā*
shared_name *1
_class'
%#loc:@policy/encoder/hidden_0/kernel*
dtype0*
	container 
õ
*policy/encoder/hidden_0/kernel/Adam/AssignAssign#policy/encoder/hidden_0/kernel/Adam5policy/encoder/hidden_0/kernel/Adam/Initializer/zeros*
use_locking(*
T0*1
_class'
%#loc:@policy/encoder/hidden_0/kernel*
validate_shape(

(policy/encoder/hidden_0/kernel/Adam/readIdentity#policy/encoder/hidden_0/kernel/Adam*
T0*1
_class'
%#loc:@policy/encoder/hidden_0/kernel
Æ
Gpolicy/encoder/hidden_0/kernel/Adam_1/Initializer/zeros/shape_as_tensorConst*
valueB"b     *1
_class'
%#loc:@policy/encoder/hidden_0/kernel*
dtype0

=policy/encoder/hidden_0/kernel/Adam_1/Initializer/zeros/ConstConst*
valueB
 *    *1
_class'
%#loc:@policy/encoder/hidden_0/kernel*
dtype0

7policy/encoder/hidden_0/kernel/Adam_1/Initializer/zerosFillGpolicy/encoder/hidden_0/kernel/Adam_1/Initializer/zeros/shape_as_tensor=policy/encoder/hidden_0/kernel/Adam_1/Initializer/zeros/Const*
T0*

index_type0*1
_class'
%#loc:@policy/encoder/hidden_0/kernel
®
%policy/encoder/hidden_0/kernel/Adam_1
VariableV2*
shape:
ā*
shared_name *1
_class'
%#loc:@policy/encoder/hidden_0/kernel*
dtype0*
	container 
ū
,policy/encoder/hidden_0/kernel/Adam_1/AssignAssign%policy/encoder/hidden_0/kernel/Adam_17policy/encoder/hidden_0/kernel/Adam_1/Initializer/zeros*
use_locking(*
T0*1
_class'
%#loc:@policy/encoder/hidden_0/kernel*
validate_shape(

*policy/encoder/hidden_0/kernel/Adam_1/readIdentity%policy/encoder/hidden_0/kernel/Adam_1*
T0*1
_class'
%#loc:@policy/encoder/hidden_0/kernel

3policy/encoder/hidden_0/bias/Adam/Initializer/zerosConst*
valueB*    */
_class%
#!loc:@policy/encoder/hidden_0/bias*
dtype0
£
!policy/encoder/hidden_0/bias/Adam
VariableV2*
shape:*
shared_name */
_class%
#!loc:@policy/encoder/hidden_0/bias*
dtype0*
	container 
ķ
(policy/encoder/hidden_0/bias/Adam/AssignAssign!policy/encoder/hidden_0/bias/Adam3policy/encoder/hidden_0/bias/Adam/Initializer/zeros*
use_locking(*
T0*/
_class%
#!loc:@policy/encoder/hidden_0/bias*
validate_shape(

&policy/encoder/hidden_0/bias/Adam/readIdentity!policy/encoder/hidden_0/bias/Adam*
T0*/
_class%
#!loc:@policy/encoder/hidden_0/bias

5policy/encoder/hidden_0/bias/Adam_1/Initializer/zerosConst*
valueB*    */
_class%
#!loc:@policy/encoder/hidden_0/bias*
dtype0
„
#policy/encoder/hidden_0/bias/Adam_1
VariableV2*
shape:*
shared_name */
_class%
#!loc:@policy/encoder/hidden_0/bias*
dtype0*
	container 
ó
*policy/encoder/hidden_0/bias/Adam_1/AssignAssign#policy/encoder/hidden_0/bias/Adam_15policy/encoder/hidden_0/bias/Adam_1/Initializer/zeros*
use_locking(*
T0*/
_class%
#!loc:@policy/encoder/hidden_0/bias*
validate_shape(

(policy/encoder/hidden_0/bias/Adam_1/readIdentity#policy/encoder/hidden_0/bias/Adam_1*
T0*/
_class%
#!loc:@policy/encoder/hidden_0/bias
­
Epolicy/encoder/hidden_1/kernel/Adam/Initializer/zeros/shape_as_tensorConst*
valueB"      *1
_class'
%#loc:@policy/encoder/hidden_1/kernel*
dtype0

;policy/encoder/hidden_1/kernel/Adam/Initializer/zeros/ConstConst*
valueB
 *    *1
_class'
%#loc:@policy/encoder/hidden_1/kernel*
dtype0

5policy/encoder/hidden_1/kernel/Adam/Initializer/zerosFillEpolicy/encoder/hidden_1/kernel/Adam/Initializer/zeros/shape_as_tensor;policy/encoder/hidden_1/kernel/Adam/Initializer/zeros/Const*
T0*

index_type0*1
_class'
%#loc:@policy/encoder/hidden_1/kernel
¬
#policy/encoder/hidden_1/kernel/Adam
VariableV2*
shape:
*
shared_name *1
_class'
%#loc:@policy/encoder/hidden_1/kernel*
dtype0*
	container 
õ
*policy/encoder/hidden_1/kernel/Adam/AssignAssign#policy/encoder/hidden_1/kernel/Adam5policy/encoder/hidden_1/kernel/Adam/Initializer/zeros*
use_locking(*
T0*1
_class'
%#loc:@policy/encoder/hidden_1/kernel*
validate_shape(

(policy/encoder/hidden_1/kernel/Adam/readIdentity#policy/encoder/hidden_1/kernel/Adam*
T0*1
_class'
%#loc:@policy/encoder/hidden_1/kernel
Æ
Gpolicy/encoder/hidden_1/kernel/Adam_1/Initializer/zeros/shape_as_tensorConst*
valueB"      *1
_class'
%#loc:@policy/encoder/hidden_1/kernel*
dtype0

=policy/encoder/hidden_1/kernel/Adam_1/Initializer/zeros/ConstConst*
valueB
 *    *1
_class'
%#loc:@policy/encoder/hidden_1/kernel*
dtype0

7policy/encoder/hidden_1/kernel/Adam_1/Initializer/zerosFillGpolicy/encoder/hidden_1/kernel/Adam_1/Initializer/zeros/shape_as_tensor=policy/encoder/hidden_1/kernel/Adam_1/Initializer/zeros/Const*
T0*

index_type0*1
_class'
%#loc:@policy/encoder/hidden_1/kernel
®
%policy/encoder/hidden_1/kernel/Adam_1
VariableV2*
shape:
*
shared_name *1
_class'
%#loc:@policy/encoder/hidden_1/kernel*
dtype0*
	container 
ū
,policy/encoder/hidden_1/kernel/Adam_1/AssignAssign%policy/encoder/hidden_1/kernel/Adam_17policy/encoder/hidden_1/kernel/Adam_1/Initializer/zeros*
use_locking(*
T0*1
_class'
%#loc:@policy/encoder/hidden_1/kernel*
validate_shape(

*policy/encoder/hidden_1/kernel/Adam_1/readIdentity%policy/encoder/hidden_1/kernel/Adam_1*
T0*1
_class'
%#loc:@policy/encoder/hidden_1/kernel

3policy/encoder/hidden_1/bias/Adam/Initializer/zerosConst*
valueB*    */
_class%
#!loc:@policy/encoder/hidden_1/bias*
dtype0
£
!policy/encoder/hidden_1/bias/Adam
VariableV2*
shape:*
shared_name */
_class%
#!loc:@policy/encoder/hidden_1/bias*
dtype0*
	container 
ķ
(policy/encoder/hidden_1/bias/Adam/AssignAssign!policy/encoder/hidden_1/bias/Adam3policy/encoder/hidden_1/bias/Adam/Initializer/zeros*
use_locking(*
T0*/
_class%
#!loc:@policy/encoder/hidden_1/bias*
validate_shape(

&policy/encoder/hidden_1/bias/Adam/readIdentity!policy/encoder/hidden_1/bias/Adam*
T0*/
_class%
#!loc:@policy/encoder/hidden_1/bias

5policy/encoder/hidden_1/bias/Adam_1/Initializer/zerosConst*
valueB*    */
_class%
#!loc:@policy/encoder/hidden_1/bias*
dtype0
„
#policy/encoder/hidden_1/bias/Adam_1
VariableV2*
shape:*
shared_name */
_class%
#!loc:@policy/encoder/hidden_1/bias*
dtype0*
	container 
ó
*policy/encoder/hidden_1/bias/Adam_1/AssignAssign#policy/encoder/hidden_1/bias/Adam_15policy/encoder/hidden_1/bias/Adam_1/Initializer/zeros*
use_locking(*
T0*/
_class%
#!loc:@policy/encoder/hidden_1/bias*
validate_shape(

(policy/encoder/hidden_1/bias/Adam_1/readIdentity#policy/encoder/hidden_1/bias/Adam_1*
T0*/
_class%
#!loc:@policy/encoder/hidden_1/bias

*policy/dense/kernel/Adam/Initializer/zerosConst*
valueB	*    *&
_class
loc:@policy/dense/kernel*
dtype0

policy/dense/kernel/Adam
VariableV2*
shape:	*
shared_name *&
_class
loc:@policy/dense/kernel*
dtype0*
	container 
É
policy/dense/kernel/Adam/AssignAssignpolicy/dense/kernel/Adam*policy/dense/kernel/Adam/Initializer/zeros*
use_locking(*
T0*&
_class
loc:@policy/dense/kernel*
validate_shape(
t
policy/dense/kernel/Adam/readIdentitypolicy/dense/kernel/Adam*
T0*&
_class
loc:@policy/dense/kernel

,policy/dense/kernel/Adam_1/Initializer/zerosConst*
valueB	*    *&
_class
loc:@policy/dense/kernel*
dtype0

policy/dense/kernel/Adam_1
VariableV2*
shape:	*
shared_name *&
_class
loc:@policy/dense/kernel*
dtype0*
	container 
Ļ
!policy/dense/kernel/Adam_1/AssignAssignpolicy/dense/kernel/Adam_1,policy/dense/kernel/Adam_1/Initializer/zeros*
use_locking(*
T0*&
_class
loc:@policy/dense/kernel*
validate_shape(
x
policy/dense/kernel/Adam_1/readIdentitypolicy/dense/kernel/Adam_1*
T0*&
_class
loc:@policy/dense/kernel

,policy/dense_1/kernel/Adam/Initializer/zerosConst*
valueB	*    *(
_class
loc:@policy/dense_1/kernel*
dtype0

policy/dense_1/kernel/Adam
VariableV2*
shape:	*
shared_name *(
_class
loc:@policy/dense_1/kernel*
dtype0*
	container 
Ń
!policy/dense_1/kernel/Adam/AssignAssignpolicy/dense_1/kernel/Adam,policy/dense_1/kernel/Adam/Initializer/zeros*
use_locking(*
T0*(
_class
loc:@policy/dense_1/kernel*
validate_shape(
z
policy/dense_1/kernel/Adam/readIdentitypolicy/dense_1/kernel/Adam*
T0*(
_class
loc:@policy/dense_1/kernel

.policy/dense_1/kernel/Adam_1/Initializer/zerosConst*
valueB	*    *(
_class
loc:@policy/dense_1/kernel*
dtype0

policy/dense_1/kernel/Adam_1
VariableV2*
shape:	*
shared_name *(
_class
loc:@policy/dense_1/kernel*
dtype0*
	container 
×
#policy/dense_1/kernel/Adam_1/AssignAssignpolicy/dense_1/kernel/Adam_1.policy/dense_1/kernel/Adam_1/Initializer/zeros*
use_locking(*
T0*(
_class
loc:@policy/dense_1/kernel*
validate_shape(
~
!policy/dense_1/kernel/Adam_1/readIdentitypolicy/dense_1/kernel/Adam_1*
T0*(
_class
loc:@policy/dense_1/kernel

,policy/dense_2/kernel/Adam/Initializer/zerosConst*
valueB	*    *(
_class
loc:@policy/dense_2/kernel*
dtype0

policy/dense_2/kernel/Adam
VariableV2*
shape:	*
shared_name *(
_class
loc:@policy/dense_2/kernel*
dtype0*
	container 
Ń
!policy/dense_2/kernel/Adam/AssignAssignpolicy/dense_2/kernel/Adam,policy/dense_2/kernel/Adam/Initializer/zeros*
use_locking(*
T0*(
_class
loc:@policy/dense_2/kernel*
validate_shape(
z
policy/dense_2/kernel/Adam/readIdentitypolicy/dense_2/kernel/Adam*
T0*(
_class
loc:@policy/dense_2/kernel

.policy/dense_2/kernel/Adam_1/Initializer/zerosConst*
valueB	*    *(
_class
loc:@policy/dense_2/kernel*
dtype0

policy/dense_2/kernel/Adam_1
VariableV2*
shape:	*
shared_name *(
_class
loc:@policy/dense_2/kernel*
dtype0*
	container 
×
#policy/dense_2/kernel/Adam_1/AssignAssignpolicy/dense_2/kernel/Adam_1.policy/dense_2/kernel/Adam_1/Initializer/zeros*
use_locking(*
T0*(
_class
loc:@policy/dense_2/kernel*
validate_shape(
~
!policy/dense_2/kernel/Adam_1/readIdentitypolicy/dense_2/kernel/Adam_1*
T0*(
_class
loc:@policy/dense_2/kernel
7

Adam/beta1Const*
valueB
 *fff?*
dtype0
7

Adam/beta2Const*
valueB
 *w¾?*
dtype0
9
Adam/epsilonConst*
valueB
 *wĢ+2*
dtype0
µ
4Adam/update_policy/encoder/hidden_0/kernel/ApplyAdam	ApplyAdampolicy/encoder/hidden_0/kernel#policy/encoder/hidden_0/kernel/Adam%policy/encoder/hidden_0/kernel/Adam_1beta1_power/readbeta2_power/readVariable_2/read
Adam/beta1
Adam/beta2Adam/epsilonHgradients/policy/encoder/hidden_0/MatMul_grad/tuple/control_dependency_1*
use_locking( *
T0*1
_class'
%#loc:@policy/encoder/hidden_0/kernel*
use_nesterov( 
¬
2Adam/update_policy/encoder/hidden_0/bias/ApplyAdam	ApplyAdampolicy/encoder/hidden_0/bias!policy/encoder/hidden_0/bias/Adam#policy/encoder/hidden_0/bias/Adam_1beta1_power/readbeta2_power/readVariable_2/read
Adam/beta1
Adam/beta2Adam/epsilonIgradients/policy/encoder/hidden_0/BiasAdd_grad/tuple/control_dependency_1*
use_locking( *
T0*/
_class%
#!loc:@policy/encoder/hidden_0/bias*
use_nesterov( 
µ
4Adam/update_policy/encoder/hidden_1/kernel/ApplyAdam	ApplyAdampolicy/encoder/hidden_1/kernel#policy/encoder/hidden_1/kernel/Adam%policy/encoder/hidden_1/kernel/Adam_1beta1_power/readbeta2_power/readVariable_2/read
Adam/beta1
Adam/beta2Adam/epsilonHgradients/policy/encoder/hidden_1/MatMul_grad/tuple/control_dependency_1*
use_locking( *
T0*1
_class'
%#loc:@policy/encoder/hidden_1/kernel*
use_nesterov( 
¬
2Adam/update_policy/encoder/hidden_1/bias/ApplyAdam	ApplyAdampolicy/encoder/hidden_1/bias!policy/encoder/hidden_1/bias/Adam#policy/encoder/hidden_1/bias/Adam_1beta1_power/readbeta2_power/readVariable_2/read
Adam/beta1
Adam/beta2Adam/epsilonIgradients/policy/encoder/hidden_1/BiasAdd_grad/tuple/control_dependency_1*
use_locking( *
T0*/
_class%
#!loc:@policy/encoder/hidden_1/bias*
use_nesterov( 
õ
)Adam/update_policy/dense/kernel/ApplyAdam	ApplyAdampolicy/dense/kernelpolicy/dense/kernel/Adampolicy/dense/kernel/Adam_1beta1_power/readbeta2_power/readVariable_2/read
Adam/beta1
Adam/beta2Adam/epsilon?gradients/policy_1/dense/MatMul_grad/tuple/control_dependency_1*
use_locking( *
T0*&
_class
loc:@policy/dense/kernel*
use_nesterov( 

+Adam/update_policy/dense_1/kernel/ApplyAdam	ApplyAdampolicy/dense_1/kernelpolicy/dense_1/kernel/Adampolicy/dense_1/kernel/Adam_1beta1_power/readbeta2_power/readVariable_2/read
Adam/beta1
Adam/beta2Adam/epsilonAgradients/policy_1/dense_1/MatMul_grad/tuple/control_dependency_1*
use_locking( *
T0*(
_class
loc:@policy/dense_1/kernel*
use_nesterov( 

+Adam/update_policy/dense_2/kernel/ApplyAdam	ApplyAdampolicy/dense_2/kernelpolicy/dense_2/kernel/Adampolicy/dense_2/kernel/Adam_1beta1_power/readbeta2_power/readVariable_2/read
Adam/beta1
Adam/beta2Adam/epsilonAgradients/policy_1/dense_2/MatMul_grad/tuple/control_dependency_1*
use_locking( *
T0*(
_class
loc:@policy/dense_2/kernel*
use_nesterov( 
¾
Adam/mulMulbeta1_power/read
Adam/beta1*^Adam/update_policy/dense/kernel/ApplyAdam,^Adam/update_policy/dense_1/kernel/ApplyAdam,^Adam/update_policy/dense_2/kernel/ApplyAdam3^Adam/update_policy/encoder/hidden_0/bias/ApplyAdam5^Adam/update_policy/encoder/hidden_0/kernel/ApplyAdam3^Adam/update_policy/encoder/hidden_1/bias/ApplyAdam5^Adam/update_policy/encoder/hidden_1/kernel/ApplyAdam*
T0*&
_class
loc:@policy/dense/kernel

Adam/AssignAssignbeta1_powerAdam/mul*
use_locking( *
T0*&
_class
loc:@policy/dense/kernel*
validate_shape(
Ą

Adam/mul_1Mulbeta2_power/read
Adam/beta2*^Adam/update_policy/dense/kernel/ApplyAdam,^Adam/update_policy/dense_1/kernel/ApplyAdam,^Adam/update_policy/dense_2/kernel/ApplyAdam3^Adam/update_policy/encoder/hidden_0/bias/ApplyAdam5^Adam/update_policy/encoder/hidden_0/kernel/ApplyAdam3^Adam/update_policy/encoder/hidden_1/bias/ApplyAdam5^Adam/update_policy/encoder/hidden_1/kernel/ApplyAdam*
T0*&
_class
loc:@policy/dense/kernel

Adam/Assign_1Assignbeta2_power
Adam/mul_1*
use_locking( *
T0*&
_class
loc:@policy/dense/kernel*
validate_shape(

AdamNoOp^Adam/Assign^Adam/Assign_1*^Adam/update_policy/dense/kernel/ApplyAdam,^Adam/update_policy/dense_1/kernel/ApplyAdam,^Adam/update_policy/dense_2/kernel/ApplyAdam3^Adam/update_policy/encoder/hidden_0/bias/ApplyAdam5^Adam/update_policy/encoder/hidden_0/kernel/ApplyAdam3^Adam/update_policy/encoder/hidden_1/bias/ApplyAdam5^Adam/update_policy/encoder/hidden_1/kernel/ApplyAdam
A
gradients_1/ShapeConst^Adam*
valueB *
dtype0
I
gradients_1/grad_ys_0Const^Adam*
valueB
 *  ?*
dtype0
]
gradients_1/FillFillgradients_1/Shapegradients_1/grad_ys_0*
T0*

index_type0
I
'gradients_1/add_9_grad/tuple/group_depsNoOp^Adam^gradients_1/Fill
„
/gradients_1/add_9_grad/tuple/control_dependencyIdentitygradients_1/Fill(^gradients_1/add_9_grad/tuple/group_deps*
T0*#
_class
loc:@gradients_1/Fill
§
1gradients_1/add_9_grad/tuple/control_dependency_1Identitygradients_1/Fill(^gradients_1/add_9_grad/tuple/group_deps*
T0*#
_class
loc:@gradients_1/Fill
h
'gradients_1/add_8_grad/tuple/group_depsNoOp^Adam0^gradients_1/add_9_grad/tuple/control_dependency
Ä
/gradients_1/add_8_grad/tuple/control_dependencyIdentity/gradients_1/add_9_grad/tuple/control_dependency(^gradients_1/add_8_grad/tuple/group_deps*
T0*#
_class
loc:@gradients_1/Fill
Ę
1gradients_1/add_8_grad/tuple/control_dependency_1Identity/gradients_1/add_9_grad/tuple/control_dependency(^gradients_1/add_8_grad/tuple/group_deps*
T0*#
_class
loc:@gradients_1/Fill
[
&gradients_1/Mean_21_grad/Reshape/shapeConst^Adam*
valueB:*
dtype0

 gradients_1/Mean_21_grad/ReshapeReshape1gradients_1/add_9_grad/tuple/control_dependency_1&gradients_1/Mean_21_grad/Reshape/shape*
T0*
Tshape0
S
gradients_1/Mean_21_grad/ConstConst^Adam*
valueB:*
dtype0

gradients_1/Mean_21_grad/TileTile gradients_1/Mean_21_grad/Reshapegradients_1/Mean_21_grad/Const*

Tmultiples0*
T0
T
 gradients_1/Mean_21_grad/Const_1Const^Adam*
valueB
 *   @*
dtype0
u
 gradients_1/Mean_21_grad/truedivRealDivgradients_1/Mean_21_grad/Tile gradients_1/Mean_21_grad/Const_1*
T0
[
&gradients_1/Mean_12_grad/Reshape/shapeConst^Adam*
valueB:*
dtype0

 gradients_1/Mean_12_grad/ReshapeReshape/gradients_1/add_8_grad/tuple/control_dependency&gradients_1/Mean_12_grad/Reshape/shape*
T0*
Tshape0
S
gradients_1/Mean_12_grad/ConstConst^Adam*
valueB:*
dtype0

gradients_1/Mean_12_grad/TileTile gradients_1/Mean_12_grad/Reshapegradients_1/Mean_12_grad/Const*

Tmultiples0*
T0
T
 gradients_1/Mean_12_grad/Const_1Const^Adam*
valueB
 *   @*
dtype0
u
 gradients_1/Mean_12_grad/truedivRealDivgradients_1/Mean_12_grad/Tile gradients_1/Mean_12_grad/Const_1*
T0
[
&gradients_1/Mean_13_grad/Reshape/shapeConst^Adam*
valueB:*
dtype0

 gradients_1/Mean_13_grad/ReshapeReshape1gradients_1/add_8_grad/tuple/control_dependency_1&gradients_1/Mean_13_grad/Reshape/shape*
T0*
Tshape0
S
gradients_1/Mean_13_grad/ConstConst^Adam*
valueB:*
dtype0

gradients_1/Mean_13_grad/TileTile gradients_1/Mean_13_grad/Reshapegradients_1/Mean_13_grad/Const*

Tmultiples0*
T0
T
 gradients_1/Mean_13_grad/Const_1Const^Adam*
valueB
 *   @*
dtype0
u
 gradients_1/Mean_13_grad/truedivRealDivgradients_1/Mean_13_grad/Tile gradients_1/Mean_13_grad/Const_1*
T0
r
&gradients_1/Mean_21/input_grad/unstackUnpack gradients_1/Mean_21_grad/truediv*
T0*	
num*

axis 
g
/gradients_1/Mean_21/input_grad/tuple/group_depsNoOp^Adam'^gradients_1/Mean_21/input_grad/unstack
į
7gradients_1/Mean_21/input_grad/tuple/control_dependencyIdentity&gradients_1/Mean_21/input_grad/unstack0^gradients_1/Mean_21/input_grad/tuple/group_deps*
T0*9
_class/
-+loc:@gradients_1/Mean_21/input_grad/unstack
å
9gradients_1/Mean_21/input_grad/tuple/control_dependency_1Identity(gradients_1/Mean_21/input_grad/unstack:10^gradients_1/Mean_21/input_grad/tuple/group_deps*
T0*9
_class/
-+loc:@gradients_1/Mean_21/input_grad/unstack
r
&gradients_1/Mean_12/input_grad/unstackUnpack gradients_1/Mean_12_grad/truediv*
T0*	
num*

axis 
g
/gradients_1/Mean_12/input_grad/tuple/group_depsNoOp^Adam'^gradients_1/Mean_12/input_grad/unstack
į
7gradients_1/Mean_12/input_grad/tuple/control_dependencyIdentity&gradients_1/Mean_12/input_grad/unstack0^gradients_1/Mean_12/input_grad/tuple/group_deps*
T0*9
_class/
-+loc:@gradients_1/Mean_12/input_grad/unstack
å
9gradients_1/Mean_12/input_grad/tuple/control_dependency_1Identity(gradients_1/Mean_12/input_grad/unstack:10^gradients_1/Mean_12/input_grad/tuple/group_deps*
T0*9
_class/
-+loc:@gradients_1/Mean_12/input_grad/unstack
r
&gradients_1/Mean_13/input_grad/unstackUnpack gradients_1/Mean_13_grad/truediv*
T0*	
num*

axis 
g
/gradients_1/Mean_13/input_grad/tuple/group_depsNoOp^Adam'^gradients_1/Mean_13/input_grad/unstack
į
7gradients_1/Mean_13/input_grad/tuple/control_dependencyIdentity&gradients_1/Mean_13/input_grad/unstack0^gradients_1/Mean_13/input_grad/tuple/group_deps*
T0*9
_class/
-+loc:@gradients_1/Mean_13/input_grad/unstack
å
9gradients_1/Mean_13/input_grad/tuple/control_dependency_1Identity(gradients_1/Mean_13/input_grad/unstack:10^gradients_1/Mean_13/input_grad/tuple/group_deps*
T0*9
_class/
-+loc:@gradients_1/Mean_13/input_grad/unstack
m
gradients_1/mul_33_grad/MulMul7gradients_1/Mean_21/input_grad/tuple/control_dependencyMean_18*
T0
p
gradients_1/mul_33_grad/Mul_1Mul7gradients_1/Mean_21/input_grad/tuple/control_dependencymul_33/x*
T0
u
(gradients_1/mul_33_grad/tuple/group_depsNoOp^Adam^gradients_1/mul_33_grad/Mul^gradients_1/mul_33_grad/Mul_1
½
0gradients_1/mul_33_grad/tuple/control_dependencyIdentitygradients_1/mul_33_grad/Mul)^gradients_1/mul_33_grad/tuple/group_deps*
T0*.
_class$
" loc:@gradients_1/mul_33_grad/Mul
Ć
2gradients_1/mul_33_grad/tuple/control_dependency_1Identitygradients_1/mul_33_grad/Mul_1)^gradients_1/mul_33_grad/tuple/group_deps*
T0*0
_class&
$"loc:@gradients_1/mul_33_grad/Mul_1
o
gradients_1/mul_35_grad/MulMul9gradients_1/Mean_21/input_grad/tuple/control_dependency_1Mean_20*
T0
r
gradients_1/mul_35_grad/Mul_1Mul9gradients_1/Mean_21/input_grad/tuple/control_dependency_1mul_35/x*
T0
u
(gradients_1/mul_35_grad/tuple/group_depsNoOp^Adam^gradients_1/mul_35_grad/Mul^gradients_1/mul_35_grad/Mul_1
½
0gradients_1/mul_35_grad/tuple/control_dependencyIdentitygradients_1/mul_35_grad/Mul)^gradients_1/mul_35_grad/tuple/group_deps*
T0*.
_class$
" loc:@gradients_1/mul_35_grad/Mul
Ć
2gradients_1/mul_35_grad/tuple/control_dependency_1Identitygradients_1/mul_35_grad/Mul_1)^gradients_1/mul_35_grad/tuple/group_deps*
T0*0
_class&
$"loc:@gradients_1/mul_35_grad/Mul_1
l
gradients_1/mul_10_grad/MulMul7gradients_1/Mean_12/input_grad/tuple/control_dependencyMean_6*
T0
p
gradients_1/mul_10_grad/Mul_1Mul7gradients_1/Mean_12/input_grad/tuple/control_dependencymul_10/x*
T0
u
(gradients_1/mul_10_grad/tuple/group_depsNoOp^Adam^gradients_1/mul_10_grad/Mul^gradients_1/mul_10_grad/Mul_1
½
0gradients_1/mul_10_grad/tuple/control_dependencyIdentitygradients_1/mul_10_grad/Mul)^gradients_1/mul_10_grad/tuple/group_deps*
T0*.
_class$
" loc:@gradients_1/mul_10_grad/Mul
Ć
2gradients_1/mul_10_grad/tuple/control_dependency_1Identitygradients_1/mul_10_grad/Mul_1)^gradients_1/mul_10_grad/tuple/group_deps*
T0*0
_class&
$"loc:@gradients_1/mul_10_grad/Mul_1
o
gradients_1/mul_19_grad/MulMul9gradients_1/Mean_12/input_grad/tuple/control_dependency_1Mean_10*
T0
r
gradients_1/mul_19_grad/Mul_1Mul9gradients_1/Mean_12/input_grad/tuple/control_dependency_1mul_19/x*
T0
u
(gradients_1/mul_19_grad/tuple/group_depsNoOp^Adam^gradients_1/mul_19_grad/Mul^gradients_1/mul_19_grad/Mul_1
½
0gradients_1/mul_19_grad/tuple/control_dependencyIdentitygradients_1/mul_19_grad/Mul)^gradients_1/mul_19_grad/tuple/group_deps*
T0*.
_class$
" loc:@gradients_1/mul_19_grad/Mul
Ć
2gradients_1/mul_19_grad/tuple/control_dependency_1Identitygradients_1/mul_19_grad/Mul_1)^gradients_1/mul_19_grad/tuple/group_deps*
T0*0
_class&
$"loc:@gradients_1/mul_19_grad/Mul_1
l
gradients_1/mul_12_grad/MulMul7gradients_1/Mean_13/input_grad/tuple/control_dependencyMean_7*
T0
p
gradients_1/mul_12_grad/Mul_1Mul7gradients_1/Mean_13/input_grad/tuple/control_dependencymul_12/x*
T0
u
(gradients_1/mul_12_grad/tuple/group_depsNoOp^Adam^gradients_1/mul_12_grad/Mul^gradients_1/mul_12_grad/Mul_1
½
0gradients_1/mul_12_grad/tuple/control_dependencyIdentitygradients_1/mul_12_grad/Mul)^gradients_1/mul_12_grad/tuple/group_deps*
T0*.
_class$
" loc:@gradients_1/mul_12_grad/Mul
Ć
2gradients_1/mul_12_grad/tuple/control_dependency_1Identitygradients_1/mul_12_grad/Mul_1)^gradients_1/mul_12_grad/tuple/group_deps*
T0*0
_class&
$"loc:@gradients_1/mul_12_grad/Mul_1
o
gradients_1/mul_21_grad/MulMul9gradients_1/Mean_13/input_grad/tuple/control_dependency_1Mean_11*
T0
r
gradients_1/mul_21_grad/Mul_1Mul9gradients_1/Mean_13/input_grad/tuple/control_dependency_1mul_21/x*
T0
u
(gradients_1/mul_21_grad/tuple/group_depsNoOp^Adam^gradients_1/mul_21_grad/Mul^gradients_1/mul_21_grad/Mul_1
½
0gradients_1/mul_21_grad/tuple/control_dependencyIdentitygradients_1/mul_21_grad/Mul)^gradients_1/mul_21_grad/tuple/group_deps*
T0*.
_class$
" loc:@gradients_1/mul_21_grad/Mul
Ć
2gradients_1/mul_21_grad/tuple/control_dependency_1Identitygradients_1/mul_21_grad/Mul_1)^gradients_1/mul_21_grad/tuple/group_deps*
T0*0
_class&
$"loc:@gradients_1/mul_21_grad/Mul_1
b
&gradients_1/Mean_18_grad/Reshape/shapeConst^Adam*
valueB"      *
dtype0

 gradients_1/Mean_18_grad/ReshapeReshape2gradients_1/mul_33_grad/tuple/control_dependency_1&gradients_1/Mean_18_grad/Reshape/shape*
T0*
Tshape0
O
gradients_1/Mean_18_grad/ShapeShapemul_32^Adam*
T0*
out_type0

gradients_1/Mean_18_grad/TileTile gradients_1/Mean_18_grad/Reshapegradients_1/Mean_18_grad/Shape*

Tmultiples0*
T0
Q
 gradients_1/Mean_18_grad/Shape_1Shapemul_32^Adam*
T0*
out_type0
P
 gradients_1/Mean_18_grad/Shape_2Const^Adam*
valueB *
dtype0
S
gradients_1/Mean_18_grad/ConstConst^Adam*
valueB: *
dtype0

gradients_1/Mean_18_grad/ProdProd gradients_1/Mean_18_grad/Shape_1gradients_1/Mean_18_grad/Const*

Tidx0*
	keep_dims( *
T0
U
 gradients_1/Mean_18_grad/Const_1Const^Adam*
valueB: *
dtype0

gradients_1/Mean_18_grad/Prod_1Prod gradients_1/Mean_18_grad/Shape_2 gradients_1/Mean_18_grad/Const_1*

Tidx0*
	keep_dims( *
T0
S
"gradients_1/Mean_18_grad/Maximum/yConst^Adam*
value	B :*
dtype0
y
 gradients_1/Mean_18_grad/MaximumMaximumgradients_1/Mean_18_grad/Prod_1"gradients_1/Mean_18_grad/Maximum/y*
T0
w
!gradients_1/Mean_18_grad/floordivFloorDivgradients_1/Mean_18_grad/Prod gradients_1/Mean_18_grad/Maximum*
T0
p
gradients_1/Mean_18_grad/CastCast!gradients_1/Mean_18_grad/floordiv*

SrcT0*
Truncate( *

DstT0
r
 gradients_1/Mean_18_grad/truedivRealDivgradients_1/Mean_18_grad/Tilegradients_1/Mean_18_grad/Cast*
T0
b
&gradients_1/Mean_20_grad/Reshape/shapeConst^Adam*
valueB"      *
dtype0

 gradients_1/Mean_20_grad/ReshapeReshape2gradients_1/mul_35_grad/tuple/control_dependency_1&gradients_1/Mean_20_grad/Reshape/shape*
T0*
Tshape0
O
gradients_1/Mean_20_grad/ShapeShapemul_34^Adam*
T0*
out_type0

gradients_1/Mean_20_grad/TileTile gradients_1/Mean_20_grad/Reshapegradients_1/Mean_20_grad/Shape*

Tmultiples0*
T0
Q
 gradients_1/Mean_20_grad/Shape_1Shapemul_34^Adam*
T0*
out_type0
P
 gradients_1/Mean_20_grad/Shape_2Const^Adam*
valueB *
dtype0
S
gradients_1/Mean_20_grad/ConstConst^Adam*
valueB: *
dtype0

gradients_1/Mean_20_grad/ProdProd gradients_1/Mean_20_grad/Shape_1gradients_1/Mean_20_grad/Const*

Tidx0*
	keep_dims( *
T0
U
 gradients_1/Mean_20_grad/Const_1Const^Adam*
valueB: *
dtype0

gradients_1/Mean_20_grad/Prod_1Prod gradients_1/Mean_20_grad/Shape_2 gradients_1/Mean_20_grad/Const_1*

Tidx0*
	keep_dims( *
T0
S
"gradients_1/Mean_20_grad/Maximum/yConst^Adam*
value	B :*
dtype0
y
 gradients_1/Mean_20_grad/MaximumMaximumgradients_1/Mean_20_grad/Prod_1"gradients_1/Mean_20_grad/Maximum/y*
T0
w
!gradients_1/Mean_20_grad/floordivFloorDivgradients_1/Mean_20_grad/Prod gradients_1/Mean_20_grad/Maximum*
T0
p
gradients_1/Mean_20_grad/CastCast!gradients_1/Mean_20_grad/floordiv*

SrcT0*
Truncate( *

DstT0
r
 gradients_1/Mean_20_grad/truedivRealDivgradients_1/Mean_20_grad/Tilegradients_1/Mean_20_grad/Cast*
T0
a
%gradients_1/Mean_6_grad/Reshape/shapeConst^Adam*
valueB"      *
dtype0

gradients_1/Mean_6_grad/ReshapeReshape2gradients_1/mul_10_grad/tuple/control_dependency_1%gradients_1/Mean_6_grad/Reshape/shape*
T0*
Tshape0
M
gradients_1/Mean_6_grad/ShapeShapemul_9^Adam*
T0*
out_type0

gradients_1/Mean_6_grad/TileTilegradients_1/Mean_6_grad/Reshapegradients_1/Mean_6_grad/Shape*

Tmultiples0*
T0
O
gradients_1/Mean_6_grad/Shape_1Shapemul_9^Adam*
T0*
out_type0
O
gradients_1/Mean_6_grad/Shape_2Const^Adam*
valueB *
dtype0
R
gradients_1/Mean_6_grad/ConstConst^Adam*
valueB: *
dtype0

gradients_1/Mean_6_grad/ProdProdgradients_1/Mean_6_grad/Shape_1gradients_1/Mean_6_grad/Const*

Tidx0*
	keep_dims( *
T0
T
gradients_1/Mean_6_grad/Const_1Const^Adam*
valueB: *
dtype0

gradients_1/Mean_6_grad/Prod_1Prodgradients_1/Mean_6_grad/Shape_2gradients_1/Mean_6_grad/Const_1*

Tidx0*
	keep_dims( *
T0
R
!gradients_1/Mean_6_grad/Maximum/yConst^Adam*
value	B :*
dtype0
v
gradients_1/Mean_6_grad/MaximumMaximumgradients_1/Mean_6_grad/Prod_1!gradients_1/Mean_6_grad/Maximum/y*
T0
t
 gradients_1/Mean_6_grad/floordivFloorDivgradients_1/Mean_6_grad/Prodgradients_1/Mean_6_grad/Maximum*
T0
n
gradients_1/Mean_6_grad/CastCast gradients_1/Mean_6_grad/floordiv*

SrcT0*
Truncate( *

DstT0
o
gradients_1/Mean_6_grad/truedivRealDivgradients_1/Mean_6_grad/Tilegradients_1/Mean_6_grad/Cast*
T0
b
&gradients_1/Mean_10_grad/Reshape/shapeConst^Adam*
valueB"      *
dtype0

 gradients_1/Mean_10_grad/ReshapeReshape2gradients_1/mul_19_grad/tuple/control_dependency_1&gradients_1/Mean_10_grad/Reshape/shape*
T0*
Tshape0
O
gradients_1/Mean_10_grad/ShapeShapemul_18^Adam*
T0*
out_type0

gradients_1/Mean_10_grad/TileTile gradients_1/Mean_10_grad/Reshapegradients_1/Mean_10_grad/Shape*

Tmultiples0*
T0
Q
 gradients_1/Mean_10_grad/Shape_1Shapemul_18^Adam*
T0*
out_type0
P
 gradients_1/Mean_10_grad/Shape_2Const^Adam*
valueB *
dtype0
S
gradients_1/Mean_10_grad/ConstConst^Adam*
valueB: *
dtype0

gradients_1/Mean_10_grad/ProdProd gradients_1/Mean_10_grad/Shape_1gradients_1/Mean_10_grad/Const*

Tidx0*
	keep_dims( *
T0
U
 gradients_1/Mean_10_grad/Const_1Const^Adam*
valueB: *
dtype0

gradients_1/Mean_10_grad/Prod_1Prod gradients_1/Mean_10_grad/Shape_2 gradients_1/Mean_10_grad/Const_1*

Tidx0*
	keep_dims( *
T0
S
"gradients_1/Mean_10_grad/Maximum/yConst^Adam*
value	B :*
dtype0
y
 gradients_1/Mean_10_grad/MaximumMaximumgradients_1/Mean_10_grad/Prod_1"gradients_1/Mean_10_grad/Maximum/y*
T0
w
!gradients_1/Mean_10_grad/floordivFloorDivgradients_1/Mean_10_grad/Prod gradients_1/Mean_10_grad/Maximum*
T0
p
gradients_1/Mean_10_grad/CastCast!gradients_1/Mean_10_grad/floordiv*

SrcT0*
Truncate( *

DstT0
r
 gradients_1/Mean_10_grad/truedivRealDivgradients_1/Mean_10_grad/Tilegradients_1/Mean_10_grad/Cast*
T0
a
%gradients_1/Mean_7_grad/Reshape/shapeConst^Adam*
valueB"      *
dtype0

gradients_1/Mean_7_grad/ReshapeReshape2gradients_1/mul_12_grad/tuple/control_dependency_1%gradients_1/Mean_7_grad/Reshape/shape*
T0*
Tshape0
N
gradients_1/Mean_7_grad/ShapeShapemul_11^Adam*
T0*
out_type0

gradients_1/Mean_7_grad/TileTilegradients_1/Mean_7_grad/Reshapegradients_1/Mean_7_grad/Shape*

Tmultiples0*
T0
P
gradients_1/Mean_7_grad/Shape_1Shapemul_11^Adam*
T0*
out_type0
O
gradients_1/Mean_7_grad/Shape_2Const^Adam*
valueB *
dtype0
R
gradients_1/Mean_7_grad/ConstConst^Adam*
valueB: *
dtype0

gradients_1/Mean_7_grad/ProdProdgradients_1/Mean_7_grad/Shape_1gradients_1/Mean_7_grad/Const*

Tidx0*
	keep_dims( *
T0
T
gradients_1/Mean_7_grad/Const_1Const^Adam*
valueB: *
dtype0

gradients_1/Mean_7_grad/Prod_1Prodgradients_1/Mean_7_grad/Shape_2gradients_1/Mean_7_grad/Const_1*

Tidx0*
	keep_dims( *
T0
R
!gradients_1/Mean_7_grad/Maximum/yConst^Adam*
value	B :*
dtype0
v
gradients_1/Mean_7_grad/MaximumMaximumgradients_1/Mean_7_grad/Prod_1!gradients_1/Mean_7_grad/Maximum/y*
T0
t
 gradients_1/Mean_7_grad/floordivFloorDivgradients_1/Mean_7_grad/Prodgradients_1/Mean_7_grad/Maximum*
T0
n
gradients_1/Mean_7_grad/CastCast gradients_1/Mean_7_grad/floordiv*

SrcT0*
Truncate( *

DstT0
o
gradients_1/Mean_7_grad/truedivRealDivgradients_1/Mean_7_grad/Tilegradients_1/Mean_7_grad/Cast*
T0
b
&gradients_1/Mean_11_grad/Reshape/shapeConst^Adam*
valueB"      *
dtype0

 gradients_1/Mean_11_grad/ReshapeReshape2gradients_1/mul_21_grad/tuple/control_dependency_1&gradients_1/Mean_11_grad/Reshape/shape*
T0*
Tshape0
O
gradients_1/Mean_11_grad/ShapeShapemul_20^Adam*
T0*
out_type0

gradients_1/Mean_11_grad/TileTile gradients_1/Mean_11_grad/Reshapegradients_1/Mean_11_grad/Shape*

Tmultiples0*
T0
Q
 gradients_1/Mean_11_grad/Shape_1Shapemul_20^Adam*
T0*
out_type0
P
 gradients_1/Mean_11_grad/Shape_2Const^Adam*
valueB *
dtype0
S
gradients_1/Mean_11_grad/ConstConst^Adam*
valueB: *
dtype0

gradients_1/Mean_11_grad/ProdProd gradients_1/Mean_11_grad/Shape_1gradients_1/Mean_11_grad/Const*

Tidx0*
	keep_dims( *
T0
U
 gradients_1/Mean_11_grad/Const_1Const^Adam*
valueB: *
dtype0

gradients_1/Mean_11_grad/Prod_1Prod gradients_1/Mean_11_grad/Shape_2 gradients_1/Mean_11_grad/Const_1*

Tidx0*
	keep_dims( *
T0
S
"gradients_1/Mean_11_grad/Maximum/yConst^Adam*
value	B :*
dtype0
y
 gradients_1/Mean_11_grad/MaximumMaximumgradients_1/Mean_11_grad/Prod_1"gradients_1/Mean_11_grad/Maximum/y*
T0
w
!gradients_1/Mean_11_grad/floordivFloorDivgradients_1/Mean_11_grad/Prod gradients_1/Mean_11_grad/Maximum*
T0
p
gradients_1/Mean_11_grad/CastCast!gradients_1/Mean_11_grad/floordiv*

SrcT0*
Truncate( *

DstT0
r
 gradients_1/Mean_11_grad/truedivRealDivgradients_1/Mean_11_grad/Tilegradients_1/Mean_11_grad/Cast*
T0
Q
gradients_1/mul_32_grad/ShapeShape	ToFloat_6^Adam*
T0*
out_type0
]
gradients_1/mul_32_grad/Shape_1ShapeSquaredDifference_4^Adam*
T0*
out_type0

-gradients_1/mul_32_grad/BroadcastGradientArgsBroadcastGradientArgsgradients_1/mul_32_grad/Shapegradients_1/mul_32_grad/Shape_1*
T0
b
gradients_1/mul_32_grad/MulMul gradients_1/Mean_18_grad/truedivSquaredDifference_4*
T0

gradients_1/mul_32_grad/SumSumgradients_1/mul_32_grad/Mul-gradients_1/mul_32_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0
}
gradients_1/mul_32_grad/ReshapeReshapegradients_1/mul_32_grad/Sumgradients_1/mul_32_grad/Shape*
T0*
Tshape0
Z
gradients_1/mul_32_grad/Mul_1Mul	ToFloat_6 gradients_1/Mean_18_grad/truediv*
T0

gradients_1/mul_32_grad/Sum_1Sumgradients_1/mul_32_grad/Mul_1/gradients_1/mul_32_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0

!gradients_1/mul_32_grad/Reshape_1Reshapegradients_1/mul_32_grad/Sum_1gradients_1/mul_32_grad/Shape_1*
T0*
Tshape0
}
(gradients_1/mul_32_grad/tuple/group_depsNoOp^Adam ^gradients_1/mul_32_grad/Reshape"^gradients_1/mul_32_grad/Reshape_1
Å
0gradients_1/mul_32_grad/tuple/control_dependencyIdentitygradients_1/mul_32_grad/Reshape)^gradients_1/mul_32_grad/tuple/group_deps*
T0*2
_class(
&$loc:@gradients_1/mul_32_grad/Reshape
Ė
2gradients_1/mul_32_grad/tuple/control_dependency_1Identity!gradients_1/mul_32_grad/Reshape_1)^gradients_1/mul_32_grad/tuple/group_deps*
T0*4
_class*
(&loc:@gradients_1/mul_32_grad/Reshape_1
Q
gradients_1/mul_34_grad/ShapeShape	ToFloat_7^Adam*
T0*
out_type0
]
gradients_1/mul_34_grad/Shape_1ShapeSquaredDifference_5^Adam*
T0*
out_type0

-gradients_1/mul_34_grad/BroadcastGradientArgsBroadcastGradientArgsgradients_1/mul_34_grad/Shapegradients_1/mul_34_grad/Shape_1*
T0
b
gradients_1/mul_34_grad/MulMul gradients_1/Mean_20_grad/truedivSquaredDifference_5*
T0

gradients_1/mul_34_grad/SumSumgradients_1/mul_34_grad/Mul-gradients_1/mul_34_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0
}
gradients_1/mul_34_grad/ReshapeReshapegradients_1/mul_34_grad/Sumgradients_1/mul_34_grad/Shape*
T0*
Tshape0
Z
gradients_1/mul_34_grad/Mul_1Mul	ToFloat_7 gradients_1/Mean_20_grad/truediv*
T0

gradients_1/mul_34_grad/Sum_1Sumgradients_1/mul_34_grad/Mul_1/gradients_1/mul_34_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0

!gradients_1/mul_34_grad/Reshape_1Reshapegradients_1/mul_34_grad/Sum_1gradients_1/mul_34_grad/Shape_1*
T0*
Tshape0
}
(gradients_1/mul_34_grad/tuple/group_depsNoOp^Adam ^gradients_1/mul_34_grad/Reshape"^gradients_1/mul_34_grad/Reshape_1
Å
0gradients_1/mul_34_grad/tuple/control_dependencyIdentitygradients_1/mul_34_grad/Reshape)^gradients_1/mul_34_grad/tuple/group_deps*
T0*2
_class(
&$loc:@gradients_1/mul_34_grad/Reshape
Ė
2gradients_1/mul_34_grad/tuple/control_dependency_1Identity!gradients_1/mul_34_grad/Reshape_1)^gradients_1/mul_34_grad/tuple/group_deps*
T0*4
_class*
(&loc:@gradients_1/mul_34_grad/Reshape_1
N
gradients_1/mul_9_grad/ShapeShapeToFloat^Adam*
T0*
out_type0
Z
gradients_1/mul_9_grad/Shape_1ShapeSquaredDifference^Adam*
T0*
out_type0

,gradients_1/mul_9_grad/BroadcastGradientArgsBroadcastGradientArgsgradients_1/mul_9_grad/Shapegradients_1/mul_9_grad/Shape_1*
T0
^
gradients_1/mul_9_grad/MulMulgradients_1/Mean_6_grad/truedivSquaredDifference*
T0

gradients_1/mul_9_grad/SumSumgradients_1/mul_9_grad/Mul,gradients_1/mul_9_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0
z
gradients_1/mul_9_grad/ReshapeReshapegradients_1/mul_9_grad/Sumgradients_1/mul_9_grad/Shape*
T0*
Tshape0
V
gradients_1/mul_9_grad/Mul_1MulToFloatgradients_1/Mean_6_grad/truediv*
T0

gradients_1/mul_9_grad/Sum_1Sumgradients_1/mul_9_grad/Mul_1.gradients_1/mul_9_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0

 gradients_1/mul_9_grad/Reshape_1Reshapegradients_1/mul_9_grad/Sum_1gradients_1/mul_9_grad/Shape_1*
T0*
Tshape0
z
'gradients_1/mul_9_grad/tuple/group_depsNoOp^Adam^gradients_1/mul_9_grad/Reshape!^gradients_1/mul_9_grad/Reshape_1
Į
/gradients_1/mul_9_grad/tuple/control_dependencyIdentitygradients_1/mul_9_grad/Reshape(^gradients_1/mul_9_grad/tuple/group_deps*
T0*1
_class'
%#loc:@gradients_1/mul_9_grad/Reshape
Ē
1gradients_1/mul_9_grad/tuple/control_dependency_1Identity gradients_1/mul_9_grad/Reshape_1(^gradients_1/mul_9_grad/tuple/group_deps*
T0*3
_class)
'%loc:@gradients_1/mul_9_grad/Reshape_1
Q
gradients_1/mul_18_grad/ShapeShape	ToFloat_2^Adam*
T0*
out_type0
]
gradients_1/mul_18_grad/Shape_1ShapeSquaredDifference_2^Adam*
T0*
out_type0

-gradients_1/mul_18_grad/BroadcastGradientArgsBroadcastGradientArgsgradients_1/mul_18_grad/Shapegradients_1/mul_18_grad/Shape_1*
T0
b
gradients_1/mul_18_grad/MulMul gradients_1/Mean_10_grad/truedivSquaredDifference_2*
T0

gradients_1/mul_18_grad/SumSumgradients_1/mul_18_grad/Mul-gradients_1/mul_18_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0
}
gradients_1/mul_18_grad/ReshapeReshapegradients_1/mul_18_grad/Sumgradients_1/mul_18_grad/Shape*
T0*
Tshape0
Z
gradients_1/mul_18_grad/Mul_1Mul	ToFloat_2 gradients_1/Mean_10_grad/truediv*
T0

gradients_1/mul_18_grad/Sum_1Sumgradients_1/mul_18_grad/Mul_1/gradients_1/mul_18_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0

!gradients_1/mul_18_grad/Reshape_1Reshapegradients_1/mul_18_grad/Sum_1gradients_1/mul_18_grad/Shape_1*
T0*
Tshape0
}
(gradients_1/mul_18_grad/tuple/group_depsNoOp^Adam ^gradients_1/mul_18_grad/Reshape"^gradients_1/mul_18_grad/Reshape_1
Å
0gradients_1/mul_18_grad/tuple/control_dependencyIdentitygradients_1/mul_18_grad/Reshape)^gradients_1/mul_18_grad/tuple/group_deps*
T0*2
_class(
&$loc:@gradients_1/mul_18_grad/Reshape
Ė
2gradients_1/mul_18_grad/tuple/control_dependency_1Identity!gradients_1/mul_18_grad/Reshape_1)^gradients_1/mul_18_grad/tuple/group_deps*
T0*4
_class*
(&loc:@gradients_1/mul_18_grad/Reshape_1
Q
gradients_1/mul_11_grad/ShapeShape	ToFloat_1^Adam*
T0*
out_type0
]
gradients_1/mul_11_grad/Shape_1ShapeSquaredDifference_1^Adam*
T0*
out_type0

-gradients_1/mul_11_grad/BroadcastGradientArgsBroadcastGradientArgsgradients_1/mul_11_grad/Shapegradients_1/mul_11_grad/Shape_1*
T0
a
gradients_1/mul_11_grad/MulMulgradients_1/Mean_7_grad/truedivSquaredDifference_1*
T0

gradients_1/mul_11_grad/SumSumgradients_1/mul_11_grad/Mul-gradients_1/mul_11_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0
}
gradients_1/mul_11_grad/ReshapeReshapegradients_1/mul_11_grad/Sumgradients_1/mul_11_grad/Shape*
T0*
Tshape0
Y
gradients_1/mul_11_grad/Mul_1Mul	ToFloat_1gradients_1/Mean_7_grad/truediv*
T0

gradients_1/mul_11_grad/Sum_1Sumgradients_1/mul_11_grad/Mul_1/gradients_1/mul_11_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0

!gradients_1/mul_11_grad/Reshape_1Reshapegradients_1/mul_11_grad/Sum_1gradients_1/mul_11_grad/Shape_1*
T0*
Tshape0
}
(gradients_1/mul_11_grad/tuple/group_depsNoOp^Adam ^gradients_1/mul_11_grad/Reshape"^gradients_1/mul_11_grad/Reshape_1
Å
0gradients_1/mul_11_grad/tuple/control_dependencyIdentitygradients_1/mul_11_grad/Reshape)^gradients_1/mul_11_grad/tuple/group_deps*
T0*2
_class(
&$loc:@gradients_1/mul_11_grad/Reshape
Ė
2gradients_1/mul_11_grad/tuple/control_dependency_1Identity!gradients_1/mul_11_grad/Reshape_1)^gradients_1/mul_11_grad/tuple/group_deps*
T0*4
_class*
(&loc:@gradients_1/mul_11_grad/Reshape_1
Q
gradients_1/mul_20_grad/ShapeShape	ToFloat_3^Adam*
T0*
out_type0
]
gradients_1/mul_20_grad/Shape_1ShapeSquaredDifference_3^Adam*
T0*
out_type0

-gradients_1/mul_20_grad/BroadcastGradientArgsBroadcastGradientArgsgradients_1/mul_20_grad/Shapegradients_1/mul_20_grad/Shape_1*
T0
b
gradients_1/mul_20_grad/MulMul gradients_1/Mean_11_grad/truedivSquaredDifference_3*
T0

gradients_1/mul_20_grad/SumSumgradients_1/mul_20_grad/Mul-gradients_1/mul_20_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0
}
gradients_1/mul_20_grad/ReshapeReshapegradients_1/mul_20_grad/Sumgradients_1/mul_20_grad/Shape*
T0*
Tshape0
Z
gradients_1/mul_20_grad/Mul_1Mul	ToFloat_3 gradients_1/Mean_11_grad/truediv*
T0

gradients_1/mul_20_grad/Sum_1Sumgradients_1/mul_20_grad/Mul_1/gradients_1/mul_20_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0

!gradients_1/mul_20_grad/Reshape_1Reshapegradients_1/mul_20_grad/Sum_1gradients_1/mul_20_grad/Shape_1*
T0*
Tshape0
}
(gradients_1/mul_20_grad/tuple/group_depsNoOp^Adam ^gradients_1/mul_20_grad/Reshape"^gradients_1/mul_20_grad/Reshape_1
Å
0gradients_1/mul_20_grad/tuple/control_dependencyIdentitygradients_1/mul_20_grad/Reshape)^gradients_1/mul_20_grad/tuple/group_deps*
T0*2
_class(
&$loc:@gradients_1/mul_20_grad/Reshape
Ė
2gradients_1/mul_20_grad/tuple/control_dependency_1Identity!gradients_1/mul_20_grad/Reshape_1)^gradients_1/mul_20_grad/tuple/group_deps*
T0*4
_class*
(&loc:@gradients_1/mul_20_grad/Reshape_1

+gradients_1/SquaredDifference_4_grad/scalarConst^Adam3^gradients_1/mul_32_grad/tuple/control_dependency_1*
valueB
 *   @*
dtype0

(gradients_1/SquaredDifference_4_grad/MulMul+gradients_1/SquaredDifference_4_grad/scalar2gradients_1/mul_32_grad/tuple/control_dependency_1*
T0
Ŗ
(gradients_1/SquaredDifference_4_grad/subSub$critic/value/extrinsic_value/BiasAddStopGradient_3^Adam3^gradients_1/mul_32_grad/tuple/control_dependency_1*
T0

*gradients_1/SquaredDifference_4_grad/mul_1Mul(gradients_1/SquaredDifference_4_grad/Mul(gradients_1/SquaredDifference_4_grad/sub*
T0
y
*gradients_1/SquaredDifference_4_grad/ShapeShape$critic/value/extrinsic_value/BiasAdd^Adam*
T0*
out_type0
e
,gradients_1/SquaredDifference_4_grad/Shape_1ShapeStopGradient_3^Adam*
T0*
out_type0
¶
:gradients_1/SquaredDifference_4_grad/BroadcastGradientArgsBroadcastGradientArgs*gradients_1/SquaredDifference_4_grad/Shape,gradients_1/SquaredDifference_4_grad/Shape_1*
T0
½
(gradients_1/SquaredDifference_4_grad/SumSum*gradients_1/SquaredDifference_4_grad/mul_1:gradients_1/SquaredDifference_4_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0
¤
,gradients_1/SquaredDifference_4_grad/ReshapeReshape(gradients_1/SquaredDifference_4_grad/Sum*gradients_1/SquaredDifference_4_grad/Shape*
T0*
Tshape0
Į
*gradients_1/SquaredDifference_4_grad/Sum_1Sum*gradients_1/SquaredDifference_4_grad/mul_1<gradients_1/SquaredDifference_4_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0
Ŗ
.gradients_1/SquaredDifference_4_grad/Reshape_1Reshape*gradients_1/SquaredDifference_4_grad/Sum_1,gradients_1/SquaredDifference_4_grad/Shape_1*
T0*
Tshape0
h
(gradients_1/SquaredDifference_4_grad/NegNeg.gradients_1/SquaredDifference_4_grad/Reshape_1*
T0

5gradients_1/SquaredDifference_4_grad/tuple/group_depsNoOp^Adam)^gradients_1/SquaredDifference_4_grad/Neg-^gradients_1/SquaredDifference_4_grad/Reshape
ł
=gradients_1/SquaredDifference_4_grad/tuple/control_dependencyIdentity,gradients_1/SquaredDifference_4_grad/Reshape6^gradients_1/SquaredDifference_4_grad/tuple/group_deps*
T0*?
_class5
31loc:@gradients_1/SquaredDifference_4_grad/Reshape
ó
?gradients_1/SquaredDifference_4_grad/tuple/control_dependency_1Identity(gradients_1/SquaredDifference_4_grad/Neg6^gradients_1/SquaredDifference_4_grad/tuple/group_deps*
T0*;
_class1
/-loc:@gradients_1/SquaredDifference_4_grad/Neg

+gradients_1/SquaredDifference_5_grad/scalarConst^Adam3^gradients_1/mul_34_grad/tuple/control_dependency_1*
valueB
 *   @*
dtype0

(gradients_1/SquaredDifference_5_grad/MulMul+gradients_1/SquaredDifference_5_grad/scalar2gradients_1/mul_34_grad/tuple/control_dependency_1*
T0
„
(gradients_1/SquaredDifference_5_grad/subSubcritic/value/gail_value/BiasAddStopGradient_4^Adam3^gradients_1/mul_34_grad/tuple/control_dependency_1*
T0

*gradients_1/SquaredDifference_5_grad/mul_1Mul(gradients_1/SquaredDifference_5_grad/Mul(gradients_1/SquaredDifference_5_grad/sub*
T0
t
*gradients_1/SquaredDifference_5_grad/ShapeShapecritic/value/gail_value/BiasAdd^Adam*
T0*
out_type0
e
,gradients_1/SquaredDifference_5_grad/Shape_1ShapeStopGradient_4^Adam*
T0*
out_type0
¶
:gradients_1/SquaredDifference_5_grad/BroadcastGradientArgsBroadcastGradientArgs*gradients_1/SquaredDifference_5_grad/Shape,gradients_1/SquaredDifference_5_grad/Shape_1*
T0
½
(gradients_1/SquaredDifference_5_grad/SumSum*gradients_1/SquaredDifference_5_grad/mul_1:gradients_1/SquaredDifference_5_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0
¤
,gradients_1/SquaredDifference_5_grad/ReshapeReshape(gradients_1/SquaredDifference_5_grad/Sum*gradients_1/SquaredDifference_5_grad/Shape*
T0*
Tshape0
Į
*gradients_1/SquaredDifference_5_grad/Sum_1Sum*gradients_1/SquaredDifference_5_grad/mul_1<gradients_1/SquaredDifference_5_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0
Ŗ
.gradients_1/SquaredDifference_5_grad/Reshape_1Reshape*gradients_1/SquaredDifference_5_grad/Sum_1,gradients_1/SquaredDifference_5_grad/Shape_1*
T0*
Tshape0
h
(gradients_1/SquaredDifference_5_grad/NegNeg.gradients_1/SquaredDifference_5_grad/Reshape_1*
T0

5gradients_1/SquaredDifference_5_grad/tuple/group_depsNoOp^Adam)^gradients_1/SquaredDifference_5_grad/Neg-^gradients_1/SquaredDifference_5_grad/Reshape
ł
=gradients_1/SquaredDifference_5_grad/tuple/control_dependencyIdentity,gradients_1/SquaredDifference_5_grad/Reshape6^gradients_1/SquaredDifference_5_grad/tuple/group_deps*
T0*?
_class5
31loc:@gradients_1/SquaredDifference_5_grad/Reshape
ó
?gradients_1/SquaredDifference_5_grad/tuple/control_dependency_1Identity(gradients_1/SquaredDifference_5_grad/Neg6^gradients_1/SquaredDifference_5_grad/tuple/group_deps*
T0*;
_class1
/-loc:@gradients_1/SquaredDifference_5_grad/Neg

)gradients_1/SquaredDifference_grad/scalarConst^Adam2^gradients_1/mul_9_grad/tuple/control_dependency_1*
valueB
 *   @*
dtype0

&gradients_1/SquaredDifference_grad/MulMul)gradients_1/SquaredDifference_grad/scalar1gradients_1/mul_9_grad/tuple/control_dependency_1*
T0

&gradients_1/SquaredDifference_grad/subSubStopGradientMean_4^Adam2^gradients_1/mul_9_grad/tuple/control_dependency_1*
T0

(gradients_1/SquaredDifference_grad/mul_1Mul&gradients_1/SquaredDifference_grad/Mul&gradients_1/SquaredDifference_grad/sub*
T0
_
(gradients_1/SquaredDifference_grad/ShapeShapeStopGradient^Adam*
T0*
out_type0
[
*gradients_1/SquaredDifference_grad/Shape_1ShapeMean_4^Adam*
T0*
out_type0
°
8gradients_1/SquaredDifference_grad/BroadcastGradientArgsBroadcastGradientArgs(gradients_1/SquaredDifference_grad/Shape*gradients_1/SquaredDifference_grad/Shape_1*
T0
·
&gradients_1/SquaredDifference_grad/SumSum(gradients_1/SquaredDifference_grad/mul_18gradients_1/SquaredDifference_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0

*gradients_1/SquaredDifference_grad/ReshapeReshape&gradients_1/SquaredDifference_grad/Sum(gradients_1/SquaredDifference_grad/Shape*
T0*
Tshape0
»
(gradients_1/SquaredDifference_grad/Sum_1Sum(gradients_1/SquaredDifference_grad/mul_1:gradients_1/SquaredDifference_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0
¤
,gradients_1/SquaredDifference_grad/Reshape_1Reshape(gradients_1/SquaredDifference_grad/Sum_1*gradients_1/SquaredDifference_grad/Shape_1*
T0*
Tshape0
d
&gradients_1/SquaredDifference_grad/NegNeg,gradients_1/SquaredDifference_grad/Reshape_1*
T0

3gradients_1/SquaredDifference_grad/tuple/group_depsNoOp^Adam'^gradients_1/SquaredDifference_grad/Neg+^gradients_1/SquaredDifference_grad/Reshape
ń
;gradients_1/SquaredDifference_grad/tuple/control_dependencyIdentity*gradients_1/SquaredDifference_grad/Reshape4^gradients_1/SquaredDifference_grad/tuple/group_deps*
T0*=
_class3
1/loc:@gradients_1/SquaredDifference_grad/Reshape
ė
=gradients_1/SquaredDifference_grad/tuple/control_dependency_1Identity&gradients_1/SquaredDifference_grad/Neg4^gradients_1/SquaredDifference_grad/tuple/group_deps*
T0*9
_class/
-+loc:@gradients_1/SquaredDifference_grad/Neg

+gradients_1/SquaredDifference_2_grad/scalarConst^Adam3^gradients_1/mul_18_grad/tuple/control_dependency_1*
valueB
 *   @*
dtype0

(gradients_1/SquaredDifference_2_grad/MulMul+gradients_1/SquaredDifference_2_grad/scalar2gradients_1/mul_18_grad/tuple/control_dependency_1*
T0

(gradients_1/SquaredDifference_2_grad/subSubStopGradient_1Mean_8^Adam3^gradients_1/mul_18_grad/tuple/control_dependency_1*
T0

*gradients_1/SquaredDifference_2_grad/mul_1Mul(gradients_1/SquaredDifference_2_grad/Mul(gradients_1/SquaredDifference_2_grad/sub*
T0
c
*gradients_1/SquaredDifference_2_grad/ShapeShapeStopGradient_1^Adam*
T0*
out_type0
]
,gradients_1/SquaredDifference_2_grad/Shape_1ShapeMean_8^Adam*
T0*
out_type0
¶
:gradients_1/SquaredDifference_2_grad/BroadcastGradientArgsBroadcastGradientArgs*gradients_1/SquaredDifference_2_grad/Shape,gradients_1/SquaredDifference_2_grad/Shape_1*
T0
½
(gradients_1/SquaredDifference_2_grad/SumSum*gradients_1/SquaredDifference_2_grad/mul_1:gradients_1/SquaredDifference_2_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0
¤
,gradients_1/SquaredDifference_2_grad/ReshapeReshape(gradients_1/SquaredDifference_2_grad/Sum*gradients_1/SquaredDifference_2_grad/Shape*
T0*
Tshape0
Į
*gradients_1/SquaredDifference_2_grad/Sum_1Sum*gradients_1/SquaredDifference_2_grad/mul_1<gradients_1/SquaredDifference_2_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0
Ŗ
.gradients_1/SquaredDifference_2_grad/Reshape_1Reshape*gradients_1/SquaredDifference_2_grad/Sum_1,gradients_1/SquaredDifference_2_grad/Shape_1*
T0*
Tshape0
h
(gradients_1/SquaredDifference_2_grad/NegNeg.gradients_1/SquaredDifference_2_grad/Reshape_1*
T0

5gradients_1/SquaredDifference_2_grad/tuple/group_depsNoOp^Adam)^gradients_1/SquaredDifference_2_grad/Neg-^gradients_1/SquaredDifference_2_grad/Reshape
ł
=gradients_1/SquaredDifference_2_grad/tuple/control_dependencyIdentity,gradients_1/SquaredDifference_2_grad/Reshape6^gradients_1/SquaredDifference_2_grad/tuple/group_deps*
T0*?
_class5
31loc:@gradients_1/SquaredDifference_2_grad/Reshape
ó
?gradients_1/SquaredDifference_2_grad/tuple/control_dependency_1Identity(gradients_1/SquaredDifference_2_grad/Neg6^gradients_1/SquaredDifference_2_grad/tuple/group_deps*
T0*;
_class1
/-loc:@gradients_1/SquaredDifference_2_grad/Neg

+gradients_1/SquaredDifference_1_grad/scalarConst^Adam3^gradients_1/mul_11_grad/tuple/control_dependency_1*
valueB
 *   @*
dtype0

(gradients_1/SquaredDifference_1_grad/MulMul+gradients_1/SquaredDifference_1_grad/scalar2gradients_1/mul_11_grad/tuple/control_dependency_1*
T0

(gradients_1/SquaredDifference_1_grad/subSubStopGradientMean_5^Adam3^gradients_1/mul_11_grad/tuple/control_dependency_1*
T0

*gradients_1/SquaredDifference_1_grad/mul_1Mul(gradients_1/SquaredDifference_1_grad/Mul(gradients_1/SquaredDifference_1_grad/sub*
T0
a
*gradients_1/SquaredDifference_1_grad/ShapeShapeStopGradient^Adam*
T0*
out_type0
]
,gradients_1/SquaredDifference_1_grad/Shape_1ShapeMean_5^Adam*
T0*
out_type0
¶
:gradients_1/SquaredDifference_1_grad/BroadcastGradientArgsBroadcastGradientArgs*gradients_1/SquaredDifference_1_grad/Shape,gradients_1/SquaredDifference_1_grad/Shape_1*
T0
½
(gradients_1/SquaredDifference_1_grad/SumSum*gradients_1/SquaredDifference_1_grad/mul_1:gradients_1/SquaredDifference_1_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0
¤
,gradients_1/SquaredDifference_1_grad/ReshapeReshape(gradients_1/SquaredDifference_1_grad/Sum*gradients_1/SquaredDifference_1_grad/Shape*
T0*
Tshape0
Į
*gradients_1/SquaredDifference_1_grad/Sum_1Sum*gradients_1/SquaredDifference_1_grad/mul_1<gradients_1/SquaredDifference_1_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0
Ŗ
.gradients_1/SquaredDifference_1_grad/Reshape_1Reshape*gradients_1/SquaredDifference_1_grad/Sum_1,gradients_1/SquaredDifference_1_grad/Shape_1*
T0*
Tshape0
h
(gradients_1/SquaredDifference_1_grad/NegNeg.gradients_1/SquaredDifference_1_grad/Reshape_1*
T0

5gradients_1/SquaredDifference_1_grad/tuple/group_depsNoOp^Adam)^gradients_1/SquaredDifference_1_grad/Neg-^gradients_1/SquaredDifference_1_grad/Reshape
ł
=gradients_1/SquaredDifference_1_grad/tuple/control_dependencyIdentity,gradients_1/SquaredDifference_1_grad/Reshape6^gradients_1/SquaredDifference_1_grad/tuple/group_deps*
T0*?
_class5
31loc:@gradients_1/SquaredDifference_1_grad/Reshape
ó
?gradients_1/SquaredDifference_1_grad/tuple/control_dependency_1Identity(gradients_1/SquaredDifference_1_grad/Neg6^gradients_1/SquaredDifference_1_grad/tuple/group_deps*
T0*;
_class1
/-loc:@gradients_1/SquaredDifference_1_grad/Neg

+gradients_1/SquaredDifference_3_grad/scalarConst^Adam3^gradients_1/mul_20_grad/tuple/control_dependency_1*
valueB
 *   @*
dtype0

(gradients_1/SquaredDifference_3_grad/MulMul+gradients_1/SquaredDifference_3_grad/scalar2gradients_1/mul_20_grad/tuple/control_dependency_1*
T0

(gradients_1/SquaredDifference_3_grad/subSubStopGradient_1Mean_9^Adam3^gradients_1/mul_20_grad/tuple/control_dependency_1*
T0

*gradients_1/SquaredDifference_3_grad/mul_1Mul(gradients_1/SquaredDifference_3_grad/Mul(gradients_1/SquaredDifference_3_grad/sub*
T0
c
*gradients_1/SquaredDifference_3_grad/ShapeShapeStopGradient_1^Adam*
T0*
out_type0
]
,gradients_1/SquaredDifference_3_grad/Shape_1ShapeMean_9^Adam*
T0*
out_type0
¶
:gradients_1/SquaredDifference_3_grad/BroadcastGradientArgsBroadcastGradientArgs*gradients_1/SquaredDifference_3_grad/Shape,gradients_1/SquaredDifference_3_grad/Shape_1*
T0
½
(gradients_1/SquaredDifference_3_grad/SumSum*gradients_1/SquaredDifference_3_grad/mul_1:gradients_1/SquaredDifference_3_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0
¤
,gradients_1/SquaredDifference_3_grad/ReshapeReshape(gradients_1/SquaredDifference_3_grad/Sum*gradients_1/SquaredDifference_3_grad/Shape*
T0*
Tshape0
Į
*gradients_1/SquaredDifference_3_grad/Sum_1Sum*gradients_1/SquaredDifference_3_grad/mul_1<gradients_1/SquaredDifference_3_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0
Ŗ
.gradients_1/SquaredDifference_3_grad/Reshape_1Reshape*gradients_1/SquaredDifference_3_grad/Sum_1,gradients_1/SquaredDifference_3_grad/Shape_1*
T0*
Tshape0
h
(gradients_1/SquaredDifference_3_grad/NegNeg.gradients_1/SquaredDifference_3_grad/Reshape_1*
T0

5gradients_1/SquaredDifference_3_grad/tuple/group_depsNoOp^Adam)^gradients_1/SquaredDifference_3_grad/Neg-^gradients_1/SquaredDifference_3_grad/Reshape
ł
=gradients_1/SquaredDifference_3_grad/tuple/control_dependencyIdentity,gradients_1/SquaredDifference_3_grad/Reshape6^gradients_1/SquaredDifference_3_grad/tuple/group_deps*
T0*?
_class5
31loc:@gradients_1/SquaredDifference_3_grad/Reshape
ó
?gradients_1/SquaredDifference_3_grad/tuple/control_dependency_1Identity(gradients_1/SquaredDifference_3_grad/Neg6^gradients_1/SquaredDifference_3_grad/tuple/group_deps*
T0*;
_class1
/-loc:@gradients_1/SquaredDifference_3_grad/Neg
Æ
Agradients_1/critic/value/extrinsic_value/BiasAdd_grad/BiasAddGradBiasAddGrad=gradients_1/SquaredDifference_4_grad/tuple/control_dependency*
T0*
data_formatNHWC
Ł
Fgradients_1/critic/value/extrinsic_value/BiasAdd_grad/tuple/group_depsNoOp^Adam>^gradients_1/SquaredDifference_4_grad/tuple/control_dependencyB^gradients_1/critic/value/extrinsic_value/BiasAdd_grad/BiasAddGrad
¬
Ngradients_1/critic/value/extrinsic_value/BiasAdd_grad/tuple/control_dependencyIdentity=gradients_1/SquaredDifference_4_grad/tuple/control_dependencyG^gradients_1/critic/value/extrinsic_value/BiasAdd_grad/tuple/group_deps*
T0*?
_class5
31loc:@gradients_1/SquaredDifference_4_grad/Reshape
Ē
Pgradients_1/critic/value/extrinsic_value/BiasAdd_grad/tuple/control_dependency_1IdentityAgradients_1/critic/value/extrinsic_value/BiasAdd_grad/BiasAddGradG^gradients_1/critic/value/extrinsic_value/BiasAdd_grad/tuple/group_deps*
T0*T
_classJ
HFloc:@gradients_1/critic/value/extrinsic_value/BiasAdd_grad/BiasAddGrad
Ŗ
<gradients_1/critic/value/gail_value/BiasAdd_grad/BiasAddGradBiasAddGrad=gradients_1/SquaredDifference_5_grad/tuple/control_dependency*
T0*
data_formatNHWC
Ļ
Agradients_1/critic/value/gail_value/BiasAdd_grad/tuple/group_depsNoOp^Adam>^gradients_1/SquaredDifference_5_grad/tuple/control_dependency=^gradients_1/critic/value/gail_value/BiasAdd_grad/BiasAddGrad
¢
Igradients_1/critic/value/gail_value/BiasAdd_grad/tuple/control_dependencyIdentity=gradients_1/SquaredDifference_5_grad/tuple/control_dependencyB^gradients_1/critic/value/gail_value/BiasAdd_grad/tuple/group_deps*
T0*?
_class5
31loc:@gradients_1/SquaredDifference_5_grad/Reshape
³
Kgradients_1/critic/value/gail_value/BiasAdd_grad/tuple/control_dependency_1Identity<gradients_1/critic/value/gail_value/BiasAdd_grad/BiasAddGradB^gradients_1/critic/value/gail_value/BiasAdd_grad/tuple/group_deps*
T0*O
_classE
CAloc:@gradients_1/critic/value/gail_value/BiasAdd_grad/BiasAddGrad
T
gradients_1/Mean_4_grad/ShapeShapeMean_4/input^Adam*
T0*
out_type0

gradients_1/Mean_4_grad/SizeConst^Adam*0
_class&
$"loc:@gradients_1/Mean_4_grad/Shape*
value	B :*
dtype0

gradients_1/Mean_4_grad/addAddV2Mean_4/reduction_indicesgradients_1/Mean_4_grad/Size*
T0*0
_class&
$"loc:@gradients_1/Mean_4_grad/Shape

gradients_1/Mean_4_grad/modFloorModgradients_1/Mean_4_grad/addgradients_1/Mean_4_grad/Size*
T0*0
_class&
$"loc:@gradients_1/Mean_4_grad/Shape

gradients_1/Mean_4_grad/Shape_1Const^Adam*0
_class&
$"loc:@gradients_1/Mean_4_grad/Shape*
valueB *
dtype0

#gradients_1/Mean_4_grad/range/startConst^Adam*0
_class&
$"loc:@gradients_1/Mean_4_grad/Shape*
value	B : *
dtype0

#gradients_1/Mean_4_grad/range/deltaConst^Adam*0
_class&
$"loc:@gradients_1/Mean_4_grad/Shape*
value	B :*
dtype0
Ģ
gradients_1/Mean_4_grad/rangeRange#gradients_1/Mean_4_grad/range/startgradients_1/Mean_4_grad/Size#gradients_1/Mean_4_grad/range/delta*

Tidx0*0
_class&
$"loc:@gradients_1/Mean_4_grad/Shape

"gradients_1/Mean_4_grad/Fill/valueConst^Adam*0
_class&
$"loc:@gradients_1/Mean_4_grad/Shape*
value	B :*
dtype0
¶
gradients_1/Mean_4_grad/FillFillgradients_1/Mean_4_grad/Shape_1"gradients_1/Mean_4_grad/Fill/value*
T0*0
_class&
$"loc:@gradients_1/Mean_4_grad/Shape*

index_type0
ó
%gradients_1/Mean_4_grad/DynamicStitchDynamicStitchgradients_1/Mean_4_grad/rangegradients_1/Mean_4_grad/modgradients_1/Mean_4_grad/Shapegradients_1/Mean_4_grad/Fill*
T0*0
_class&
$"loc:@gradients_1/Mean_4_grad/Shape*
N

!gradients_1/Mean_4_grad/Maximum/yConst^Adam*0
_class&
$"loc:@gradients_1/Mean_4_grad/Shape*
value	B :*
dtype0
Æ
gradients_1/Mean_4_grad/MaximumMaximum%gradients_1/Mean_4_grad/DynamicStitch!gradients_1/Mean_4_grad/Maximum/y*
T0*0
_class&
$"loc:@gradients_1/Mean_4_grad/Shape
§
 gradients_1/Mean_4_grad/floordivFloorDivgradients_1/Mean_4_grad/Shapegradients_1/Mean_4_grad/Maximum*
T0*0
_class&
$"loc:@gradients_1/Mean_4_grad/Shape
§
gradients_1/Mean_4_grad/ReshapeReshape=gradients_1/SquaredDifference_grad/tuple/control_dependency_1%gradients_1/Mean_4_grad/DynamicStitch*
T0*
Tshape0

gradients_1/Mean_4_grad/TileTilegradients_1/Mean_4_grad/Reshape gradients_1/Mean_4_grad/floordiv*

Tmultiples0*
T0
V
gradients_1/Mean_4_grad/Shape_2ShapeMean_4/input^Adam*
T0*
out_type0
P
gradients_1/Mean_4_grad/Shape_3ShapeMean_4^Adam*
T0*
out_type0
R
gradients_1/Mean_4_grad/ConstConst^Adam*
valueB: *
dtype0

gradients_1/Mean_4_grad/ProdProdgradients_1/Mean_4_grad/Shape_2gradients_1/Mean_4_grad/Const*

Tidx0*
	keep_dims( *
T0
T
gradients_1/Mean_4_grad/Const_1Const^Adam*
valueB: *
dtype0

gradients_1/Mean_4_grad/Prod_1Prodgradients_1/Mean_4_grad/Shape_3gradients_1/Mean_4_grad/Const_1*

Tidx0*
	keep_dims( *
T0
T
#gradients_1/Mean_4_grad/Maximum_1/yConst^Adam*
value	B :*
dtype0
z
!gradients_1/Mean_4_grad/Maximum_1Maximumgradients_1/Mean_4_grad/Prod_1#gradients_1/Mean_4_grad/Maximum_1/y*
T0
x
"gradients_1/Mean_4_grad/floordiv_1FloorDivgradients_1/Mean_4_grad/Prod!gradients_1/Mean_4_grad/Maximum_1*
T0
p
gradients_1/Mean_4_grad/CastCast"gradients_1/Mean_4_grad/floordiv_1*

SrcT0*
Truncate( *

DstT0
o
gradients_1/Mean_4_grad/truedivRealDivgradients_1/Mean_4_grad/Tilegradients_1/Mean_4_grad/Cast*
T0
T
gradients_1/Mean_8_grad/ShapeShapeMean_8/input^Adam*
T0*
out_type0

gradients_1/Mean_8_grad/SizeConst^Adam*0
_class&
$"loc:@gradients_1/Mean_8_grad/Shape*
value	B :*
dtype0

gradients_1/Mean_8_grad/addAddV2Mean_8/reduction_indicesgradients_1/Mean_8_grad/Size*
T0*0
_class&
$"loc:@gradients_1/Mean_8_grad/Shape

gradients_1/Mean_8_grad/modFloorModgradients_1/Mean_8_grad/addgradients_1/Mean_8_grad/Size*
T0*0
_class&
$"loc:@gradients_1/Mean_8_grad/Shape

gradients_1/Mean_8_grad/Shape_1Const^Adam*0
_class&
$"loc:@gradients_1/Mean_8_grad/Shape*
valueB *
dtype0

#gradients_1/Mean_8_grad/range/startConst^Adam*0
_class&
$"loc:@gradients_1/Mean_8_grad/Shape*
value	B : *
dtype0

#gradients_1/Mean_8_grad/range/deltaConst^Adam*0
_class&
$"loc:@gradients_1/Mean_8_grad/Shape*
value	B :*
dtype0
Ģ
gradients_1/Mean_8_grad/rangeRange#gradients_1/Mean_8_grad/range/startgradients_1/Mean_8_grad/Size#gradients_1/Mean_8_grad/range/delta*

Tidx0*0
_class&
$"loc:@gradients_1/Mean_8_grad/Shape

"gradients_1/Mean_8_grad/Fill/valueConst^Adam*0
_class&
$"loc:@gradients_1/Mean_8_grad/Shape*
value	B :*
dtype0
¶
gradients_1/Mean_8_grad/FillFillgradients_1/Mean_8_grad/Shape_1"gradients_1/Mean_8_grad/Fill/value*
T0*0
_class&
$"loc:@gradients_1/Mean_8_grad/Shape*

index_type0
ó
%gradients_1/Mean_8_grad/DynamicStitchDynamicStitchgradients_1/Mean_8_grad/rangegradients_1/Mean_8_grad/modgradients_1/Mean_8_grad/Shapegradients_1/Mean_8_grad/Fill*
T0*0
_class&
$"loc:@gradients_1/Mean_8_grad/Shape*
N

!gradients_1/Mean_8_grad/Maximum/yConst^Adam*0
_class&
$"loc:@gradients_1/Mean_8_grad/Shape*
value	B :*
dtype0
Æ
gradients_1/Mean_8_grad/MaximumMaximum%gradients_1/Mean_8_grad/DynamicStitch!gradients_1/Mean_8_grad/Maximum/y*
T0*0
_class&
$"loc:@gradients_1/Mean_8_grad/Shape
§
 gradients_1/Mean_8_grad/floordivFloorDivgradients_1/Mean_8_grad/Shapegradients_1/Mean_8_grad/Maximum*
T0*0
_class&
$"loc:@gradients_1/Mean_8_grad/Shape
©
gradients_1/Mean_8_grad/ReshapeReshape?gradients_1/SquaredDifference_2_grad/tuple/control_dependency_1%gradients_1/Mean_8_grad/DynamicStitch*
T0*
Tshape0

gradients_1/Mean_8_grad/TileTilegradients_1/Mean_8_grad/Reshape gradients_1/Mean_8_grad/floordiv*

Tmultiples0*
T0
V
gradients_1/Mean_8_grad/Shape_2ShapeMean_8/input^Adam*
T0*
out_type0
P
gradients_1/Mean_8_grad/Shape_3ShapeMean_8^Adam*
T0*
out_type0
R
gradients_1/Mean_8_grad/ConstConst^Adam*
valueB: *
dtype0

gradients_1/Mean_8_grad/ProdProdgradients_1/Mean_8_grad/Shape_2gradients_1/Mean_8_grad/Const*

Tidx0*
	keep_dims( *
T0
T
gradients_1/Mean_8_grad/Const_1Const^Adam*
valueB: *
dtype0

gradients_1/Mean_8_grad/Prod_1Prodgradients_1/Mean_8_grad/Shape_3gradients_1/Mean_8_grad/Const_1*

Tidx0*
	keep_dims( *
T0
T
#gradients_1/Mean_8_grad/Maximum_1/yConst^Adam*
value	B :*
dtype0
z
!gradients_1/Mean_8_grad/Maximum_1Maximumgradients_1/Mean_8_grad/Prod_1#gradients_1/Mean_8_grad/Maximum_1/y*
T0
x
"gradients_1/Mean_8_grad/floordiv_1FloorDivgradients_1/Mean_8_grad/Prod!gradients_1/Mean_8_grad/Maximum_1*
T0
p
gradients_1/Mean_8_grad/CastCast"gradients_1/Mean_8_grad/floordiv_1*

SrcT0*
Truncate( *

DstT0
o
gradients_1/Mean_8_grad/truedivRealDivgradients_1/Mean_8_grad/Tilegradients_1/Mean_8_grad/Cast*
T0
T
gradients_1/Mean_5_grad/ShapeShapeMean_5/input^Adam*
T0*
out_type0

gradients_1/Mean_5_grad/SizeConst^Adam*0
_class&
$"loc:@gradients_1/Mean_5_grad/Shape*
value	B :*
dtype0

gradients_1/Mean_5_grad/addAddV2Mean_5/reduction_indicesgradients_1/Mean_5_grad/Size*
T0*0
_class&
$"loc:@gradients_1/Mean_5_grad/Shape

gradients_1/Mean_5_grad/modFloorModgradients_1/Mean_5_grad/addgradients_1/Mean_5_grad/Size*
T0*0
_class&
$"loc:@gradients_1/Mean_5_grad/Shape

gradients_1/Mean_5_grad/Shape_1Const^Adam*0
_class&
$"loc:@gradients_1/Mean_5_grad/Shape*
valueB *
dtype0

#gradients_1/Mean_5_grad/range/startConst^Adam*0
_class&
$"loc:@gradients_1/Mean_5_grad/Shape*
value	B : *
dtype0

#gradients_1/Mean_5_grad/range/deltaConst^Adam*0
_class&
$"loc:@gradients_1/Mean_5_grad/Shape*
value	B :*
dtype0
Ģ
gradients_1/Mean_5_grad/rangeRange#gradients_1/Mean_5_grad/range/startgradients_1/Mean_5_grad/Size#gradients_1/Mean_5_grad/range/delta*

Tidx0*0
_class&
$"loc:@gradients_1/Mean_5_grad/Shape

"gradients_1/Mean_5_grad/Fill/valueConst^Adam*0
_class&
$"loc:@gradients_1/Mean_5_grad/Shape*
value	B :*
dtype0
¶
gradients_1/Mean_5_grad/FillFillgradients_1/Mean_5_grad/Shape_1"gradients_1/Mean_5_grad/Fill/value*
T0*0
_class&
$"loc:@gradients_1/Mean_5_grad/Shape*

index_type0
ó
%gradients_1/Mean_5_grad/DynamicStitchDynamicStitchgradients_1/Mean_5_grad/rangegradients_1/Mean_5_grad/modgradients_1/Mean_5_grad/Shapegradients_1/Mean_5_grad/Fill*
T0*0
_class&
$"loc:@gradients_1/Mean_5_grad/Shape*
N

!gradients_1/Mean_5_grad/Maximum/yConst^Adam*0
_class&
$"loc:@gradients_1/Mean_5_grad/Shape*
value	B :*
dtype0
Æ
gradients_1/Mean_5_grad/MaximumMaximum%gradients_1/Mean_5_grad/DynamicStitch!gradients_1/Mean_5_grad/Maximum/y*
T0*0
_class&
$"loc:@gradients_1/Mean_5_grad/Shape
§
 gradients_1/Mean_5_grad/floordivFloorDivgradients_1/Mean_5_grad/Shapegradients_1/Mean_5_grad/Maximum*
T0*0
_class&
$"loc:@gradients_1/Mean_5_grad/Shape
©
gradients_1/Mean_5_grad/ReshapeReshape?gradients_1/SquaredDifference_1_grad/tuple/control_dependency_1%gradients_1/Mean_5_grad/DynamicStitch*
T0*
Tshape0

gradients_1/Mean_5_grad/TileTilegradients_1/Mean_5_grad/Reshape gradients_1/Mean_5_grad/floordiv*

Tmultiples0*
T0
V
gradients_1/Mean_5_grad/Shape_2ShapeMean_5/input^Adam*
T0*
out_type0
P
gradients_1/Mean_5_grad/Shape_3ShapeMean_5^Adam*
T0*
out_type0
R
gradients_1/Mean_5_grad/ConstConst^Adam*
valueB: *
dtype0

gradients_1/Mean_5_grad/ProdProdgradients_1/Mean_5_grad/Shape_2gradients_1/Mean_5_grad/Const*

Tidx0*
	keep_dims( *
T0
T
gradients_1/Mean_5_grad/Const_1Const^Adam*
valueB: *
dtype0

gradients_1/Mean_5_grad/Prod_1Prodgradients_1/Mean_5_grad/Shape_3gradients_1/Mean_5_grad/Const_1*

Tidx0*
	keep_dims( *
T0
T
#gradients_1/Mean_5_grad/Maximum_1/yConst^Adam*
value	B :*
dtype0
z
!gradients_1/Mean_5_grad/Maximum_1Maximumgradients_1/Mean_5_grad/Prod_1#gradients_1/Mean_5_grad/Maximum_1/y*
T0
x
"gradients_1/Mean_5_grad/floordiv_1FloorDivgradients_1/Mean_5_grad/Prod!gradients_1/Mean_5_grad/Maximum_1*
T0
p
gradients_1/Mean_5_grad/CastCast"gradients_1/Mean_5_grad/floordiv_1*

SrcT0*
Truncate( *

DstT0
o
gradients_1/Mean_5_grad/truedivRealDivgradients_1/Mean_5_grad/Tilegradients_1/Mean_5_grad/Cast*
T0
T
gradients_1/Mean_9_grad/ShapeShapeMean_9/input^Adam*
T0*
out_type0

gradients_1/Mean_9_grad/SizeConst^Adam*0
_class&
$"loc:@gradients_1/Mean_9_grad/Shape*
value	B :*
dtype0

gradients_1/Mean_9_grad/addAddV2Mean_9/reduction_indicesgradients_1/Mean_9_grad/Size*
T0*0
_class&
$"loc:@gradients_1/Mean_9_grad/Shape

gradients_1/Mean_9_grad/modFloorModgradients_1/Mean_9_grad/addgradients_1/Mean_9_grad/Size*
T0*0
_class&
$"loc:@gradients_1/Mean_9_grad/Shape

gradients_1/Mean_9_grad/Shape_1Const^Adam*0
_class&
$"loc:@gradients_1/Mean_9_grad/Shape*
valueB *
dtype0

#gradients_1/Mean_9_grad/range/startConst^Adam*0
_class&
$"loc:@gradients_1/Mean_9_grad/Shape*
value	B : *
dtype0

#gradients_1/Mean_9_grad/range/deltaConst^Adam*0
_class&
$"loc:@gradients_1/Mean_9_grad/Shape*
value	B :*
dtype0
Ģ
gradients_1/Mean_9_grad/rangeRange#gradients_1/Mean_9_grad/range/startgradients_1/Mean_9_grad/Size#gradients_1/Mean_9_grad/range/delta*

Tidx0*0
_class&
$"loc:@gradients_1/Mean_9_grad/Shape

"gradients_1/Mean_9_grad/Fill/valueConst^Adam*0
_class&
$"loc:@gradients_1/Mean_9_grad/Shape*
value	B :*
dtype0
¶
gradients_1/Mean_9_grad/FillFillgradients_1/Mean_9_grad/Shape_1"gradients_1/Mean_9_grad/Fill/value*
T0*0
_class&
$"loc:@gradients_1/Mean_9_grad/Shape*

index_type0
ó
%gradients_1/Mean_9_grad/DynamicStitchDynamicStitchgradients_1/Mean_9_grad/rangegradients_1/Mean_9_grad/modgradients_1/Mean_9_grad/Shapegradients_1/Mean_9_grad/Fill*
T0*0
_class&
$"loc:@gradients_1/Mean_9_grad/Shape*
N

!gradients_1/Mean_9_grad/Maximum/yConst^Adam*0
_class&
$"loc:@gradients_1/Mean_9_grad/Shape*
value	B :*
dtype0
Æ
gradients_1/Mean_9_grad/MaximumMaximum%gradients_1/Mean_9_grad/DynamicStitch!gradients_1/Mean_9_grad/Maximum/y*
T0*0
_class&
$"loc:@gradients_1/Mean_9_grad/Shape
§
 gradients_1/Mean_9_grad/floordivFloorDivgradients_1/Mean_9_grad/Shapegradients_1/Mean_9_grad/Maximum*
T0*0
_class&
$"loc:@gradients_1/Mean_9_grad/Shape
©
gradients_1/Mean_9_grad/ReshapeReshape?gradients_1/SquaredDifference_3_grad/tuple/control_dependency_1%gradients_1/Mean_9_grad/DynamicStitch*
T0*
Tshape0

gradients_1/Mean_9_grad/TileTilegradients_1/Mean_9_grad/Reshape gradients_1/Mean_9_grad/floordiv*

Tmultiples0*
T0
V
gradients_1/Mean_9_grad/Shape_2ShapeMean_9/input^Adam*
T0*
out_type0
P
gradients_1/Mean_9_grad/Shape_3ShapeMean_9^Adam*
T0*
out_type0
R
gradients_1/Mean_9_grad/ConstConst^Adam*
valueB: *
dtype0

gradients_1/Mean_9_grad/ProdProdgradients_1/Mean_9_grad/Shape_2gradients_1/Mean_9_grad/Const*

Tidx0*
	keep_dims( *
T0
T
gradients_1/Mean_9_grad/Const_1Const^Adam*
valueB: *
dtype0

gradients_1/Mean_9_grad/Prod_1Prodgradients_1/Mean_9_grad/Shape_3gradients_1/Mean_9_grad/Const_1*

Tidx0*
	keep_dims( *
T0
T
#gradients_1/Mean_9_grad/Maximum_1/yConst^Adam*
value	B :*
dtype0
z
!gradients_1/Mean_9_grad/Maximum_1Maximumgradients_1/Mean_9_grad/Prod_1#gradients_1/Mean_9_grad/Maximum_1/y*
T0
x
"gradients_1/Mean_9_grad/floordiv_1FloorDivgradients_1/Mean_9_grad/Prod!gradients_1/Mean_9_grad/Maximum_1*
T0
p
gradients_1/Mean_9_grad/CastCast"gradients_1/Mean_9_grad/floordiv_1*

SrcT0*
Truncate( *

DstT0
o
gradients_1/Mean_9_grad/truedivRealDivgradients_1/Mean_9_grad/Tilegradients_1/Mean_9_grad/Cast*
T0
ī
;gradients_1/critic/value/extrinsic_value/MatMul_grad/MatMulMatMulNgradients_1/critic/value/extrinsic_value/BiasAdd_grad/tuple/control_dependency(critic/value/extrinsic_value/kernel/read*
transpose_b(*
T0*
transpose_a( 
é
=gradients_1/critic/value/extrinsic_value/MatMul_grad/MatMul_1MatMul!critic/value/encoder/hidden_1/MulNgradients_1/critic/value/extrinsic_value/BiasAdd_grad/tuple/control_dependency*
transpose_b( *
T0*
transpose_a(
Ņ
Egradients_1/critic/value/extrinsic_value/MatMul_grad/tuple/group_depsNoOp^Adam<^gradients_1/critic/value/extrinsic_value/MatMul_grad/MatMul>^gradients_1/critic/value/extrinsic_value/MatMul_grad/MatMul_1
·
Mgradients_1/critic/value/extrinsic_value/MatMul_grad/tuple/control_dependencyIdentity;gradients_1/critic/value/extrinsic_value/MatMul_grad/MatMulF^gradients_1/critic/value/extrinsic_value/MatMul_grad/tuple/group_deps*
T0*N
_classD
B@loc:@gradients_1/critic/value/extrinsic_value/MatMul_grad/MatMul
½
Ogradients_1/critic/value/extrinsic_value/MatMul_grad/tuple/control_dependency_1Identity=gradients_1/critic/value/extrinsic_value/MatMul_grad/MatMul_1F^gradients_1/critic/value/extrinsic_value/MatMul_grad/tuple/group_deps*
T0*P
_classF
DBloc:@gradients_1/critic/value/extrinsic_value/MatMul_grad/MatMul_1
ß
6gradients_1/critic/value/gail_value/MatMul_grad/MatMulMatMulIgradients_1/critic/value/gail_value/BiasAdd_grad/tuple/control_dependency#critic/value/gail_value/kernel/read*
transpose_b(*
T0*
transpose_a( 
ß
8gradients_1/critic/value/gail_value/MatMul_grad/MatMul_1MatMul!critic/value/encoder/hidden_1/MulIgradients_1/critic/value/gail_value/BiasAdd_grad/tuple/control_dependency*
transpose_b( *
T0*
transpose_a(
Ć
@gradients_1/critic/value/gail_value/MatMul_grad/tuple/group_depsNoOp^Adam7^gradients_1/critic/value/gail_value/MatMul_grad/MatMul9^gradients_1/critic/value/gail_value/MatMul_grad/MatMul_1
£
Hgradients_1/critic/value/gail_value/MatMul_grad/tuple/control_dependencyIdentity6gradients_1/critic/value/gail_value/MatMul_grad/MatMulA^gradients_1/critic/value/gail_value/MatMul_grad/tuple/group_deps*
T0*I
_class?
=;loc:@gradients_1/critic/value/gail_value/MatMul_grad/MatMul
©
Jgradients_1/critic/value/gail_value/MatMul_grad/tuple/control_dependency_1Identity8gradients_1/critic/value/gail_value/MatMul_grad/MatMul_1A^gradients_1/critic/value/gail_value/MatMul_grad/tuple/group_deps*
T0*K
_classA
?=loc:@gradients_1/critic/value/gail_value/MatMul_grad/MatMul_1
p
%gradients_1/Mean_4/input_grad/unstackUnpackgradients_1/Mean_4_grad/truediv*
T0*	
num*

axis 
e
.gradients_1/Mean_4/input_grad/tuple/group_depsNoOp^Adam&^gradients_1/Mean_4/input_grad/unstack
Ż
6gradients_1/Mean_4/input_grad/tuple/control_dependencyIdentity%gradients_1/Mean_4/input_grad/unstack/^gradients_1/Mean_4/input_grad/tuple/group_deps*
T0*8
_class.
,*loc:@gradients_1/Mean_4/input_grad/unstack
į
8gradients_1/Mean_4/input_grad/tuple/control_dependency_1Identity'gradients_1/Mean_4/input_grad/unstack:1/^gradients_1/Mean_4/input_grad/tuple/group_deps*
T0*8
_class.
,*loc:@gradients_1/Mean_4/input_grad/unstack
į
8gradients_1/Mean_4/input_grad/tuple/control_dependency_2Identity'gradients_1/Mean_4/input_grad/unstack:2/^gradients_1/Mean_4/input_grad/tuple/group_deps*
T0*8
_class.
,*loc:@gradients_1/Mean_4/input_grad/unstack
p
%gradients_1/Mean_8/input_grad/unstackUnpackgradients_1/Mean_8_grad/truediv*
T0*	
num*

axis 
e
.gradients_1/Mean_8/input_grad/tuple/group_depsNoOp^Adam&^gradients_1/Mean_8/input_grad/unstack
Ż
6gradients_1/Mean_8/input_grad/tuple/control_dependencyIdentity%gradients_1/Mean_8/input_grad/unstack/^gradients_1/Mean_8/input_grad/tuple/group_deps*
T0*8
_class.
,*loc:@gradients_1/Mean_8/input_grad/unstack
į
8gradients_1/Mean_8/input_grad/tuple/control_dependency_1Identity'gradients_1/Mean_8/input_grad/unstack:1/^gradients_1/Mean_8/input_grad/tuple/group_deps*
T0*8
_class.
,*loc:@gradients_1/Mean_8/input_grad/unstack
į
8gradients_1/Mean_8/input_grad/tuple/control_dependency_2Identity'gradients_1/Mean_8/input_grad/unstack:2/^gradients_1/Mean_8/input_grad/tuple/group_deps*
T0*8
_class.
,*loc:@gradients_1/Mean_8/input_grad/unstack
p
%gradients_1/Mean_5/input_grad/unstackUnpackgradients_1/Mean_5_grad/truediv*
T0*	
num*

axis 
e
.gradients_1/Mean_5/input_grad/tuple/group_depsNoOp^Adam&^gradients_1/Mean_5/input_grad/unstack
Ż
6gradients_1/Mean_5/input_grad/tuple/control_dependencyIdentity%gradients_1/Mean_5/input_grad/unstack/^gradients_1/Mean_5/input_grad/tuple/group_deps*
T0*8
_class.
,*loc:@gradients_1/Mean_5/input_grad/unstack
į
8gradients_1/Mean_5/input_grad/tuple/control_dependency_1Identity'gradients_1/Mean_5/input_grad/unstack:1/^gradients_1/Mean_5/input_grad/tuple/group_deps*
T0*8
_class.
,*loc:@gradients_1/Mean_5/input_grad/unstack
į
8gradients_1/Mean_5/input_grad/tuple/control_dependency_2Identity'gradients_1/Mean_5/input_grad/unstack:2/^gradients_1/Mean_5/input_grad/tuple/group_deps*
T0*8
_class.
,*loc:@gradients_1/Mean_5/input_grad/unstack
p
%gradients_1/Mean_9/input_grad/unstackUnpackgradients_1/Mean_9_grad/truediv*
T0*	
num*

axis 
e
.gradients_1/Mean_9/input_grad/tuple/group_depsNoOp^Adam&^gradients_1/Mean_9/input_grad/unstack
Ż
6gradients_1/Mean_9/input_grad/tuple/control_dependencyIdentity%gradients_1/Mean_9/input_grad/unstack/^gradients_1/Mean_9/input_grad/tuple/group_deps*
T0*8
_class.
,*loc:@gradients_1/Mean_9/input_grad/unstack
į
8gradients_1/Mean_9/input_grad/tuple/control_dependency_1Identity'gradients_1/Mean_9/input_grad/unstack:1/^gradients_1/Mean_9/input_grad/tuple/group_deps*
T0*8
_class.
,*loc:@gradients_1/Mean_9/input_grad/unstack
į
8gradients_1/Mean_9/input_grad/tuple/control_dependency_2Identity'gradients_1/Mean_9/input_grad/unstack:2/^gradients_1/Mean_9/input_grad/tuple/group_deps*
T0*8
_class.
,*loc:@gradients_1/Mean_9/input_grad/unstack

gradients_1/AddNAddNMgradients_1/critic/value/extrinsic_value/MatMul_grad/tuple/control_dependencyHgradients_1/critic/value/gail_value/MatMul_grad/tuple/control_dependency*
T0*N
_classD
B@loc:@gradients_1/critic/value/extrinsic_value/MatMul_grad/MatMul*
N

8gradients_1/critic/value/encoder/hidden_1/Mul_grad/ShapeShape%critic/value/encoder/hidden_1/BiasAdd^Adam*
T0*
out_type0

:gradients_1/critic/value/encoder/hidden_1/Mul_grad/Shape_1Shape%critic/value/encoder/hidden_1/Sigmoid^Adam*
T0*
out_type0
ą
Hgradients_1/critic/value/encoder/hidden_1/Mul_grad/BroadcastGradientArgsBroadcastGradientArgs8gradients_1/critic/value/encoder/hidden_1/Mul_grad/Shape:gradients_1/critic/value/encoder/hidden_1/Mul_grad/Shape_1*
T0

6gradients_1/critic/value/encoder/hidden_1/Mul_grad/MulMulgradients_1/AddN%critic/value/encoder/hidden_1/Sigmoid*
T0
å
6gradients_1/critic/value/encoder/hidden_1/Mul_grad/SumSum6gradients_1/critic/value/encoder/hidden_1/Mul_grad/MulHgradients_1/critic/value/encoder/hidden_1/Mul_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0
Ī
:gradients_1/critic/value/encoder/hidden_1/Mul_grad/ReshapeReshape6gradients_1/critic/value/encoder/hidden_1/Mul_grad/Sum8gradients_1/critic/value/encoder/hidden_1/Mul_grad/Shape*
T0*
Tshape0

8gradients_1/critic/value/encoder/hidden_1/Mul_grad/Mul_1Mul%critic/value/encoder/hidden_1/BiasAddgradients_1/AddN*
T0
ė
8gradients_1/critic/value/encoder/hidden_1/Mul_grad/Sum_1Sum8gradients_1/critic/value/encoder/hidden_1/Mul_grad/Mul_1Jgradients_1/critic/value/encoder/hidden_1/Mul_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0
Ō
<gradients_1/critic/value/encoder/hidden_1/Mul_grad/Reshape_1Reshape8gradients_1/critic/value/encoder/hidden_1/Mul_grad/Sum_1:gradients_1/critic/value/encoder/hidden_1/Mul_grad/Shape_1*
T0*
Tshape0
Ī
Cgradients_1/critic/value/encoder/hidden_1/Mul_grad/tuple/group_depsNoOp^Adam;^gradients_1/critic/value/encoder/hidden_1/Mul_grad/Reshape=^gradients_1/critic/value/encoder/hidden_1/Mul_grad/Reshape_1
±
Kgradients_1/critic/value/encoder/hidden_1/Mul_grad/tuple/control_dependencyIdentity:gradients_1/critic/value/encoder/hidden_1/Mul_grad/ReshapeD^gradients_1/critic/value/encoder/hidden_1/Mul_grad/tuple/group_deps*
T0*M
_classC
A?loc:@gradients_1/critic/value/encoder/hidden_1/Mul_grad/Reshape
·
Mgradients_1/critic/value/encoder/hidden_1/Mul_grad/tuple/control_dependency_1Identity<gradients_1/critic/value/encoder/hidden_1/Mul_grad/Reshape_1D^gradients_1/critic/value/encoder/hidden_1/Mul_grad/tuple/group_deps*
T0*O
_classE
CAloc:@gradients_1/critic/value/encoder/hidden_1/Mul_grad/Reshape_1
X
gradients_1/Sum_12_grad/ShapeShapestrided_slice_12^Adam*
T0*
out_type0

gradients_1/Sum_12_grad/SizeConst^Adam*0
_class&
$"loc:@gradients_1/Sum_12_grad/Shape*
value	B :*
dtype0

gradients_1/Sum_12_grad/addAddV2Sum_12/reduction_indicesgradients_1/Sum_12_grad/Size*
T0*0
_class&
$"loc:@gradients_1/Sum_12_grad/Shape

gradients_1/Sum_12_grad/modFloorModgradients_1/Sum_12_grad/addgradients_1/Sum_12_grad/Size*
T0*0
_class&
$"loc:@gradients_1/Sum_12_grad/Shape

gradients_1/Sum_12_grad/Shape_1Const^Adam*0
_class&
$"loc:@gradients_1/Sum_12_grad/Shape*
valueB *
dtype0

#gradients_1/Sum_12_grad/range/startConst^Adam*0
_class&
$"loc:@gradients_1/Sum_12_grad/Shape*
value	B : *
dtype0

#gradients_1/Sum_12_grad/range/deltaConst^Adam*0
_class&
$"loc:@gradients_1/Sum_12_grad/Shape*
value	B :*
dtype0
Ģ
gradients_1/Sum_12_grad/rangeRange#gradients_1/Sum_12_grad/range/startgradients_1/Sum_12_grad/Size#gradients_1/Sum_12_grad/range/delta*

Tidx0*0
_class&
$"loc:@gradients_1/Sum_12_grad/Shape

"gradients_1/Sum_12_grad/Fill/valueConst^Adam*0
_class&
$"loc:@gradients_1/Sum_12_grad/Shape*
value	B :*
dtype0
¶
gradients_1/Sum_12_grad/FillFillgradients_1/Sum_12_grad/Shape_1"gradients_1/Sum_12_grad/Fill/value*
T0*0
_class&
$"loc:@gradients_1/Sum_12_grad/Shape*

index_type0
ó
%gradients_1/Sum_12_grad/DynamicStitchDynamicStitchgradients_1/Sum_12_grad/rangegradients_1/Sum_12_grad/modgradients_1/Sum_12_grad/Shapegradients_1/Sum_12_grad/Fill*
T0*0
_class&
$"loc:@gradients_1/Sum_12_grad/Shape*
N

!gradients_1/Sum_12_grad/Maximum/yConst^Adam*0
_class&
$"loc:@gradients_1/Sum_12_grad/Shape*
value	B :*
dtype0
Æ
gradients_1/Sum_12_grad/MaximumMaximum%gradients_1/Sum_12_grad/DynamicStitch!gradients_1/Sum_12_grad/Maximum/y*
T0*0
_class&
$"loc:@gradients_1/Sum_12_grad/Shape
§
 gradients_1/Sum_12_grad/floordivFloorDivgradients_1/Sum_12_grad/Shapegradients_1/Sum_12_grad/Maximum*
T0*0
_class&
$"loc:@gradients_1/Sum_12_grad/Shape
 
gradients_1/Sum_12_grad/ReshapeReshape6gradients_1/Mean_4/input_grad/tuple/control_dependency%gradients_1/Sum_12_grad/DynamicStitch*
T0*
Tshape0

gradients_1/Sum_12_grad/TileTilegradients_1/Sum_12_grad/Reshape gradients_1/Sum_12_grad/floordiv*

Tmultiples0*
T0
X
gradients_1/Sum_13_grad/ShapeShapestrided_slice_13^Adam*
T0*
out_type0

gradients_1/Sum_13_grad/SizeConst^Adam*0
_class&
$"loc:@gradients_1/Sum_13_grad/Shape*
value	B :*
dtype0

gradients_1/Sum_13_grad/addAddV2Sum_13/reduction_indicesgradients_1/Sum_13_grad/Size*
T0*0
_class&
$"loc:@gradients_1/Sum_13_grad/Shape

gradients_1/Sum_13_grad/modFloorModgradients_1/Sum_13_grad/addgradients_1/Sum_13_grad/Size*
T0*0
_class&
$"loc:@gradients_1/Sum_13_grad/Shape

gradients_1/Sum_13_grad/Shape_1Const^Adam*0
_class&
$"loc:@gradients_1/Sum_13_grad/Shape*
valueB *
dtype0

#gradients_1/Sum_13_grad/range/startConst^Adam*0
_class&
$"loc:@gradients_1/Sum_13_grad/Shape*
value	B : *
dtype0

#gradients_1/Sum_13_grad/range/deltaConst^Adam*0
_class&
$"loc:@gradients_1/Sum_13_grad/Shape*
value	B :*
dtype0
Ģ
gradients_1/Sum_13_grad/rangeRange#gradients_1/Sum_13_grad/range/startgradients_1/Sum_13_grad/Size#gradients_1/Sum_13_grad/range/delta*

Tidx0*0
_class&
$"loc:@gradients_1/Sum_13_grad/Shape

"gradients_1/Sum_13_grad/Fill/valueConst^Adam*0
_class&
$"loc:@gradients_1/Sum_13_grad/Shape*
value	B :*
dtype0
¶
gradients_1/Sum_13_grad/FillFillgradients_1/Sum_13_grad/Shape_1"gradients_1/Sum_13_grad/Fill/value*
T0*0
_class&
$"loc:@gradients_1/Sum_13_grad/Shape*

index_type0
ó
%gradients_1/Sum_13_grad/DynamicStitchDynamicStitchgradients_1/Sum_13_grad/rangegradients_1/Sum_13_grad/modgradients_1/Sum_13_grad/Shapegradients_1/Sum_13_grad/Fill*
T0*0
_class&
$"loc:@gradients_1/Sum_13_grad/Shape*
N

!gradients_1/Sum_13_grad/Maximum/yConst^Adam*0
_class&
$"loc:@gradients_1/Sum_13_grad/Shape*
value	B :*
dtype0
Æ
gradients_1/Sum_13_grad/MaximumMaximum%gradients_1/Sum_13_grad/DynamicStitch!gradients_1/Sum_13_grad/Maximum/y*
T0*0
_class&
$"loc:@gradients_1/Sum_13_grad/Shape
§
 gradients_1/Sum_13_grad/floordivFloorDivgradients_1/Sum_13_grad/Shapegradients_1/Sum_13_grad/Maximum*
T0*0
_class&
$"loc:@gradients_1/Sum_13_grad/Shape
¢
gradients_1/Sum_13_grad/ReshapeReshape8gradients_1/Mean_4/input_grad/tuple/control_dependency_1%gradients_1/Sum_13_grad/DynamicStitch*
T0*
Tshape0

gradients_1/Sum_13_grad/TileTilegradients_1/Sum_13_grad/Reshape gradients_1/Sum_13_grad/floordiv*

Tmultiples0*
T0
X
gradients_1/Sum_14_grad/ShapeShapestrided_slice_14^Adam*
T0*
out_type0

gradients_1/Sum_14_grad/SizeConst^Adam*0
_class&
$"loc:@gradients_1/Sum_14_grad/Shape*
value	B :*
dtype0

gradients_1/Sum_14_grad/addAddV2Sum_14/reduction_indicesgradients_1/Sum_14_grad/Size*
T0*0
_class&
$"loc:@gradients_1/Sum_14_grad/Shape

gradients_1/Sum_14_grad/modFloorModgradients_1/Sum_14_grad/addgradients_1/Sum_14_grad/Size*
T0*0
_class&
$"loc:@gradients_1/Sum_14_grad/Shape

gradients_1/Sum_14_grad/Shape_1Const^Adam*0
_class&
$"loc:@gradients_1/Sum_14_grad/Shape*
valueB *
dtype0

#gradients_1/Sum_14_grad/range/startConst^Adam*0
_class&
$"loc:@gradients_1/Sum_14_grad/Shape*
value	B : *
dtype0

#gradients_1/Sum_14_grad/range/deltaConst^Adam*0
_class&
$"loc:@gradients_1/Sum_14_grad/Shape*
value	B :*
dtype0
Ģ
gradients_1/Sum_14_grad/rangeRange#gradients_1/Sum_14_grad/range/startgradients_1/Sum_14_grad/Size#gradients_1/Sum_14_grad/range/delta*

Tidx0*0
_class&
$"loc:@gradients_1/Sum_14_grad/Shape

"gradients_1/Sum_14_grad/Fill/valueConst^Adam*0
_class&
$"loc:@gradients_1/Sum_14_grad/Shape*
value	B :*
dtype0
¶
gradients_1/Sum_14_grad/FillFillgradients_1/Sum_14_grad/Shape_1"gradients_1/Sum_14_grad/Fill/value*
T0*0
_class&
$"loc:@gradients_1/Sum_14_grad/Shape*

index_type0
ó
%gradients_1/Sum_14_grad/DynamicStitchDynamicStitchgradients_1/Sum_14_grad/rangegradients_1/Sum_14_grad/modgradients_1/Sum_14_grad/Shapegradients_1/Sum_14_grad/Fill*
T0*0
_class&
$"loc:@gradients_1/Sum_14_grad/Shape*
N

!gradients_1/Sum_14_grad/Maximum/yConst^Adam*0
_class&
$"loc:@gradients_1/Sum_14_grad/Shape*
value	B :*
dtype0
Æ
gradients_1/Sum_14_grad/MaximumMaximum%gradients_1/Sum_14_grad/DynamicStitch!gradients_1/Sum_14_grad/Maximum/y*
T0*0
_class&
$"loc:@gradients_1/Sum_14_grad/Shape
§
 gradients_1/Sum_14_grad/floordivFloorDivgradients_1/Sum_14_grad/Shapegradients_1/Sum_14_grad/Maximum*
T0*0
_class&
$"loc:@gradients_1/Sum_14_grad/Shape
¢
gradients_1/Sum_14_grad/ReshapeReshape8gradients_1/Mean_4/input_grad/tuple/control_dependency_2%gradients_1/Sum_14_grad/DynamicStitch*
T0*
Tshape0

gradients_1/Sum_14_grad/TileTilegradients_1/Sum_14_grad/Reshape gradients_1/Sum_14_grad/floordiv*

Tmultiples0*
T0
X
gradients_1/Sum_18_grad/ShapeShapestrided_slice_18^Adam*
T0*
out_type0

gradients_1/Sum_18_grad/SizeConst^Adam*0
_class&
$"loc:@gradients_1/Sum_18_grad/Shape*
value	B :*
dtype0

gradients_1/Sum_18_grad/addAddV2Sum_18/reduction_indicesgradients_1/Sum_18_grad/Size*
T0*0
_class&
$"loc:@gradients_1/Sum_18_grad/Shape

gradients_1/Sum_18_grad/modFloorModgradients_1/Sum_18_grad/addgradients_1/Sum_18_grad/Size*
T0*0
_class&
$"loc:@gradients_1/Sum_18_grad/Shape

gradients_1/Sum_18_grad/Shape_1Const^Adam*0
_class&
$"loc:@gradients_1/Sum_18_grad/Shape*
valueB *
dtype0

#gradients_1/Sum_18_grad/range/startConst^Adam*0
_class&
$"loc:@gradients_1/Sum_18_grad/Shape*
value	B : *
dtype0

#gradients_1/Sum_18_grad/range/deltaConst^Adam*0
_class&
$"loc:@gradients_1/Sum_18_grad/Shape*
value	B :*
dtype0
Ģ
gradients_1/Sum_18_grad/rangeRange#gradients_1/Sum_18_grad/range/startgradients_1/Sum_18_grad/Size#gradients_1/Sum_18_grad/range/delta*

Tidx0*0
_class&
$"loc:@gradients_1/Sum_18_grad/Shape

"gradients_1/Sum_18_grad/Fill/valueConst^Adam*0
_class&
$"loc:@gradients_1/Sum_18_grad/Shape*
value	B :*
dtype0
¶
gradients_1/Sum_18_grad/FillFillgradients_1/Sum_18_grad/Shape_1"gradients_1/Sum_18_grad/Fill/value*
T0*0
_class&
$"loc:@gradients_1/Sum_18_grad/Shape*

index_type0
ó
%gradients_1/Sum_18_grad/DynamicStitchDynamicStitchgradients_1/Sum_18_grad/rangegradients_1/Sum_18_grad/modgradients_1/Sum_18_grad/Shapegradients_1/Sum_18_grad/Fill*
T0*0
_class&
$"loc:@gradients_1/Sum_18_grad/Shape*
N

!gradients_1/Sum_18_grad/Maximum/yConst^Adam*0
_class&
$"loc:@gradients_1/Sum_18_grad/Shape*
value	B :*
dtype0
Æ
gradients_1/Sum_18_grad/MaximumMaximum%gradients_1/Sum_18_grad/DynamicStitch!gradients_1/Sum_18_grad/Maximum/y*
T0*0
_class&
$"loc:@gradients_1/Sum_18_grad/Shape
§
 gradients_1/Sum_18_grad/floordivFloorDivgradients_1/Sum_18_grad/Shapegradients_1/Sum_18_grad/Maximum*
T0*0
_class&
$"loc:@gradients_1/Sum_18_grad/Shape
 
gradients_1/Sum_18_grad/ReshapeReshape6gradients_1/Mean_8/input_grad/tuple/control_dependency%gradients_1/Sum_18_grad/DynamicStitch*
T0*
Tshape0

gradients_1/Sum_18_grad/TileTilegradients_1/Sum_18_grad/Reshape gradients_1/Sum_18_grad/floordiv*

Tmultiples0*
T0
X
gradients_1/Sum_19_grad/ShapeShapestrided_slice_19^Adam*
T0*
out_type0

gradients_1/Sum_19_grad/SizeConst^Adam*0
_class&
$"loc:@gradients_1/Sum_19_grad/Shape*
value	B :*
dtype0

gradients_1/Sum_19_grad/addAddV2Sum_19/reduction_indicesgradients_1/Sum_19_grad/Size*
T0*0
_class&
$"loc:@gradients_1/Sum_19_grad/Shape

gradients_1/Sum_19_grad/modFloorModgradients_1/Sum_19_grad/addgradients_1/Sum_19_grad/Size*
T0*0
_class&
$"loc:@gradients_1/Sum_19_grad/Shape

gradients_1/Sum_19_grad/Shape_1Const^Adam*0
_class&
$"loc:@gradients_1/Sum_19_grad/Shape*
valueB *
dtype0

#gradients_1/Sum_19_grad/range/startConst^Adam*0
_class&
$"loc:@gradients_1/Sum_19_grad/Shape*
value	B : *
dtype0

#gradients_1/Sum_19_grad/range/deltaConst^Adam*0
_class&
$"loc:@gradients_1/Sum_19_grad/Shape*
value	B :*
dtype0
Ģ
gradients_1/Sum_19_grad/rangeRange#gradients_1/Sum_19_grad/range/startgradients_1/Sum_19_grad/Size#gradients_1/Sum_19_grad/range/delta*

Tidx0*0
_class&
$"loc:@gradients_1/Sum_19_grad/Shape

"gradients_1/Sum_19_grad/Fill/valueConst^Adam*0
_class&
$"loc:@gradients_1/Sum_19_grad/Shape*
value	B :*
dtype0
¶
gradients_1/Sum_19_grad/FillFillgradients_1/Sum_19_grad/Shape_1"gradients_1/Sum_19_grad/Fill/value*
T0*0
_class&
$"loc:@gradients_1/Sum_19_grad/Shape*

index_type0
ó
%gradients_1/Sum_19_grad/DynamicStitchDynamicStitchgradients_1/Sum_19_grad/rangegradients_1/Sum_19_grad/modgradients_1/Sum_19_grad/Shapegradients_1/Sum_19_grad/Fill*
T0*0
_class&
$"loc:@gradients_1/Sum_19_grad/Shape*
N

!gradients_1/Sum_19_grad/Maximum/yConst^Adam*0
_class&
$"loc:@gradients_1/Sum_19_grad/Shape*
value	B :*
dtype0
Æ
gradients_1/Sum_19_grad/MaximumMaximum%gradients_1/Sum_19_grad/DynamicStitch!gradients_1/Sum_19_grad/Maximum/y*
T0*0
_class&
$"loc:@gradients_1/Sum_19_grad/Shape
§
 gradients_1/Sum_19_grad/floordivFloorDivgradients_1/Sum_19_grad/Shapegradients_1/Sum_19_grad/Maximum*
T0*0
_class&
$"loc:@gradients_1/Sum_19_grad/Shape
¢
gradients_1/Sum_19_grad/ReshapeReshape8gradients_1/Mean_8/input_grad/tuple/control_dependency_1%gradients_1/Sum_19_grad/DynamicStitch*
T0*
Tshape0

gradients_1/Sum_19_grad/TileTilegradients_1/Sum_19_grad/Reshape gradients_1/Sum_19_grad/floordiv*

Tmultiples0*
T0
X
gradients_1/Sum_20_grad/ShapeShapestrided_slice_20^Adam*
T0*
out_type0

gradients_1/Sum_20_grad/SizeConst^Adam*0
_class&
$"loc:@gradients_1/Sum_20_grad/Shape*
value	B :*
dtype0

gradients_1/Sum_20_grad/addAddV2Sum_20/reduction_indicesgradients_1/Sum_20_grad/Size*
T0*0
_class&
$"loc:@gradients_1/Sum_20_grad/Shape

gradients_1/Sum_20_grad/modFloorModgradients_1/Sum_20_grad/addgradients_1/Sum_20_grad/Size*
T0*0
_class&
$"loc:@gradients_1/Sum_20_grad/Shape

gradients_1/Sum_20_grad/Shape_1Const^Adam*0
_class&
$"loc:@gradients_1/Sum_20_grad/Shape*
valueB *
dtype0

#gradients_1/Sum_20_grad/range/startConst^Adam*0
_class&
$"loc:@gradients_1/Sum_20_grad/Shape*
value	B : *
dtype0

#gradients_1/Sum_20_grad/range/deltaConst^Adam*0
_class&
$"loc:@gradients_1/Sum_20_grad/Shape*
value	B :*
dtype0
Ģ
gradients_1/Sum_20_grad/rangeRange#gradients_1/Sum_20_grad/range/startgradients_1/Sum_20_grad/Size#gradients_1/Sum_20_grad/range/delta*

Tidx0*0
_class&
$"loc:@gradients_1/Sum_20_grad/Shape

"gradients_1/Sum_20_grad/Fill/valueConst^Adam*0
_class&
$"loc:@gradients_1/Sum_20_grad/Shape*
value	B :*
dtype0
¶
gradients_1/Sum_20_grad/FillFillgradients_1/Sum_20_grad/Shape_1"gradients_1/Sum_20_grad/Fill/value*
T0*0
_class&
$"loc:@gradients_1/Sum_20_grad/Shape*

index_type0
ó
%gradients_1/Sum_20_grad/DynamicStitchDynamicStitchgradients_1/Sum_20_grad/rangegradients_1/Sum_20_grad/modgradients_1/Sum_20_grad/Shapegradients_1/Sum_20_grad/Fill*
T0*0
_class&
$"loc:@gradients_1/Sum_20_grad/Shape*
N

!gradients_1/Sum_20_grad/Maximum/yConst^Adam*0
_class&
$"loc:@gradients_1/Sum_20_grad/Shape*
value	B :*
dtype0
Æ
gradients_1/Sum_20_grad/MaximumMaximum%gradients_1/Sum_20_grad/DynamicStitch!gradients_1/Sum_20_grad/Maximum/y*
T0*0
_class&
$"loc:@gradients_1/Sum_20_grad/Shape
§
 gradients_1/Sum_20_grad/floordivFloorDivgradients_1/Sum_20_grad/Shapegradients_1/Sum_20_grad/Maximum*
T0*0
_class&
$"loc:@gradients_1/Sum_20_grad/Shape
¢
gradients_1/Sum_20_grad/ReshapeReshape8gradients_1/Mean_8/input_grad/tuple/control_dependency_2%gradients_1/Sum_20_grad/DynamicStitch*
T0*
Tshape0

gradients_1/Sum_20_grad/TileTilegradients_1/Sum_20_grad/Reshape gradients_1/Sum_20_grad/floordiv*

Tmultiples0*
T0
X
gradients_1/Sum_15_grad/ShapeShapestrided_slice_15^Adam*
T0*
out_type0

gradients_1/Sum_15_grad/SizeConst^Adam*0
_class&
$"loc:@gradients_1/Sum_15_grad/Shape*
value	B :*
dtype0

gradients_1/Sum_15_grad/addAddV2Sum_15/reduction_indicesgradients_1/Sum_15_grad/Size*
T0*0
_class&
$"loc:@gradients_1/Sum_15_grad/Shape

gradients_1/Sum_15_grad/modFloorModgradients_1/Sum_15_grad/addgradients_1/Sum_15_grad/Size*
T0*0
_class&
$"loc:@gradients_1/Sum_15_grad/Shape

gradients_1/Sum_15_grad/Shape_1Const^Adam*0
_class&
$"loc:@gradients_1/Sum_15_grad/Shape*
valueB *
dtype0

#gradients_1/Sum_15_grad/range/startConst^Adam*0
_class&
$"loc:@gradients_1/Sum_15_grad/Shape*
value	B : *
dtype0

#gradients_1/Sum_15_grad/range/deltaConst^Adam*0
_class&
$"loc:@gradients_1/Sum_15_grad/Shape*
value	B :*
dtype0
Ģ
gradients_1/Sum_15_grad/rangeRange#gradients_1/Sum_15_grad/range/startgradients_1/Sum_15_grad/Size#gradients_1/Sum_15_grad/range/delta*

Tidx0*0
_class&
$"loc:@gradients_1/Sum_15_grad/Shape

"gradients_1/Sum_15_grad/Fill/valueConst^Adam*0
_class&
$"loc:@gradients_1/Sum_15_grad/Shape*
value	B :*
dtype0
¶
gradients_1/Sum_15_grad/FillFillgradients_1/Sum_15_grad/Shape_1"gradients_1/Sum_15_grad/Fill/value*
T0*0
_class&
$"loc:@gradients_1/Sum_15_grad/Shape*

index_type0
ó
%gradients_1/Sum_15_grad/DynamicStitchDynamicStitchgradients_1/Sum_15_grad/rangegradients_1/Sum_15_grad/modgradients_1/Sum_15_grad/Shapegradients_1/Sum_15_grad/Fill*
T0*0
_class&
$"loc:@gradients_1/Sum_15_grad/Shape*
N

!gradients_1/Sum_15_grad/Maximum/yConst^Adam*0
_class&
$"loc:@gradients_1/Sum_15_grad/Shape*
value	B :*
dtype0
Æ
gradients_1/Sum_15_grad/MaximumMaximum%gradients_1/Sum_15_grad/DynamicStitch!gradients_1/Sum_15_grad/Maximum/y*
T0*0
_class&
$"loc:@gradients_1/Sum_15_grad/Shape
§
 gradients_1/Sum_15_grad/floordivFloorDivgradients_1/Sum_15_grad/Shapegradients_1/Sum_15_grad/Maximum*
T0*0
_class&
$"loc:@gradients_1/Sum_15_grad/Shape
 
gradients_1/Sum_15_grad/ReshapeReshape6gradients_1/Mean_5/input_grad/tuple/control_dependency%gradients_1/Sum_15_grad/DynamicStitch*
T0*
Tshape0

gradients_1/Sum_15_grad/TileTilegradients_1/Sum_15_grad/Reshape gradients_1/Sum_15_grad/floordiv*

Tmultiples0*
T0
X
gradients_1/Sum_16_grad/ShapeShapestrided_slice_16^Adam*
T0*
out_type0

gradients_1/Sum_16_grad/SizeConst^Adam*0
_class&
$"loc:@gradients_1/Sum_16_grad/Shape*
value	B :*
dtype0

gradients_1/Sum_16_grad/addAddV2Sum_16/reduction_indicesgradients_1/Sum_16_grad/Size*
T0*0
_class&
$"loc:@gradients_1/Sum_16_grad/Shape

gradients_1/Sum_16_grad/modFloorModgradients_1/Sum_16_grad/addgradients_1/Sum_16_grad/Size*
T0*0
_class&
$"loc:@gradients_1/Sum_16_grad/Shape

gradients_1/Sum_16_grad/Shape_1Const^Adam*0
_class&
$"loc:@gradients_1/Sum_16_grad/Shape*
valueB *
dtype0

#gradients_1/Sum_16_grad/range/startConst^Adam*0
_class&
$"loc:@gradients_1/Sum_16_grad/Shape*
value	B : *
dtype0

#gradients_1/Sum_16_grad/range/deltaConst^Adam*0
_class&
$"loc:@gradients_1/Sum_16_grad/Shape*
value	B :*
dtype0
Ģ
gradients_1/Sum_16_grad/rangeRange#gradients_1/Sum_16_grad/range/startgradients_1/Sum_16_grad/Size#gradients_1/Sum_16_grad/range/delta*

Tidx0*0
_class&
$"loc:@gradients_1/Sum_16_grad/Shape

"gradients_1/Sum_16_grad/Fill/valueConst^Adam*0
_class&
$"loc:@gradients_1/Sum_16_grad/Shape*
value	B :*
dtype0
¶
gradients_1/Sum_16_grad/FillFillgradients_1/Sum_16_grad/Shape_1"gradients_1/Sum_16_grad/Fill/value*
T0*0
_class&
$"loc:@gradients_1/Sum_16_grad/Shape*

index_type0
ó
%gradients_1/Sum_16_grad/DynamicStitchDynamicStitchgradients_1/Sum_16_grad/rangegradients_1/Sum_16_grad/modgradients_1/Sum_16_grad/Shapegradients_1/Sum_16_grad/Fill*
T0*0
_class&
$"loc:@gradients_1/Sum_16_grad/Shape*
N

!gradients_1/Sum_16_grad/Maximum/yConst^Adam*0
_class&
$"loc:@gradients_1/Sum_16_grad/Shape*
value	B :*
dtype0
Æ
gradients_1/Sum_16_grad/MaximumMaximum%gradients_1/Sum_16_grad/DynamicStitch!gradients_1/Sum_16_grad/Maximum/y*
T0*0
_class&
$"loc:@gradients_1/Sum_16_grad/Shape
§
 gradients_1/Sum_16_grad/floordivFloorDivgradients_1/Sum_16_grad/Shapegradients_1/Sum_16_grad/Maximum*
T0*0
_class&
$"loc:@gradients_1/Sum_16_grad/Shape
¢
gradients_1/Sum_16_grad/ReshapeReshape8gradients_1/Mean_5/input_grad/tuple/control_dependency_1%gradients_1/Sum_16_grad/DynamicStitch*
T0*
Tshape0

gradients_1/Sum_16_grad/TileTilegradients_1/Sum_16_grad/Reshape gradients_1/Sum_16_grad/floordiv*

Tmultiples0*
T0
X
gradients_1/Sum_17_grad/ShapeShapestrided_slice_17^Adam*
T0*
out_type0

gradients_1/Sum_17_grad/SizeConst^Adam*0
_class&
$"loc:@gradients_1/Sum_17_grad/Shape*
value	B :*
dtype0

gradients_1/Sum_17_grad/addAddV2Sum_17/reduction_indicesgradients_1/Sum_17_grad/Size*
T0*0
_class&
$"loc:@gradients_1/Sum_17_grad/Shape

gradients_1/Sum_17_grad/modFloorModgradients_1/Sum_17_grad/addgradients_1/Sum_17_grad/Size*
T0*0
_class&
$"loc:@gradients_1/Sum_17_grad/Shape

gradients_1/Sum_17_grad/Shape_1Const^Adam*0
_class&
$"loc:@gradients_1/Sum_17_grad/Shape*
valueB *
dtype0

#gradients_1/Sum_17_grad/range/startConst^Adam*0
_class&
$"loc:@gradients_1/Sum_17_grad/Shape*
value	B : *
dtype0

#gradients_1/Sum_17_grad/range/deltaConst^Adam*0
_class&
$"loc:@gradients_1/Sum_17_grad/Shape*
value	B :*
dtype0
Ģ
gradients_1/Sum_17_grad/rangeRange#gradients_1/Sum_17_grad/range/startgradients_1/Sum_17_grad/Size#gradients_1/Sum_17_grad/range/delta*

Tidx0*0
_class&
$"loc:@gradients_1/Sum_17_grad/Shape

"gradients_1/Sum_17_grad/Fill/valueConst^Adam*0
_class&
$"loc:@gradients_1/Sum_17_grad/Shape*
value	B :*
dtype0
¶
gradients_1/Sum_17_grad/FillFillgradients_1/Sum_17_grad/Shape_1"gradients_1/Sum_17_grad/Fill/value*
T0*0
_class&
$"loc:@gradients_1/Sum_17_grad/Shape*

index_type0
ó
%gradients_1/Sum_17_grad/DynamicStitchDynamicStitchgradients_1/Sum_17_grad/rangegradients_1/Sum_17_grad/modgradients_1/Sum_17_grad/Shapegradients_1/Sum_17_grad/Fill*
T0*0
_class&
$"loc:@gradients_1/Sum_17_grad/Shape*
N

!gradients_1/Sum_17_grad/Maximum/yConst^Adam*0
_class&
$"loc:@gradients_1/Sum_17_grad/Shape*
value	B :*
dtype0
Æ
gradients_1/Sum_17_grad/MaximumMaximum%gradients_1/Sum_17_grad/DynamicStitch!gradients_1/Sum_17_grad/Maximum/y*
T0*0
_class&
$"loc:@gradients_1/Sum_17_grad/Shape
§
 gradients_1/Sum_17_grad/floordivFloorDivgradients_1/Sum_17_grad/Shapegradients_1/Sum_17_grad/Maximum*
T0*0
_class&
$"loc:@gradients_1/Sum_17_grad/Shape
¢
gradients_1/Sum_17_grad/ReshapeReshape8gradients_1/Mean_5/input_grad/tuple/control_dependency_2%gradients_1/Sum_17_grad/DynamicStitch*
T0*
Tshape0

gradients_1/Sum_17_grad/TileTilegradients_1/Sum_17_grad/Reshape gradients_1/Sum_17_grad/floordiv*

Tmultiples0*
T0
X
gradients_1/Sum_21_grad/ShapeShapestrided_slice_21^Adam*
T0*
out_type0

gradients_1/Sum_21_grad/SizeConst^Adam*0
_class&
$"loc:@gradients_1/Sum_21_grad/Shape*
value	B :*
dtype0

gradients_1/Sum_21_grad/addAddV2Sum_21/reduction_indicesgradients_1/Sum_21_grad/Size*
T0*0
_class&
$"loc:@gradients_1/Sum_21_grad/Shape

gradients_1/Sum_21_grad/modFloorModgradients_1/Sum_21_grad/addgradients_1/Sum_21_grad/Size*
T0*0
_class&
$"loc:@gradients_1/Sum_21_grad/Shape

gradients_1/Sum_21_grad/Shape_1Const^Adam*0
_class&
$"loc:@gradients_1/Sum_21_grad/Shape*
valueB *
dtype0

#gradients_1/Sum_21_grad/range/startConst^Adam*0
_class&
$"loc:@gradients_1/Sum_21_grad/Shape*
value	B : *
dtype0

#gradients_1/Sum_21_grad/range/deltaConst^Adam*0
_class&
$"loc:@gradients_1/Sum_21_grad/Shape*
value	B :*
dtype0
Ģ
gradients_1/Sum_21_grad/rangeRange#gradients_1/Sum_21_grad/range/startgradients_1/Sum_21_grad/Size#gradients_1/Sum_21_grad/range/delta*

Tidx0*0
_class&
$"loc:@gradients_1/Sum_21_grad/Shape

"gradients_1/Sum_21_grad/Fill/valueConst^Adam*0
_class&
$"loc:@gradients_1/Sum_21_grad/Shape*
value	B :*
dtype0
¶
gradients_1/Sum_21_grad/FillFillgradients_1/Sum_21_grad/Shape_1"gradients_1/Sum_21_grad/Fill/value*
T0*0
_class&
$"loc:@gradients_1/Sum_21_grad/Shape*

index_type0
ó
%gradients_1/Sum_21_grad/DynamicStitchDynamicStitchgradients_1/Sum_21_grad/rangegradients_1/Sum_21_grad/modgradients_1/Sum_21_grad/Shapegradients_1/Sum_21_grad/Fill*
T0*0
_class&
$"loc:@gradients_1/Sum_21_grad/Shape*
N

!gradients_1/Sum_21_grad/Maximum/yConst^Adam*0
_class&
$"loc:@gradients_1/Sum_21_grad/Shape*
value	B :*
dtype0
Æ
gradients_1/Sum_21_grad/MaximumMaximum%gradients_1/Sum_21_grad/DynamicStitch!gradients_1/Sum_21_grad/Maximum/y*
T0*0
_class&
$"loc:@gradients_1/Sum_21_grad/Shape
§
 gradients_1/Sum_21_grad/floordivFloorDivgradients_1/Sum_21_grad/Shapegradients_1/Sum_21_grad/Maximum*
T0*0
_class&
$"loc:@gradients_1/Sum_21_grad/Shape
 
gradients_1/Sum_21_grad/ReshapeReshape6gradients_1/Mean_9/input_grad/tuple/control_dependency%gradients_1/Sum_21_grad/DynamicStitch*
T0*
Tshape0

gradients_1/Sum_21_grad/TileTilegradients_1/Sum_21_grad/Reshape gradients_1/Sum_21_grad/floordiv*

Tmultiples0*
T0
X
gradients_1/Sum_22_grad/ShapeShapestrided_slice_22^Adam*
T0*
out_type0

gradients_1/Sum_22_grad/SizeConst^Adam*0
_class&
$"loc:@gradients_1/Sum_22_grad/Shape*
value	B :*
dtype0

gradients_1/Sum_22_grad/addAddV2Sum_22/reduction_indicesgradients_1/Sum_22_grad/Size*
T0*0
_class&
$"loc:@gradients_1/Sum_22_grad/Shape

gradients_1/Sum_22_grad/modFloorModgradients_1/Sum_22_grad/addgradients_1/Sum_22_grad/Size*
T0*0
_class&
$"loc:@gradients_1/Sum_22_grad/Shape

gradients_1/Sum_22_grad/Shape_1Const^Adam*0
_class&
$"loc:@gradients_1/Sum_22_grad/Shape*
valueB *
dtype0

#gradients_1/Sum_22_grad/range/startConst^Adam*0
_class&
$"loc:@gradients_1/Sum_22_grad/Shape*
value	B : *
dtype0

#gradients_1/Sum_22_grad/range/deltaConst^Adam*0
_class&
$"loc:@gradients_1/Sum_22_grad/Shape*
value	B :*
dtype0
Ģ
gradients_1/Sum_22_grad/rangeRange#gradients_1/Sum_22_grad/range/startgradients_1/Sum_22_grad/Size#gradients_1/Sum_22_grad/range/delta*

Tidx0*0
_class&
$"loc:@gradients_1/Sum_22_grad/Shape

"gradients_1/Sum_22_grad/Fill/valueConst^Adam*0
_class&
$"loc:@gradients_1/Sum_22_grad/Shape*
value	B :*
dtype0
¶
gradients_1/Sum_22_grad/FillFillgradients_1/Sum_22_grad/Shape_1"gradients_1/Sum_22_grad/Fill/value*
T0*0
_class&
$"loc:@gradients_1/Sum_22_grad/Shape*

index_type0
ó
%gradients_1/Sum_22_grad/DynamicStitchDynamicStitchgradients_1/Sum_22_grad/rangegradients_1/Sum_22_grad/modgradients_1/Sum_22_grad/Shapegradients_1/Sum_22_grad/Fill*
T0*0
_class&
$"loc:@gradients_1/Sum_22_grad/Shape*
N

!gradients_1/Sum_22_grad/Maximum/yConst^Adam*0
_class&
$"loc:@gradients_1/Sum_22_grad/Shape*
value	B :*
dtype0
Æ
gradients_1/Sum_22_grad/MaximumMaximum%gradients_1/Sum_22_grad/DynamicStitch!gradients_1/Sum_22_grad/Maximum/y*
T0*0
_class&
$"loc:@gradients_1/Sum_22_grad/Shape
§
 gradients_1/Sum_22_grad/floordivFloorDivgradients_1/Sum_22_grad/Shapegradients_1/Sum_22_grad/Maximum*
T0*0
_class&
$"loc:@gradients_1/Sum_22_grad/Shape
¢
gradients_1/Sum_22_grad/ReshapeReshape8gradients_1/Mean_9/input_grad/tuple/control_dependency_1%gradients_1/Sum_22_grad/DynamicStitch*
T0*
Tshape0

gradients_1/Sum_22_grad/TileTilegradients_1/Sum_22_grad/Reshape gradients_1/Sum_22_grad/floordiv*

Tmultiples0*
T0
X
gradients_1/Sum_23_grad/ShapeShapestrided_slice_23^Adam*
T0*
out_type0

gradients_1/Sum_23_grad/SizeConst^Adam*0
_class&
$"loc:@gradients_1/Sum_23_grad/Shape*
value	B :*
dtype0

gradients_1/Sum_23_grad/addAddV2Sum_23/reduction_indicesgradients_1/Sum_23_grad/Size*
T0*0
_class&
$"loc:@gradients_1/Sum_23_grad/Shape

gradients_1/Sum_23_grad/modFloorModgradients_1/Sum_23_grad/addgradients_1/Sum_23_grad/Size*
T0*0
_class&
$"loc:@gradients_1/Sum_23_grad/Shape

gradients_1/Sum_23_grad/Shape_1Const^Adam*0
_class&
$"loc:@gradients_1/Sum_23_grad/Shape*
valueB *
dtype0

#gradients_1/Sum_23_grad/range/startConst^Adam*0
_class&
$"loc:@gradients_1/Sum_23_grad/Shape*
value	B : *
dtype0

#gradients_1/Sum_23_grad/range/deltaConst^Adam*0
_class&
$"loc:@gradients_1/Sum_23_grad/Shape*
value	B :*
dtype0
Ģ
gradients_1/Sum_23_grad/rangeRange#gradients_1/Sum_23_grad/range/startgradients_1/Sum_23_grad/Size#gradients_1/Sum_23_grad/range/delta*

Tidx0*0
_class&
$"loc:@gradients_1/Sum_23_grad/Shape

"gradients_1/Sum_23_grad/Fill/valueConst^Adam*0
_class&
$"loc:@gradients_1/Sum_23_grad/Shape*
value	B :*
dtype0
¶
gradients_1/Sum_23_grad/FillFillgradients_1/Sum_23_grad/Shape_1"gradients_1/Sum_23_grad/Fill/value*
T0*0
_class&
$"loc:@gradients_1/Sum_23_grad/Shape*

index_type0
ó
%gradients_1/Sum_23_grad/DynamicStitchDynamicStitchgradients_1/Sum_23_grad/rangegradients_1/Sum_23_grad/modgradients_1/Sum_23_grad/Shapegradients_1/Sum_23_grad/Fill*
T0*0
_class&
$"loc:@gradients_1/Sum_23_grad/Shape*
N

!gradients_1/Sum_23_grad/Maximum/yConst^Adam*0
_class&
$"loc:@gradients_1/Sum_23_grad/Shape*
value	B :*
dtype0
Æ
gradients_1/Sum_23_grad/MaximumMaximum%gradients_1/Sum_23_grad/DynamicStitch!gradients_1/Sum_23_grad/Maximum/y*
T0*0
_class&
$"loc:@gradients_1/Sum_23_grad/Shape
§
 gradients_1/Sum_23_grad/floordivFloorDivgradients_1/Sum_23_grad/Shapegradients_1/Sum_23_grad/Maximum*
T0*0
_class&
$"loc:@gradients_1/Sum_23_grad/Shape
¢
gradients_1/Sum_23_grad/ReshapeReshape8gradients_1/Mean_9/input_grad/tuple/control_dependency_2%gradients_1/Sum_23_grad/DynamicStitch*
T0*
Tshape0

gradients_1/Sum_23_grad/TileTilegradients_1/Sum_23_grad/Reshape gradients_1/Sum_23_grad/floordiv*

Tmultiples0*
T0
Š
Bgradients_1/critic/value/encoder/hidden_1/Sigmoid_grad/SigmoidGradSigmoidGrad%critic/value/encoder/hidden_1/SigmoidMgradients_1/critic/value/encoder/hidden_1/Mul_grad/tuple/control_dependency_1*
T0
W
'gradients_1/strided_slice_12_grad/ShapeShapemul_7^Adam*
T0*
out_type0
Ó
2gradients_1/strided_slice_12_grad/StridedSliceGradStridedSliceGrad'gradients_1/strided_slice_12_grad/Shapestrided_slice_12/stackstrided_slice_12/stack_1strided_slice_12/stack_2gradients_1/Sum_12_grad/Tile*
Index0*
T0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
W
'gradients_1/strided_slice_13_grad/ShapeShapemul_7^Adam*
T0*
out_type0
Ó
2gradients_1/strided_slice_13_grad/StridedSliceGradStridedSliceGrad'gradients_1/strided_slice_13_grad/Shapestrided_slice_13/stackstrided_slice_13/stack_1strided_slice_13/stack_2gradients_1/Sum_13_grad/Tile*
Index0*
T0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
W
'gradients_1/strided_slice_14_grad/ShapeShapemul_7^Adam*
T0*
out_type0
Ó
2gradients_1/strided_slice_14_grad/StridedSliceGradStridedSliceGrad'gradients_1/strided_slice_14_grad/Shapestrided_slice_14/stackstrided_slice_14/stack_1strided_slice_14/stack_2gradients_1/Sum_14_grad/Tile*
Index0*
T0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
X
'gradients_1/strided_slice_18_grad/ShapeShapemul_16^Adam*
T0*
out_type0
Ó
2gradients_1/strided_slice_18_grad/StridedSliceGradStridedSliceGrad'gradients_1/strided_slice_18_grad/Shapestrided_slice_18/stackstrided_slice_18/stack_1strided_slice_18/stack_2gradients_1/Sum_18_grad/Tile*
Index0*
T0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
X
'gradients_1/strided_slice_19_grad/ShapeShapemul_16^Adam*
T0*
out_type0
Ó
2gradients_1/strided_slice_19_grad/StridedSliceGradStridedSliceGrad'gradients_1/strided_slice_19_grad/Shapestrided_slice_19/stackstrided_slice_19/stack_1strided_slice_19/stack_2gradients_1/Sum_19_grad/Tile*
Index0*
T0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
X
'gradients_1/strided_slice_20_grad/ShapeShapemul_16^Adam*
T0*
out_type0
Ó
2gradients_1/strided_slice_20_grad/StridedSliceGradStridedSliceGrad'gradients_1/strided_slice_20_grad/Shapestrided_slice_20/stackstrided_slice_20/stack_1strided_slice_20/stack_2gradients_1/Sum_20_grad/Tile*
Index0*
T0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
W
'gradients_1/strided_slice_15_grad/ShapeShapemul_8^Adam*
T0*
out_type0
Ó
2gradients_1/strided_slice_15_grad/StridedSliceGradStridedSliceGrad'gradients_1/strided_slice_15_grad/Shapestrided_slice_15/stackstrided_slice_15/stack_1strided_slice_15/stack_2gradients_1/Sum_15_grad/Tile*
Index0*
T0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
W
'gradients_1/strided_slice_16_grad/ShapeShapemul_8^Adam*
T0*
out_type0
Ó
2gradients_1/strided_slice_16_grad/StridedSliceGradStridedSliceGrad'gradients_1/strided_slice_16_grad/Shapestrided_slice_16/stackstrided_slice_16/stack_1strided_slice_16/stack_2gradients_1/Sum_16_grad/Tile*
Index0*
T0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
W
'gradients_1/strided_slice_17_grad/ShapeShapemul_8^Adam*
T0*
out_type0
Ó
2gradients_1/strided_slice_17_grad/StridedSliceGradStridedSliceGrad'gradients_1/strided_slice_17_grad/Shapestrided_slice_17/stackstrided_slice_17/stack_1strided_slice_17/stack_2gradients_1/Sum_17_grad/Tile*
Index0*
T0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
X
'gradients_1/strided_slice_21_grad/ShapeShapemul_17^Adam*
T0*
out_type0
Ó
2gradients_1/strided_slice_21_grad/StridedSliceGradStridedSliceGrad'gradients_1/strided_slice_21_grad/Shapestrided_slice_21/stackstrided_slice_21/stack_1strided_slice_21/stack_2gradients_1/Sum_21_grad/Tile*
Index0*
T0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
X
'gradients_1/strided_slice_22_grad/ShapeShapemul_17^Adam*
T0*
out_type0
Ó
2gradients_1/strided_slice_22_grad/StridedSliceGradStridedSliceGrad'gradients_1/strided_slice_22_grad/Shapestrided_slice_22/stackstrided_slice_22/stack_1strided_slice_22/stack_2gradients_1/Sum_22_grad/Tile*
Index0*
T0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
X
'gradients_1/strided_slice_23_grad/ShapeShapemul_17^Adam*
T0*
out_type0
Ó
2gradients_1/strided_slice_23_grad/StridedSliceGradStridedSliceGrad'gradients_1/strided_slice_23_grad/Shapestrided_slice_23/stackstrided_slice_23/stack_1strided_slice_23/stack_2gradients_1/Sum_23_grad/Tile*
Index0*
T0*
shrink_axis_mask *

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask

gradients_1/AddN_1AddNKgradients_1/critic/value/encoder/hidden_1/Mul_grad/tuple/control_dependencyBgradients_1/critic/value/encoder/hidden_1/Sigmoid_grad/SigmoidGrad*
T0*M
_classC
A?loc:@gradients_1/critic/value/encoder/hidden_1/Mul_grad/Reshape*
N

Bgradients_1/critic/value/encoder/hidden_1/BiasAdd_grad/BiasAddGradBiasAddGradgradients_1/AddN_1*
T0*
data_formatNHWC
°
Ggradients_1/critic/value/encoder/hidden_1/BiasAdd_grad/tuple/group_depsNoOp^Adam^gradients_1/AddN_1C^gradients_1/critic/value/encoder/hidden_1/BiasAdd_grad/BiasAddGrad

Ogradients_1/critic/value/encoder/hidden_1/BiasAdd_grad/tuple/control_dependencyIdentitygradients_1/AddN_1H^gradients_1/critic/value/encoder/hidden_1/BiasAdd_grad/tuple/group_deps*
T0*M
_classC
A?loc:@gradients_1/critic/value/encoder/hidden_1/Mul_grad/Reshape
Ė
Qgradients_1/critic/value/encoder/hidden_1/BiasAdd_grad/tuple/control_dependency_1IdentityBgradients_1/critic/value/encoder/hidden_1/BiasAdd_grad/BiasAddGradH^gradients_1/critic/value/encoder/hidden_1/BiasAdd_grad/tuple/group_deps*
T0*U
_classK
IGloc:@gradients_1/critic/value/encoder/hidden_1/BiasAdd_grad/BiasAddGrad

gradients_1/AddN_2AddN2gradients_1/strided_slice_12_grad/StridedSliceGrad2gradients_1/strided_slice_13_grad/StridedSliceGrad2gradients_1/strided_slice_14_grad/StridedSliceGrad*
T0*E
_class;
97loc:@gradients_1/strided_slice_12_grad/StridedSliceGrad*
N
X
gradients_1/mul_7_grad/ShapeShapepolicy_1/concat_4^Adam*
T0*
out_type0
r
gradients_1/mul_7_grad/Shape_1Shape)critic/q/q1_encoding/extrinsic_q1/BiasAdd^Adam*
T0*
out_type0

,gradients_1/mul_7_grad/BroadcastGradientArgsBroadcastGradientArgsgradients_1/mul_7_grad/Shapegradients_1/mul_7_grad/Shape_1*
T0
i
gradients_1/mul_7_grad/MulMulgradients_1/AddN_2)critic/q/q1_encoding/extrinsic_q1/BiasAdd*
T0

gradients_1/mul_7_grad/SumSumgradients_1/mul_7_grad/Mul,gradients_1/mul_7_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0
z
gradients_1/mul_7_grad/ReshapeReshapegradients_1/mul_7_grad/Sumgradients_1/mul_7_grad/Shape*
T0*
Tshape0
S
gradients_1/mul_7_grad/Mul_1Mulpolicy_1/concat_4gradients_1/AddN_2*
T0

gradients_1/mul_7_grad/Sum_1Sumgradients_1/mul_7_grad/Mul_1.gradients_1/mul_7_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0

 gradients_1/mul_7_grad/Reshape_1Reshapegradients_1/mul_7_grad/Sum_1gradients_1/mul_7_grad/Shape_1*
T0*
Tshape0
z
'gradients_1/mul_7_grad/tuple/group_depsNoOp^Adam^gradients_1/mul_7_grad/Reshape!^gradients_1/mul_7_grad/Reshape_1
Į
/gradients_1/mul_7_grad/tuple/control_dependencyIdentitygradients_1/mul_7_grad/Reshape(^gradients_1/mul_7_grad/tuple/group_deps*
T0*1
_class'
%#loc:@gradients_1/mul_7_grad/Reshape
Ē
1gradients_1/mul_7_grad/tuple/control_dependency_1Identity gradients_1/mul_7_grad/Reshape_1(^gradients_1/mul_7_grad/tuple/group_deps*
T0*3
_class)
'%loc:@gradients_1/mul_7_grad/Reshape_1

gradients_1/AddN_3AddN2gradients_1/strided_slice_18_grad/StridedSliceGrad2gradients_1/strided_slice_19_grad/StridedSliceGrad2gradients_1/strided_slice_20_grad/StridedSliceGrad*
T0*E
_class;
97loc:@gradients_1/strided_slice_18_grad/StridedSliceGrad*
N
Y
gradients_1/mul_16_grad/ShapeShapepolicy_1/concat_4^Adam*
T0*
out_type0
n
gradients_1/mul_16_grad/Shape_1Shape$critic/q/q1_encoding/gail_q1/BiasAdd^Adam*
T0*
out_type0

-gradients_1/mul_16_grad/BroadcastGradientArgsBroadcastGradientArgsgradients_1/mul_16_grad/Shapegradients_1/mul_16_grad/Shape_1*
T0
e
gradients_1/mul_16_grad/MulMulgradients_1/AddN_3$critic/q/q1_encoding/gail_q1/BiasAdd*
T0

gradients_1/mul_16_grad/SumSumgradients_1/mul_16_grad/Mul-gradients_1/mul_16_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0
}
gradients_1/mul_16_grad/ReshapeReshapegradients_1/mul_16_grad/Sumgradients_1/mul_16_grad/Shape*
T0*
Tshape0
T
gradients_1/mul_16_grad/Mul_1Mulpolicy_1/concat_4gradients_1/AddN_3*
T0

gradients_1/mul_16_grad/Sum_1Sumgradients_1/mul_16_grad/Mul_1/gradients_1/mul_16_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0

!gradients_1/mul_16_grad/Reshape_1Reshapegradients_1/mul_16_grad/Sum_1gradients_1/mul_16_grad/Shape_1*
T0*
Tshape0
}
(gradients_1/mul_16_grad/tuple/group_depsNoOp^Adam ^gradients_1/mul_16_grad/Reshape"^gradients_1/mul_16_grad/Reshape_1
Å
0gradients_1/mul_16_grad/tuple/control_dependencyIdentitygradients_1/mul_16_grad/Reshape)^gradients_1/mul_16_grad/tuple/group_deps*
T0*2
_class(
&$loc:@gradients_1/mul_16_grad/Reshape
Ė
2gradients_1/mul_16_grad/tuple/control_dependency_1Identity!gradients_1/mul_16_grad/Reshape_1)^gradients_1/mul_16_grad/tuple/group_deps*
T0*4
_class*
(&loc:@gradients_1/mul_16_grad/Reshape_1

gradients_1/AddN_4AddN2gradients_1/strided_slice_15_grad/StridedSliceGrad2gradients_1/strided_slice_16_grad/StridedSliceGrad2gradients_1/strided_slice_17_grad/StridedSliceGrad*
T0*E
_class;
97loc:@gradients_1/strided_slice_15_grad/StridedSliceGrad*
N
X
gradients_1/mul_8_grad/ShapeShapepolicy_1/concat_4^Adam*
T0*
out_type0
r
gradients_1/mul_8_grad/Shape_1Shape)critic/q/q2_encoding/extrinsic_q2/BiasAdd^Adam*
T0*
out_type0

,gradients_1/mul_8_grad/BroadcastGradientArgsBroadcastGradientArgsgradients_1/mul_8_grad/Shapegradients_1/mul_8_grad/Shape_1*
T0
i
gradients_1/mul_8_grad/MulMulgradients_1/AddN_4)critic/q/q2_encoding/extrinsic_q2/BiasAdd*
T0

gradients_1/mul_8_grad/SumSumgradients_1/mul_8_grad/Mul,gradients_1/mul_8_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0
z
gradients_1/mul_8_grad/ReshapeReshapegradients_1/mul_8_grad/Sumgradients_1/mul_8_grad/Shape*
T0*
Tshape0
S
gradients_1/mul_8_grad/Mul_1Mulpolicy_1/concat_4gradients_1/AddN_4*
T0

gradients_1/mul_8_grad/Sum_1Sumgradients_1/mul_8_grad/Mul_1.gradients_1/mul_8_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0

 gradients_1/mul_8_grad/Reshape_1Reshapegradients_1/mul_8_grad/Sum_1gradients_1/mul_8_grad/Shape_1*
T0*
Tshape0
z
'gradients_1/mul_8_grad/tuple/group_depsNoOp^Adam^gradients_1/mul_8_grad/Reshape!^gradients_1/mul_8_grad/Reshape_1
Į
/gradients_1/mul_8_grad/tuple/control_dependencyIdentitygradients_1/mul_8_grad/Reshape(^gradients_1/mul_8_grad/tuple/group_deps*
T0*1
_class'
%#loc:@gradients_1/mul_8_grad/Reshape
Ē
1gradients_1/mul_8_grad/tuple/control_dependency_1Identity gradients_1/mul_8_grad/Reshape_1(^gradients_1/mul_8_grad/tuple/group_deps*
T0*3
_class)
'%loc:@gradients_1/mul_8_grad/Reshape_1

gradients_1/AddN_5AddN2gradients_1/strided_slice_21_grad/StridedSliceGrad2gradients_1/strided_slice_22_grad/StridedSliceGrad2gradients_1/strided_slice_23_grad/StridedSliceGrad*
T0*E
_class;
97loc:@gradients_1/strided_slice_21_grad/StridedSliceGrad*
N
Y
gradients_1/mul_17_grad/ShapeShapepolicy_1/concat_4^Adam*
T0*
out_type0
n
gradients_1/mul_17_grad/Shape_1Shape$critic/q/q2_encoding/gail_q2/BiasAdd^Adam*
T0*
out_type0

-gradients_1/mul_17_grad/BroadcastGradientArgsBroadcastGradientArgsgradients_1/mul_17_grad/Shapegradients_1/mul_17_grad/Shape_1*
T0
e
gradients_1/mul_17_grad/MulMulgradients_1/AddN_5$critic/q/q2_encoding/gail_q2/BiasAdd*
T0

gradients_1/mul_17_grad/SumSumgradients_1/mul_17_grad/Mul-gradients_1/mul_17_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0
}
gradients_1/mul_17_grad/ReshapeReshapegradients_1/mul_17_grad/Sumgradients_1/mul_17_grad/Shape*
T0*
Tshape0
T
gradients_1/mul_17_grad/Mul_1Mulpolicy_1/concat_4gradients_1/AddN_5*
T0

gradients_1/mul_17_grad/Sum_1Sumgradients_1/mul_17_grad/Mul_1/gradients_1/mul_17_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0

!gradients_1/mul_17_grad/Reshape_1Reshapegradients_1/mul_17_grad/Sum_1gradients_1/mul_17_grad/Shape_1*
T0*
Tshape0
}
(gradients_1/mul_17_grad/tuple/group_depsNoOp^Adam ^gradients_1/mul_17_grad/Reshape"^gradients_1/mul_17_grad/Reshape_1
Å
0gradients_1/mul_17_grad/tuple/control_dependencyIdentitygradients_1/mul_17_grad/Reshape)^gradients_1/mul_17_grad/tuple/group_deps*
T0*2
_class(
&$loc:@gradients_1/mul_17_grad/Reshape
Ė
2gradients_1/mul_17_grad/tuple/control_dependency_1Identity!gradients_1/mul_17_grad/Reshape_1)^gradients_1/mul_17_grad/tuple/group_deps*
T0*4
_class*
(&loc:@gradients_1/mul_17_grad/Reshape_1
ń
<gradients_1/critic/value/encoder/hidden_1/MatMul_grad/MatMulMatMulOgradients_1/critic/value/encoder/hidden_1/BiasAdd_grad/tuple/control_dependency)critic/value/encoder/hidden_1/kernel/read*
transpose_b(*
T0*
transpose_a( 
ė
>gradients_1/critic/value/encoder/hidden_1/MatMul_grad/MatMul_1MatMul!critic/value/encoder/hidden_0/MulOgradients_1/critic/value/encoder/hidden_1/BiasAdd_grad/tuple/control_dependency*
transpose_b( *
T0*
transpose_a(
Õ
Fgradients_1/critic/value/encoder/hidden_1/MatMul_grad/tuple/group_depsNoOp^Adam=^gradients_1/critic/value/encoder/hidden_1/MatMul_grad/MatMul?^gradients_1/critic/value/encoder/hidden_1/MatMul_grad/MatMul_1
»
Ngradients_1/critic/value/encoder/hidden_1/MatMul_grad/tuple/control_dependencyIdentity<gradients_1/critic/value/encoder/hidden_1/MatMul_grad/MatMulG^gradients_1/critic/value/encoder/hidden_1/MatMul_grad/tuple/group_deps*
T0*O
_classE
CAloc:@gradients_1/critic/value/encoder/hidden_1/MatMul_grad/MatMul
Į
Pgradients_1/critic/value/encoder/hidden_1/MatMul_grad/tuple/control_dependency_1Identity>gradients_1/critic/value/encoder/hidden_1/MatMul_grad/MatMul_1G^gradients_1/critic/value/encoder/hidden_1/MatMul_grad/tuple/group_deps*
T0*Q
_classG
ECloc:@gradients_1/critic/value/encoder/hidden_1/MatMul_grad/MatMul_1
Ø
Fgradients_1/critic/q/q1_encoding/extrinsic_q1/BiasAdd_grad/BiasAddGradBiasAddGrad1gradients_1/mul_7_grad/tuple/control_dependency_1*
T0*
data_formatNHWC
×
Kgradients_1/critic/q/q1_encoding/extrinsic_q1/BiasAdd_grad/tuple/group_depsNoOp^AdamG^gradients_1/critic/q/q1_encoding/extrinsic_q1/BiasAdd_grad/BiasAddGrad2^gradients_1/mul_7_grad/tuple/control_dependency_1

Sgradients_1/critic/q/q1_encoding/extrinsic_q1/BiasAdd_grad/tuple/control_dependencyIdentity1gradients_1/mul_7_grad/tuple/control_dependency_1L^gradients_1/critic/q/q1_encoding/extrinsic_q1/BiasAdd_grad/tuple/group_deps*
T0*3
_class)
'%loc:@gradients_1/mul_7_grad/Reshape_1
Ū
Ugradients_1/critic/q/q1_encoding/extrinsic_q1/BiasAdd_grad/tuple/control_dependency_1IdentityFgradients_1/critic/q/q1_encoding/extrinsic_q1/BiasAdd_grad/BiasAddGradL^gradients_1/critic/q/q1_encoding/extrinsic_q1/BiasAdd_grad/tuple/group_deps*
T0*Y
_classO
MKloc:@gradients_1/critic/q/q1_encoding/extrinsic_q1/BiasAdd_grad/BiasAddGrad
¤
Agradients_1/critic/q/q1_encoding/gail_q1/BiasAdd_grad/BiasAddGradBiasAddGrad2gradients_1/mul_16_grad/tuple/control_dependency_1*
T0*
data_formatNHWC
Ī
Fgradients_1/critic/q/q1_encoding/gail_q1/BiasAdd_grad/tuple/group_depsNoOp^AdamB^gradients_1/critic/q/q1_encoding/gail_q1/BiasAdd_grad/BiasAddGrad3^gradients_1/mul_16_grad/tuple/control_dependency_1

Ngradients_1/critic/q/q1_encoding/gail_q1/BiasAdd_grad/tuple/control_dependencyIdentity2gradients_1/mul_16_grad/tuple/control_dependency_1G^gradients_1/critic/q/q1_encoding/gail_q1/BiasAdd_grad/tuple/group_deps*
T0*4
_class*
(&loc:@gradients_1/mul_16_grad/Reshape_1
Ē
Pgradients_1/critic/q/q1_encoding/gail_q1/BiasAdd_grad/tuple/control_dependency_1IdentityAgradients_1/critic/q/q1_encoding/gail_q1/BiasAdd_grad/BiasAddGradG^gradients_1/critic/q/q1_encoding/gail_q1/BiasAdd_grad/tuple/group_deps*
T0*T
_classJ
HFloc:@gradients_1/critic/q/q1_encoding/gail_q1/BiasAdd_grad/BiasAddGrad
Ø
Fgradients_1/critic/q/q2_encoding/extrinsic_q2/BiasAdd_grad/BiasAddGradBiasAddGrad1gradients_1/mul_8_grad/tuple/control_dependency_1*
T0*
data_formatNHWC
×
Kgradients_1/critic/q/q2_encoding/extrinsic_q2/BiasAdd_grad/tuple/group_depsNoOp^AdamG^gradients_1/critic/q/q2_encoding/extrinsic_q2/BiasAdd_grad/BiasAddGrad2^gradients_1/mul_8_grad/tuple/control_dependency_1

Sgradients_1/critic/q/q2_encoding/extrinsic_q2/BiasAdd_grad/tuple/control_dependencyIdentity1gradients_1/mul_8_grad/tuple/control_dependency_1L^gradients_1/critic/q/q2_encoding/extrinsic_q2/BiasAdd_grad/tuple/group_deps*
T0*3
_class)
'%loc:@gradients_1/mul_8_grad/Reshape_1
Ū
Ugradients_1/critic/q/q2_encoding/extrinsic_q2/BiasAdd_grad/tuple/control_dependency_1IdentityFgradients_1/critic/q/q2_encoding/extrinsic_q2/BiasAdd_grad/BiasAddGradL^gradients_1/critic/q/q2_encoding/extrinsic_q2/BiasAdd_grad/tuple/group_deps*
T0*Y
_classO
MKloc:@gradients_1/critic/q/q2_encoding/extrinsic_q2/BiasAdd_grad/BiasAddGrad
¤
Agradients_1/critic/q/q2_encoding/gail_q2/BiasAdd_grad/BiasAddGradBiasAddGrad2gradients_1/mul_17_grad/tuple/control_dependency_1*
T0*
data_formatNHWC
Ī
Fgradients_1/critic/q/q2_encoding/gail_q2/BiasAdd_grad/tuple/group_depsNoOp^AdamB^gradients_1/critic/q/q2_encoding/gail_q2/BiasAdd_grad/BiasAddGrad3^gradients_1/mul_17_grad/tuple/control_dependency_1

Ngradients_1/critic/q/q2_encoding/gail_q2/BiasAdd_grad/tuple/control_dependencyIdentity2gradients_1/mul_17_grad/tuple/control_dependency_1G^gradients_1/critic/q/q2_encoding/gail_q2/BiasAdd_grad/tuple/group_deps*
T0*4
_class*
(&loc:@gradients_1/mul_17_grad/Reshape_1
Ē
Pgradients_1/critic/q/q2_encoding/gail_q2/BiasAdd_grad/tuple/control_dependency_1IdentityAgradients_1/critic/q/q2_encoding/gail_q2/BiasAdd_grad/BiasAddGradG^gradients_1/critic/q/q2_encoding/gail_q2/BiasAdd_grad/tuple/group_deps*
T0*T
_classJ
HFloc:@gradients_1/critic/q/q2_encoding/gail_q2/BiasAdd_grad/BiasAddGrad

8gradients_1/critic/value/encoder/hidden_0/Mul_grad/ShapeShape%critic/value/encoder/hidden_0/BiasAdd^Adam*
T0*
out_type0

:gradients_1/critic/value/encoder/hidden_0/Mul_grad/Shape_1Shape%critic/value/encoder/hidden_0/Sigmoid^Adam*
T0*
out_type0
ą
Hgradients_1/critic/value/encoder/hidden_0/Mul_grad/BroadcastGradientArgsBroadcastGradientArgs8gradients_1/critic/value/encoder/hidden_0/Mul_grad/Shape:gradients_1/critic/value/encoder/hidden_0/Mul_grad/Shape_1*
T0
½
6gradients_1/critic/value/encoder/hidden_0/Mul_grad/MulMulNgradients_1/critic/value/encoder/hidden_1/MatMul_grad/tuple/control_dependency%critic/value/encoder/hidden_0/Sigmoid*
T0
å
6gradients_1/critic/value/encoder/hidden_0/Mul_grad/SumSum6gradients_1/critic/value/encoder/hidden_0/Mul_grad/MulHgradients_1/critic/value/encoder/hidden_0/Mul_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0
Ī
:gradients_1/critic/value/encoder/hidden_0/Mul_grad/ReshapeReshape6gradients_1/critic/value/encoder/hidden_0/Mul_grad/Sum8gradients_1/critic/value/encoder/hidden_0/Mul_grad/Shape*
T0*
Tshape0
æ
8gradients_1/critic/value/encoder/hidden_0/Mul_grad/Mul_1Mul%critic/value/encoder/hidden_0/BiasAddNgradients_1/critic/value/encoder/hidden_1/MatMul_grad/tuple/control_dependency*
T0
ė
8gradients_1/critic/value/encoder/hidden_0/Mul_grad/Sum_1Sum8gradients_1/critic/value/encoder/hidden_0/Mul_grad/Mul_1Jgradients_1/critic/value/encoder/hidden_0/Mul_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0
Ō
<gradients_1/critic/value/encoder/hidden_0/Mul_grad/Reshape_1Reshape8gradients_1/critic/value/encoder/hidden_0/Mul_grad/Sum_1:gradients_1/critic/value/encoder/hidden_0/Mul_grad/Shape_1*
T0*
Tshape0
Ī
Cgradients_1/critic/value/encoder/hidden_0/Mul_grad/tuple/group_depsNoOp^Adam;^gradients_1/critic/value/encoder/hidden_0/Mul_grad/Reshape=^gradients_1/critic/value/encoder/hidden_0/Mul_grad/Reshape_1
±
Kgradients_1/critic/value/encoder/hidden_0/Mul_grad/tuple/control_dependencyIdentity:gradients_1/critic/value/encoder/hidden_0/Mul_grad/ReshapeD^gradients_1/critic/value/encoder/hidden_0/Mul_grad/tuple/group_deps*
T0*M
_classC
A?loc:@gradients_1/critic/value/encoder/hidden_0/Mul_grad/Reshape
·
Mgradients_1/critic/value/encoder/hidden_0/Mul_grad/tuple/control_dependency_1Identity<gradients_1/critic/value/encoder/hidden_0/Mul_grad/Reshape_1D^gradients_1/critic/value/encoder/hidden_0/Mul_grad/tuple/group_deps*
T0*O
_classE
CAloc:@gradients_1/critic/value/encoder/hidden_0/Mul_grad/Reshape_1
ż
@gradients_1/critic/q/q1_encoding/extrinsic_q1/MatMul_grad/MatMulMatMulSgradients_1/critic/q/q1_encoding/extrinsic_q1/BiasAdd_grad/tuple/control_dependency-critic/q/q1_encoding/extrinsic_q1/kernel/read*
transpose_b(*
T0*
transpose_a( 
ž
Bgradients_1/critic/q/q1_encoding/extrinsic_q1/MatMul_grad/MatMul_1MatMul,critic/q/q1_encoding/q1_encoder/hidden_1/MulSgradients_1/critic/q/q1_encoding/extrinsic_q1/BiasAdd_grad/tuple/control_dependency*
transpose_b( *
T0*
transpose_a(
į
Jgradients_1/critic/q/q1_encoding/extrinsic_q1/MatMul_grad/tuple/group_depsNoOp^AdamA^gradients_1/critic/q/q1_encoding/extrinsic_q1/MatMul_grad/MatMulC^gradients_1/critic/q/q1_encoding/extrinsic_q1/MatMul_grad/MatMul_1
Ė
Rgradients_1/critic/q/q1_encoding/extrinsic_q1/MatMul_grad/tuple/control_dependencyIdentity@gradients_1/critic/q/q1_encoding/extrinsic_q1/MatMul_grad/MatMulK^gradients_1/critic/q/q1_encoding/extrinsic_q1/MatMul_grad/tuple/group_deps*
T0*S
_classI
GEloc:@gradients_1/critic/q/q1_encoding/extrinsic_q1/MatMul_grad/MatMul
Ń
Tgradients_1/critic/q/q1_encoding/extrinsic_q1/MatMul_grad/tuple/control_dependency_1IdentityBgradients_1/critic/q/q1_encoding/extrinsic_q1/MatMul_grad/MatMul_1K^gradients_1/critic/q/q1_encoding/extrinsic_q1/MatMul_grad/tuple/group_deps*
T0*U
_classK
IGloc:@gradients_1/critic/q/q1_encoding/extrinsic_q1/MatMul_grad/MatMul_1
ī
;gradients_1/critic/q/q1_encoding/gail_q1/MatMul_grad/MatMulMatMulNgradients_1/critic/q/q1_encoding/gail_q1/BiasAdd_grad/tuple/control_dependency(critic/q/q1_encoding/gail_q1/kernel/read*
transpose_b(*
T0*
transpose_a( 
ō
=gradients_1/critic/q/q1_encoding/gail_q1/MatMul_grad/MatMul_1MatMul,critic/q/q1_encoding/q1_encoder/hidden_1/MulNgradients_1/critic/q/q1_encoding/gail_q1/BiasAdd_grad/tuple/control_dependency*
transpose_b( *
T0*
transpose_a(
Ņ
Egradients_1/critic/q/q1_encoding/gail_q1/MatMul_grad/tuple/group_depsNoOp^Adam<^gradients_1/critic/q/q1_encoding/gail_q1/MatMul_grad/MatMul>^gradients_1/critic/q/q1_encoding/gail_q1/MatMul_grad/MatMul_1
·
Mgradients_1/critic/q/q1_encoding/gail_q1/MatMul_grad/tuple/control_dependencyIdentity;gradients_1/critic/q/q1_encoding/gail_q1/MatMul_grad/MatMulF^gradients_1/critic/q/q1_encoding/gail_q1/MatMul_grad/tuple/group_deps*
T0*N
_classD
B@loc:@gradients_1/critic/q/q1_encoding/gail_q1/MatMul_grad/MatMul
½
Ogradients_1/critic/q/q1_encoding/gail_q1/MatMul_grad/tuple/control_dependency_1Identity=gradients_1/critic/q/q1_encoding/gail_q1/MatMul_grad/MatMul_1F^gradients_1/critic/q/q1_encoding/gail_q1/MatMul_grad/tuple/group_deps*
T0*P
_classF
DBloc:@gradients_1/critic/q/q1_encoding/gail_q1/MatMul_grad/MatMul_1
ż
@gradients_1/critic/q/q2_encoding/extrinsic_q2/MatMul_grad/MatMulMatMulSgradients_1/critic/q/q2_encoding/extrinsic_q2/BiasAdd_grad/tuple/control_dependency-critic/q/q2_encoding/extrinsic_q2/kernel/read*
transpose_b(*
T0*
transpose_a( 
ž
Bgradients_1/critic/q/q2_encoding/extrinsic_q2/MatMul_grad/MatMul_1MatMul,critic/q/q2_encoding/q2_encoder/hidden_1/MulSgradients_1/critic/q/q2_encoding/extrinsic_q2/BiasAdd_grad/tuple/control_dependency*
transpose_b( *
T0*
transpose_a(
į
Jgradients_1/critic/q/q2_encoding/extrinsic_q2/MatMul_grad/tuple/group_depsNoOp^AdamA^gradients_1/critic/q/q2_encoding/extrinsic_q2/MatMul_grad/MatMulC^gradients_1/critic/q/q2_encoding/extrinsic_q2/MatMul_grad/MatMul_1
Ė
Rgradients_1/critic/q/q2_encoding/extrinsic_q2/MatMul_grad/tuple/control_dependencyIdentity@gradients_1/critic/q/q2_encoding/extrinsic_q2/MatMul_grad/MatMulK^gradients_1/critic/q/q2_encoding/extrinsic_q2/MatMul_grad/tuple/group_deps*
T0*S
_classI
GEloc:@gradients_1/critic/q/q2_encoding/extrinsic_q2/MatMul_grad/MatMul
Ń
Tgradients_1/critic/q/q2_encoding/extrinsic_q2/MatMul_grad/tuple/control_dependency_1IdentityBgradients_1/critic/q/q2_encoding/extrinsic_q2/MatMul_grad/MatMul_1K^gradients_1/critic/q/q2_encoding/extrinsic_q2/MatMul_grad/tuple/group_deps*
T0*U
_classK
IGloc:@gradients_1/critic/q/q2_encoding/extrinsic_q2/MatMul_grad/MatMul_1
ī
;gradients_1/critic/q/q2_encoding/gail_q2/MatMul_grad/MatMulMatMulNgradients_1/critic/q/q2_encoding/gail_q2/BiasAdd_grad/tuple/control_dependency(critic/q/q2_encoding/gail_q2/kernel/read*
transpose_b(*
T0*
transpose_a( 
ō
=gradients_1/critic/q/q2_encoding/gail_q2/MatMul_grad/MatMul_1MatMul,critic/q/q2_encoding/q2_encoder/hidden_1/MulNgradients_1/critic/q/q2_encoding/gail_q2/BiasAdd_grad/tuple/control_dependency*
transpose_b( *
T0*
transpose_a(
Ņ
Egradients_1/critic/q/q2_encoding/gail_q2/MatMul_grad/tuple/group_depsNoOp^Adam<^gradients_1/critic/q/q2_encoding/gail_q2/MatMul_grad/MatMul>^gradients_1/critic/q/q2_encoding/gail_q2/MatMul_grad/MatMul_1
·
Mgradients_1/critic/q/q2_encoding/gail_q2/MatMul_grad/tuple/control_dependencyIdentity;gradients_1/critic/q/q2_encoding/gail_q2/MatMul_grad/MatMulF^gradients_1/critic/q/q2_encoding/gail_q2/MatMul_grad/tuple/group_deps*
T0*N
_classD
B@loc:@gradients_1/critic/q/q2_encoding/gail_q2/MatMul_grad/MatMul
½
Ogradients_1/critic/q/q2_encoding/gail_q2/MatMul_grad/tuple/control_dependency_1Identity=gradients_1/critic/q/q2_encoding/gail_q2/MatMul_grad/MatMul_1F^gradients_1/critic/q/q2_encoding/gail_q2/MatMul_grad/tuple/group_deps*
T0*P
_classF
DBloc:@gradients_1/critic/q/q2_encoding/gail_q2/MatMul_grad/MatMul_1
Š
Bgradients_1/critic/value/encoder/hidden_0/Sigmoid_grad/SigmoidGradSigmoidGrad%critic/value/encoder/hidden_0/SigmoidMgradients_1/critic/value/encoder/hidden_0/Mul_grad/tuple/control_dependency_1*
T0
¤
gradients_1/AddN_6AddNRgradients_1/critic/q/q1_encoding/extrinsic_q1/MatMul_grad/tuple/control_dependencyMgradients_1/critic/q/q1_encoding/gail_q1/MatMul_grad/tuple/control_dependency*
T0*S
_classI
GEloc:@gradients_1/critic/q/q1_encoding/extrinsic_q1/MatMul_grad/MatMul*
N

Cgradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/ShapeShape0critic/q/q1_encoding/q1_encoder/hidden_1/BiasAdd^Adam*
T0*
out_type0
 
Egradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/Shape_1Shape0critic/q/q1_encoding/q1_encoder/hidden_1/Sigmoid^Adam*
T0*
out_type0

Sgradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/BroadcastGradientArgsBroadcastGradientArgsCgradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/ShapeEgradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/Shape_1*
T0

Agradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/MulMulgradients_1/AddN_60critic/q/q1_encoding/q1_encoder/hidden_1/Sigmoid*
T0

Agradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/SumSumAgradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/MulSgradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0
ļ
Egradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/ReshapeReshapeAgradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/SumCgradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/Shape*
T0*
Tshape0

Cgradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/Mul_1Mul0critic/q/q1_encoding/q1_encoder/hidden_1/BiasAddgradients_1/AddN_6*
T0

Cgradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/Sum_1SumCgradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/Mul_1Ugradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0
õ
Ggradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/Reshape_1ReshapeCgradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/Sum_1Egradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/Shape_1*
T0*
Tshape0
ļ
Ngradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/tuple/group_depsNoOp^AdamF^gradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/ReshapeH^gradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/Reshape_1
Ż
Vgradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/tuple/control_dependencyIdentityEgradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/ReshapeO^gradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/tuple/group_deps*
T0*X
_classN
LJloc:@gradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/Reshape
ć
Xgradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/tuple/control_dependency_1IdentityGgradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/Reshape_1O^gradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/tuple/group_deps*
T0*Z
_classP
NLloc:@gradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/Reshape_1
¤
gradients_1/AddN_7AddNRgradients_1/critic/q/q2_encoding/extrinsic_q2/MatMul_grad/tuple/control_dependencyMgradients_1/critic/q/q2_encoding/gail_q2/MatMul_grad/tuple/control_dependency*
T0*S
_classI
GEloc:@gradients_1/critic/q/q2_encoding/extrinsic_q2/MatMul_grad/MatMul*
N

Cgradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/ShapeShape0critic/q/q2_encoding/q2_encoder/hidden_1/BiasAdd^Adam*
T0*
out_type0
 
Egradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/Shape_1Shape0critic/q/q2_encoding/q2_encoder/hidden_1/Sigmoid^Adam*
T0*
out_type0

Sgradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/BroadcastGradientArgsBroadcastGradientArgsCgradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/ShapeEgradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/Shape_1*
T0

Agradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/MulMulgradients_1/AddN_70critic/q/q2_encoding/q2_encoder/hidden_1/Sigmoid*
T0

Agradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/SumSumAgradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/MulSgradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0
ļ
Egradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/ReshapeReshapeAgradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/SumCgradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/Shape*
T0*
Tshape0

Cgradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/Mul_1Mul0critic/q/q2_encoding/q2_encoder/hidden_1/BiasAddgradients_1/AddN_7*
T0

Cgradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/Sum_1SumCgradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/Mul_1Ugradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0
õ
Ggradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/Reshape_1ReshapeCgradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/Sum_1Egradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/Shape_1*
T0*
Tshape0
ļ
Ngradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/tuple/group_depsNoOp^AdamF^gradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/ReshapeH^gradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/Reshape_1
Ż
Vgradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/tuple/control_dependencyIdentityEgradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/ReshapeO^gradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/tuple/group_deps*
T0*X
_classN
LJloc:@gradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/Reshape
ć
Xgradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/tuple/control_dependency_1IdentityGgradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/Reshape_1O^gradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/tuple/group_deps*
T0*Z
_classP
NLloc:@gradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/Reshape_1

gradients_1/AddN_8AddNKgradients_1/critic/value/encoder/hidden_0/Mul_grad/tuple/control_dependencyBgradients_1/critic/value/encoder/hidden_0/Sigmoid_grad/SigmoidGrad*
T0*M
_classC
A?loc:@gradients_1/critic/value/encoder/hidden_0/Mul_grad/Reshape*
N

Bgradients_1/critic/value/encoder/hidden_0/BiasAdd_grad/BiasAddGradBiasAddGradgradients_1/AddN_8*
T0*
data_formatNHWC
°
Ggradients_1/critic/value/encoder/hidden_0/BiasAdd_grad/tuple/group_depsNoOp^Adam^gradients_1/AddN_8C^gradients_1/critic/value/encoder/hidden_0/BiasAdd_grad/BiasAddGrad

Ogradients_1/critic/value/encoder/hidden_0/BiasAdd_grad/tuple/control_dependencyIdentitygradients_1/AddN_8H^gradients_1/critic/value/encoder/hidden_0/BiasAdd_grad/tuple/group_deps*
T0*M
_classC
A?loc:@gradients_1/critic/value/encoder/hidden_0/Mul_grad/Reshape
Ė
Qgradients_1/critic/value/encoder/hidden_0/BiasAdd_grad/tuple/control_dependency_1IdentityBgradients_1/critic/value/encoder/hidden_0/BiasAdd_grad/BiasAddGradH^gradients_1/critic/value/encoder/hidden_0/BiasAdd_grad/tuple/group_deps*
T0*U
_classK
IGloc:@gradients_1/critic/value/encoder/hidden_0/BiasAdd_grad/BiasAddGrad
ń
Mgradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/Sigmoid_grad/SigmoidGradSigmoidGrad0critic/q/q1_encoding/q1_encoder/hidden_1/SigmoidXgradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/tuple/control_dependency_1*
T0
ń
Mgradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/Sigmoid_grad/SigmoidGradSigmoidGrad0critic/q/q2_encoding/q2_encoder/hidden_1/SigmoidXgradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/tuple/control_dependency_1*
T0
ń
<gradients_1/critic/value/encoder/hidden_0/MatMul_grad/MatMulMatMulOgradients_1/critic/value/encoder/hidden_0/BiasAdd_grad/tuple/control_dependency)critic/value/encoder/hidden_0/kernel/read*
transpose_b(*
T0*
transpose_a( 
Ü
>gradients_1/critic/value/encoder/hidden_0/MatMul_grad/MatMul_1MatMulvector_observationOgradients_1/critic/value/encoder/hidden_0/BiasAdd_grad/tuple/control_dependency*
transpose_b( *
T0*
transpose_a(
Õ
Fgradients_1/critic/value/encoder/hidden_0/MatMul_grad/tuple/group_depsNoOp^Adam=^gradients_1/critic/value/encoder/hidden_0/MatMul_grad/MatMul?^gradients_1/critic/value/encoder/hidden_0/MatMul_grad/MatMul_1
»
Ngradients_1/critic/value/encoder/hidden_0/MatMul_grad/tuple/control_dependencyIdentity<gradients_1/critic/value/encoder/hidden_0/MatMul_grad/MatMulG^gradients_1/critic/value/encoder/hidden_0/MatMul_grad/tuple/group_deps*
T0*O
_classE
CAloc:@gradients_1/critic/value/encoder/hidden_0/MatMul_grad/MatMul
Į
Pgradients_1/critic/value/encoder/hidden_0/MatMul_grad/tuple/control_dependency_1Identity>gradients_1/critic/value/encoder/hidden_0/MatMul_grad/MatMul_1G^gradients_1/critic/value/encoder/hidden_0/MatMul_grad/tuple/group_deps*
T0*Q
_classG
ECloc:@gradients_1/critic/value/encoder/hidden_0/MatMul_grad/MatMul_1
­
gradients_1/AddN_9AddNVgradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/tuple/control_dependencyMgradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/Sigmoid_grad/SigmoidGrad*
T0*X
_classN
LJloc:@gradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/Reshape*
N

Mgradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/BiasAdd_grad/BiasAddGradBiasAddGradgradients_1/AddN_9*
T0*
data_formatNHWC
Ę
Rgradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/BiasAdd_grad/tuple/group_depsNoOp^Adam^gradients_1/AddN_9N^gradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/BiasAdd_grad/BiasAddGrad
²
Zgradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/BiasAdd_grad/tuple/control_dependencyIdentitygradients_1/AddN_9S^gradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/BiasAdd_grad/tuple/group_deps*
T0*X
_classN
LJloc:@gradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/Mul_grad/Reshape
÷
\gradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/BiasAdd_grad/tuple/control_dependency_1IdentityMgradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/BiasAdd_grad/BiasAddGradS^gradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/BiasAdd_grad/tuple/group_deps*
T0*`
_classV
TRloc:@gradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/BiasAdd_grad/BiasAddGrad
®
gradients_1/AddN_10AddNVgradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/tuple/control_dependencyMgradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/Sigmoid_grad/SigmoidGrad*
T0*X
_classN
LJloc:@gradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/Reshape*
N

Mgradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/BiasAdd_grad/BiasAddGradBiasAddGradgradients_1/AddN_10*
T0*
data_formatNHWC
Ē
Rgradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/BiasAdd_grad/tuple/group_depsNoOp^Adam^gradients_1/AddN_10N^gradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/BiasAdd_grad/BiasAddGrad
³
Zgradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/BiasAdd_grad/tuple/control_dependencyIdentitygradients_1/AddN_10S^gradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/BiasAdd_grad/tuple/group_deps*
T0*X
_classN
LJloc:@gradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/Mul_grad/Reshape
÷
\gradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/BiasAdd_grad/tuple/control_dependency_1IdentityMgradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/BiasAdd_grad/BiasAddGradS^gradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/BiasAdd_grad/tuple/group_deps*
T0*`
_classV
TRloc:@gradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/BiasAdd_grad/BiasAddGrad

Ggradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/MatMul_grad/MatMulMatMulZgradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/BiasAdd_grad/tuple/control_dependency4critic/q/q1_encoding/q1_encoder/hidden_1/kernel/read*
transpose_b(*
T0*
transpose_a( 

Igradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/MatMul_grad/MatMul_1MatMul,critic/q/q1_encoding/q1_encoder/hidden_0/MulZgradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/BiasAdd_grad/tuple/control_dependency*
transpose_b( *
T0*
transpose_a(
ö
Qgradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/MatMul_grad/tuple/group_depsNoOp^AdamH^gradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/MatMul_grad/MatMulJ^gradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/MatMul_grad/MatMul_1
ē
Ygradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/MatMul_grad/tuple/control_dependencyIdentityGgradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/MatMul_grad/MatMulR^gradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/MatMul_grad/tuple/group_deps*
T0*Z
_classP
NLloc:@gradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/MatMul_grad/MatMul
ķ
[gradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/MatMul_grad/tuple/control_dependency_1IdentityIgradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/MatMul_grad/MatMul_1R^gradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/MatMul_grad/tuple/group_deps*
T0*\
_classR
PNloc:@gradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/MatMul_grad/MatMul_1

Ggradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/MatMul_grad/MatMulMatMulZgradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/BiasAdd_grad/tuple/control_dependency4critic/q/q2_encoding/q2_encoder/hidden_1/kernel/read*
transpose_b(*
T0*
transpose_a( 

Igradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/MatMul_grad/MatMul_1MatMul,critic/q/q2_encoding/q2_encoder/hidden_0/MulZgradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/BiasAdd_grad/tuple/control_dependency*
transpose_b( *
T0*
transpose_a(
ö
Qgradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/MatMul_grad/tuple/group_depsNoOp^AdamH^gradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/MatMul_grad/MatMulJ^gradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/MatMul_grad/MatMul_1
ē
Ygradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/MatMul_grad/tuple/control_dependencyIdentityGgradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/MatMul_grad/MatMulR^gradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/MatMul_grad/tuple/group_deps*
T0*Z
_classP
NLloc:@gradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/MatMul_grad/MatMul
ķ
[gradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/MatMul_grad/tuple/control_dependency_1IdentityIgradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/MatMul_grad/MatMul_1R^gradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/MatMul_grad/tuple/group_deps*
T0*\
_classR
PNloc:@gradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/MatMul_grad/MatMul_1

Cgradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/ShapeShape0critic/q/q1_encoding/q1_encoder/hidden_0/BiasAdd^Adam*
T0*
out_type0
 
Egradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/Shape_1Shape0critic/q/q1_encoding/q1_encoder/hidden_0/Sigmoid^Adam*
T0*
out_type0

Sgradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/BroadcastGradientArgsBroadcastGradientArgsCgradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/ShapeEgradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/Shape_1*
T0
Ž
Agradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/MulMulYgradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/MatMul_grad/tuple/control_dependency0critic/q/q1_encoding/q1_encoder/hidden_0/Sigmoid*
T0

Agradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/SumSumAgradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/MulSgradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0
ļ
Egradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/ReshapeReshapeAgradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/SumCgradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/Shape*
T0*
Tshape0
ą
Cgradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/Mul_1Mul0critic/q/q1_encoding/q1_encoder/hidden_0/BiasAddYgradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/MatMul_grad/tuple/control_dependency*
T0

Cgradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/Sum_1SumCgradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/Mul_1Ugradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0
õ
Ggradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/Reshape_1ReshapeCgradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/Sum_1Egradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/Shape_1*
T0*
Tshape0
ļ
Ngradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/tuple/group_depsNoOp^AdamF^gradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/ReshapeH^gradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/Reshape_1
Ż
Vgradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/tuple/control_dependencyIdentityEgradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/ReshapeO^gradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/tuple/group_deps*
T0*X
_classN
LJloc:@gradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/Reshape
ć
Xgradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/tuple/control_dependency_1IdentityGgradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/Reshape_1O^gradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/tuple/group_deps*
T0*Z
_classP
NLloc:@gradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/Reshape_1

Cgradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/ShapeShape0critic/q/q2_encoding/q2_encoder/hidden_0/BiasAdd^Adam*
T0*
out_type0
 
Egradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/Shape_1Shape0critic/q/q2_encoding/q2_encoder/hidden_0/Sigmoid^Adam*
T0*
out_type0

Sgradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/BroadcastGradientArgsBroadcastGradientArgsCgradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/ShapeEgradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/Shape_1*
T0
Ž
Agradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/MulMulYgradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/MatMul_grad/tuple/control_dependency0critic/q/q2_encoding/q2_encoder/hidden_0/Sigmoid*
T0

Agradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/SumSumAgradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/MulSgradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0
ļ
Egradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/ReshapeReshapeAgradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/SumCgradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/Shape*
T0*
Tshape0
ą
Cgradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/Mul_1Mul0critic/q/q2_encoding/q2_encoder/hidden_0/BiasAddYgradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/MatMul_grad/tuple/control_dependency*
T0

Cgradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/Sum_1SumCgradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/Mul_1Ugradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0
õ
Ggradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/Reshape_1ReshapeCgradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/Sum_1Egradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/Shape_1*
T0*
Tshape0
ļ
Ngradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/tuple/group_depsNoOp^AdamF^gradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/ReshapeH^gradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/Reshape_1
Ż
Vgradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/tuple/control_dependencyIdentityEgradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/ReshapeO^gradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/tuple/group_deps*
T0*X
_classN
LJloc:@gradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/Reshape
ć
Xgradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/tuple/control_dependency_1IdentityGgradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/Reshape_1O^gradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/tuple/group_deps*
T0*Z
_classP
NLloc:@gradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/Reshape_1
ń
Mgradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/Sigmoid_grad/SigmoidGradSigmoidGrad0critic/q/q1_encoding/q1_encoder/hidden_0/SigmoidXgradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/tuple/control_dependency_1*
T0
ń
Mgradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/Sigmoid_grad/SigmoidGradSigmoidGrad0critic/q/q2_encoding/q2_encoder/hidden_0/SigmoidXgradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/tuple/control_dependency_1*
T0
®
gradients_1/AddN_11AddNVgradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/tuple/control_dependencyMgradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/Sigmoid_grad/SigmoidGrad*
T0*X
_classN
LJloc:@gradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/Reshape*
N

Mgradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/BiasAdd_grad/BiasAddGradBiasAddGradgradients_1/AddN_11*
T0*
data_formatNHWC
Ē
Rgradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/BiasAdd_grad/tuple/group_depsNoOp^Adam^gradients_1/AddN_11N^gradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/BiasAdd_grad/BiasAddGrad
³
Zgradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/BiasAdd_grad/tuple/control_dependencyIdentitygradients_1/AddN_11S^gradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/BiasAdd_grad/tuple/group_deps*
T0*X
_classN
LJloc:@gradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/Mul_grad/Reshape
÷
\gradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/BiasAdd_grad/tuple/control_dependency_1IdentityMgradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/BiasAdd_grad/BiasAddGradS^gradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/BiasAdd_grad/tuple/group_deps*
T0*`
_classV
TRloc:@gradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/BiasAdd_grad/BiasAddGrad
®
gradients_1/AddN_12AddNVgradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/tuple/control_dependencyMgradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/Sigmoid_grad/SigmoidGrad*
T0*X
_classN
LJloc:@gradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/Reshape*
N

Mgradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/BiasAdd_grad/BiasAddGradBiasAddGradgradients_1/AddN_12*
T0*
data_formatNHWC
Ē
Rgradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/BiasAdd_grad/tuple/group_depsNoOp^Adam^gradients_1/AddN_12N^gradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/BiasAdd_grad/BiasAddGrad
³
Zgradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/BiasAdd_grad/tuple/control_dependencyIdentitygradients_1/AddN_12S^gradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/BiasAdd_grad/tuple/group_deps*
T0*X
_classN
LJloc:@gradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/Mul_grad/Reshape
÷
\gradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/BiasAdd_grad/tuple/control_dependency_1IdentityMgradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/BiasAdd_grad/BiasAddGradS^gradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/BiasAdd_grad/tuple/group_deps*
T0*`
_classV
TRloc:@gradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/BiasAdd_grad/BiasAddGrad

Ggradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/MatMul_grad/MatMulMatMulZgradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/BiasAdd_grad/tuple/control_dependency4critic/q/q1_encoding/q1_encoder/hidden_0/kernel/read*
transpose_b(*
T0*
transpose_a( 
ņ
Igradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/MatMul_grad/MatMul_1MatMulvector_observationZgradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/BiasAdd_grad/tuple/control_dependency*
transpose_b( *
T0*
transpose_a(
ö
Qgradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/MatMul_grad/tuple/group_depsNoOp^AdamH^gradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/MatMul_grad/MatMulJ^gradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/MatMul_grad/MatMul_1
ē
Ygradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/MatMul_grad/tuple/control_dependencyIdentityGgradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/MatMul_grad/MatMulR^gradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/MatMul_grad/tuple/group_deps*
T0*Z
_classP
NLloc:@gradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/MatMul_grad/MatMul
ķ
[gradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/MatMul_grad/tuple/control_dependency_1IdentityIgradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/MatMul_grad/MatMul_1R^gradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/MatMul_grad/tuple/group_deps*
T0*\
_classR
PNloc:@gradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/MatMul_grad/MatMul_1

Ggradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/MatMul_grad/MatMulMatMulZgradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/BiasAdd_grad/tuple/control_dependency4critic/q/q2_encoding/q2_encoder/hidden_0/kernel/read*
transpose_b(*
T0*
transpose_a( 
ņ
Igradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/MatMul_grad/MatMul_1MatMulvector_observationZgradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/BiasAdd_grad/tuple/control_dependency*
transpose_b( *
T0*
transpose_a(
ö
Qgradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/MatMul_grad/tuple/group_depsNoOp^AdamH^gradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/MatMul_grad/MatMulJ^gradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/MatMul_grad/MatMul_1
ē
Ygradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/MatMul_grad/tuple/control_dependencyIdentityGgradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/MatMul_grad/MatMulR^gradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/MatMul_grad/tuple/group_deps*
T0*Z
_classP
NLloc:@gradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/MatMul_grad/MatMul
ķ
[gradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/MatMul_grad/tuple/control_dependency_1IdentityIgradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/MatMul_grad/MatMul_1R^gradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/MatMul_grad/tuple/group_deps*
T0*\
_classR
PNloc:@gradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/MatMul_grad/MatMul_1

beta1_power_1/initial_valueConst*9
_class/
-+loc:@critic/q/q1_encoding/extrinsic_q1/bias*
valueB
 *fff?*
dtype0

beta1_power_1
VariableV2*
shape: *
shared_name *9
_class/
-+loc:@critic/q/q1_encoding/extrinsic_q1/bias*
dtype0*
	container 
·
beta1_power_1/AssignAssignbeta1_power_1beta1_power_1/initial_value*
use_locking(*
T0*9
_class/
-+loc:@critic/q/q1_encoding/extrinsic_q1/bias*
validate_shape(
q
beta1_power_1/readIdentitybeta1_power_1*
T0*9
_class/
-+loc:@critic/q/q1_encoding/extrinsic_q1/bias

beta2_power_1/initial_valueConst*9
_class/
-+loc:@critic/q/q1_encoding/extrinsic_q1/bias*
valueB
 *w¾?*
dtype0

beta2_power_1
VariableV2*
shape: *
shared_name *9
_class/
-+loc:@critic/q/q1_encoding/extrinsic_q1/bias*
dtype0*
	container 
·
beta2_power_1/AssignAssignbeta2_power_1beta2_power_1/initial_value*
use_locking(*
T0*9
_class/
-+loc:@critic/q/q1_encoding/extrinsic_q1/bias*
validate_shape(
q
beta2_power_1/readIdentitybeta2_power_1*
T0*9
_class/
-+loc:@critic/q/q1_encoding/extrinsic_q1/bias
¹
Kcritic/value/encoder/hidden_0/kernel/Adam/Initializer/zeros/shape_as_tensorConst*
valueB"b     *7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel*
dtype0
§
Acritic/value/encoder/hidden_0/kernel/Adam/Initializer/zeros/ConstConst*
valueB
 *    *7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel*
dtype0
§
;critic/value/encoder/hidden_0/kernel/Adam/Initializer/zerosFillKcritic/value/encoder/hidden_0/kernel/Adam/Initializer/zeros/shape_as_tensorAcritic/value/encoder/hidden_0/kernel/Adam/Initializer/zeros/Const*
T0*

index_type0*7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel
ø
)critic/value/encoder/hidden_0/kernel/Adam
VariableV2*
shape:
ā*
shared_name *7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel*
dtype0*
	container 

0critic/value/encoder/hidden_0/kernel/Adam/AssignAssign)critic/value/encoder/hidden_0/kernel/Adam;critic/value/encoder/hidden_0/kernel/Adam/Initializer/zeros*
use_locking(*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel*
validate_shape(
§
.critic/value/encoder/hidden_0/kernel/Adam/readIdentity)critic/value/encoder/hidden_0/kernel/Adam*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel
»
Mcritic/value/encoder/hidden_0/kernel/Adam_1/Initializer/zeros/shape_as_tensorConst*
valueB"b     *7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel*
dtype0
©
Ccritic/value/encoder/hidden_0/kernel/Adam_1/Initializer/zeros/ConstConst*
valueB
 *    *7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel*
dtype0
­
=critic/value/encoder/hidden_0/kernel/Adam_1/Initializer/zerosFillMcritic/value/encoder/hidden_0/kernel/Adam_1/Initializer/zeros/shape_as_tensorCcritic/value/encoder/hidden_0/kernel/Adam_1/Initializer/zeros/Const*
T0*

index_type0*7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel
ŗ
+critic/value/encoder/hidden_0/kernel/Adam_1
VariableV2*
shape:
ā*
shared_name *7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel*
dtype0*
	container 

2critic/value/encoder/hidden_0/kernel/Adam_1/AssignAssign+critic/value/encoder/hidden_0/kernel/Adam_1=critic/value/encoder/hidden_0/kernel/Adam_1/Initializer/zeros*
use_locking(*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel*
validate_shape(
«
0critic/value/encoder/hidden_0/kernel/Adam_1/readIdentity+critic/value/encoder/hidden_0/kernel/Adam_1*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel
¢
9critic/value/encoder/hidden_0/bias/Adam/Initializer/zerosConst*
valueB*    *5
_class+
)'loc:@critic/value/encoder/hidden_0/bias*
dtype0
Æ
'critic/value/encoder/hidden_0/bias/Adam
VariableV2*
shape:*
shared_name *5
_class+
)'loc:@critic/value/encoder/hidden_0/bias*
dtype0*
	container 

.critic/value/encoder/hidden_0/bias/Adam/AssignAssign'critic/value/encoder/hidden_0/bias/Adam9critic/value/encoder/hidden_0/bias/Adam/Initializer/zeros*
use_locking(*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_0/bias*
validate_shape(
”
,critic/value/encoder/hidden_0/bias/Adam/readIdentity'critic/value/encoder/hidden_0/bias/Adam*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_0/bias
¤
;critic/value/encoder/hidden_0/bias/Adam_1/Initializer/zerosConst*
valueB*    *5
_class+
)'loc:@critic/value/encoder/hidden_0/bias*
dtype0
±
)critic/value/encoder/hidden_0/bias/Adam_1
VariableV2*
shape:*
shared_name *5
_class+
)'loc:@critic/value/encoder/hidden_0/bias*
dtype0*
	container 

0critic/value/encoder/hidden_0/bias/Adam_1/AssignAssign)critic/value/encoder/hidden_0/bias/Adam_1;critic/value/encoder/hidden_0/bias/Adam_1/Initializer/zeros*
use_locking(*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_0/bias*
validate_shape(
„
.critic/value/encoder/hidden_0/bias/Adam_1/readIdentity)critic/value/encoder/hidden_0/bias/Adam_1*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_0/bias
¹
Kcritic/value/encoder/hidden_1/kernel/Adam/Initializer/zeros/shape_as_tensorConst*
valueB"      *7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel*
dtype0
§
Acritic/value/encoder/hidden_1/kernel/Adam/Initializer/zeros/ConstConst*
valueB
 *    *7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel*
dtype0
§
;critic/value/encoder/hidden_1/kernel/Adam/Initializer/zerosFillKcritic/value/encoder/hidden_1/kernel/Adam/Initializer/zeros/shape_as_tensorAcritic/value/encoder/hidden_1/kernel/Adam/Initializer/zeros/Const*
T0*

index_type0*7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel
ø
)critic/value/encoder/hidden_1/kernel/Adam
VariableV2*
shape:
*
shared_name *7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel*
dtype0*
	container 

0critic/value/encoder/hidden_1/kernel/Adam/AssignAssign)critic/value/encoder/hidden_1/kernel/Adam;critic/value/encoder/hidden_1/kernel/Adam/Initializer/zeros*
use_locking(*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel*
validate_shape(
§
.critic/value/encoder/hidden_1/kernel/Adam/readIdentity)critic/value/encoder/hidden_1/kernel/Adam*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel
»
Mcritic/value/encoder/hidden_1/kernel/Adam_1/Initializer/zeros/shape_as_tensorConst*
valueB"      *7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel*
dtype0
©
Ccritic/value/encoder/hidden_1/kernel/Adam_1/Initializer/zeros/ConstConst*
valueB
 *    *7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel*
dtype0
­
=critic/value/encoder/hidden_1/kernel/Adam_1/Initializer/zerosFillMcritic/value/encoder/hidden_1/kernel/Adam_1/Initializer/zeros/shape_as_tensorCcritic/value/encoder/hidden_1/kernel/Adam_1/Initializer/zeros/Const*
T0*

index_type0*7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel
ŗ
+critic/value/encoder/hidden_1/kernel/Adam_1
VariableV2*
shape:
*
shared_name *7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel*
dtype0*
	container 

2critic/value/encoder/hidden_1/kernel/Adam_1/AssignAssign+critic/value/encoder/hidden_1/kernel/Adam_1=critic/value/encoder/hidden_1/kernel/Adam_1/Initializer/zeros*
use_locking(*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel*
validate_shape(
«
0critic/value/encoder/hidden_1/kernel/Adam_1/readIdentity+critic/value/encoder/hidden_1/kernel/Adam_1*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel
¢
9critic/value/encoder/hidden_1/bias/Adam/Initializer/zerosConst*
valueB*    *5
_class+
)'loc:@critic/value/encoder/hidden_1/bias*
dtype0
Æ
'critic/value/encoder/hidden_1/bias/Adam
VariableV2*
shape:*
shared_name *5
_class+
)'loc:@critic/value/encoder/hidden_1/bias*
dtype0*
	container 

.critic/value/encoder/hidden_1/bias/Adam/AssignAssign'critic/value/encoder/hidden_1/bias/Adam9critic/value/encoder/hidden_1/bias/Adam/Initializer/zeros*
use_locking(*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_1/bias*
validate_shape(
”
,critic/value/encoder/hidden_1/bias/Adam/readIdentity'critic/value/encoder/hidden_1/bias/Adam*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_1/bias
¤
;critic/value/encoder/hidden_1/bias/Adam_1/Initializer/zerosConst*
valueB*    *5
_class+
)'loc:@critic/value/encoder/hidden_1/bias*
dtype0
±
)critic/value/encoder/hidden_1/bias/Adam_1
VariableV2*
shape:*
shared_name *5
_class+
)'loc:@critic/value/encoder/hidden_1/bias*
dtype0*
	container 

0critic/value/encoder/hidden_1/bias/Adam_1/AssignAssign)critic/value/encoder/hidden_1/bias/Adam_1;critic/value/encoder/hidden_1/bias/Adam_1/Initializer/zeros*
use_locking(*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_1/bias*
validate_shape(
„
.critic/value/encoder/hidden_1/bias/Adam_1/readIdentity)critic/value/encoder/hidden_1/bias/Adam_1*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_1/bias
Ø
:critic/value/extrinsic_value/kernel/Adam/Initializer/zerosConst*
valueB	*    *6
_class,
*(loc:@critic/value/extrinsic_value/kernel*
dtype0
µ
(critic/value/extrinsic_value/kernel/Adam
VariableV2*
shape:	*
shared_name *6
_class,
*(loc:@critic/value/extrinsic_value/kernel*
dtype0*
	container 

/critic/value/extrinsic_value/kernel/Adam/AssignAssign(critic/value/extrinsic_value/kernel/Adam:critic/value/extrinsic_value/kernel/Adam/Initializer/zeros*
use_locking(*
T0*6
_class,
*(loc:@critic/value/extrinsic_value/kernel*
validate_shape(
¤
-critic/value/extrinsic_value/kernel/Adam/readIdentity(critic/value/extrinsic_value/kernel/Adam*
T0*6
_class,
*(loc:@critic/value/extrinsic_value/kernel
Ŗ
<critic/value/extrinsic_value/kernel/Adam_1/Initializer/zerosConst*
valueB	*    *6
_class,
*(loc:@critic/value/extrinsic_value/kernel*
dtype0
·
*critic/value/extrinsic_value/kernel/Adam_1
VariableV2*
shape:	*
shared_name *6
_class,
*(loc:@critic/value/extrinsic_value/kernel*
dtype0*
	container 

1critic/value/extrinsic_value/kernel/Adam_1/AssignAssign*critic/value/extrinsic_value/kernel/Adam_1<critic/value/extrinsic_value/kernel/Adam_1/Initializer/zeros*
use_locking(*
T0*6
_class,
*(loc:@critic/value/extrinsic_value/kernel*
validate_shape(
Ø
/critic/value/extrinsic_value/kernel/Adam_1/readIdentity*critic/value/extrinsic_value/kernel/Adam_1*
T0*6
_class,
*(loc:@critic/value/extrinsic_value/kernel

8critic/value/extrinsic_value/bias/Adam/Initializer/zerosConst*
valueB*    *4
_class*
(&loc:@critic/value/extrinsic_value/bias*
dtype0
¬
&critic/value/extrinsic_value/bias/Adam
VariableV2*
shape:*
shared_name *4
_class*
(&loc:@critic/value/extrinsic_value/bias*
dtype0*
	container 

-critic/value/extrinsic_value/bias/Adam/AssignAssign&critic/value/extrinsic_value/bias/Adam8critic/value/extrinsic_value/bias/Adam/Initializer/zeros*
use_locking(*
T0*4
_class*
(&loc:@critic/value/extrinsic_value/bias*
validate_shape(

+critic/value/extrinsic_value/bias/Adam/readIdentity&critic/value/extrinsic_value/bias/Adam*
T0*4
_class*
(&loc:@critic/value/extrinsic_value/bias
”
:critic/value/extrinsic_value/bias/Adam_1/Initializer/zerosConst*
valueB*    *4
_class*
(&loc:@critic/value/extrinsic_value/bias*
dtype0
®
(critic/value/extrinsic_value/bias/Adam_1
VariableV2*
shape:*
shared_name *4
_class*
(&loc:@critic/value/extrinsic_value/bias*
dtype0*
	container 

/critic/value/extrinsic_value/bias/Adam_1/AssignAssign(critic/value/extrinsic_value/bias/Adam_1:critic/value/extrinsic_value/bias/Adam_1/Initializer/zeros*
use_locking(*
T0*4
_class*
(&loc:@critic/value/extrinsic_value/bias*
validate_shape(
¢
-critic/value/extrinsic_value/bias/Adam_1/readIdentity(critic/value/extrinsic_value/bias/Adam_1*
T0*4
_class*
(&loc:@critic/value/extrinsic_value/bias

5critic/value/gail_value/kernel/Adam/Initializer/zerosConst*
valueB	*    *1
_class'
%#loc:@critic/value/gail_value/kernel*
dtype0
«
#critic/value/gail_value/kernel/Adam
VariableV2*
shape:	*
shared_name *1
_class'
%#loc:@critic/value/gail_value/kernel*
dtype0*
	container 
õ
*critic/value/gail_value/kernel/Adam/AssignAssign#critic/value/gail_value/kernel/Adam5critic/value/gail_value/kernel/Adam/Initializer/zeros*
use_locking(*
T0*1
_class'
%#loc:@critic/value/gail_value/kernel*
validate_shape(

(critic/value/gail_value/kernel/Adam/readIdentity#critic/value/gail_value/kernel/Adam*
T0*1
_class'
%#loc:@critic/value/gail_value/kernel
 
7critic/value/gail_value/kernel/Adam_1/Initializer/zerosConst*
valueB	*    *1
_class'
%#loc:@critic/value/gail_value/kernel*
dtype0
­
%critic/value/gail_value/kernel/Adam_1
VariableV2*
shape:	*
shared_name *1
_class'
%#loc:@critic/value/gail_value/kernel*
dtype0*
	container 
ū
,critic/value/gail_value/kernel/Adam_1/AssignAssign%critic/value/gail_value/kernel/Adam_17critic/value/gail_value/kernel/Adam_1/Initializer/zeros*
use_locking(*
T0*1
_class'
%#loc:@critic/value/gail_value/kernel*
validate_shape(

*critic/value/gail_value/kernel/Adam_1/readIdentity%critic/value/gail_value/kernel/Adam_1*
T0*1
_class'
%#loc:@critic/value/gail_value/kernel

3critic/value/gail_value/bias/Adam/Initializer/zerosConst*
valueB*    */
_class%
#!loc:@critic/value/gail_value/bias*
dtype0
¢
!critic/value/gail_value/bias/Adam
VariableV2*
shape:*
shared_name */
_class%
#!loc:@critic/value/gail_value/bias*
dtype0*
	container 
ķ
(critic/value/gail_value/bias/Adam/AssignAssign!critic/value/gail_value/bias/Adam3critic/value/gail_value/bias/Adam/Initializer/zeros*
use_locking(*
T0*/
_class%
#!loc:@critic/value/gail_value/bias*
validate_shape(

&critic/value/gail_value/bias/Adam/readIdentity!critic/value/gail_value/bias/Adam*
T0*/
_class%
#!loc:@critic/value/gail_value/bias

5critic/value/gail_value/bias/Adam_1/Initializer/zerosConst*
valueB*    */
_class%
#!loc:@critic/value/gail_value/bias*
dtype0
¤
#critic/value/gail_value/bias/Adam_1
VariableV2*
shape:*
shared_name */
_class%
#!loc:@critic/value/gail_value/bias*
dtype0*
	container 
ó
*critic/value/gail_value/bias/Adam_1/AssignAssign#critic/value/gail_value/bias/Adam_15critic/value/gail_value/bias/Adam_1/Initializer/zeros*
use_locking(*
T0*/
_class%
#!loc:@critic/value/gail_value/bias*
validate_shape(

(critic/value/gail_value/bias/Adam_1/readIdentity#critic/value/gail_value/bias/Adam_1*
T0*/
_class%
#!loc:@critic/value/gail_value/bias
Ļ
Vcritic/q/q1_encoding/q1_encoder/hidden_0/kernel/Adam/Initializer/zeros/shape_as_tensorConst*
valueB"b     *B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel*
dtype0
½
Lcritic/q/q1_encoding/q1_encoder/hidden_0/kernel/Adam/Initializer/zeros/ConstConst*
valueB
 *    *B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel*
dtype0
Ó
Fcritic/q/q1_encoding/q1_encoder/hidden_0/kernel/Adam/Initializer/zerosFillVcritic/q/q1_encoding/q1_encoder/hidden_0/kernel/Adam/Initializer/zeros/shape_as_tensorLcritic/q/q1_encoding/q1_encoder/hidden_0/kernel/Adam/Initializer/zeros/Const*
T0*

index_type0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel
Ī
4critic/q/q1_encoding/q1_encoder/hidden_0/kernel/Adam
VariableV2*
shape:
ā*
shared_name *B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel*
dtype0*
	container 
¹
;critic/q/q1_encoding/q1_encoder/hidden_0/kernel/Adam/AssignAssign4critic/q/q1_encoding/q1_encoder/hidden_0/kernel/AdamFcritic/q/q1_encoding/q1_encoder/hidden_0/kernel/Adam/Initializer/zeros*
use_locking(*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel*
validate_shape(
Č
9critic/q/q1_encoding/q1_encoder/hidden_0/kernel/Adam/readIdentity4critic/q/q1_encoding/q1_encoder/hidden_0/kernel/Adam*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel
Ń
Xcritic/q/q1_encoding/q1_encoder/hidden_0/kernel/Adam_1/Initializer/zeros/shape_as_tensorConst*
valueB"b     *B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel*
dtype0
æ
Ncritic/q/q1_encoding/q1_encoder/hidden_0/kernel/Adam_1/Initializer/zeros/ConstConst*
valueB
 *    *B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel*
dtype0
Ł
Hcritic/q/q1_encoding/q1_encoder/hidden_0/kernel/Adam_1/Initializer/zerosFillXcritic/q/q1_encoding/q1_encoder/hidden_0/kernel/Adam_1/Initializer/zeros/shape_as_tensorNcritic/q/q1_encoding/q1_encoder/hidden_0/kernel/Adam_1/Initializer/zeros/Const*
T0*

index_type0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel
Š
6critic/q/q1_encoding/q1_encoder/hidden_0/kernel/Adam_1
VariableV2*
shape:
ā*
shared_name *B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel*
dtype0*
	container 
æ
=critic/q/q1_encoding/q1_encoder/hidden_0/kernel/Adam_1/AssignAssign6critic/q/q1_encoding/q1_encoder/hidden_0/kernel/Adam_1Hcritic/q/q1_encoding/q1_encoder/hidden_0/kernel/Adam_1/Initializer/zeros*
use_locking(*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel*
validate_shape(
Ģ
;critic/q/q1_encoding/q1_encoder/hidden_0/kernel/Adam_1/readIdentity6critic/q/q1_encoding/q1_encoder/hidden_0/kernel/Adam_1*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel
ø
Dcritic/q/q1_encoding/q1_encoder/hidden_0/bias/Adam/Initializer/zerosConst*
valueB*    *@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_0/bias*
dtype0
Å
2critic/q/q1_encoding/q1_encoder/hidden_0/bias/Adam
VariableV2*
shape:*
shared_name *@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_0/bias*
dtype0*
	container 
±
9critic/q/q1_encoding/q1_encoder/hidden_0/bias/Adam/AssignAssign2critic/q/q1_encoding/q1_encoder/hidden_0/bias/AdamDcritic/q/q1_encoding/q1_encoder/hidden_0/bias/Adam/Initializer/zeros*
use_locking(*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_0/bias*
validate_shape(
Ā
7critic/q/q1_encoding/q1_encoder/hidden_0/bias/Adam/readIdentity2critic/q/q1_encoding/q1_encoder/hidden_0/bias/Adam*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_0/bias
ŗ
Fcritic/q/q1_encoding/q1_encoder/hidden_0/bias/Adam_1/Initializer/zerosConst*
valueB*    *@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_0/bias*
dtype0
Ē
4critic/q/q1_encoding/q1_encoder/hidden_0/bias/Adam_1
VariableV2*
shape:*
shared_name *@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_0/bias*
dtype0*
	container 
·
;critic/q/q1_encoding/q1_encoder/hidden_0/bias/Adam_1/AssignAssign4critic/q/q1_encoding/q1_encoder/hidden_0/bias/Adam_1Fcritic/q/q1_encoding/q1_encoder/hidden_0/bias/Adam_1/Initializer/zeros*
use_locking(*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_0/bias*
validate_shape(
Ę
9critic/q/q1_encoding/q1_encoder/hidden_0/bias/Adam_1/readIdentity4critic/q/q1_encoding/q1_encoder/hidden_0/bias/Adam_1*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_0/bias
Ļ
Vcritic/q/q1_encoding/q1_encoder/hidden_1/kernel/Adam/Initializer/zeros/shape_as_tensorConst*
valueB"      *B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel*
dtype0
½
Lcritic/q/q1_encoding/q1_encoder/hidden_1/kernel/Adam/Initializer/zeros/ConstConst*
valueB
 *    *B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel*
dtype0
Ó
Fcritic/q/q1_encoding/q1_encoder/hidden_1/kernel/Adam/Initializer/zerosFillVcritic/q/q1_encoding/q1_encoder/hidden_1/kernel/Adam/Initializer/zeros/shape_as_tensorLcritic/q/q1_encoding/q1_encoder/hidden_1/kernel/Adam/Initializer/zeros/Const*
T0*

index_type0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel
Ī
4critic/q/q1_encoding/q1_encoder/hidden_1/kernel/Adam
VariableV2*
shape:
*
shared_name *B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel*
dtype0*
	container 
¹
;critic/q/q1_encoding/q1_encoder/hidden_1/kernel/Adam/AssignAssign4critic/q/q1_encoding/q1_encoder/hidden_1/kernel/AdamFcritic/q/q1_encoding/q1_encoder/hidden_1/kernel/Adam/Initializer/zeros*
use_locking(*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel*
validate_shape(
Č
9critic/q/q1_encoding/q1_encoder/hidden_1/kernel/Adam/readIdentity4critic/q/q1_encoding/q1_encoder/hidden_1/kernel/Adam*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel
Ń
Xcritic/q/q1_encoding/q1_encoder/hidden_1/kernel/Adam_1/Initializer/zeros/shape_as_tensorConst*
valueB"      *B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel*
dtype0
æ
Ncritic/q/q1_encoding/q1_encoder/hidden_1/kernel/Adam_1/Initializer/zeros/ConstConst*
valueB
 *    *B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel*
dtype0
Ł
Hcritic/q/q1_encoding/q1_encoder/hidden_1/kernel/Adam_1/Initializer/zerosFillXcritic/q/q1_encoding/q1_encoder/hidden_1/kernel/Adam_1/Initializer/zeros/shape_as_tensorNcritic/q/q1_encoding/q1_encoder/hidden_1/kernel/Adam_1/Initializer/zeros/Const*
T0*

index_type0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel
Š
6critic/q/q1_encoding/q1_encoder/hidden_1/kernel/Adam_1
VariableV2*
shape:
*
shared_name *B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel*
dtype0*
	container 
æ
=critic/q/q1_encoding/q1_encoder/hidden_1/kernel/Adam_1/AssignAssign6critic/q/q1_encoding/q1_encoder/hidden_1/kernel/Adam_1Hcritic/q/q1_encoding/q1_encoder/hidden_1/kernel/Adam_1/Initializer/zeros*
use_locking(*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel*
validate_shape(
Ģ
;critic/q/q1_encoding/q1_encoder/hidden_1/kernel/Adam_1/readIdentity6critic/q/q1_encoding/q1_encoder/hidden_1/kernel/Adam_1*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel
ø
Dcritic/q/q1_encoding/q1_encoder/hidden_1/bias/Adam/Initializer/zerosConst*
valueB*    *@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_1/bias*
dtype0
Å
2critic/q/q1_encoding/q1_encoder/hidden_1/bias/Adam
VariableV2*
shape:*
shared_name *@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_1/bias*
dtype0*
	container 
±
9critic/q/q1_encoding/q1_encoder/hidden_1/bias/Adam/AssignAssign2critic/q/q1_encoding/q1_encoder/hidden_1/bias/AdamDcritic/q/q1_encoding/q1_encoder/hidden_1/bias/Adam/Initializer/zeros*
use_locking(*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_1/bias*
validate_shape(
Ā
7critic/q/q1_encoding/q1_encoder/hidden_1/bias/Adam/readIdentity2critic/q/q1_encoding/q1_encoder/hidden_1/bias/Adam*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_1/bias
ŗ
Fcritic/q/q1_encoding/q1_encoder/hidden_1/bias/Adam_1/Initializer/zerosConst*
valueB*    *@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_1/bias*
dtype0
Ē
4critic/q/q1_encoding/q1_encoder/hidden_1/bias/Adam_1
VariableV2*
shape:*
shared_name *@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_1/bias*
dtype0*
	container 
·
;critic/q/q1_encoding/q1_encoder/hidden_1/bias/Adam_1/AssignAssign4critic/q/q1_encoding/q1_encoder/hidden_1/bias/Adam_1Fcritic/q/q1_encoding/q1_encoder/hidden_1/bias/Adam_1/Initializer/zeros*
use_locking(*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_1/bias*
validate_shape(
Ę
9critic/q/q1_encoding/q1_encoder/hidden_1/bias/Adam_1/readIdentity4critic/q/q1_encoding/q1_encoder/hidden_1/bias/Adam_1*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_1/bias
Į
Ocritic/q/q1_encoding/extrinsic_q1/kernel/Adam/Initializer/zeros/shape_as_tensorConst*
valueB"      *;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel*
dtype0
Æ
Ecritic/q/q1_encoding/extrinsic_q1/kernel/Adam/Initializer/zeros/ConstConst*
valueB
 *    *;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel*
dtype0
·
?critic/q/q1_encoding/extrinsic_q1/kernel/Adam/Initializer/zerosFillOcritic/q/q1_encoding/extrinsic_q1/kernel/Adam/Initializer/zeros/shape_as_tensorEcritic/q/q1_encoding/extrinsic_q1/kernel/Adam/Initializer/zeros/Const*
T0*

index_type0*;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel
æ
-critic/q/q1_encoding/extrinsic_q1/kernel/Adam
VariableV2*
shape:	*
shared_name *;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel*
dtype0*
	container 

4critic/q/q1_encoding/extrinsic_q1/kernel/Adam/AssignAssign-critic/q/q1_encoding/extrinsic_q1/kernel/Adam?critic/q/q1_encoding/extrinsic_q1/kernel/Adam/Initializer/zeros*
use_locking(*
T0*;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel*
validate_shape(
³
2critic/q/q1_encoding/extrinsic_q1/kernel/Adam/readIdentity-critic/q/q1_encoding/extrinsic_q1/kernel/Adam*
T0*;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel
Ć
Qcritic/q/q1_encoding/extrinsic_q1/kernel/Adam_1/Initializer/zeros/shape_as_tensorConst*
valueB"      *;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel*
dtype0
±
Gcritic/q/q1_encoding/extrinsic_q1/kernel/Adam_1/Initializer/zeros/ConstConst*
valueB
 *    *;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel*
dtype0
½
Acritic/q/q1_encoding/extrinsic_q1/kernel/Adam_1/Initializer/zerosFillQcritic/q/q1_encoding/extrinsic_q1/kernel/Adam_1/Initializer/zeros/shape_as_tensorGcritic/q/q1_encoding/extrinsic_q1/kernel/Adam_1/Initializer/zeros/Const*
T0*

index_type0*;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel
Į
/critic/q/q1_encoding/extrinsic_q1/kernel/Adam_1
VariableV2*
shape:	*
shared_name *;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel*
dtype0*
	container 
£
6critic/q/q1_encoding/extrinsic_q1/kernel/Adam_1/AssignAssign/critic/q/q1_encoding/extrinsic_q1/kernel/Adam_1Acritic/q/q1_encoding/extrinsic_q1/kernel/Adam_1/Initializer/zeros*
use_locking(*
T0*;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel*
validate_shape(
·
4critic/q/q1_encoding/extrinsic_q1/kernel/Adam_1/readIdentity/critic/q/q1_encoding/extrinsic_q1/kernel/Adam_1*
T0*;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel
©
=critic/q/q1_encoding/extrinsic_q1/bias/Adam/Initializer/zerosConst*
valueB*    *9
_class/
-+loc:@critic/q/q1_encoding/extrinsic_q1/bias*
dtype0
¶
+critic/q/q1_encoding/extrinsic_q1/bias/Adam
VariableV2*
shape:*
shared_name *9
_class/
-+loc:@critic/q/q1_encoding/extrinsic_q1/bias*
dtype0*
	container 

2critic/q/q1_encoding/extrinsic_q1/bias/Adam/AssignAssign+critic/q/q1_encoding/extrinsic_q1/bias/Adam=critic/q/q1_encoding/extrinsic_q1/bias/Adam/Initializer/zeros*
use_locking(*
T0*9
_class/
-+loc:@critic/q/q1_encoding/extrinsic_q1/bias*
validate_shape(
­
0critic/q/q1_encoding/extrinsic_q1/bias/Adam/readIdentity+critic/q/q1_encoding/extrinsic_q1/bias/Adam*
T0*9
_class/
-+loc:@critic/q/q1_encoding/extrinsic_q1/bias
«
?critic/q/q1_encoding/extrinsic_q1/bias/Adam_1/Initializer/zerosConst*
valueB*    *9
_class/
-+loc:@critic/q/q1_encoding/extrinsic_q1/bias*
dtype0
ø
-critic/q/q1_encoding/extrinsic_q1/bias/Adam_1
VariableV2*
shape:*
shared_name *9
_class/
-+loc:@critic/q/q1_encoding/extrinsic_q1/bias*
dtype0*
	container 

4critic/q/q1_encoding/extrinsic_q1/bias/Adam_1/AssignAssign-critic/q/q1_encoding/extrinsic_q1/bias/Adam_1?critic/q/q1_encoding/extrinsic_q1/bias/Adam_1/Initializer/zeros*
use_locking(*
T0*9
_class/
-+loc:@critic/q/q1_encoding/extrinsic_q1/bias*
validate_shape(
±
2critic/q/q1_encoding/extrinsic_q1/bias/Adam_1/readIdentity-critic/q/q1_encoding/extrinsic_q1/bias/Adam_1*
T0*9
_class/
-+loc:@critic/q/q1_encoding/extrinsic_q1/bias
·
Jcritic/q/q1_encoding/gail_q1/kernel/Adam/Initializer/zeros/shape_as_tensorConst*
valueB"      *6
_class,
*(loc:@critic/q/q1_encoding/gail_q1/kernel*
dtype0
„
@critic/q/q1_encoding/gail_q1/kernel/Adam/Initializer/zeros/ConstConst*
valueB
 *    *6
_class,
*(loc:@critic/q/q1_encoding/gail_q1/kernel*
dtype0
£
:critic/q/q1_encoding/gail_q1/kernel/Adam/Initializer/zerosFillJcritic/q/q1_encoding/gail_q1/kernel/Adam/Initializer/zeros/shape_as_tensor@critic/q/q1_encoding/gail_q1/kernel/Adam/Initializer/zeros/Const*
T0*

index_type0*6
_class,
*(loc:@critic/q/q1_encoding/gail_q1/kernel
µ
(critic/q/q1_encoding/gail_q1/kernel/Adam
VariableV2*
shape:	*
shared_name *6
_class,
*(loc:@critic/q/q1_encoding/gail_q1/kernel*
dtype0*
	container 

/critic/q/q1_encoding/gail_q1/kernel/Adam/AssignAssign(critic/q/q1_encoding/gail_q1/kernel/Adam:critic/q/q1_encoding/gail_q1/kernel/Adam/Initializer/zeros*
use_locking(*
T0*6
_class,
*(loc:@critic/q/q1_encoding/gail_q1/kernel*
validate_shape(
¤
-critic/q/q1_encoding/gail_q1/kernel/Adam/readIdentity(critic/q/q1_encoding/gail_q1/kernel/Adam*
T0*6
_class,
*(loc:@critic/q/q1_encoding/gail_q1/kernel
¹
Lcritic/q/q1_encoding/gail_q1/kernel/Adam_1/Initializer/zeros/shape_as_tensorConst*
valueB"      *6
_class,
*(loc:@critic/q/q1_encoding/gail_q1/kernel*
dtype0
§
Bcritic/q/q1_encoding/gail_q1/kernel/Adam_1/Initializer/zeros/ConstConst*
valueB
 *    *6
_class,
*(loc:@critic/q/q1_encoding/gail_q1/kernel*
dtype0
©
<critic/q/q1_encoding/gail_q1/kernel/Adam_1/Initializer/zerosFillLcritic/q/q1_encoding/gail_q1/kernel/Adam_1/Initializer/zeros/shape_as_tensorBcritic/q/q1_encoding/gail_q1/kernel/Adam_1/Initializer/zeros/Const*
T0*

index_type0*6
_class,
*(loc:@critic/q/q1_encoding/gail_q1/kernel
·
*critic/q/q1_encoding/gail_q1/kernel/Adam_1
VariableV2*
shape:	*
shared_name *6
_class,
*(loc:@critic/q/q1_encoding/gail_q1/kernel*
dtype0*
	container 

1critic/q/q1_encoding/gail_q1/kernel/Adam_1/AssignAssign*critic/q/q1_encoding/gail_q1/kernel/Adam_1<critic/q/q1_encoding/gail_q1/kernel/Adam_1/Initializer/zeros*
use_locking(*
T0*6
_class,
*(loc:@critic/q/q1_encoding/gail_q1/kernel*
validate_shape(
Ø
/critic/q/q1_encoding/gail_q1/kernel/Adam_1/readIdentity*critic/q/q1_encoding/gail_q1/kernel/Adam_1*
T0*6
_class,
*(loc:@critic/q/q1_encoding/gail_q1/kernel

8critic/q/q1_encoding/gail_q1/bias/Adam/Initializer/zerosConst*
valueB*    *4
_class*
(&loc:@critic/q/q1_encoding/gail_q1/bias*
dtype0
¬
&critic/q/q1_encoding/gail_q1/bias/Adam
VariableV2*
shape:*
shared_name *4
_class*
(&loc:@critic/q/q1_encoding/gail_q1/bias*
dtype0*
	container 

-critic/q/q1_encoding/gail_q1/bias/Adam/AssignAssign&critic/q/q1_encoding/gail_q1/bias/Adam8critic/q/q1_encoding/gail_q1/bias/Adam/Initializer/zeros*
use_locking(*
T0*4
_class*
(&loc:@critic/q/q1_encoding/gail_q1/bias*
validate_shape(

+critic/q/q1_encoding/gail_q1/bias/Adam/readIdentity&critic/q/q1_encoding/gail_q1/bias/Adam*
T0*4
_class*
(&loc:@critic/q/q1_encoding/gail_q1/bias
”
:critic/q/q1_encoding/gail_q1/bias/Adam_1/Initializer/zerosConst*
valueB*    *4
_class*
(&loc:@critic/q/q1_encoding/gail_q1/bias*
dtype0
®
(critic/q/q1_encoding/gail_q1/bias/Adam_1
VariableV2*
shape:*
shared_name *4
_class*
(&loc:@critic/q/q1_encoding/gail_q1/bias*
dtype0*
	container 

/critic/q/q1_encoding/gail_q1/bias/Adam_1/AssignAssign(critic/q/q1_encoding/gail_q1/bias/Adam_1:critic/q/q1_encoding/gail_q1/bias/Adam_1/Initializer/zeros*
use_locking(*
T0*4
_class*
(&loc:@critic/q/q1_encoding/gail_q1/bias*
validate_shape(
¢
-critic/q/q1_encoding/gail_q1/bias/Adam_1/readIdentity(critic/q/q1_encoding/gail_q1/bias/Adam_1*
T0*4
_class*
(&loc:@critic/q/q1_encoding/gail_q1/bias
Ļ
Vcritic/q/q2_encoding/q2_encoder/hidden_0/kernel/Adam/Initializer/zeros/shape_as_tensorConst*
valueB"b     *B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel*
dtype0
½
Lcritic/q/q2_encoding/q2_encoder/hidden_0/kernel/Adam/Initializer/zeros/ConstConst*
valueB
 *    *B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel*
dtype0
Ó
Fcritic/q/q2_encoding/q2_encoder/hidden_0/kernel/Adam/Initializer/zerosFillVcritic/q/q2_encoding/q2_encoder/hidden_0/kernel/Adam/Initializer/zeros/shape_as_tensorLcritic/q/q2_encoding/q2_encoder/hidden_0/kernel/Adam/Initializer/zeros/Const*
T0*

index_type0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel
Ī
4critic/q/q2_encoding/q2_encoder/hidden_0/kernel/Adam
VariableV2*
shape:
ā*
shared_name *B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel*
dtype0*
	container 
¹
;critic/q/q2_encoding/q2_encoder/hidden_0/kernel/Adam/AssignAssign4critic/q/q2_encoding/q2_encoder/hidden_0/kernel/AdamFcritic/q/q2_encoding/q2_encoder/hidden_0/kernel/Adam/Initializer/zeros*
use_locking(*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel*
validate_shape(
Č
9critic/q/q2_encoding/q2_encoder/hidden_0/kernel/Adam/readIdentity4critic/q/q2_encoding/q2_encoder/hidden_0/kernel/Adam*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel
Ń
Xcritic/q/q2_encoding/q2_encoder/hidden_0/kernel/Adam_1/Initializer/zeros/shape_as_tensorConst*
valueB"b     *B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel*
dtype0
æ
Ncritic/q/q2_encoding/q2_encoder/hidden_0/kernel/Adam_1/Initializer/zeros/ConstConst*
valueB
 *    *B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel*
dtype0
Ł
Hcritic/q/q2_encoding/q2_encoder/hidden_0/kernel/Adam_1/Initializer/zerosFillXcritic/q/q2_encoding/q2_encoder/hidden_0/kernel/Adam_1/Initializer/zeros/shape_as_tensorNcritic/q/q2_encoding/q2_encoder/hidden_0/kernel/Adam_1/Initializer/zeros/Const*
T0*

index_type0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel
Š
6critic/q/q2_encoding/q2_encoder/hidden_0/kernel/Adam_1
VariableV2*
shape:
ā*
shared_name *B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel*
dtype0*
	container 
æ
=critic/q/q2_encoding/q2_encoder/hidden_0/kernel/Adam_1/AssignAssign6critic/q/q2_encoding/q2_encoder/hidden_0/kernel/Adam_1Hcritic/q/q2_encoding/q2_encoder/hidden_0/kernel/Adam_1/Initializer/zeros*
use_locking(*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel*
validate_shape(
Ģ
;critic/q/q2_encoding/q2_encoder/hidden_0/kernel/Adam_1/readIdentity6critic/q/q2_encoding/q2_encoder/hidden_0/kernel/Adam_1*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel
ø
Dcritic/q/q2_encoding/q2_encoder/hidden_0/bias/Adam/Initializer/zerosConst*
valueB*    *@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_0/bias*
dtype0
Å
2critic/q/q2_encoding/q2_encoder/hidden_0/bias/Adam
VariableV2*
shape:*
shared_name *@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_0/bias*
dtype0*
	container 
±
9critic/q/q2_encoding/q2_encoder/hidden_0/bias/Adam/AssignAssign2critic/q/q2_encoding/q2_encoder/hidden_0/bias/AdamDcritic/q/q2_encoding/q2_encoder/hidden_0/bias/Adam/Initializer/zeros*
use_locking(*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_0/bias*
validate_shape(
Ā
7critic/q/q2_encoding/q2_encoder/hidden_0/bias/Adam/readIdentity2critic/q/q2_encoding/q2_encoder/hidden_0/bias/Adam*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_0/bias
ŗ
Fcritic/q/q2_encoding/q2_encoder/hidden_0/bias/Adam_1/Initializer/zerosConst*
valueB*    *@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_0/bias*
dtype0
Ē
4critic/q/q2_encoding/q2_encoder/hidden_0/bias/Adam_1
VariableV2*
shape:*
shared_name *@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_0/bias*
dtype0*
	container 
·
;critic/q/q2_encoding/q2_encoder/hidden_0/bias/Adam_1/AssignAssign4critic/q/q2_encoding/q2_encoder/hidden_0/bias/Adam_1Fcritic/q/q2_encoding/q2_encoder/hidden_0/bias/Adam_1/Initializer/zeros*
use_locking(*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_0/bias*
validate_shape(
Ę
9critic/q/q2_encoding/q2_encoder/hidden_0/bias/Adam_1/readIdentity4critic/q/q2_encoding/q2_encoder/hidden_0/bias/Adam_1*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_0/bias
Ļ
Vcritic/q/q2_encoding/q2_encoder/hidden_1/kernel/Adam/Initializer/zeros/shape_as_tensorConst*
valueB"      *B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel*
dtype0
½
Lcritic/q/q2_encoding/q2_encoder/hidden_1/kernel/Adam/Initializer/zeros/ConstConst*
valueB
 *    *B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel*
dtype0
Ó
Fcritic/q/q2_encoding/q2_encoder/hidden_1/kernel/Adam/Initializer/zerosFillVcritic/q/q2_encoding/q2_encoder/hidden_1/kernel/Adam/Initializer/zeros/shape_as_tensorLcritic/q/q2_encoding/q2_encoder/hidden_1/kernel/Adam/Initializer/zeros/Const*
T0*

index_type0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel
Ī
4critic/q/q2_encoding/q2_encoder/hidden_1/kernel/Adam
VariableV2*
shape:
*
shared_name *B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel*
dtype0*
	container 
¹
;critic/q/q2_encoding/q2_encoder/hidden_1/kernel/Adam/AssignAssign4critic/q/q2_encoding/q2_encoder/hidden_1/kernel/AdamFcritic/q/q2_encoding/q2_encoder/hidden_1/kernel/Adam/Initializer/zeros*
use_locking(*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel*
validate_shape(
Č
9critic/q/q2_encoding/q2_encoder/hidden_1/kernel/Adam/readIdentity4critic/q/q2_encoding/q2_encoder/hidden_1/kernel/Adam*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel
Ń
Xcritic/q/q2_encoding/q2_encoder/hidden_1/kernel/Adam_1/Initializer/zeros/shape_as_tensorConst*
valueB"      *B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel*
dtype0
æ
Ncritic/q/q2_encoding/q2_encoder/hidden_1/kernel/Adam_1/Initializer/zeros/ConstConst*
valueB
 *    *B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel*
dtype0
Ł
Hcritic/q/q2_encoding/q2_encoder/hidden_1/kernel/Adam_1/Initializer/zerosFillXcritic/q/q2_encoding/q2_encoder/hidden_1/kernel/Adam_1/Initializer/zeros/shape_as_tensorNcritic/q/q2_encoding/q2_encoder/hidden_1/kernel/Adam_1/Initializer/zeros/Const*
T0*

index_type0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel
Š
6critic/q/q2_encoding/q2_encoder/hidden_1/kernel/Adam_1
VariableV2*
shape:
*
shared_name *B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel*
dtype0*
	container 
æ
=critic/q/q2_encoding/q2_encoder/hidden_1/kernel/Adam_1/AssignAssign6critic/q/q2_encoding/q2_encoder/hidden_1/kernel/Adam_1Hcritic/q/q2_encoding/q2_encoder/hidden_1/kernel/Adam_1/Initializer/zeros*
use_locking(*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel*
validate_shape(
Ģ
;critic/q/q2_encoding/q2_encoder/hidden_1/kernel/Adam_1/readIdentity6critic/q/q2_encoding/q2_encoder/hidden_1/kernel/Adam_1*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel
ø
Dcritic/q/q2_encoding/q2_encoder/hidden_1/bias/Adam/Initializer/zerosConst*
valueB*    *@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_1/bias*
dtype0
Å
2critic/q/q2_encoding/q2_encoder/hidden_1/bias/Adam
VariableV2*
shape:*
shared_name *@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_1/bias*
dtype0*
	container 
±
9critic/q/q2_encoding/q2_encoder/hidden_1/bias/Adam/AssignAssign2critic/q/q2_encoding/q2_encoder/hidden_1/bias/AdamDcritic/q/q2_encoding/q2_encoder/hidden_1/bias/Adam/Initializer/zeros*
use_locking(*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_1/bias*
validate_shape(
Ā
7critic/q/q2_encoding/q2_encoder/hidden_1/bias/Adam/readIdentity2critic/q/q2_encoding/q2_encoder/hidden_1/bias/Adam*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_1/bias
ŗ
Fcritic/q/q2_encoding/q2_encoder/hidden_1/bias/Adam_1/Initializer/zerosConst*
valueB*    *@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_1/bias*
dtype0
Ē
4critic/q/q2_encoding/q2_encoder/hidden_1/bias/Adam_1
VariableV2*
shape:*
shared_name *@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_1/bias*
dtype0*
	container 
·
;critic/q/q2_encoding/q2_encoder/hidden_1/bias/Adam_1/AssignAssign4critic/q/q2_encoding/q2_encoder/hidden_1/bias/Adam_1Fcritic/q/q2_encoding/q2_encoder/hidden_1/bias/Adam_1/Initializer/zeros*
use_locking(*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_1/bias*
validate_shape(
Ę
9critic/q/q2_encoding/q2_encoder/hidden_1/bias/Adam_1/readIdentity4critic/q/q2_encoding/q2_encoder/hidden_1/bias/Adam_1*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_1/bias
Į
Ocritic/q/q2_encoding/extrinsic_q2/kernel/Adam/Initializer/zeros/shape_as_tensorConst*
valueB"      *;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel*
dtype0
Æ
Ecritic/q/q2_encoding/extrinsic_q2/kernel/Adam/Initializer/zeros/ConstConst*
valueB
 *    *;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel*
dtype0
·
?critic/q/q2_encoding/extrinsic_q2/kernel/Adam/Initializer/zerosFillOcritic/q/q2_encoding/extrinsic_q2/kernel/Adam/Initializer/zeros/shape_as_tensorEcritic/q/q2_encoding/extrinsic_q2/kernel/Adam/Initializer/zeros/Const*
T0*

index_type0*;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel
æ
-critic/q/q2_encoding/extrinsic_q2/kernel/Adam
VariableV2*
shape:	*
shared_name *;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel*
dtype0*
	container 

4critic/q/q2_encoding/extrinsic_q2/kernel/Adam/AssignAssign-critic/q/q2_encoding/extrinsic_q2/kernel/Adam?critic/q/q2_encoding/extrinsic_q2/kernel/Adam/Initializer/zeros*
use_locking(*
T0*;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel*
validate_shape(
³
2critic/q/q2_encoding/extrinsic_q2/kernel/Adam/readIdentity-critic/q/q2_encoding/extrinsic_q2/kernel/Adam*
T0*;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel
Ć
Qcritic/q/q2_encoding/extrinsic_q2/kernel/Adam_1/Initializer/zeros/shape_as_tensorConst*
valueB"      *;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel*
dtype0
±
Gcritic/q/q2_encoding/extrinsic_q2/kernel/Adam_1/Initializer/zeros/ConstConst*
valueB
 *    *;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel*
dtype0
½
Acritic/q/q2_encoding/extrinsic_q2/kernel/Adam_1/Initializer/zerosFillQcritic/q/q2_encoding/extrinsic_q2/kernel/Adam_1/Initializer/zeros/shape_as_tensorGcritic/q/q2_encoding/extrinsic_q2/kernel/Adam_1/Initializer/zeros/Const*
T0*

index_type0*;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel
Į
/critic/q/q2_encoding/extrinsic_q2/kernel/Adam_1
VariableV2*
shape:	*
shared_name *;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel*
dtype0*
	container 
£
6critic/q/q2_encoding/extrinsic_q2/kernel/Adam_1/AssignAssign/critic/q/q2_encoding/extrinsic_q2/kernel/Adam_1Acritic/q/q2_encoding/extrinsic_q2/kernel/Adam_1/Initializer/zeros*
use_locking(*
T0*;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel*
validate_shape(
·
4critic/q/q2_encoding/extrinsic_q2/kernel/Adam_1/readIdentity/critic/q/q2_encoding/extrinsic_q2/kernel/Adam_1*
T0*;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel
©
=critic/q/q2_encoding/extrinsic_q2/bias/Adam/Initializer/zerosConst*
valueB*    *9
_class/
-+loc:@critic/q/q2_encoding/extrinsic_q2/bias*
dtype0
¶
+critic/q/q2_encoding/extrinsic_q2/bias/Adam
VariableV2*
shape:*
shared_name *9
_class/
-+loc:@critic/q/q2_encoding/extrinsic_q2/bias*
dtype0*
	container 

2critic/q/q2_encoding/extrinsic_q2/bias/Adam/AssignAssign+critic/q/q2_encoding/extrinsic_q2/bias/Adam=critic/q/q2_encoding/extrinsic_q2/bias/Adam/Initializer/zeros*
use_locking(*
T0*9
_class/
-+loc:@critic/q/q2_encoding/extrinsic_q2/bias*
validate_shape(
­
0critic/q/q2_encoding/extrinsic_q2/bias/Adam/readIdentity+critic/q/q2_encoding/extrinsic_q2/bias/Adam*
T0*9
_class/
-+loc:@critic/q/q2_encoding/extrinsic_q2/bias
«
?critic/q/q2_encoding/extrinsic_q2/bias/Adam_1/Initializer/zerosConst*
valueB*    *9
_class/
-+loc:@critic/q/q2_encoding/extrinsic_q2/bias*
dtype0
ø
-critic/q/q2_encoding/extrinsic_q2/bias/Adam_1
VariableV2*
shape:*
shared_name *9
_class/
-+loc:@critic/q/q2_encoding/extrinsic_q2/bias*
dtype0*
	container 

4critic/q/q2_encoding/extrinsic_q2/bias/Adam_1/AssignAssign-critic/q/q2_encoding/extrinsic_q2/bias/Adam_1?critic/q/q2_encoding/extrinsic_q2/bias/Adam_1/Initializer/zeros*
use_locking(*
T0*9
_class/
-+loc:@critic/q/q2_encoding/extrinsic_q2/bias*
validate_shape(
±
2critic/q/q2_encoding/extrinsic_q2/bias/Adam_1/readIdentity-critic/q/q2_encoding/extrinsic_q2/bias/Adam_1*
T0*9
_class/
-+loc:@critic/q/q2_encoding/extrinsic_q2/bias
·
Jcritic/q/q2_encoding/gail_q2/kernel/Adam/Initializer/zeros/shape_as_tensorConst*
valueB"      *6
_class,
*(loc:@critic/q/q2_encoding/gail_q2/kernel*
dtype0
„
@critic/q/q2_encoding/gail_q2/kernel/Adam/Initializer/zeros/ConstConst*
valueB
 *    *6
_class,
*(loc:@critic/q/q2_encoding/gail_q2/kernel*
dtype0
£
:critic/q/q2_encoding/gail_q2/kernel/Adam/Initializer/zerosFillJcritic/q/q2_encoding/gail_q2/kernel/Adam/Initializer/zeros/shape_as_tensor@critic/q/q2_encoding/gail_q2/kernel/Adam/Initializer/zeros/Const*
T0*

index_type0*6
_class,
*(loc:@critic/q/q2_encoding/gail_q2/kernel
µ
(critic/q/q2_encoding/gail_q2/kernel/Adam
VariableV2*
shape:	*
shared_name *6
_class,
*(loc:@critic/q/q2_encoding/gail_q2/kernel*
dtype0*
	container 

/critic/q/q2_encoding/gail_q2/kernel/Adam/AssignAssign(critic/q/q2_encoding/gail_q2/kernel/Adam:critic/q/q2_encoding/gail_q2/kernel/Adam/Initializer/zeros*
use_locking(*
T0*6
_class,
*(loc:@critic/q/q2_encoding/gail_q2/kernel*
validate_shape(
¤
-critic/q/q2_encoding/gail_q2/kernel/Adam/readIdentity(critic/q/q2_encoding/gail_q2/kernel/Adam*
T0*6
_class,
*(loc:@critic/q/q2_encoding/gail_q2/kernel
¹
Lcritic/q/q2_encoding/gail_q2/kernel/Adam_1/Initializer/zeros/shape_as_tensorConst*
valueB"      *6
_class,
*(loc:@critic/q/q2_encoding/gail_q2/kernel*
dtype0
§
Bcritic/q/q2_encoding/gail_q2/kernel/Adam_1/Initializer/zeros/ConstConst*
valueB
 *    *6
_class,
*(loc:@critic/q/q2_encoding/gail_q2/kernel*
dtype0
©
<critic/q/q2_encoding/gail_q2/kernel/Adam_1/Initializer/zerosFillLcritic/q/q2_encoding/gail_q2/kernel/Adam_1/Initializer/zeros/shape_as_tensorBcritic/q/q2_encoding/gail_q2/kernel/Adam_1/Initializer/zeros/Const*
T0*

index_type0*6
_class,
*(loc:@critic/q/q2_encoding/gail_q2/kernel
·
*critic/q/q2_encoding/gail_q2/kernel/Adam_1
VariableV2*
shape:	*
shared_name *6
_class,
*(loc:@critic/q/q2_encoding/gail_q2/kernel*
dtype0*
	container 

1critic/q/q2_encoding/gail_q2/kernel/Adam_1/AssignAssign*critic/q/q2_encoding/gail_q2/kernel/Adam_1<critic/q/q2_encoding/gail_q2/kernel/Adam_1/Initializer/zeros*
use_locking(*
T0*6
_class,
*(loc:@critic/q/q2_encoding/gail_q2/kernel*
validate_shape(
Ø
/critic/q/q2_encoding/gail_q2/kernel/Adam_1/readIdentity*critic/q/q2_encoding/gail_q2/kernel/Adam_1*
T0*6
_class,
*(loc:@critic/q/q2_encoding/gail_q2/kernel

8critic/q/q2_encoding/gail_q2/bias/Adam/Initializer/zerosConst*
valueB*    *4
_class*
(&loc:@critic/q/q2_encoding/gail_q2/bias*
dtype0
¬
&critic/q/q2_encoding/gail_q2/bias/Adam
VariableV2*
shape:*
shared_name *4
_class*
(&loc:@critic/q/q2_encoding/gail_q2/bias*
dtype0*
	container 

-critic/q/q2_encoding/gail_q2/bias/Adam/AssignAssign&critic/q/q2_encoding/gail_q2/bias/Adam8critic/q/q2_encoding/gail_q2/bias/Adam/Initializer/zeros*
use_locking(*
T0*4
_class*
(&loc:@critic/q/q2_encoding/gail_q2/bias*
validate_shape(

+critic/q/q2_encoding/gail_q2/bias/Adam/readIdentity&critic/q/q2_encoding/gail_q2/bias/Adam*
T0*4
_class*
(&loc:@critic/q/q2_encoding/gail_q2/bias
”
:critic/q/q2_encoding/gail_q2/bias/Adam_1/Initializer/zerosConst*
valueB*    *4
_class*
(&loc:@critic/q/q2_encoding/gail_q2/bias*
dtype0
®
(critic/q/q2_encoding/gail_q2/bias/Adam_1
VariableV2*
shape:*
shared_name *4
_class*
(&loc:@critic/q/q2_encoding/gail_q2/bias*
dtype0*
	container 

/critic/q/q2_encoding/gail_q2/bias/Adam_1/AssignAssign(critic/q/q2_encoding/gail_q2/bias/Adam_1:critic/q/q2_encoding/gail_q2/bias/Adam_1/Initializer/zeros*
use_locking(*
T0*4
_class*
(&loc:@critic/q/q2_encoding/gail_q2/bias*
validate_shape(
¢
-critic/q/q2_encoding/gail_q2/bias/Adam_1/readIdentity(critic/q/q2_encoding/gail_q2/bias/Adam_1*
T0*4
_class*
(&loc:@critic/q/q2_encoding/gail_q2/bias
@
Adam_1/beta1Const^Adam*
valueB
 *fff?*
dtype0
@
Adam_1/beta2Const^Adam*
valueB
 *w¾?*
dtype0
B
Adam_1/epsilonConst^Adam*
valueB
 *wĢ+2*
dtype0
ē
<Adam_1/update_critic/value/encoder/hidden_0/kernel/ApplyAdam	ApplyAdam$critic/value/encoder/hidden_0/kernel)critic/value/encoder/hidden_0/kernel/Adam+critic/value/encoder/hidden_0/kernel/Adam_1beta1_power_1/readbeta2_power_1/readVariable_2/readAdam_1/beta1Adam_1/beta2Adam_1/epsilonPgradients_1/critic/value/encoder/hidden_0/MatMul_grad/tuple/control_dependency_1*
use_locking( *
T0*7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel*
use_nesterov( 
Ž
:Adam_1/update_critic/value/encoder/hidden_0/bias/ApplyAdam	ApplyAdam"critic/value/encoder/hidden_0/bias'critic/value/encoder/hidden_0/bias/Adam)critic/value/encoder/hidden_0/bias/Adam_1beta1_power_1/readbeta2_power_1/readVariable_2/readAdam_1/beta1Adam_1/beta2Adam_1/epsilonQgradients_1/critic/value/encoder/hidden_0/BiasAdd_grad/tuple/control_dependency_1*
use_locking( *
T0*5
_class+
)'loc:@critic/value/encoder/hidden_0/bias*
use_nesterov( 
ē
<Adam_1/update_critic/value/encoder/hidden_1/kernel/ApplyAdam	ApplyAdam$critic/value/encoder/hidden_1/kernel)critic/value/encoder/hidden_1/kernel/Adam+critic/value/encoder/hidden_1/kernel/Adam_1beta1_power_1/readbeta2_power_1/readVariable_2/readAdam_1/beta1Adam_1/beta2Adam_1/epsilonPgradients_1/critic/value/encoder/hidden_1/MatMul_grad/tuple/control_dependency_1*
use_locking( *
T0*7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel*
use_nesterov( 
Ž
:Adam_1/update_critic/value/encoder/hidden_1/bias/ApplyAdam	ApplyAdam"critic/value/encoder/hidden_1/bias'critic/value/encoder/hidden_1/bias/Adam)critic/value/encoder/hidden_1/bias/Adam_1beta1_power_1/readbeta2_power_1/readVariable_2/readAdam_1/beta1Adam_1/beta2Adam_1/epsilonQgradients_1/critic/value/encoder/hidden_1/BiasAdd_grad/tuple/control_dependency_1*
use_locking( *
T0*5
_class+
)'loc:@critic/value/encoder/hidden_1/bias*
use_nesterov( 
į
;Adam_1/update_critic/value/extrinsic_value/kernel/ApplyAdam	ApplyAdam#critic/value/extrinsic_value/kernel(critic/value/extrinsic_value/kernel/Adam*critic/value/extrinsic_value/kernel/Adam_1beta1_power_1/readbeta2_power_1/readVariable_2/readAdam_1/beta1Adam_1/beta2Adam_1/epsilonOgradients_1/critic/value/extrinsic_value/MatMul_grad/tuple/control_dependency_1*
use_locking( *
T0*6
_class,
*(loc:@critic/value/extrinsic_value/kernel*
use_nesterov( 
Ų
9Adam_1/update_critic/value/extrinsic_value/bias/ApplyAdam	ApplyAdam!critic/value/extrinsic_value/bias&critic/value/extrinsic_value/bias/Adam(critic/value/extrinsic_value/bias/Adam_1beta1_power_1/readbeta2_power_1/readVariable_2/readAdam_1/beta1Adam_1/beta2Adam_1/epsilonPgradients_1/critic/value/extrinsic_value/BiasAdd_grad/tuple/control_dependency_1*
use_locking( *
T0*4
_class*
(&loc:@critic/value/extrinsic_value/bias*
use_nesterov( 
Ć
6Adam_1/update_critic/value/gail_value/kernel/ApplyAdam	ApplyAdamcritic/value/gail_value/kernel#critic/value/gail_value/kernel/Adam%critic/value/gail_value/kernel/Adam_1beta1_power_1/readbeta2_power_1/readVariable_2/readAdam_1/beta1Adam_1/beta2Adam_1/epsilonJgradients_1/critic/value/gail_value/MatMul_grad/tuple/control_dependency_1*
use_locking( *
T0*1
_class'
%#loc:@critic/value/gail_value/kernel*
use_nesterov( 
ŗ
4Adam_1/update_critic/value/gail_value/bias/ApplyAdam	ApplyAdamcritic/value/gail_value/bias!critic/value/gail_value/bias/Adam#critic/value/gail_value/bias/Adam_1beta1_power_1/readbeta2_power_1/readVariable_2/readAdam_1/beta1Adam_1/beta2Adam_1/epsilonKgradients_1/critic/value/gail_value/BiasAdd_grad/tuple/control_dependency_1*
use_locking( *
T0*/
_class%
#!loc:@critic/value/gail_value/bias*
use_nesterov( 
©
GAdam_1/update_critic/q/q1_encoding/q1_encoder/hidden_0/kernel/ApplyAdam	ApplyAdam/critic/q/q1_encoding/q1_encoder/hidden_0/kernel4critic/q/q1_encoding/q1_encoder/hidden_0/kernel/Adam6critic/q/q1_encoding/q1_encoder/hidden_0/kernel/Adam_1beta1_power_1/readbeta2_power_1/readVariable_2/readAdam_1/beta1Adam_1/beta2Adam_1/epsilon[gradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/MatMul_grad/tuple/control_dependency_1*
use_locking( *
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel*
use_nesterov( 
 
EAdam_1/update_critic/q/q1_encoding/q1_encoder/hidden_0/bias/ApplyAdam	ApplyAdam-critic/q/q1_encoding/q1_encoder/hidden_0/bias2critic/q/q1_encoding/q1_encoder/hidden_0/bias/Adam4critic/q/q1_encoding/q1_encoder/hidden_0/bias/Adam_1beta1_power_1/readbeta2_power_1/readVariable_2/readAdam_1/beta1Adam_1/beta2Adam_1/epsilon\gradients_1/critic/q/q1_encoding/q1_encoder/hidden_0/BiasAdd_grad/tuple/control_dependency_1*
use_locking( *
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_0/bias*
use_nesterov( 
©
GAdam_1/update_critic/q/q1_encoding/q1_encoder/hidden_1/kernel/ApplyAdam	ApplyAdam/critic/q/q1_encoding/q1_encoder/hidden_1/kernel4critic/q/q1_encoding/q1_encoder/hidden_1/kernel/Adam6critic/q/q1_encoding/q1_encoder/hidden_1/kernel/Adam_1beta1_power_1/readbeta2_power_1/readVariable_2/readAdam_1/beta1Adam_1/beta2Adam_1/epsilon[gradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/MatMul_grad/tuple/control_dependency_1*
use_locking( *
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel*
use_nesterov( 
 
EAdam_1/update_critic/q/q1_encoding/q1_encoder/hidden_1/bias/ApplyAdam	ApplyAdam-critic/q/q1_encoding/q1_encoder/hidden_1/bias2critic/q/q1_encoding/q1_encoder/hidden_1/bias/Adam4critic/q/q1_encoding/q1_encoder/hidden_1/bias/Adam_1beta1_power_1/readbeta2_power_1/readVariable_2/readAdam_1/beta1Adam_1/beta2Adam_1/epsilon\gradients_1/critic/q/q1_encoding/q1_encoder/hidden_1/BiasAdd_grad/tuple/control_dependency_1*
use_locking( *
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_1/bias*
use_nesterov( 
’
@Adam_1/update_critic/q/q1_encoding/extrinsic_q1/kernel/ApplyAdam	ApplyAdam(critic/q/q1_encoding/extrinsic_q1/kernel-critic/q/q1_encoding/extrinsic_q1/kernel/Adam/critic/q/q1_encoding/extrinsic_q1/kernel/Adam_1beta1_power_1/readbeta2_power_1/readVariable_2/readAdam_1/beta1Adam_1/beta2Adam_1/epsilonTgradients_1/critic/q/q1_encoding/extrinsic_q1/MatMul_grad/tuple/control_dependency_1*
use_locking( *
T0*;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel*
use_nesterov( 
ö
>Adam_1/update_critic/q/q1_encoding/extrinsic_q1/bias/ApplyAdam	ApplyAdam&critic/q/q1_encoding/extrinsic_q1/bias+critic/q/q1_encoding/extrinsic_q1/bias/Adam-critic/q/q1_encoding/extrinsic_q1/bias/Adam_1beta1_power_1/readbeta2_power_1/readVariable_2/readAdam_1/beta1Adam_1/beta2Adam_1/epsilonUgradients_1/critic/q/q1_encoding/extrinsic_q1/BiasAdd_grad/tuple/control_dependency_1*
use_locking( *
T0*9
_class/
-+loc:@critic/q/q1_encoding/extrinsic_q1/bias*
use_nesterov( 
į
;Adam_1/update_critic/q/q1_encoding/gail_q1/kernel/ApplyAdam	ApplyAdam#critic/q/q1_encoding/gail_q1/kernel(critic/q/q1_encoding/gail_q1/kernel/Adam*critic/q/q1_encoding/gail_q1/kernel/Adam_1beta1_power_1/readbeta2_power_1/readVariable_2/readAdam_1/beta1Adam_1/beta2Adam_1/epsilonOgradients_1/critic/q/q1_encoding/gail_q1/MatMul_grad/tuple/control_dependency_1*
use_locking( *
T0*6
_class,
*(loc:@critic/q/q1_encoding/gail_q1/kernel*
use_nesterov( 
Ų
9Adam_1/update_critic/q/q1_encoding/gail_q1/bias/ApplyAdam	ApplyAdam!critic/q/q1_encoding/gail_q1/bias&critic/q/q1_encoding/gail_q1/bias/Adam(critic/q/q1_encoding/gail_q1/bias/Adam_1beta1_power_1/readbeta2_power_1/readVariable_2/readAdam_1/beta1Adam_1/beta2Adam_1/epsilonPgradients_1/critic/q/q1_encoding/gail_q1/BiasAdd_grad/tuple/control_dependency_1*
use_locking( *
T0*4
_class*
(&loc:@critic/q/q1_encoding/gail_q1/bias*
use_nesterov( 
©
GAdam_1/update_critic/q/q2_encoding/q2_encoder/hidden_0/kernel/ApplyAdam	ApplyAdam/critic/q/q2_encoding/q2_encoder/hidden_0/kernel4critic/q/q2_encoding/q2_encoder/hidden_0/kernel/Adam6critic/q/q2_encoding/q2_encoder/hidden_0/kernel/Adam_1beta1_power_1/readbeta2_power_1/readVariable_2/readAdam_1/beta1Adam_1/beta2Adam_1/epsilon[gradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/MatMul_grad/tuple/control_dependency_1*
use_locking( *
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel*
use_nesterov( 
 
EAdam_1/update_critic/q/q2_encoding/q2_encoder/hidden_0/bias/ApplyAdam	ApplyAdam-critic/q/q2_encoding/q2_encoder/hidden_0/bias2critic/q/q2_encoding/q2_encoder/hidden_0/bias/Adam4critic/q/q2_encoding/q2_encoder/hidden_0/bias/Adam_1beta1_power_1/readbeta2_power_1/readVariable_2/readAdam_1/beta1Adam_1/beta2Adam_1/epsilon\gradients_1/critic/q/q2_encoding/q2_encoder/hidden_0/BiasAdd_grad/tuple/control_dependency_1*
use_locking( *
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_0/bias*
use_nesterov( 
©
GAdam_1/update_critic/q/q2_encoding/q2_encoder/hidden_1/kernel/ApplyAdam	ApplyAdam/critic/q/q2_encoding/q2_encoder/hidden_1/kernel4critic/q/q2_encoding/q2_encoder/hidden_1/kernel/Adam6critic/q/q2_encoding/q2_encoder/hidden_1/kernel/Adam_1beta1_power_1/readbeta2_power_1/readVariable_2/readAdam_1/beta1Adam_1/beta2Adam_1/epsilon[gradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/MatMul_grad/tuple/control_dependency_1*
use_locking( *
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel*
use_nesterov( 
 
EAdam_1/update_critic/q/q2_encoding/q2_encoder/hidden_1/bias/ApplyAdam	ApplyAdam-critic/q/q2_encoding/q2_encoder/hidden_1/bias2critic/q/q2_encoding/q2_encoder/hidden_1/bias/Adam4critic/q/q2_encoding/q2_encoder/hidden_1/bias/Adam_1beta1_power_1/readbeta2_power_1/readVariable_2/readAdam_1/beta1Adam_1/beta2Adam_1/epsilon\gradients_1/critic/q/q2_encoding/q2_encoder/hidden_1/BiasAdd_grad/tuple/control_dependency_1*
use_locking( *
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_1/bias*
use_nesterov( 
’
@Adam_1/update_critic/q/q2_encoding/extrinsic_q2/kernel/ApplyAdam	ApplyAdam(critic/q/q2_encoding/extrinsic_q2/kernel-critic/q/q2_encoding/extrinsic_q2/kernel/Adam/critic/q/q2_encoding/extrinsic_q2/kernel/Adam_1beta1_power_1/readbeta2_power_1/readVariable_2/readAdam_1/beta1Adam_1/beta2Adam_1/epsilonTgradients_1/critic/q/q2_encoding/extrinsic_q2/MatMul_grad/tuple/control_dependency_1*
use_locking( *
T0*;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel*
use_nesterov( 
ö
>Adam_1/update_critic/q/q2_encoding/extrinsic_q2/bias/ApplyAdam	ApplyAdam&critic/q/q2_encoding/extrinsic_q2/bias+critic/q/q2_encoding/extrinsic_q2/bias/Adam-critic/q/q2_encoding/extrinsic_q2/bias/Adam_1beta1_power_1/readbeta2_power_1/readVariable_2/readAdam_1/beta1Adam_1/beta2Adam_1/epsilonUgradients_1/critic/q/q2_encoding/extrinsic_q2/BiasAdd_grad/tuple/control_dependency_1*
use_locking( *
T0*9
_class/
-+loc:@critic/q/q2_encoding/extrinsic_q2/bias*
use_nesterov( 
į
;Adam_1/update_critic/q/q2_encoding/gail_q2/kernel/ApplyAdam	ApplyAdam#critic/q/q2_encoding/gail_q2/kernel(critic/q/q2_encoding/gail_q2/kernel/Adam*critic/q/q2_encoding/gail_q2/kernel/Adam_1beta1_power_1/readbeta2_power_1/readVariable_2/readAdam_1/beta1Adam_1/beta2Adam_1/epsilonOgradients_1/critic/q/q2_encoding/gail_q2/MatMul_grad/tuple/control_dependency_1*
use_locking( *
T0*6
_class,
*(loc:@critic/q/q2_encoding/gail_q2/kernel*
use_nesterov( 
Ų
9Adam_1/update_critic/q/q2_encoding/gail_q2/bias/ApplyAdam	ApplyAdam!critic/q/q2_encoding/gail_q2/bias&critic/q/q2_encoding/gail_q2/bias/Adam(critic/q/q2_encoding/gail_q2/bias/Adam_1beta1_power_1/readbeta2_power_1/readVariable_2/readAdam_1/beta1Adam_1/beta2Adam_1/epsilonPgradients_1/critic/q/q2_encoding/gail_q2/BiasAdd_grad/tuple/control_dependency_1*
use_locking( *
T0*4
_class*
(&loc:@critic/q/q2_encoding/gail_q2/bias*
use_nesterov( 


Adam_1/mulMulbeta1_power_1/readAdam_1/beta1?^Adam_1/update_critic/q/q1_encoding/extrinsic_q1/bias/ApplyAdamA^Adam_1/update_critic/q/q1_encoding/extrinsic_q1/kernel/ApplyAdam:^Adam_1/update_critic/q/q1_encoding/gail_q1/bias/ApplyAdam<^Adam_1/update_critic/q/q1_encoding/gail_q1/kernel/ApplyAdamF^Adam_1/update_critic/q/q1_encoding/q1_encoder/hidden_0/bias/ApplyAdamH^Adam_1/update_critic/q/q1_encoding/q1_encoder/hidden_0/kernel/ApplyAdamF^Adam_1/update_critic/q/q1_encoding/q1_encoder/hidden_1/bias/ApplyAdamH^Adam_1/update_critic/q/q1_encoding/q1_encoder/hidden_1/kernel/ApplyAdam?^Adam_1/update_critic/q/q2_encoding/extrinsic_q2/bias/ApplyAdamA^Adam_1/update_critic/q/q2_encoding/extrinsic_q2/kernel/ApplyAdam:^Adam_1/update_critic/q/q2_encoding/gail_q2/bias/ApplyAdam<^Adam_1/update_critic/q/q2_encoding/gail_q2/kernel/ApplyAdamF^Adam_1/update_critic/q/q2_encoding/q2_encoder/hidden_0/bias/ApplyAdamH^Adam_1/update_critic/q/q2_encoding/q2_encoder/hidden_0/kernel/ApplyAdamF^Adam_1/update_critic/q/q2_encoding/q2_encoder/hidden_1/bias/ApplyAdamH^Adam_1/update_critic/q/q2_encoding/q2_encoder/hidden_1/kernel/ApplyAdam;^Adam_1/update_critic/value/encoder/hidden_0/bias/ApplyAdam=^Adam_1/update_critic/value/encoder/hidden_0/kernel/ApplyAdam;^Adam_1/update_critic/value/encoder/hidden_1/bias/ApplyAdam=^Adam_1/update_critic/value/encoder/hidden_1/kernel/ApplyAdam:^Adam_1/update_critic/value/extrinsic_value/bias/ApplyAdam<^Adam_1/update_critic/value/extrinsic_value/kernel/ApplyAdam5^Adam_1/update_critic/value/gail_value/bias/ApplyAdam7^Adam_1/update_critic/value/gail_value/kernel/ApplyAdam*
T0*9
_class/
-+loc:@critic/q/q1_encoding/extrinsic_q1/bias

Adam_1/AssignAssignbeta1_power_1
Adam_1/mul*
use_locking( *
T0*9
_class/
-+loc:@critic/q/q1_encoding/extrinsic_q1/bias*
validate_shape(

Adam_1/mul_1Mulbeta2_power_1/readAdam_1/beta2?^Adam_1/update_critic/q/q1_encoding/extrinsic_q1/bias/ApplyAdamA^Adam_1/update_critic/q/q1_encoding/extrinsic_q1/kernel/ApplyAdam:^Adam_1/update_critic/q/q1_encoding/gail_q1/bias/ApplyAdam<^Adam_1/update_critic/q/q1_encoding/gail_q1/kernel/ApplyAdamF^Adam_1/update_critic/q/q1_encoding/q1_encoder/hidden_0/bias/ApplyAdamH^Adam_1/update_critic/q/q1_encoding/q1_encoder/hidden_0/kernel/ApplyAdamF^Adam_1/update_critic/q/q1_encoding/q1_encoder/hidden_1/bias/ApplyAdamH^Adam_1/update_critic/q/q1_encoding/q1_encoder/hidden_1/kernel/ApplyAdam?^Adam_1/update_critic/q/q2_encoding/extrinsic_q2/bias/ApplyAdamA^Adam_1/update_critic/q/q2_encoding/extrinsic_q2/kernel/ApplyAdam:^Adam_1/update_critic/q/q2_encoding/gail_q2/bias/ApplyAdam<^Adam_1/update_critic/q/q2_encoding/gail_q2/kernel/ApplyAdamF^Adam_1/update_critic/q/q2_encoding/q2_encoder/hidden_0/bias/ApplyAdamH^Adam_1/update_critic/q/q2_encoding/q2_encoder/hidden_0/kernel/ApplyAdamF^Adam_1/update_critic/q/q2_encoding/q2_encoder/hidden_1/bias/ApplyAdamH^Adam_1/update_critic/q/q2_encoding/q2_encoder/hidden_1/kernel/ApplyAdam;^Adam_1/update_critic/value/encoder/hidden_0/bias/ApplyAdam=^Adam_1/update_critic/value/encoder/hidden_0/kernel/ApplyAdam;^Adam_1/update_critic/value/encoder/hidden_1/bias/ApplyAdam=^Adam_1/update_critic/value/encoder/hidden_1/kernel/ApplyAdam:^Adam_1/update_critic/value/extrinsic_value/bias/ApplyAdam<^Adam_1/update_critic/value/extrinsic_value/kernel/ApplyAdam5^Adam_1/update_critic/value/gail_value/bias/ApplyAdam7^Adam_1/update_critic/value/gail_value/kernel/ApplyAdam*
T0*9
_class/
-+loc:@critic/q/q1_encoding/extrinsic_q1/bias
£
Adam_1/Assign_1Assignbeta2_power_1Adam_1/mul_1*
use_locking( *
T0*9
_class/
-+loc:@critic/q/q1_encoding/extrinsic_q1/bias*
validate_shape(
Ż
Adam_1NoOp^Adam^Adam_1/Assign^Adam_1/Assign_1?^Adam_1/update_critic/q/q1_encoding/extrinsic_q1/bias/ApplyAdamA^Adam_1/update_critic/q/q1_encoding/extrinsic_q1/kernel/ApplyAdam:^Adam_1/update_critic/q/q1_encoding/gail_q1/bias/ApplyAdam<^Adam_1/update_critic/q/q1_encoding/gail_q1/kernel/ApplyAdamF^Adam_1/update_critic/q/q1_encoding/q1_encoder/hidden_0/bias/ApplyAdamH^Adam_1/update_critic/q/q1_encoding/q1_encoder/hidden_0/kernel/ApplyAdamF^Adam_1/update_critic/q/q1_encoding/q1_encoder/hidden_1/bias/ApplyAdamH^Adam_1/update_critic/q/q1_encoding/q1_encoder/hidden_1/kernel/ApplyAdam?^Adam_1/update_critic/q/q2_encoding/extrinsic_q2/bias/ApplyAdamA^Adam_1/update_critic/q/q2_encoding/extrinsic_q2/kernel/ApplyAdam:^Adam_1/update_critic/q/q2_encoding/gail_q2/bias/ApplyAdam<^Adam_1/update_critic/q/q2_encoding/gail_q2/kernel/ApplyAdamF^Adam_1/update_critic/q/q2_encoding/q2_encoder/hidden_0/bias/ApplyAdamH^Adam_1/update_critic/q/q2_encoding/q2_encoder/hidden_0/kernel/ApplyAdamF^Adam_1/update_critic/q/q2_encoding/q2_encoder/hidden_1/bias/ApplyAdamH^Adam_1/update_critic/q/q2_encoding/q2_encoder/hidden_1/kernel/ApplyAdam;^Adam_1/update_critic/value/encoder/hidden_0/bias/ApplyAdam=^Adam_1/update_critic/value/encoder/hidden_0/kernel/ApplyAdam;^Adam_1/update_critic/value/encoder/hidden_1/bias/ApplyAdam=^Adam_1/update_critic/value/encoder/hidden_1/kernel/ApplyAdam:^Adam_1/update_critic/value/extrinsic_value/bias/ApplyAdam<^Adam_1/update_critic/value/extrinsic_value/kernel/ApplyAdam5^Adam_1/update_critic/value/gail_value/bias/ApplyAdam7^Adam_1/update_critic/value/gail_value/kernel/ApplyAdam
J
gradients_2/ShapeConst^Adam^Adam_1*
valueB *
dtype0
R
gradients_2/grad_ys_0Const^Adam^Adam_1*
valueB
 *  ?*
dtype0
]
gradients_2/FillFillgradients_2/Shapegradients_2/grad_ys_0*
T0*

index_type0
:
gradients_2/Neg_grad/NegNeggradients_2/Fill*
T0
d
&gradients_2/Mean_15_grad/Reshape/shapeConst^Adam^Adam_1*
valueB:*
dtype0

 gradients_2/Mean_15_grad/ReshapeReshapegradients_2/Neg_grad/Neg&gradients_2/Mean_15_grad/Reshape/shape*
T0*
Tshape0
X
gradients_2/Mean_15_grad/ShapeShapemul_23^Adam^Adam_1*
T0*
out_type0

gradients_2/Mean_15_grad/TileTile gradients_2/Mean_15_grad/Reshapegradients_2/Mean_15_grad/Shape*

Tmultiples0*
T0
Z
 gradients_2/Mean_15_grad/Shape_1Shapemul_23^Adam^Adam_1*
T0*
out_type0
Y
 gradients_2/Mean_15_grad/Shape_2Const^Adam^Adam_1*
valueB *
dtype0
\
gradients_2/Mean_15_grad/ConstConst^Adam^Adam_1*
valueB: *
dtype0

gradients_2/Mean_15_grad/ProdProd gradients_2/Mean_15_grad/Shape_1gradients_2/Mean_15_grad/Const*

Tidx0*
	keep_dims( *
T0
^
 gradients_2/Mean_15_grad/Const_1Const^Adam^Adam_1*
valueB: *
dtype0

gradients_2/Mean_15_grad/Prod_1Prod gradients_2/Mean_15_grad/Shape_2 gradients_2/Mean_15_grad/Const_1*

Tidx0*
	keep_dims( *
T0
\
"gradients_2/Mean_15_grad/Maximum/yConst^Adam^Adam_1*
value	B :*
dtype0
y
 gradients_2/Mean_15_grad/MaximumMaximumgradients_2/Mean_15_grad/Prod_1"gradients_2/Mean_15_grad/Maximum/y*
T0
w
!gradients_2/Mean_15_grad/floordivFloorDivgradients_2/Mean_15_grad/Prod gradients_2/Mean_15_grad/Maximum*
T0
p
gradients_2/Mean_15_grad/CastCast!gradients_2/Mean_15_grad/floordiv*

SrcT0*
Truncate( *

DstT0
r
 gradients_2/Mean_15_grad/truedivRealDivgradients_2/Mean_15_grad/Tilegradients_2/Mean_15_grad/Cast*
T0
Z
gradients_2/mul_23_grad/ShapeShape	ToFloat_4^Adam^Adam_1*
T0*
out_type0
Z
gradients_2/mul_23_grad/Shape_1ShapeMean_14^Adam^Adam_1*
T0*
out_type0

-gradients_2/mul_23_grad/BroadcastGradientArgsBroadcastGradientArgsgradients_2/mul_23_grad/Shapegradients_2/mul_23_grad/Shape_1*
T0
V
gradients_2/mul_23_grad/MulMul gradients_2/Mean_15_grad/truedivMean_14*
T0

gradients_2/mul_23_grad/SumSumgradients_2/mul_23_grad/Mul-gradients_2/mul_23_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0
}
gradients_2/mul_23_grad/ReshapeReshapegradients_2/mul_23_grad/Sumgradients_2/mul_23_grad/Shape*
T0*
Tshape0
Z
gradients_2/mul_23_grad/Mul_1Mul	ToFloat_4 gradients_2/Mean_15_grad/truediv*
T0

gradients_2/mul_23_grad/Sum_1Sumgradients_2/mul_23_grad/Mul_1/gradients_2/mul_23_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0

!gradients_2/mul_23_grad/Reshape_1Reshapegradients_2/mul_23_grad/Sum_1gradients_2/mul_23_grad/Shape_1*
T0*
Tshape0

(gradients_2/mul_23_grad/tuple/group_depsNoOp^Adam^Adam_1 ^gradients_2/mul_23_grad/Reshape"^gradients_2/mul_23_grad/Reshape_1
Å
0gradients_2/mul_23_grad/tuple/control_dependencyIdentitygradients_2/mul_23_grad/Reshape)^gradients_2/mul_23_grad/tuple/group_deps*
T0*2
_class(
&$loc:@gradients_2/mul_23_grad/Reshape
Ė
2gradients_2/mul_23_grad/tuple/control_dependency_1Identity!gradients_2/mul_23_grad/Reshape_1)^gradients_2/mul_23_grad/tuple/group_deps*
T0*4
_class*
(&loc:@gradients_2/mul_23_grad/Reshape_1
X
gradients_2/Mean_14_grad/ShapeShapemul_22^Adam^Adam_1*
T0*
out_type0

gradients_2/Mean_14_grad/SizeConst^Adam^Adam_1*1
_class'
%#loc:@gradients_2/Mean_14_grad/Shape*
value	B :*
dtype0

gradients_2/Mean_14_grad/addAddV2Mean_14/reduction_indicesgradients_2/Mean_14_grad/Size*
T0*1
_class'
%#loc:@gradients_2/Mean_14_grad/Shape
”
gradients_2/Mean_14_grad/modFloorModgradients_2/Mean_14_grad/addgradients_2/Mean_14_grad/Size*
T0*1
_class'
%#loc:@gradients_2/Mean_14_grad/Shape

 gradients_2/Mean_14_grad/Shape_1Const^Adam^Adam_1*1
_class'
%#loc:@gradients_2/Mean_14_grad/Shape*
valueB *
dtype0

$gradients_2/Mean_14_grad/range/startConst^Adam^Adam_1*1
_class'
%#loc:@gradients_2/Mean_14_grad/Shape*
value	B : *
dtype0

$gradients_2/Mean_14_grad/range/deltaConst^Adam^Adam_1*1
_class'
%#loc:@gradients_2/Mean_14_grad/Shape*
value	B :*
dtype0
Ń
gradients_2/Mean_14_grad/rangeRange$gradients_2/Mean_14_grad/range/startgradients_2/Mean_14_grad/Size$gradients_2/Mean_14_grad/range/delta*

Tidx0*1
_class'
%#loc:@gradients_2/Mean_14_grad/Shape

#gradients_2/Mean_14_grad/Fill/valueConst^Adam^Adam_1*1
_class'
%#loc:@gradients_2/Mean_14_grad/Shape*
value	B :*
dtype0
ŗ
gradients_2/Mean_14_grad/FillFill gradients_2/Mean_14_grad/Shape_1#gradients_2/Mean_14_grad/Fill/value*
T0*1
_class'
%#loc:@gradients_2/Mean_14_grad/Shape*

index_type0
ł
&gradients_2/Mean_14_grad/DynamicStitchDynamicStitchgradients_2/Mean_14_grad/rangegradients_2/Mean_14_grad/modgradients_2/Mean_14_grad/Shapegradients_2/Mean_14_grad/Fill*
T0*1
_class'
%#loc:@gradients_2/Mean_14_grad/Shape*
N

"gradients_2/Mean_14_grad/Maximum/yConst^Adam^Adam_1*1
_class'
%#loc:@gradients_2/Mean_14_grad/Shape*
value	B :*
dtype0
³
 gradients_2/Mean_14_grad/MaximumMaximum&gradients_2/Mean_14_grad/DynamicStitch"gradients_2/Mean_14_grad/Maximum/y*
T0*1
_class'
%#loc:@gradients_2/Mean_14_grad/Shape
«
!gradients_2/Mean_14_grad/floordivFloorDivgradients_2/Mean_14_grad/Shape gradients_2/Mean_14_grad/Maximum*
T0*1
_class'
%#loc:@gradients_2/Mean_14_grad/Shape

 gradients_2/Mean_14_grad/ReshapeReshape2gradients_2/mul_23_grad/tuple/control_dependency_1&gradients_2/Mean_14_grad/DynamicStitch*
T0*
Tshape0

gradients_2/Mean_14_grad/TileTile gradients_2/Mean_14_grad/Reshape!gradients_2/Mean_14_grad/floordiv*

Tmultiples0*
T0
Z
 gradients_2/Mean_14_grad/Shape_2Shapemul_22^Adam^Adam_1*
T0*
out_type0
[
 gradients_2/Mean_14_grad/Shape_3ShapeMean_14^Adam^Adam_1*
T0*
out_type0
\
gradients_2/Mean_14_grad/ConstConst^Adam^Adam_1*
valueB: *
dtype0

gradients_2/Mean_14_grad/ProdProd gradients_2/Mean_14_grad/Shape_2gradients_2/Mean_14_grad/Const*

Tidx0*
	keep_dims( *
T0
^
 gradients_2/Mean_14_grad/Const_1Const^Adam^Adam_1*
valueB: *
dtype0

gradients_2/Mean_14_grad/Prod_1Prod gradients_2/Mean_14_grad/Shape_3 gradients_2/Mean_14_grad/Const_1*

Tidx0*
	keep_dims( *
T0
^
$gradients_2/Mean_14_grad/Maximum_1/yConst^Adam^Adam_1*
value	B :*
dtype0
}
"gradients_2/Mean_14_grad/Maximum_1Maximumgradients_2/Mean_14_grad/Prod_1$gradients_2/Mean_14_grad/Maximum_1/y*
T0
{
#gradients_2/Mean_14_grad/floordiv_1FloorDivgradients_2/Mean_14_grad/Prod"gradients_2/Mean_14_grad/Maximum_1*
T0
r
gradients_2/Mean_14_grad/CastCast#gradients_2/Mean_14_grad/floordiv_1*

SrcT0*
Truncate( *

DstT0
r
 gradients_2/Mean_14_grad/truedivRealDivgradients_2/Mean_14_grad/Tilegradients_2/Mean_14_grad/Cast*
T0
b
gradients_2/mul_22_grad/ShapeShapelog_ent_coef/read^Adam^Adam_1*
T0*
out_type0
Z
gradients_2/mul_22_grad/Shape_1ShapeSqueeze^Adam^Adam_1*
T0*
out_type0

-gradients_2/mul_22_grad/BroadcastGradientArgsBroadcastGradientArgsgradients_2/mul_22_grad/Shapegradients_2/mul_22_grad/Shape_1*
T0
V
gradients_2/mul_22_grad/MulMul gradients_2/Mean_14_grad/truedivSqueeze*
T0

gradients_2/mul_22_grad/SumSumgradients_2/mul_22_grad/Mul-gradients_2/mul_22_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0
}
gradients_2/mul_22_grad/ReshapeReshapegradients_2/mul_22_grad/Sumgradients_2/mul_22_grad/Shape*
T0*
Tshape0
b
gradients_2/mul_22_grad/Mul_1Mullog_ent_coef/read gradients_2/Mean_14_grad/truediv*
T0

gradients_2/mul_22_grad/Sum_1Sumgradients_2/mul_22_grad/Mul_1/gradients_2/mul_22_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0

!gradients_2/mul_22_grad/Reshape_1Reshapegradients_2/mul_22_grad/Sum_1gradients_2/mul_22_grad/Shape_1*
T0*
Tshape0

(gradients_2/mul_22_grad/tuple/group_depsNoOp^Adam^Adam_1 ^gradients_2/mul_22_grad/Reshape"^gradients_2/mul_22_grad/Reshape_1
Å
0gradients_2/mul_22_grad/tuple/control_dependencyIdentitygradients_2/mul_22_grad/Reshape)^gradients_2/mul_22_grad/tuple/group_deps*
T0*2
_class(
&$loc:@gradients_2/mul_22_grad/Reshape
Ė
2gradients_2/mul_22_grad/tuple/control_dependency_1Identity!gradients_2/mul_22_grad/Reshape_1)^gradients_2/mul_22_grad/tuple/group_deps*
T0*4
_class*
(&loc:@gradients_2/mul_22_grad/Reshape_1
i
beta1_power_2/initial_valueConst*
_class
loc:@log_ent_coef*
valueB
 *fff?*
dtype0
z
beta1_power_2
VariableV2*
shape: *
shared_name *
_class
loc:@log_ent_coef*
dtype0*
	container 

beta1_power_2/AssignAssignbeta1_power_2beta1_power_2/initial_value*
use_locking(*
T0*
_class
loc:@log_ent_coef*
validate_shape(
W
beta1_power_2/readIdentitybeta1_power_2*
T0*
_class
loc:@log_ent_coef
i
beta2_power_2/initial_valueConst*
_class
loc:@log_ent_coef*
valueB
 *w¾?*
dtype0
z
beta2_power_2
VariableV2*
shape: *
shared_name *
_class
loc:@log_ent_coef*
dtype0*
	container 

beta2_power_2/AssignAssignbeta2_power_2beta2_power_2/initial_value*
use_locking(*
T0*
_class
loc:@log_ent_coef*
validate_shape(
W
beta2_power_2/readIdentitybeta2_power_2*
T0*
_class
loc:@log_ent_coef
u
#log_ent_coef/Adam/Initializer/zerosConst*
valueB*    *
_class
loc:@log_ent_coef*
dtype0

log_ent_coef/Adam
VariableV2*
shape:*
shared_name *
_class
loc:@log_ent_coef*
dtype0*
	container 
­
log_ent_coef/Adam/AssignAssignlog_ent_coef/Adam#log_ent_coef/Adam/Initializer/zeros*
use_locking(*
T0*
_class
loc:@log_ent_coef*
validate_shape(
_
log_ent_coef/Adam/readIdentitylog_ent_coef/Adam*
T0*
_class
loc:@log_ent_coef
w
%log_ent_coef/Adam_1/Initializer/zerosConst*
valueB*    *
_class
loc:@log_ent_coef*
dtype0

log_ent_coef/Adam_1
VariableV2*
shape:*
shared_name *
_class
loc:@log_ent_coef*
dtype0*
	container 
³
log_ent_coef/Adam_1/AssignAssignlog_ent_coef/Adam_1%log_ent_coef/Adam_1/Initializer/zeros*
use_locking(*
T0*
_class
loc:@log_ent_coef*
validate_shape(
c
log_ent_coef/Adam_1/readIdentitylog_ent_coef/Adam_1*
T0*
_class
loc:@log_ent_coef
I
Adam_2/beta1Const^Adam^Adam_1*
valueB
 *fff?*
dtype0
I
Adam_2/beta2Const^Adam^Adam_1*
valueB
 *w¾?*
dtype0
K
Adam_2/epsilonConst^Adam^Adam_1*
valueB
 *wĢ+2*
dtype0
Ļ
$Adam_2/update_log_ent_coef/ApplyAdam	ApplyAdamlog_ent_coeflog_ent_coef/Adamlog_ent_coef/Adam_1beta1_power_2/readbeta2_power_2/readVariable_2/readAdam_2/beta1Adam_2/beta2Adam_2/epsilon0gradients_2/mul_22_grad/tuple/control_dependency*
use_locking( *
T0*
_class
loc:@log_ent_coef*
use_nesterov( 


Adam_2/mulMulbeta1_power_2/readAdam_2/beta1%^Adam_2/update_log_ent_coef/ApplyAdam*
T0*
_class
loc:@log_ent_coef

Adam_2/AssignAssignbeta1_power_2
Adam_2/mul*
use_locking( *
T0*
_class
loc:@log_ent_coef*
validate_shape(

Adam_2/mul_1Mulbeta2_power_2/readAdam_2/beta2%^Adam_2/update_log_ent_coef/ApplyAdam*
T0*
_class
loc:@log_ent_coef

Adam_2/Assign_1Assignbeta2_power_2Adam_2/mul_1*
use_locking( *
T0*
_class
loc:@log_ent_coef*
validate_shape(
g
Adam_2NoOp^Adam^Adam_1^Adam_2/Assign^Adam_2/Assign_1%^Adam_2/update_log_ent_coef/ApplyAdam
A
PlaceholderPlaceholder*
shape:’’’’’’’’’*
dtype0
C
Placeholder_1Placeholder*
shape:’’’’’’’’’*
dtype0
C
ExpandDims_3/dimConst*
valueB :
’’’’’’’’’*
dtype0
N
ExpandDims_3
ExpandDimsPlaceholderExpandDims_3/dim*

Tdim0*
T0
C
ExpandDims_4/dimConst*
valueB :
’’’’’’’’’*
dtype0
P
ExpandDims_4
ExpandDimsPlaceholder_1ExpandDims_4/dim*

Tdim0*
T0
G
Placeholder_2Placeholder*
shape:’’’’’’’’’*
dtype0
K
strided_slice_36/stackConst*
valueB"        *
dtype0
M
strided_slice_36/stack_1Const*
valueB"       *
dtype0
M
strided_slice_36/stack_2Const*
valueB"      *
dtype0
õ
strided_slice_36StridedSlicePlaceholder_2strided_slice_36/stackstrided_slice_36/stack_1strided_slice_36/stack_2*
T0*
Index0*
shrink_axis_mask*

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
=
one_hot/on_valueConst*
valueB
 *  ?*
dtype0
>
one_hot/off_valueConst*
valueB
 *    *
dtype0
7
one_hot/depthConst*
value	B :*
dtype0

one_hotOneHotstrided_slice_36one_hot/depthone_hot/on_valueone_hot/off_value*
T0*
TI0*
axis’’’’’’’’’
K
strided_slice_37/stackConst*
valueB"       *
dtype0
M
strided_slice_37/stack_1Const*
valueB"       *
dtype0
M
strided_slice_37/stack_2Const*
valueB"      *
dtype0
õ
strided_slice_37StridedSlicePlaceholder_2strided_slice_37/stackstrided_slice_37/stack_1strided_slice_37/stack_2*
T0*
Index0*
shrink_axis_mask*

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
?
one_hot_1/on_valueConst*
valueB
 *  ?*
dtype0
@
one_hot_1/off_valueConst*
valueB
 *    *
dtype0
9
one_hot_1/depthConst*
value	B :*
dtype0

	one_hot_1OneHotstrided_slice_37one_hot_1/depthone_hot_1/on_valueone_hot_1/off_value*
T0*
TI0*
axis’’’’’’’’’
K
strided_slice_38/stackConst*
valueB"       *
dtype0
M
strided_slice_38/stack_1Const*
valueB"       *
dtype0
M
strided_slice_38/stack_2Const*
valueB"      *
dtype0
õ
strided_slice_38StridedSlicePlaceholder_2strided_slice_38/stackstrided_slice_38/stack_1strided_slice_38/stack_2*
T0*
Index0*
shrink_axis_mask*

begin_mask*
ellipsis_mask *
new_axis_mask *
end_mask
?
one_hot_2/on_valueConst*
valueB
 *  ?*
dtype0
@
one_hot_2/off_valueConst*
valueB
 *    *
dtype0
9
one_hot_2/depthConst*
value	B :*
dtype0

	one_hot_2OneHotstrided_slice_38one_hot_2/depthone_hot_2/on_valueone_hot_2/off_value*
T0*
TI0*
axis’’’’’’’’’
5
concat/axisConst*
value	B :*
dtype0
\
concatConcatV2one_hot	one_hot_1	one_hot_2concat/axis*

Tidx0*
T0*
N
H
Placeholder_3Placeholder*
shape:’’’’’’’’’ā*
dtype0
=
concat_1/concat_dimConst*
value	B :*
dtype0
3
concat_1/concatIdentityPlaceholder_3*
T0
=
concat_2/concat_dimConst*
value	B :*
dtype0
8
concat_2/concatIdentityvector_observation*
T0
@
GAIL_model/concat/axisConst*
value	B :*
dtype0
z
GAIL_model/concatConcatV2concat_1/concatconcatExpandDims_3GAIL_model/concat/axis*

Tidx0*
T0*
N
­
BGAIL_model/gail_d_hidden_1/kernel/Initializer/random_uniform/shapeConst*4
_class*
(&loc:@GAIL_model/gail_d_hidden_1/kernel*
valueB"k     *
dtype0
£
@GAIL_model/gail_d_hidden_1/kernel/Initializer/random_uniform/minConst*4
_class*
(&loc:@GAIL_model/gail_d_hidden_1/kernel*
valueB
 *Śdā½*
dtype0
£
@GAIL_model/gail_d_hidden_1/kernel/Initializer/random_uniform/maxConst*4
_class*
(&loc:@GAIL_model/gail_d_hidden_1/kernel*
valueB
 *Śdā=*
dtype0

JGAIL_model/gail_d_hidden_1/kernel/Initializer/random_uniform/RandomUniformRandomUniformBGAIL_model/gail_d_hidden_1/kernel/Initializer/random_uniform/shape*
seedĶ%*
T0*4
_class*
(&loc:@GAIL_model/gail_d_hidden_1/kernel*
dtype0*
seed2«

@GAIL_model/gail_d_hidden_1/kernel/Initializer/random_uniform/subSub@GAIL_model/gail_d_hidden_1/kernel/Initializer/random_uniform/max@GAIL_model/gail_d_hidden_1/kernel/Initializer/random_uniform/min*
T0*4
_class*
(&loc:@GAIL_model/gail_d_hidden_1/kernel

@GAIL_model/gail_d_hidden_1/kernel/Initializer/random_uniform/mulMulJGAIL_model/gail_d_hidden_1/kernel/Initializer/random_uniform/RandomUniform@GAIL_model/gail_d_hidden_1/kernel/Initializer/random_uniform/sub*
T0*4
_class*
(&loc:@GAIL_model/gail_d_hidden_1/kernel

<GAIL_model/gail_d_hidden_1/kernel/Initializer/random_uniformAdd@GAIL_model/gail_d_hidden_1/kernel/Initializer/random_uniform/mul@GAIL_model/gail_d_hidden_1/kernel/Initializer/random_uniform/min*
T0*4
_class*
(&loc:@GAIL_model/gail_d_hidden_1/kernel
­
!GAIL_model/gail_d_hidden_1/kernel
VariableV2*
shape:
ė*
shared_name *4
_class*
(&loc:@GAIL_model/gail_d_hidden_1/kernel*
dtype0*
	container 
ū
(GAIL_model/gail_d_hidden_1/kernel/AssignAssign!GAIL_model/gail_d_hidden_1/kernel<GAIL_model/gail_d_hidden_1/kernel/Initializer/random_uniform*
use_locking(*
T0*4
_class*
(&loc:@GAIL_model/gail_d_hidden_1/kernel*
validate_shape(

&GAIL_model/gail_d_hidden_1/kernel/readIdentity!GAIL_model/gail_d_hidden_1/kernel*
T0*4
_class*
(&loc:@GAIL_model/gail_d_hidden_1/kernel

1GAIL_model/gail_d_hidden_1/bias/Initializer/zerosConst*2
_class(
&$loc:@GAIL_model/gail_d_hidden_1/bias*
valueB*    *
dtype0
¤
GAIL_model/gail_d_hidden_1/bias
VariableV2*
shape:*
shared_name *2
_class(
&$loc:@GAIL_model/gail_d_hidden_1/bias*
dtype0*
	container 
ź
&GAIL_model/gail_d_hidden_1/bias/AssignAssignGAIL_model/gail_d_hidden_1/bias1GAIL_model/gail_d_hidden_1/bias/Initializer/zeros*
use_locking(*
T0*2
_class(
&$loc:@GAIL_model/gail_d_hidden_1/bias*
validate_shape(

$GAIL_model/gail_d_hidden_1/bias/readIdentityGAIL_model/gail_d_hidden_1/bias*
T0*2
_class(
&$loc:@GAIL_model/gail_d_hidden_1/bias

!GAIL_model/gail_d_hidden_1/MatMulMatMulGAIL_model/concat&GAIL_model/gail_d_hidden_1/kernel/read*
transpose_b( *
T0*
transpose_a( 

"GAIL_model/gail_d_hidden_1/BiasAddBiasAdd!GAIL_model/gail_d_hidden_1/MatMul$GAIL_model/gail_d_hidden_1/bias/read*
T0*
data_formatNHWC
Z
"GAIL_model/gail_d_hidden_1/SigmoidSigmoid"GAIL_model/gail_d_hidden_1/BiasAdd*
T0
v
GAIL_model/gail_d_hidden_1/MulMul"GAIL_model/gail_d_hidden_1/BiasAdd"GAIL_model/gail_d_hidden_1/Sigmoid*
T0
­
BGAIL_model/gail_d_hidden_2/kernel/Initializer/random_uniform/shapeConst*4
_class*
(&loc:@GAIL_model/gail_d_hidden_2/kernel*
valueB"      *
dtype0
£
@GAIL_model/gail_d_hidden_2/kernel/Initializer/random_uniform/minConst*4
_class*
(&loc:@GAIL_model/gail_d_hidden_2/kernel*
valueB
 *qÄ¾*
dtype0
£
@GAIL_model/gail_d_hidden_2/kernel/Initializer/random_uniform/maxConst*4
_class*
(&loc:@GAIL_model/gail_d_hidden_2/kernel*
valueB
 *qÄ>*
dtype0

JGAIL_model/gail_d_hidden_2/kernel/Initializer/random_uniform/RandomUniformRandomUniformBGAIL_model/gail_d_hidden_2/kernel/Initializer/random_uniform/shape*
seedĶ%*
T0*4
_class*
(&loc:@GAIL_model/gail_d_hidden_2/kernel*
dtype0*
seed2½

@GAIL_model/gail_d_hidden_2/kernel/Initializer/random_uniform/subSub@GAIL_model/gail_d_hidden_2/kernel/Initializer/random_uniform/max@GAIL_model/gail_d_hidden_2/kernel/Initializer/random_uniform/min*
T0*4
_class*
(&loc:@GAIL_model/gail_d_hidden_2/kernel

@GAIL_model/gail_d_hidden_2/kernel/Initializer/random_uniform/mulMulJGAIL_model/gail_d_hidden_2/kernel/Initializer/random_uniform/RandomUniform@GAIL_model/gail_d_hidden_2/kernel/Initializer/random_uniform/sub*
T0*4
_class*
(&loc:@GAIL_model/gail_d_hidden_2/kernel

<GAIL_model/gail_d_hidden_2/kernel/Initializer/random_uniformAdd@GAIL_model/gail_d_hidden_2/kernel/Initializer/random_uniform/mul@GAIL_model/gail_d_hidden_2/kernel/Initializer/random_uniform/min*
T0*4
_class*
(&loc:@GAIL_model/gail_d_hidden_2/kernel
­
!GAIL_model/gail_d_hidden_2/kernel
VariableV2*
shape:
*
shared_name *4
_class*
(&loc:@GAIL_model/gail_d_hidden_2/kernel*
dtype0*
	container 
ū
(GAIL_model/gail_d_hidden_2/kernel/AssignAssign!GAIL_model/gail_d_hidden_2/kernel<GAIL_model/gail_d_hidden_2/kernel/Initializer/random_uniform*
use_locking(*
T0*4
_class*
(&loc:@GAIL_model/gail_d_hidden_2/kernel*
validate_shape(

&GAIL_model/gail_d_hidden_2/kernel/readIdentity!GAIL_model/gail_d_hidden_2/kernel*
T0*4
_class*
(&loc:@GAIL_model/gail_d_hidden_2/kernel

1GAIL_model/gail_d_hidden_2/bias/Initializer/zerosConst*2
_class(
&$loc:@GAIL_model/gail_d_hidden_2/bias*
valueB*    *
dtype0
¤
GAIL_model/gail_d_hidden_2/bias
VariableV2*
shape:*
shared_name *2
_class(
&$loc:@GAIL_model/gail_d_hidden_2/bias*
dtype0*
	container 
ź
&GAIL_model/gail_d_hidden_2/bias/AssignAssignGAIL_model/gail_d_hidden_2/bias1GAIL_model/gail_d_hidden_2/bias/Initializer/zeros*
use_locking(*
T0*2
_class(
&$loc:@GAIL_model/gail_d_hidden_2/bias*
validate_shape(

$GAIL_model/gail_d_hidden_2/bias/readIdentityGAIL_model/gail_d_hidden_2/bias*
T0*2
_class(
&$loc:@GAIL_model/gail_d_hidden_2/bias
¢
!GAIL_model/gail_d_hidden_2/MatMulMatMulGAIL_model/gail_d_hidden_1/Mul&GAIL_model/gail_d_hidden_2/kernel/read*
transpose_b( *
T0*
transpose_a( 

"GAIL_model/gail_d_hidden_2/BiasAddBiasAdd!GAIL_model/gail_d_hidden_2/MatMul$GAIL_model/gail_d_hidden_2/bias/read*
T0*
data_formatNHWC
Z
"GAIL_model/gail_d_hidden_2/SigmoidSigmoid"GAIL_model/gail_d_hidden_2/BiasAdd*
T0
v
GAIL_model/gail_d_hidden_2/MulMul"GAIL_model/gail_d_hidden_2/BiasAdd"GAIL_model/gail_d_hidden_2/Sigmoid*
T0
­
BGAIL_model/gail_d_estimate/kernel/Initializer/random_uniform/shapeConst*4
_class*
(&loc:@GAIL_model/gail_d_estimate/kernel*
valueB"      *
dtype0
£
@GAIL_model/gail_d_estimate/kernel/Initializer/random_uniform/minConst*4
_class*
(&loc:@GAIL_model/gail_d_estimate/kernel*
valueB
 *n×\¾*
dtype0
£
@GAIL_model/gail_d_estimate/kernel/Initializer/random_uniform/maxConst*4
_class*
(&loc:@GAIL_model/gail_d_estimate/kernel*
valueB
 *n×\>*
dtype0

JGAIL_model/gail_d_estimate/kernel/Initializer/random_uniform/RandomUniformRandomUniformBGAIL_model/gail_d_estimate/kernel/Initializer/random_uniform/shape*
seedĶ%*
T0*4
_class*
(&loc:@GAIL_model/gail_d_estimate/kernel*
dtype0*
seed2Ļ

@GAIL_model/gail_d_estimate/kernel/Initializer/random_uniform/subSub@GAIL_model/gail_d_estimate/kernel/Initializer/random_uniform/max@GAIL_model/gail_d_estimate/kernel/Initializer/random_uniform/min*
T0*4
_class*
(&loc:@GAIL_model/gail_d_estimate/kernel

@GAIL_model/gail_d_estimate/kernel/Initializer/random_uniform/mulMulJGAIL_model/gail_d_estimate/kernel/Initializer/random_uniform/RandomUniform@GAIL_model/gail_d_estimate/kernel/Initializer/random_uniform/sub*
T0*4
_class*
(&loc:@GAIL_model/gail_d_estimate/kernel

<GAIL_model/gail_d_estimate/kernel/Initializer/random_uniformAdd@GAIL_model/gail_d_estimate/kernel/Initializer/random_uniform/mul@GAIL_model/gail_d_estimate/kernel/Initializer/random_uniform/min*
T0*4
_class*
(&loc:@GAIL_model/gail_d_estimate/kernel
¬
!GAIL_model/gail_d_estimate/kernel
VariableV2*
shape:	*
shared_name *4
_class*
(&loc:@GAIL_model/gail_d_estimate/kernel*
dtype0*
	container 
ū
(GAIL_model/gail_d_estimate/kernel/AssignAssign!GAIL_model/gail_d_estimate/kernel<GAIL_model/gail_d_estimate/kernel/Initializer/random_uniform*
use_locking(*
T0*4
_class*
(&loc:@GAIL_model/gail_d_estimate/kernel*
validate_shape(

&GAIL_model/gail_d_estimate/kernel/readIdentity!GAIL_model/gail_d_estimate/kernel*
T0*4
_class*
(&loc:@GAIL_model/gail_d_estimate/kernel

1GAIL_model/gail_d_estimate/bias/Initializer/zerosConst*2
_class(
&$loc:@GAIL_model/gail_d_estimate/bias*
valueB*    *
dtype0
£
GAIL_model/gail_d_estimate/bias
VariableV2*
shape:*
shared_name *2
_class(
&$loc:@GAIL_model/gail_d_estimate/bias*
dtype0*
	container 
ź
&GAIL_model/gail_d_estimate/bias/AssignAssignGAIL_model/gail_d_estimate/bias1GAIL_model/gail_d_estimate/bias/Initializer/zeros*
use_locking(*
T0*2
_class(
&$loc:@GAIL_model/gail_d_estimate/bias*
validate_shape(

$GAIL_model/gail_d_estimate/bias/readIdentityGAIL_model/gail_d_estimate/bias*
T0*2
_class(
&$loc:@GAIL_model/gail_d_estimate/bias
¢
!GAIL_model/gail_d_estimate/MatMulMatMulGAIL_model/gail_d_hidden_2/Mul&GAIL_model/gail_d_estimate/kernel/read*
transpose_b( *
T0*
transpose_a( 

"GAIL_model/gail_d_estimate/BiasAddBiasAdd!GAIL_model/gail_d_estimate/MatMul$GAIL_model/gail_d_estimate/bias/read*
T0*
data_formatNHWC
Z
"GAIL_model/gail_d_estimate/SigmoidSigmoid"GAIL_model/gail_d_estimate/BiasAdd*
T0
B
GAIL_model_1/concat/axisConst*
value	B :*
dtype0

GAIL_model_1/concatConcatV2concat_2/concatpolicy_1/StopGradientExpandDims_4GAIL_model_1/concat/axis*

Tidx0*
T0*
N

#GAIL_model_1/gail_d_hidden_1/MatMulMatMulGAIL_model_1/concat&GAIL_model/gail_d_hidden_1/kernel/read*
transpose_b( *
T0*
transpose_a( 

$GAIL_model_1/gail_d_hidden_1/BiasAddBiasAdd#GAIL_model_1/gail_d_hidden_1/MatMul$GAIL_model/gail_d_hidden_1/bias/read*
T0*
data_formatNHWC
^
$GAIL_model_1/gail_d_hidden_1/SigmoidSigmoid$GAIL_model_1/gail_d_hidden_1/BiasAdd*
T0
|
 GAIL_model_1/gail_d_hidden_1/MulMul$GAIL_model_1/gail_d_hidden_1/BiasAdd$GAIL_model_1/gail_d_hidden_1/Sigmoid*
T0
¦
#GAIL_model_1/gail_d_hidden_2/MatMulMatMul GAIL_model_1/gail_d_hidden_1/Mul&GAIL_model/gail_d_hidden_2/kernel/read*
transpose_b( *
T0*
transpose_a( 

$GAIL_model_1/gail_d_hidden_2/BiasAddBiasAdd#GAIL_model_1/gail_d_hidden_2/MatMul$GAIL_model/gail_d_hidden_2/bias/read*
T0*
data_formatNHWC
^
$GAIL_model_1/gail_d_hidden_2/SigmoidSigmoid$GAIL_model_1/gail_d_hidden_2/BiasAdd*
T0
|
 GAIL_model_1/gail_d_hidden_2/MulMul$GAIL_model_1/gail_d_hidden_2/BiasAdd$GAIL_model_1/gail_d_hidden_2/Sigmoid*
T0
¦
#GAIL_model_1/gail_d_estimate/MatMulMatMul GAIL_model_1/gail_d_hidden_2/Mul&GAIL_model/gail_d_estimate/kernel/read*
transpose_b( *
T0*
transpose_a( 

$GAIL_model_1/gail_d_estimate/BiasAddBiasAdd#GAIL_model_1/gail_d_estimate/MatMul$GAIL_model/gail_d_estimate/bias/read*
T0*
data_formatNHWC
^
$GAIL_model_1/gail_d_estimate/SigmoidSigmoid$GAIL_model_1/gail_d_estimate/BiasAdd*
T0
<
Const_8Const*
valueB"       *
dtype0
d
Mean_22Mean$GAIL_model_1/gail_d_estimate/SigmoidConst_8*

Tidx0*
	keep_dims( *
T0
<
Const_9Const*
valueB"       *
dtype0
b
Mean_23Mean"GAIL_model/gail_d_estimate/SigmoidConst_9*

Tidx0*
	keep_dims( *
T0
H
gail_reward/shapeConst*
valueB:
’’’’’’’’’*
dtype0
f
gail_rewardReshape$GAIL_model_1/gail_d_estimate/Sigmoidgail_reward/shape*
T0*
Tshape0
4
sub_7/xConst*
valueB
 *  ?*
dtype0
+
sub_7Subsub_7/xgail_reward*
T0
5
add_18/yConst*
valueB
 *æÖ3*
dtype0
)
add_18AddV2sub_7add_18/y*
T0

LogLogadd_18*
T0

Neg_1NegLog*
T0
=
Const_10Const*
valueB"       *
dtype0
c
Mean_24Mean"GAIL_model/gail_d_estimate/SigmoidConst_10*

Tidx0*
	keep_dims( *
T0
=
Const_11Const*
valueB"       *
dtype0
e
Mean_25Mean$GAIL_model_1/gail_d_estimate/SigmoidConst_11*

Tidx0*
	keep_dims( *
T0
5
add_19/yConst*
valueB
 *æÖ3*
dtype0
F
add_19AddV2"GAIL_model/gail_d_estimate/Sigmoidadd_19/y*
T0

Log_1Logadd_19*
T0
4
sub_8/xConst*
valueB
 *  ?*
dtype0
D
sub_8Subsub_8/x$GAIL_model_1/gail_d_estimate/Sigmoid*
T0
5
add_20/yConst*
valueB
 *æÖ3*
dtype0
)
add_20AddV2sub_8add_20/y*
T0

Log_2Logadd_20*
T0
&
add_21AddV2Log_1Log_2*
T0
=
Const_12Const*
valueB"       *
dtype0
G
Mean_26Meanadd_21Const_12*

Tidx0*
	keep_dims( *
T0

Neg_2NegMean_26*
T0
8
ShapeShapeconcat_1/concat*
T0*
out_type0
?
random_uniform/minConst*
valueB
 *    *
dtype0
?
random_uniform/maxConst*
valueB
 *  ?*
dtype0
e
random_uniform/RandomUniformRandomUniformShape*
seedĶ%*
T0*
dtype0*
seed2
J
random_uniform/subSubrandom_uniform/maxrandom_uniform/min*
T0
T
random_uniform/mulMulrandom_uniform/RandomUniformrandom_uniform/sub*
T0
F
random_uniformAddrandom_uniform/mulrandom_uniform/min*
T0
7
mul_52Mulrandom_uniformconcat_1/concat*
T0
4
sub_9/xConst*
valueB
 *  ?*
dtype0
.
sub_9Subsub_9/xrandom_uniform*
T0
.
mul_53Mulsub_9concat_2/concat*
T0
(
add_22AddV2mul_52mul_53*
T0
1
Shape_1Shapeconcat*
T0*
out_type0
A
random_uniform_1/minConst*
valueB
 *    *
dtype0
A
random_uniform_1/maxConst*
valueB
 *  ?*
dtype0
i
random_uniform_1/RandomUniformRandomUniformShape_1*
seedĶ%*
T0*
dtype0*
seed2
P
random_uniform_1/subSubrandom_uniform_1/maxrandom_uniform_1/min*
T0
Z
random_uniform_1/mulMulrandom_uniform_1/RandomUniformrandom_uniform_1/sub*
T0
L
random_uniform_1Addrandom_uniform_1/mulrandom_uniform_1/min*
T0
0
mul_54Mulrandom_uniform_1concat*
T0
5
sub_10/xConst*
valueB
 *  ?*
dtype0
2
sub_10Subsub_10/xrandom_uniform_1*
T0
5
mul_55Mulsub_10policy_1/StopGradient*
T0
(
add_23AddV2mul_54mul_55*
T0
7
Shape_2ShapeExpandDims_3*
T0*
out_type0
A
random_uniform_2/minConst*
valueB
 *    *
dtype0
A
random_uniform_2/maxConst*
valueB
 *  ?*
dtype0
i
random_uniform_2/RandomUniformRandomUniformShape_2*
seedĶ%*
T0*
dtype0*
seed2”
P
random_uniform_2/subSubrandom_uniform_2/maxrandom_uniform_2/min*
T0
Z
random_uniform_2/mulMulrandom_uniform_2/RandomUniformrandom_uniform_2/sub*
T0
L
random_uniform_2Addrandom_uniform_2/mulrandom_uniform_2/min*
T0
6
mul_56Mulrandom_uniform_2ExpandDims_3*
T0
5
sub_11/xConst*
valueB
 *  ?*
dtype0
2
sub_11Subsub_11/xrandom_uniform_2*
T0
,
mul_57Mulsub_11ExpandDims_4*
T0
(
add_24AddV2mul_56mul_57*
T0
B
GAIL_model_2/concat/axisConst*
value	B :*
dtype0
o
GAIL_model_2/concatConcatV2add_22add_23add_24GAIL_model_2/concat/axis*

Tidx0*
T0*
N

#GAIL_model_2/gail_d_hidden_1/MatMulMatMulGAIL_model_2/concat&GAIL_model/gail_d_hidden_1/kernel/read*
transpose_b( *
T0*
transpose_a( 

$GAIL_model_2/gail_d_hidden_1/BiasAddBiasAdd#GAIL_model_2/gail_d_hidden_1/MatMul$GAIL_model/gail_d_hidden_1/bias/read*
T0*
data_formatNHWC
^
$GAIL_model_2/gail_d_hidden_1/SigmoidSigmoid$GAIL_model_2/gail_d_hidden_1/BiasAdd*
T0
|
 GAIL_model_2/gail_d_hidden_1/MulMul$GAIL_model_2/gail_d_hidden_1/BiasAdd$GAIL_model_2/gail_d_hidden_1/Sigmoid*
T0
¦
#GAIL_model_2/gail_d_hidden_2/MatMulMatMul GAIL_model_2/gail_d_hidden_1/Mul&GAIL_model/gail_d_hidden_2/kernel/read*
transpose_b( *
T0*
transpose_a( 

$GAIL_model_2/gail_d_hidden_2/BiasAddBiasAdd#GAIL_model_2/gail_d_hidden_2/MatMul$GAIL_model/gail_d_hidden_2/bias/read*
T0*
data_formatNHWC
^
$GAIL_model_2/gail_d_hidden_2/SigmoidSigmoid$GAIL_model_2/gail_d_hidden_2/BiasAdd*
T0
|
 GAIL_model_2/gail_d_hidden_2/MulMul$GAIL_model_2/gail_d_hidden_2/BiasAdd$GAIL_model_2/gail_d_hidden_2/Sigmoid*
T0
¦
#GAIL_model_2/gail_d_estimate/MatMulMatMul GAIL_model_2/gail_d_hidden_2/Mul&GAIL_model/gail_d_estimate/kernel/read*
transpose_b( *
T0*
transpose_a( 

$GAIL_model_2/gail_d_estimate/BiasAddBiasAdd#GAIL_model_2/gail_d_estimate/MatMul$GAIL_model/gail_d_estimate/bias/read*
T0*
data_formatNHWC
^
$GAIL_model_2/gail_d_estimate/SigmoidSigmoid$GAIL_model_2/gail_d_estimate/BiasAdd*
T0
Y
gradients_3/ShapeShape$GAIL_model_2/gail_d_estimate/Sigmoid*
T0*
out_type0
B
gradients_3/grad_ys_0Const*
valueB
 *  ?*
dtype0
]
gradients_3/FillFillgradients_3/Shapegradients_3/grad_ys_0*
T0*

index_type0

Agradients_3/GAIL_model_2/gail_d_estimate/Sigmoid_grad/SigmoidGradSigmoidGrad$GAIL_model_2/gail_d_estimate/Sigmoidgradients_3/Fill*
T0
³
Agradients_3/GAIL_model_2/gail_d_estimate/BiasAdd_grad/BiasAddGradBiasAddGradAgradients_3/GAIL_model_2/gail_d_estimate/Sigmoid_grad/SigmoidGrad*
T0*
data_formatNHWC
ß
;gradients_3/GAIL_model_2/gail_d_estimate/MatMul_grad/MatMulMatMulAgradients_3/GAIL_model_2/gail_d_estimate/Sigmoid_grad/SigmoidGrad&GAIL_model/gail_d_estimate/kernel/read*
transpose_b(*
T0*
transpose_a( 
Ū
=gradients_3/GAIL_model_2/gail_d_estimate/MatMul_grad/MatMul_1MatMul GAIL_model_2/gail_d_hidden_2/MulAgradients_3/GAIL_model_2/gail_d_estimate/Sigmoid_grad/SigmoidGrad*
transpose_b( *
T0*
transpose_a(

7gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/ShapeShape$GAIL_model_2/gail_d_hidden_2/BiasAdd*
T0*
out_type0

9gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Shape_1Shape$GAIL_model_2/gail_d_hidden_2/Sigmoid*
T0*
out_type0
Ż
Ggradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/BroadcastGradientArgsBroadcastGradientArgs7gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Shape9gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Shape_1*
T0
Ø
5gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/MulMul;gradients_3/GAIL_model_2/gail_d_estimate/MatMul_grad/MatMul$GAIL_model_2/gail_d_hidden_2/Sigmoid*
T0
ā
5gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/SumSum5gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/MulGgradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0
Ė
9gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/ReshapeReshape5gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum7gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Shape*
T0*
Tshape0
Ŗ
7gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_1Mul$GAIL_model_2/gail_d_hidden_2/BiasAdd;gradients_3/GAIL_model_2/gail_d_estimate/MatMul_grad/MatMul*
T0
č
7gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1Sum7gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_1Igradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0
Ń
;gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Reshape_1Reshape7gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_19gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Shape_1*
T0*
Tshape0
¼
Agradients_3/GAIL_model_2/gail_d_hidden_2/Sigmoid_grad/SigmoidGradSigmoidGrad$GAIL_model_2/gail_d_hidden_2/Sigmoid;gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Reshape_1*
T0
ö
gradients_3/AddNAddN9gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/ReshapeAgradients_3/GAIL_model_2/gail_d_hidden_2/Sigmoid_grad/SigmoidGrad*
T0*L
_classB
@>loc:@gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Reshape*
N

Agradients_3/GAIL_model_2/gail_d_hidden_2/BiasAdd_grad/BiasAddGradBiasAddGradgradients_3/AddN*
T0*
data_formatNHWC
®
;gradients_3/GAIL_model_2/gail_d_hidden_2/MatMul_grad/MatMulMatMulgradients_3/AddN&GAIL_model/gail_d_hidden_2/kernel/read*
transpose_b(*
T0*
transpose_a( 
Ŗ
=gradients_3/GAIL_model_2/gail_d_hidden_2/MatMul_grad/MatMul_1MatMul GAIL_model_2/gail_d_hidden_1/Mulgradients_3/AddN*
transpose_b( *
T0*
transpose_a(

7gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/ShapeShape$GAIL_model_2/gail_d_hidden_1/BiasAdd*
T0*
out_type0

9gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Shape_1Shape$GAIL_model_2/gail_d_hidden_1/Sigmoid*
T0*
out_type0
Ż
Ggradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/BroadcastGradientArgsBroadcastGradientArgs7gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Shape9gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Shape_1*
T0
Ø
5gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/MulMul;gradients_3/GAIL_model_2/gail_d_hidden_2/MatMul_grad/MatMul$GAIL_model_2/gail_d_hidden_1/Sigmoid*
T0
ā
5gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/SumSum5gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/MulGgradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0
Ė
9gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/ReshapeReshape5gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum7gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Shape*
T0*
Tshape0
Ŗ
7gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_1Mul$GAIL_model_2/gail_d_hidden_1/BiasAdd;gradients_3/GAIL_model_2/gail_d_hidden_2/MatMul_grad/MatMul*
T0
č
7gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1Sum7gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_1Igradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0
Ń
;gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Reshape_1Reshape7gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_19gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Shape_1*
T0*
Tshape0
¼
Agradients_3/GAIL_model_2/gail_d_hidden_1/Sigmoid_grad/SigmoidGradSigmoidGrad$GAIL_model_2/gail_d_hidden_1/Sigmoid;gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Reshape_1*
T0
ų
gradients_3/AddN_1AddN9gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/ReshapeAgradients_3/GAIL_model_2/gail_d_hidden_1/Sigmoid_grad/SigmoidGrad*
T0*L
_classB
@>loc:@gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Reshape*
N

Agradients_3/GAIL_model_2/gail_d_hidden_1/BiasAdd_grad/BiasAddGradBiasAddGradgradients_3/AddN_1*
T0*
data_formatNHWC
°
;gradients_3/GAIL_model_2/gail_d_hidden_1/MatMul_grad/MatMulMatMulgradients_3/AddN_1&GAIL_model/gail_d_hidden_1/kernel/read*
transpose_b(*
T0*
transpose_a( 

=gradients_3/GAIL_model_2/gail_d_hidden_1/MatMul_grad/MatMul_1MatMulGAIL_model_2/concatgradients_3/AddN_1*
transpose_b( *
T0*
transpose_a(
2
pow/yConst*
valueB
 *   @*
dtype0
W
powPow;gradients_3/GAIL_model_2/gail_d_hidden_1/MatMul_grad/MatMulpow/y*
T0
K
Sum_33/reduction_indicesConst*
valueB :
’’’’’’’’’*
dtype0
R
Sum_33SumpowSum_33/reduction_indices*

Tidx0*
	keep_dims( *
T0
5
add_25/yConst*
valueB
 *æÖ3*
dtype0
*
add_25AddV2Sum_33add_25/y*
T0

SqrtSqrtadd_25*
T0
5
sub_12/yConst*
valueB
 *  ?*
dtype0
&
sub_12SubSqrtsub_12/y*
T0
4
Pow_1/yConst*
valueB
 *   @*
dtype0
&
Pow_1Powsub_12Pow_1/y*
T0
6
Const_13Const*
valueB: *
dtype0
F
Mean_27MeanPow_1Const_13*

Tidx0*
	keep_dims( *
T0
5
mul_58/xConst*
valueB
 *   A*
dtype0
)
mul_58Mulmul_58/xMean_27*
T0
'
add_26AddV2Neg_2mul_58*
T0
:
gradients_4/ShapeConst*
valueB *
dtype0
B
gradients_4/grad_ys_0Const*
valueB
 *  ?*
dtype0
]
gradients_4/FillFillgradients_4/Shapegradients_4/grad_ys_0*
T0*

index_type0
C
(gradients_4/add_26_grad/tuple/group_depsNoOp^gradients_4/Fill
§
0gradients_4/add_26_grad/tuple/control_dependencyIdentitygradients_4/Fill)^gradients_4/add_26_grad/tuple/group_deps*
T0*#
_class
loc:@gradients_4/Fill
©
2gradients_4/add_26_grad/tuple/control_dependency_1Identitygradients_4/Fill)^gradients_4/add_26_grad/tuple/group_deps*
T0*#
_class
loc:@gradients_4/Fill
\
gradients_4/Neg_2_grad/NegNeg0gradients_4/add_26_grad/tuple/control_dependency*
T0
h
gradients_4/mul_58_grad/MulMul2gradients_4/add_26_grad/tuple/control_dependency_1Mean_27*
T0
k
gradients_4/mul_58_grad/Mul_1Mul2gradients_4/add_26_grad/tuple/control_dependency_1mul_58/x*
T0
n
(gradients_4/mul_58_grad/tuple/group_depsNoOp^gradients_4/mul_58_grad/Mul^gradients_4/mul_58_grad/Mul_1
½
0gradients_4/mul_58_grad/tuple/control_dependencyIdentitygradients_4/mul_58_grad/Mul)^gradients_4/mul_58_grad/tuple/group_deps*
T0*.
_class$
" loc:@gradients_4/mul_58_grad/Mul
Ć
2gradients_4/mul_58_grad/tuple/control_dependency_1Identitygradients_4/mul_58_grad/Mul_1)^gradients_4/mul_58_grad/tuple/group_deps*
T0*0
_class&
$"loc:@gradients_4/mul_58_grad/Mul_1
[
&gradients_4/Mean_26_grad/Reshape/shapeConst*
valueB"      *
dtype0

 gradients_4/Mean_26_grad/ReshapeReshapegradients_4/Neg_2_grad/Neg&gradients_4/Mean_26_grad/Reshape/shape*
T0*
Tshape0
H
gradients_4/Mean_26_grad/ShapeShapeadd_21*
T0*
out_type0

gradients_4/Mean_26_grad/TileTile gradients_4/Mean_26_grad/Reshapegradients_4/Mean_26_grad/Shape*

Tmultiples0*
T0
J
 gradients_4/Mean_26_grad/Shape_1Shapeadd_21*
T0*
out_type0
I
 gradients_4/Mean_26_grad/Shape_2Const*
valueB *
dtype0
L
gradients_4/Mean_26_grad/ConstConst*
valueB: *
dtype0

gradients_4/Mean_26_grad/ProdProd gradients_4/Mean_26_grad/Shape_1gradients_4/Mean_26_grad/Const*

Tidx0*
	keep_dims( *
T0
N
 gradients_4/Mean_26_grad/Const_1Const*
valueB: *
dtype0

gradients_4/Mean_26_grad/Prod_1Prod gradients_4/Mean_26_grad/Shape_2 gradients_4/Mean_26_grad/Const_1*

Tidx0*
	keep_dims( *
T0
L
"gradients_4/Mean_26_grad/Maximum/yConst*
value	B :*
dtype0
y
 gradients_4/Mean_26_grad/MaximumMaximumgradients_4/Mean_26_grad/Prod_1"gradients_4/Mean_26_grad/Maximum/y*
T0
w
!gradients_4/Mean_26_grad/floordivFloorDivgradients_4/Mean_26_grad/Prod gradients_4/Mean_26_grad/Maximum*
T0
p
gradients_4/Mean_26_grad/CastCast!gradients_4/Mean_26_grad/floordiv*

SrcT0*
Truncate( *

DstT0
r
 gradients_4/Mean_26_grad/truedivRealDivgradients_4/Mean_26_grad/Tilegradients_4/Mean_26_grad/Cast*
T0
T
&gradients_4/Mean_27_grad/Reshape/shapeConst*
valueB:*
dtype0

 gradients_4/Mean_27_grad/ReshapeReshape2gradients_4/mul_58_grad/tuple/control_dependency_1&gradients_4/Mean_27_grad/Reshape/shape*
T0*
Tshape0
G
gradients_4/Mean_27_grad/ShapeShapePow_1*
T0*
out_type0

gradients_4/Mean_27_grad/TileTile gradients_4/Mean_27_grad/Reshapegradients_4/Mean_27_grad/Shape*

Tmultiples0*
T0
I
 gradients_4/Mean_27_grad/Shape_1ShapePow_1*
T0*
out_type0
I
 gradients_4/Mean_27_grad/Shape_2Const*
valueB *
dtype0
L
gradients_4/Mean_27_grad/ConstConst*
valueB: *
dtype0

gradients_4/Mean_27_grad/ProdProd gradients_4/Mean_27_grad/Shape_1gradients_4/Mean_27_grad/Const*

Tidx0*
	keep_dims( *
T0
N
 gradients_4/Mean_27_grad/Const_1Const*
valueB: *
dtype0

gradients_4/Mean_27_grad/Prod_1Prod gradients_4/Mean_27_grad/Shape_2 gradients_4/Mean_27_grad/Const_1*

Tidx0*
	keep_dims( *
T0
L
"gradients_4/Mean_27_grad/Maximum/yConst*
value	B :*
dtype0
y
 gradients_4/Mean_27_grad/MaximumMaximumgradients_4/Mean_27_grad/Prod_1"gradients_4/Mean_27_grad/Maximum/y*
T0
w
!gradients_4/Mean_27_grad/floordivFloorDivgradients_4/Mean_27_grad/Prod gradients_4/Mean_27_grad/Maximum*
T0
p
gradients_4/Mean_27_grad/CastCast!gradients_4/Mean_27_grad/floordiv*

SrcT0*
Truncate( *

DstT0
r
 gradients_4/Mean_27_grad/truedivRealDivgradients_4/Mean_27_grad/Tilegradients_4/Mean_27_grad/Cast*
T0
F
gradients_4/add_21_grad/ShapeShapeLog_1*
T0*
out_type0
H
gradients_4/add_21_grad/Shape_1ShapeLog_2*
T0*
out_type0

-gradients_4/add_21_grad/BroadcastGradientArgsBroadcastGradientArgsgradients_4/add_21_grad/Shapegradients_4/add_21_grad/Shape_1*
T0

gradients_4/add_21_grad/SumSum gradients_4/Mean_26_grad/truediv-gradients_4/add_21_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0
}
gradients_4/add_21_grad/ReshapeReshapegradients_4/add_21_grad/Sumgradients_4/add_21_grad/Shape*
T0*
Tshape0

gradients_4/add_21_grad/Sum_1Sum gradients_4/Mean_26_grad/truediv/gradients_4/add_21_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0

!gradients_4/add_21_grad/Reshape_1Reshapegradients_4/add_21_grad/Sum_1gradients_4/add_21_grad/Shape_1*
T0*
Tshape0
v
(gradients_4/add_21_grad/tuple/group_depsNoOp ^gradients_4/add_21_grad/Reshape"^gradients_4/add_21_grad/Reshape_1
Å
0gradients_4/add_21_grad/tuple/control_dependencyIdentitygradients_4/add_21_grad/Reshape)^gradients_4/add_21_grad/tuple/group_deps*
T0*2
_class(
&$loc:@gradients_4/add_21_grad/Reshape
Ė
2gradients_4/add_21_grad/tuple/control_dependency_1Identity!gradients_4/add_21_grad/Reshape_1)^gradients_4/add_21_grad/tuple/group_deps*
T0*4
_class*
(&loc:@gradients_4/add_21_grad/Reshape_1
F
gradients_4/Pow_1_grad/ShapeShapesub_12*
T0*
out_type0
I
gradients_4/Pow_1_grad/Shape_1ShapePow_1/y*
T0*
out_type0

,gradients_4/Pow_1_grad/BroadcastGradientArgsBroadcastGradientArgsgradients_4/Pow_1_grad/Shapegradients_4/Pow_1_grad/Shape_1*
T0
U
gradients_4/Pow_1_grad/mulMul gradients_4/Mean_27_grad/truedivPow_1/y*
T0
I
gradients_4/Pow_1_grad/sub/yConst*
valueB
 *  ?*
dtype0
Q
gradients_4/Pow_1_grad/subSubPow_1/ygradients_4/Pow_1_grad/sub/y*
T0
N
gradients_4/Pow_1_grad/PowPowsub_12gradients_4/Pow_1_grad/sub*
T0
d
gradients_4/Pow_1_grad/mul_1Mulgradients_4/Pow_1_grad/mulgradients_4/Pow_1_grad/Pow*
T0

gradients_4/Pow_1_grad/SumSumgradients_4/Pow_1_grad/mul_1,gradients_4/Pow_1_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0
z
gradients_4/Pow_1_grad/ReshapeReshapegradients_4/Pow_1_grad/Sumgradients_4/Pow_1_grad/Shape*
T0*
Tshape0
M
 gradients_4/Pow_1_grad/Greater/yConst*
valueB
 *    *
dtype0
\
gradients_4/Pow_1_grad/GreaterGreatersub_12 gradients_4/Pow_1_grad/Greater/y*
T0
P
&gradients_4/Pow_1_grad/ones_like/ShapeShapesub_12*
T0*
out_type0
S
&gradients_4/Pow_1_grad/ones_like/ConstConst*
valueB
 *  ?*
dtype0

 gradients_4/Pow_1_grad/ones_likeFill&gradients_4/Pow_1_grad/ones_like/Shape&gradients_4/Pow_1_grad/ones_like/Const*
T0*

index_type0
z
gradients_4/Pow_1_grad/SelectSelectgradients_4/Pow_1_grad/Greatersub_12 gradients_4/Pow_1_grad/ones_like*
T0
I
gradients_4/Pow_1_grad/LogLoggradients_4/Pow_1_grad/Select*
T0
?
!gradients_4/Pow_1_grad/zeros_like	ZerosLikesub_12*
T0

gradients_4/Pow_1_grad/Select_1Selectgradients_4/Pow_1_grad/Greatergradients_4/Pow_1_grad/Log!gradients_4/Pow_1_grad/zeros_like*
T0
U
gradients_4/Pow_1_grad/mul_2Mul gradients_4/Mean_27_grad/truedivPow_1*
T0
k
gradients_4/Pow_1_grad/mul_3Mulgradients_4/Pow_1_grad/mul_2gradients_4/Pow_1_grad/Select_1*
T0

gradients_4/Pow_1_grad/Sum_1Sumgradients_4/Pow_1_grad/mul_3.gradients_4/Pow_1_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0

 gradients_4/Pow_1_grad/Reshape_1Reshapegradients_4/Pow_1_grad/Sum_1gradients_4/Pow_1_grad/Shape_1*
T0*
Tshape0
s
'gradients_4/Pow_1_grad/tuple/group_depsNoOp^gradients_4/Pow_1_grad/Reshape!^gradients_4/Pow_1_grad/Reshape_1
Į
/gradients_4/Pow_1_grad/tuple/control_dependencyIdentitygradients_4/Pow_1_grad/Reshape(^gradients_4/Pow_1_grad/tuple/group_deps*
T0*1
_class'
%#loc:@gradients_4/Pow_1_grad/Reshape
Ē
1gradients_4/Pow_1_grad/tuple/control_dependency_1Identity gradients_4/Pow_1_grad/Reshape_1(^gradients_4/Pow_1_grad/tuple/group_deps*
T0*3
_class)
'%loc:@gradients_4/Pow_1_grad/Reshape_1
s
!gradients_4/Log_1_grad/Reciprocal
Reciprocaladd_191^gradients_4/add_21_grad/tuple/control_dependency*
T0

gradients_4/Log_1_grad/mulMul0gradients_4/add_21_grad/tuple/control_dependency!gradients_4/Log_1_grad/Reciprocal*
T0
u
!gradients_4/Log_2_grad/Reciprocal
Reciprocaladd_203^gradients_4/add_21_grad/tuple/control_dependency_1*
T0

gradients_4/Log_2_grad/mulMul2gradients_4/add_21_grad/tuple/control_dependency_1!gradients_4/Log_2_grad/Reciprocal*
T0
E
gradients_4/sub_12_grad/ShapeShapeSqrt*
T0*
out_type0
K
gradients_4/sub_12_grad/Shape_1Shapesub_12/y*
T0*
out_type0

-gradients_4/sub_12_grad/BroadcastGradientArgsBroadcastGradientArgsgradients_4/sub_12_grad/Shapegradients_4/sub_12_grad/Shape_1*
T0
Ø
gradients_4/sub_12_grad/SumSum/gradients_4/Pow_1_grad/tuple/control_dependency-gradients_4/sub_12_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0
}
gradients_4/sub_12_grad/ReshapeReshapegradients_4/sub_12_grad/Sumgradients_4/sub_12_grad/Shape*
T0*
Tshape0
\
gradients_4/sub_12_grad/NegNeg/gradients_4/Pow_1_grad/tuple/control_dependency*
T0

gradients_4/sub_12_grad/Sum_1Sumgradients_4/sub_12_grad/Neg/gradients_4/sub_12_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0

!gradients_4/sub_12_grad/Reshape_1Reshapegradients_4/sub_12_grad/Sum_1gradients_4/sub_12_grad/Shape_1*
T0*
Tshape0
v
(gradients_4/sub_12_grad/tuple/group_depsNoOp ^gradients_4/sub_12_grad/Reshape"^gradients_4/sub_12_grad/Reshape_1
Å
0gradients_4/sub_12_grad/tuple/control_dependencyIdentitygradients_4/sub_12_grad/Reshape)^gradients_4/sub_12_grad/tuple/group_deps*
T0*2
_class(
&$loc:@gradients_4/sub_12_grad/Reshape
Ė
2gradients_4/sub_12_grad/tuple/control_dependency_1Identity!gradients_4/sub_12_grad/Reshape_1)^gradients_4/sub_12_grad/tuple/group_deps*
T0*4
_class*
(&loc:@gradients_4/sub_12_grad/Reshape_1
c
gradients_4/add_19_grad/ShapeShape"GAIL_model/gail_d_estimate/Sigmoid*
T0*
out_type0
K
gradients_4/add_19_grad/Shape_1Shapeadd_19/y*
T0*
out_type0

-gradients_4/add_19_grad/BroadcastGradientArgsBroadcastGradientArgsgradients_4/add_19_grad/Shapegradients_4/add_19_grad/Shape_1*
T0

gradients_4/add_19_grad/SumSumgradients_4/Log_1_grad/mul-gradients_4/add_19_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0
}
gradients_4/add_19_grad/ReshapeReshapegradients_4/add_19_grad/Sumgradients_4/add_19_grad/Shape*
T0*
Tshape0

gradients_4/add_19_grad/Sum_1Sumgradients_4/Log_1_grad/mul/gradients_4/add_19_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0

!gradients_4/add_19_grad/Reshape_1Reshapegradients_4/add_19_grad/Sum_1gradients_4/add_19_grad/Shape_1*
T0*
Tshape0
v
(gradients_4/add_19_grad/tuple/group_depsNoOp ^gradients_4/add_19_grad/Reshape"^gradients_4/add_19_grad/Reshape_1
Å
0gradients_4/add_19_grad/tuple/control_dependencyIdentitygradients_4/add_19_grad/Reshape)^gradients_4/add_19_grad/tuple/group_deps*
T0*2
_class(
&$loc:@gradients_4/add_19_grad/Reshape
Ė
2gradients_4/add_19_grad/tuple/control_dependency_1Identity!gradients_4/add_19_grad/Reshape_1)^gradients_4/add_19_grad/tuple/group_deps*
T0*4
_class*
(&loc:@gradients_4/add_19_grad/Reshape_1
F
gradients_4/add_20_grad/ShapeShapesub_8*
T0*
out_type0
K
gradients_4/add_20_grad/Shape_1Shapeadd_20/y*
T0*
out_type0

-gradients_4/add_20_grad/BroadcastGradientArgsBroadcastGradientArgsgradients_4/add_20_grad/Shapegradients_4/add_20_grad/Shape_1*
T0

gradients_4/add_20_grad/SumSumgradients_4/Log_2_grad/mul-gradients_4/add_20_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0
}
gradients_4/add_20_grad/ReshapeReshapegradients_4/add_20_grad/Sumgradients_4/add_20_grad/Shape*
T0*
Tshape0

gradients_4/add_20_grad/Sum_1Sumgradients_4/Log_2_grad/mul/gradients_4/add_20_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0

!gradients_4/add_20_grad/Reshape_1Reshapegradients_4/add_20_grad/Sum_1gradients_4/add_20_grad/Shape_1*
T0*
Tshape0
v
(gradients_4/add_20_grad/tuple/group_depsNoOp ^gradients_4/add_20_grad/Reshape"^gradients_4/add_20_grad/Reshape_1
Å
0gradients_4/add_20_grad/tuple/control_dependencyIdentitygradients_4/add_20_grad/Reshape)^gradients_4/add_20_grad/tuple/group_deps*
T0*2
_class(
&$loc:@gradients_4/add_20_grad/Reshape
Ė
2gradients_4/add_20_grad/tuple/control_dependency_1Identity!gradients_4/add_20_grad/Reshape_1)^gradients_4/add_20_grad/tuple/group_deps*
T0*4
_class*
(&loc:@gradients_4/add_20_grad/Reshape_1
k
gradients_4/Sqrt_grad/SqrtGradSqrtGradSqrt0gradients_4/sub_12_grad/tuple/control_dependency*
T0
­
?gradients_4/GAIL_model/gail_d_estimate/Sigmoid_grad/SigmoidGradSigmoidGrad"GAIL_model/gail_d_estimate/Sigmoid0gradients_4/add_19_grad/tuple/control_dependency*
T0
G
gradients_4/sub_8_grad/ShapeShapesub_8/x*
T0*
out_type0
f
gradients_4/sub_8_grad/Shape_1Shape$GAIL_model_1/gail_d_estimate/Sigmoid*
T0*
out_type0

,gradients_4/sub_8_grad/BroadcastGradientArgsBroadcastGradientArgsgradients_4/sub_8_grad/Shapegradients_4/sub_8_grad/Shape_1*
T0
§
gradients_4/sub_8_grad/SumSum0gradients_4/add_20_grad/tuple/control_dependency,gradients_4/sub_8_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0
z
gradients_4/sub_8_grad/ReshapeReshapegradients_4/sub_8_grad/Sumgradients_4/sub_8_grad/Shape*
T0*
Tshape0
\
gradients_4/sub_8_grad/NegNeg0gradients_4/add_20_grad/tuple/control_dependency*
T0

gradients_4/sub_8_grad/Sum_1Sumgradients_4/sub_8_grad/Neg.gradients_4/sub_8_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0

 gradients_4/sub_8_grad/Reshape_1Reshapegradients_4/sub_8_grad/Sum_1gradients_4/sub_8_grad/Shape_1*
T0*
Tshape0
s
'gradients_4/sub_8_grad/tuple/group_depsNoOp^gradients_4/sub_8_grad/Reshape!^gradients_4/sub_8_grad/Reshape_1
Į
/gradients_4/sub_8_grad/tuple/control_dependencyIdentitygradients_4/sub_8_grad/Reshape(^gradients_4/sub_8_grad/tuple/group_deps*
T0*1
_class'
%#loc:@gradients_4/sub_8_grad/Reshape
Ē
1gradients_4/sub_8_grad/tuple/control_dependency_1Identity gradients_4/sub_8_grad/Reshape_1(^gradients_4/sub_8_grad/tuple/group_deps*
T0*3
_class)
'%loc:@gradients_4/sub_8_grad/Reshape_1
G
gradients_4/add_25_grad/ShapeShapeSum_33*
T0*
out_type0
K
gradients_4/add_25_grad/Shape_1Shapeadd_25/y*
T0*
out_type0

-gradients_4/add_25_grad/BroadcastGradientArgsBroadcastGradientArgsgradients_4/add_25_grad/Shapegradients_4/add_25_grad/Shape_1*
T0

gradients_4/add_25_grad/SumSumgradients_4/Sqrt_grad/SqrtGrad-gradients_4/add_25_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0
}
gradients_4/add_25_grad/ReshapeReshapegradients_4/add_25_grad/Sumgradients_4/add_25_grad/Shape*
T0*
Tshape0

gradients_4/add_25_grad/Sum_1Sumgradients_4/Sqrt_grad/SqrtGrad/gradients_4/add_25_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0

!gradients_4/add_25_grad/Reshape_1Reshapegradients_4/add_25_grad/Sum_1gradients_4/add_25_grad/Shape_1*
T0*
Tshape0
v
(gradients_4/add_25_grad/tuple/group_depsNoOp ^gradients_4/add_25_grad/Reshape"^gradients_4/add_25_grad/Reshape_1
Å
0gradients_4/add_25_grad/tuple/control_dependencyIdentitygradients_4/add_25_grad/Reshape)^gradients_4/add_25_grad/tuple/group_deps*
T0*2
_class(
&$loc:@gradients_4/add_25_grad/Reshape
Ė
2gradients_4/add_25_grad/tuple/control_dependency_1Identity!gradients_4/add_25_grad/Reshape_1)^gradients_4/add_25_grad/tuple/group_deps*
T0*4
_class*
(&loc:@gradients_4/add_25_grad/Reshape_1
Æ
?gradients_4/GAIL_model/gail_d_estimate/BiasAdd_grad/BiasAddGradBiasAddGrad?gradients_4/GAIL_model/gail_d_estimate/Sigmoid_grad/SigmoidGrad*
T0*
data_formatNHWC
Š
Dgradients_4/GAIL_model/gail_d_estimate/BiasAdd_grad/tuple/group_depsNoOp@^gradients_4/GAIL_model/gail_d_estimate/BiasAdd_grad/BiasAddGrad@^gradients_4/GAIL_model/gail_d_estimate/Sigmoid_grad/SigmoidGrad
½
Lgradients_4/GAIL_model/gail_d_estimate/BiasAdd_grad/tuple/control_dependencyIdentity?gradients_4/GAIL_model/gail_d_estimate/Sigmoid_grad/SigmoidGradE^gradients_4/GAIL_model/gail_d_estimate/BiasAdd_grad/tuple/group_deps*
T0*R
_classH
FDloc:@gradients_4/GAIL_model/gail_d_estimate/Sigmoid_grad/SigmoidGrad
æ
Ngradients_4/GAIL_model/gail_d_estimate/BiasAdd_grad/tuple/control_dependency_1Identity?gradients_4/GAIL_model/gail_d_estimate/BiasAdd_grad/BiasAddGradE^gradients_4/GAIL_model/gail_d_estimate/BiasAdd_grad/tuple/group_deps*
T0*R
_classH
FDloc:@gradients_4/GAIL_model/gail_d_estimate/BiasAdd_grad/BiasAddGrad
²
Agradients_4/GAIL_model_1/gail_d_estimate/Sigmoid_grad/SigmoidGradSigmoidGrad$GAIL_model_1/gail_d_estimate/Sigmoid1gradients_4/sub_8_grad/tuple/control_dependency_1*
T0
D
gradients_4/Sum_33_grad/ShapeShapepow*
T0*
out_type0
x
gradients_4/Sum_33_grad/SizeConst*0
_class&
$"loc:@gradients_4/Sum_33_grad/Shape*
value	B :*
dtype0

gradients_4/Sum_33_grad/addAddV2Sum_33/reduction_indicesgradients_4/Sum_33_grad/Size*
T0*0
_class&
$"loc:@gradients_4/Sum_33_grad/Shape

gradients_4/Sum_33_grad/modFloorModgradients_4/Sum_33_grad/addgradients_4/Sum_33_grad/Size*
T0*0
_class&
$"loc:@gradients_4/Sum_33_grad/Shape
z
gradients_4/Sum_33_grad/Shape_1Const*0
_class&
$"loc:@gradients_4/Sum_33_grad/Shape*
valueB *
dtype0

#gradients_4/Sum_33_grad/range/startConst*0
_class&
$"loc:@gradients_4/Sum_33_grad/Shape*
value	B : *
dtype0

#gradients_4/Sum_33_grad/range/deltaConst*0
_class&
$"loc:@gradients_4/Sum_33_grad/Shape*
value	B :*
dtype0
Ģ
gradients_4/Sum_33_grad/rangeRange#gradients_4/Sum_33_grad/range/startgradients_4/Sum_33_grad/Size#gradients_4/Sum_33_grad/range/delta*

Tidx0*0
_class&
$"loc:@gradients_4/Sum_33_grad/Shape
~
"gradients_4/Sum_33_grad/Fill/valueConst*0
_class&
$"loc:@gradients_4/Sum_33_grad/Shape*
value	B :*
dtype0
¶
gradients_4/Sum_33_grad/FillFillgradients_4/Sum_33_grad/Shape_1"gradients_4/Sum_33_grad/Fill/value*
T0*0
_class&
$"loc:@gradients_4/Sum_33_grad/Shape*

index_type0
ó
%gradients_4/Sum_33_grad/DynamicStitchDynamicStitchgradients_4/Sum_33_grad/rangegradients_4/Sum_33_grad/modgradients_4/Sum_33_grad/Shapegradients_4/Sum_33_grad/Fill*
T0*0
_class&
$"loc:@gradients_4/Sum_33_grad/Shape*
N
}
!gradients_4/Sum_33_grad/Maximum/yConst*0
_class&
$"loc:@gradients_4/Sum_33_grad/Shape*
value	B :*
dtype0
Æ
gradients_4/Sum_33_grad/MaximumMaximum%gradients_4/Sum_33_grad/DynamicStitch!gradients_4/Sum_33_grad/Maximum/y*
T0*0
_class&
$"loc:@gradients_4/Sum_33_grad/Shape
§
 gradients_4/Sum_33_grad/floordivFloorDivgradients_4/Sum_33_grad/Shapegradients_4/Sum_33_grad/Maximum*
T0*0
_class&
$"loc:@gradients_4/Sum_33_grad/Shape

gradients_4/Sum_33_grad/ReshapeReshape0gradients_4/add_25_grad/tuple/control_dependency%gradients_4/Sum_33_grad/DynamicStitch*
T0*
Tshape0

gradients_4/Sum_33_grad/TileTilegradients_4/Sum_33_grad/Reshape gradients_4/Sum_33_grad/floordiv*

Tmultiples0*
T0
č
9gradients_4/GAIL_model/gail_d_estimate/MatMul_grad/MatMulMatMulLgradients_4/GAIL_model/gail_d_estimate/BiasAdd_grad/tuple/control_dependency&GAIL_model/gail_d_estimate/kernel/read*
transpose_b(*
T0*
transpose_a( 
ā
;gradients_4/GAIL_model/gail_d_estimate/MatMul_grad/MatMul_1MatMulGAIL_model/gail_d_hidden_2/MulLgradients_4/GAIL_model/gail_d_estimate/BiasAdd_grad/tuple/control_dependency*
transpose_b( *
T0*
transpose_a(
Å
Cgradients_4/GAIL_model/gail_d_estimate/MatMul_grad/tuple/group_depsNoOp:^gradients_4/GAIL_model/gail_d_estimate/MatMul_grad/MatMul<^gradients_4/GAIL_model/gail_d_estimate/MatMul_grad/MatMul_1
Æ
Kgradients_4/GAIL_model/gail_d_estimate/MatMul_grad/tuple/control_dependencyIdentity9gradients_4/GAIL_model/gail_d_estimate/MatMul_grad/MatMulD^gradients_4/GAIL_model/gail_d_estimate/MatMul_grad/tuple/group_deps*
T0*L
_classB
@>loc:@gradients_4/GAIL_model/gail_d_estimate/MatMul_grad/MatMul
µ
Mgradients_4/GAIL_model/gail_d_estimate/MatMul_grad/tuple/control_dependency_1Identity;gradients_4/GAIL_model/gail_d_estimate/MatMul_grad/MatMul_1D^gradients_4/GAIL_model/gail_d_estimate/MatMul_grad/tuple/group_deps*
T0*N
_classD
B@loc:@gradients_4/GAIL_model/gail_d_estimate/MatMul_grad/MatMul_1
³
Agradients_4/GAIL_model_1/gail_d_estimate/BiasAdd_grad/BiasAddGradBiasAddGradAgradients_4/GAIL_model_1/gail_d_estimate/Sigmoid_grad/SigmoidGrad*
T0*
data_formatNHWC
Ö
Fgradients_4/GAIL_model_1/gail_d_estimate/BiasAdd_grad/tuple/group_depsNoOpB^gradients_4/GAIL_model_1/gail_d_estimate/BiasAdd_grad/BiasAddGradB^gradients_4/GAIL_model_1/gail_d_estimate/Sigmoid_grad/SigmoidGrad
Å
Ngradients_4/GAIL_model_1/gail_d_estimate/BiasAdd_grad/tuple/control_dependencyIdentityAgradients_4/GAIL_model_1/gail_d_estimate/Sigmoid_grad/SigmoidGradG^gradients_4/GAIL_model_1/gail_d_estimate/BiasAdd_grad/tuple/group_deps*
T0*T
_classJ
HFloc:@gradients_4/GAIL_model_1/gail_d_estimate/Sigmoid_grad/SigmoidGrad
Ē
Pgradients_4/GAIL_model_1/gail_d_estimate/BiasAdd_grad/tuple/control_dependency_1IdentityAgradients_4/GAIL_model_1/gail_d_estimate/BiasAdd_grad/BiasAddGradG^gradients_4/GAIL_model_1/gail_d_estimate/BiasAdd_grad/tuple/group_deps*
T0*T
_classJ
HFloc:@gradients_4/GAIL_model_1/gail_d_estimate/BiasAdd_grad/BiasAddGrad
y
gradients_4/pow_grad/ShapeShape;gradients_3/GAIL_model_2/gail_d_hidden_1/MatMul_grad/MatMul*
T0*
out_type0
E
gradients_4/pow_grad/Shape_1Shapepow/y*
T0*
out_type0

*gradients_4/pow_grad/BroadcastGradientArgsBroadcastGradientArgsgradients_4/pow_grad/Shapegradients_4/pow_grad/Shape_1*
T0
M
gradients_4/pow_grad/mulMulgradients_4/Sum_33_grad/Tilepow/y*
T0
G
gradients_4/pow_grad/sub/yConst*
valueB
 *  ?*
dtype0
K
gradients_4/pow_grad/subSubpow/ygradients_4/pow_grad/sub/y*
T0

gradients_4/pow_grad/PowPow;gradients_3/GAIL_model_2/gail_d_hidden_1/MatMul_grad/MatMulgradients_4/pow_grad/sub*
T0
^
gradients_4/pow_grad/mul_1Mulgradients_4/pow_grad/mulgradients_4/pow_grad/Pow*
T0

gradients_4/pow_grad/SumSumgradients_4/pow_grad/mul_1*gradients_4/pow_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0
t
gradients_4/pow_grad/ReshapeReshapegradients_4/pow_grad/Sumgradients_4/pow_grad/Shape*
T0*
Tshape0
K
gradients_4/pow_grad/Greater/yConst*
valueB
 *    *
dtype0

gradients_4/pow_grad/GreaterGreater;gradients_3/GAIL_model_2/gail_d_hidden_1/MatMul_grad/MatMulgradients_4/pow_grad/Greater/y*
T0

$gradients_4/pow_grad/ones_like/ShapeShape;gradients_3/GAIL_model_2/gail_d_hidden_1/MatMul_grad/MatMul*
T0*
out_type0
Q
$gradients_4/pow_grad/ones_like/ConstConst*
valueB
 *  ?*
dtype0

gradients_4/pow_grad/ones_likeFill$gradients_4/pow_grad/ones_like/Shape$gradients_4/pow_grad/ones_like/Const*
T0*

index_type0
©
gradients_4/pow_grad/SelectSelectgradients_4/pow_grad/Greater;gradients_3/GAIL_model_2/gail_d_hidden_1/MatMul_grad/MatMulgradients_4/pow_grad/ones_like*
T0
E
gradients_4/pow_grad/LogLoggradients_4/pow_grad/Select*
T0
r
gradients_4/pow_grad/zeros_like	ZerosLike;gradients_3/GAIL_model_2/gail_d_hidden_1/MatMul_grad/MatMul*
T0

gradients_4/pow_grad/Select_1Selectgradients_4/pow_grad/Greatergradients_4/pow_grad/Loggradients_4/pow_grad/zeros_like*
T0
M
gradients_4/pow_grad/mul_2Mulgradients_4/Sum_33_grad/Tilepow*
T0
e
gradients_4/pow_grad/mul_3Mulgradients_4/pow_grad/mul_2gradients_4/pow_grad/Select_1*
T0

gradients_4/pow_grad/Sum_1Sumgradients_4/pow_grad/mul_3,gradients_4/pow_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0
z
gradients_4/pow_grad/Reshape_1Reshapegradients_4/pow_grad/Sum_1gradients_4/pow_grad/Shape_1*
T0*
Tshape0
m
%gradients_4/pow_grad/tuple/group_depsNoOp^gradients_4/pow_grad/Reshape^gradients_4/pow_grad/Reshape_1
¹
-gradients_4/pow_grad/tuple/control_dependencyIdentitygradients_4/pow_grad/Reshape&^gradients_4/pow_grad/tuple/group_deps*
T0*/
_class%
#!loc:@gradients_4/pow_grad/Reshape
æ
/gradients_4/pow_grad/tuple/control_dependency_1Identitygradients_4/pow_grad/Reshape_1&^gradients_4/pow_grad/tuple/group_deps*
T0*1
_class'
%#loc:@gradients_4/pow_grad/Reshape_1
{
5gradients_4/GAIL_model/gail_d_hidden_2/Mul_grad/ShapeShape"GAIL_model/gail_d_hidden_2/BiasAdd*
T0*
out_type0
}
7gradients_4/GAIL_model/gail_d_hidden_2/Mul_grad/Shape_1Shape"GAIL_model/gail_d_hidden_2/Sigmoid*
T0*
out_type0
×
Egradients_4/GAIL_model/gail_d_hidden_2/Mul_grad/BroadcastGradientArgsBroadcastGradientArgs5gradients_4/GAIL_model/gail_d_hidden_2/Mul_grad/Shape7gradients_4/GAIL_model/gail_d_hidden_2/Mul_grad/Shape_1*
T0
“
3gradients_4/GAIL_model/gail_d_hidden_2/Mul_grad/MulMulKgradients_4/GAIL_model/gail_d_estimate/MatMul_grad/tuple/control_dependency"GAIL_model/gail_d_hidden_2/Sigmoid*
T0
Ü
3gradients_4/GAIL_model/gail_d_hidden_2/Mul_grad/SumSum3gradients_4/GAIL_model/gail_d_hidden_2/Mul_grad/MulEgradients_4/GAIL_model/gail_d_hidden_2/Mul_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0
Å
7gradients_4/GAIL_model/gail_d_hidden_2/Mul_grad/ReshapeReshape3gradients_4/GAIL_model/gail_d_hidden_2/Mul_grad/Sum5gradients_4/GAIL_model/gail_d_hidden_2/Mul_grad/Shape*
T0*
Tshape0
¶
5gradients_4/GAIL_model/gail_d_hidden_2/Mul_grad/Mul_1Mul"GAIL_model/gail_d_hidden_2/BiasAddKgradients_4/GAIL_model/gail_d_estimate/MatMul_grad/tuple/control_dependency*
T0
ā
5gradients_4/GAIL_model/gail_d_hidden_2/Mul_grad/Sum_1Sum5gradients_4/GAIL_model/gail_d_hidden_2/Mul_grad/Mul_1Ggradients_4/GAIL_model/gail_d_hidden_2/Mul_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0
Ė
9gradients_4/GAIL_model/gail_d_hidden_2/Mul_grad/Reshape_1Reshape5gradients_4/GAIL_model/gail_d_hidden_2/Mul_grad/Sum_17gradients_4/GAIL_model/gail_d_hidden_2/Mul_grad/Shape_1*
T0*
Tshape0
¾
@gradients_4/GAIL_model/gail_d_hidden_2/Mul_grad/tuple/group_depsNoOp8^gradients_4/GAIL_model/gail_d_hidden_2/Mul_grad/Reshape:^gradients_4/GAIL_model/gail_d_hidden_2/Mul_grad/Reshape_1
„
Hgradients_4/GAIL_model/gail_d_hidden_2/Mul_grad/tuple/control_dependencyIdentity7gradients_4/GAIL_model/gail_d_hidden_2/Mul_grad/ReshapeA^gradients_4/GAIL_model/gail_d_hidden_2/Mul_grad/tuple/group_deps*
T0*J
_class@
><loc:@gradients_4/GAIL_model/gail_d_hidden_2/Mul_grad/Reshape
«
Jgradients_4/GAIL_model/gail_d_hidden_2/Mul_grad/tuple/control_dependency_1Identity9gradients_4/GAIL_model/gail_d_hidden_2/Mul_grad/Reshape_1A^gradients_4/GAIL_model/gail_d_hidden_2/Mul_grad/tuple/group_deps*
T0*L
_classB
@>loc:@gradients_4/GAIL_model/gail_d_hidden_2/Mul_grad/Reshape_1
ģ
;gradients_4/GAIL_model_1/gail_d_estimate/MatMul_grad/MatMulMatMulNgradients_4/GAIL_model_1/gail_d_estimate/BiasAdd_grad/tuple/control_dependency&GAIL_model/gail_d_estimate/kernel/read*
transpose_b(*
T0*
transpose_a( 
č
=gradients_4/GAIL_model_1/gail_d_estimate/MatMul_grad/MatMul_1MatMul GAIL_model_1/gail_d_hidden_2/MulNgradients_4/GAIL_model_1/gail_d_estimate/BiasAdd_grad/tuple/control_dependency*
transpose_b( *
T0*
transpose_a(
Ė
Egradients_4/GAIL_model_1/gail_d_estimate/MatMul_grad/tuple/group_depsNoOp<^gradients_4/GAIL_model_1/gail_d_estimate/MatMul_grad/MatMul>^gradients_4/GAIL_model_1/gail_d_estimate/MatMul_grad/MatMul_1
·
Mgradients_4/GAIL_model_1/gail_d_estimate/MatMul_grad/tuple/control_dependencyIdentity;gradients_4/GAIL_model_1/gail_d_estimate/MatMul_grad/MatMulF^gradients_4/GAIL_model_1/gail_d_estimate/MatMul_grad/tuple/group_deps*
T0*N
_classD
B@loc:@gradients_4/GAIL_model_1/gail_d_estimate/MatMul_grad/MatMul
½
Ogradients_4/GAIL_model_1/gail_d_estimate/MatMul_grad/tuple/control_dependency_1Identity=gradients_4/GAIL_model_1/gail_d_estimate/MatMul_grad/MatMul_1F^gradients_4/GAIL_model_1/gail_d_estimate/MatMul_grad/tuple/group_deps*
T0*P
_classF
DBloc:@gradients_4/GAIL_model_1/gail_d_estimate/MatMul_grad/MatMul_1
ć
Sgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/MatMul_grad/MatMul_grad/MatMulMatMul-gradients_4/pow_grad/tuple/control_dependency&GAIL_model/gail_d_hidden_1/kernel/read*
transpose_b( *
T0*
transpose_a( 
Ń
Ugradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/MatMul_grad/MatMul_grad/MatMul_1MatMul-gradients_4/pow_grad/tuple/control_dependencygradients_3/AddN_1*
transpose_b( *
T0*
transpose_a(

]gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/MatMul_grad/MatMul_grad/tuple/group_depsNoOpT^gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/MatMul_grad/MatMul_grad/MatMulV^gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/MatMul_grad/MatMul_grad/MatMul_1

egradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/MatMul_grad/MatMul_grad/tuple/control_dependencyIdentitySgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/MatMul_grad/MatMul_grad/MatMul^^gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/MatMul_grad/MatMul_grad/tuple/group_deps*
T0*f
_class\
ZXloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/MatMul_grad/MatMul_grad/MatMul

ggradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/MatMul_grad/MatMul_grad/tuple/control_dependency_1IdentityUgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/MatMul_grad/MatMul_grad/MatMul_1^^gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/MatMul_grad/MatMul_grad/tuple/group_deps*
T0*h
_class^
\Zloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/MatMul_grad/MatMul_grad/MatMul_1
Ē
?gradients_4/GAIL_model/gail_d_hidden_2/Sigmoid_grad/SigmoidGradSigmoidGrad"GAIL_model/gail_d_hidden_2/SigmoidJgradients_4/GAIL_model/gail_d_hidden_2/Mul_grad/tuple/control_dependency_1*
T0

7gradients_4/GAIL_model_1/gail_d_hidden_2/Mul_grad/ShapeShape$GAIL_model_1/gail_d_hidden_2/BiasAdd*
T0*
out_type0

9gradients_4/GAIL_model_1/gail_d_hidden_2/Mul_grad/Shape_1Shape$GAIL_model_1/gail_d_hidden_2/Sigmoid*
T0*
out_type0
Ż
Ggradients_4/GAIL_model_1/gail_d_hidden_2/Mul_grad/BroadcastGradientArgsBroadcastGradientArgs7gradients_4/GAIL_model_1/gail_d_hidden_2/Mul_grad/Shape9gradients_4/GAIL_model_1/gail_d_hidden_2/Mul_grad/Shape_1*
T0
ŗ
5gradients_4/GAIL_model_1/gail_d_hidden_2/Mul_grad/MulMulMgradients_4/GAIL_model_1/gail_d_estimate/MatMul_grad/tuple/control_dependency$GAIL_model_1/gail_d_hidden_2/Sigmoid*
T0
ā
5gradients_4/GAIL_model_1/gail_d_hidden_2/Mul_grad/SumSum5gradients_4/GAIL_model_1/gail_d_hidden_2/Mul_grad/MulGgradients_4/GAIL_model_1/gail_d_hidden_2/Mul_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0
Ė
9gradients_4/GAIL_model_1/gail_d_hidden_2/Mul_grad/ReshapeReshape5gradients_4/GAIL_model_1/gail_d_hidden_2/Mul_grad/Sum7gradients_4/GAIL_model_1/gail_d_hidden_2/Mul_grad/Shape*
T0*
Tshape0
¼
7gradients_4/GAIL_model_1/gail_d_hidden_2/Mul_grad/Mul_1Mul$GAIL_model_1/gail_d_hidden_2/BiasAddMgradients_4/GAIL_model_1/gail_d_estimate/MatMul_grad/tuple/control_dependency*
T0
č
7gradients_4/GAIL_model_1/gail_d_hidden_2/Mul_grad/Sum_1Sum7gradients_4/GAIL_model_1/gail_d_hidden_2/Mul_grad/Mul_1Igradients_4/GAIL_model_1/gail_d_hidden_2/Mul_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0
Ń
;gradients_4/GAIL_model_1/gail_d_hidden_2/Mul_grad/Reshape_1Reshape7gradients_4/GAIL_model_1/gail_d_hidden_2/Mul_grad/Sum_19gradients_4/GAIL_model_1/gail_d_hidden_2/Mul_grad/Shape_1*
T0*
Tshape0
Ä
Bgradients_4/GAIL_model_1/gail_d_hidden_2/Mul_grad/tuple/group_depsNoOp:^gradients_4/GAIL_model_1/gail_d_hidden_2/Mul_grad/Reshape<^gradients_4/GAIL_model_1/gail_d_hidden_2/Mul_grad/Reshape_1
­
Jgradients_4/GAIL_model_1/gail_d_hidden_2/Mul_grad/tuple/control_dependencyIdentity9gradients_4/GAIL_model_1/gail_d_hidden_2/Mul_grad/ReshapeC^gradients_4/GAIL_model_1/gail_d_hidden_2/Mul_grad/tuple/group_deps*
T0*L
_classB
@>loc:@gradients_4/GAIL_model_1/gail_d_hidden_2/Mul_grad/Reshape
³
Lgradients_4/GAIL_model_1/gail_d_hidden_2/Mul_grad/tuple/control_dependency_1Identity;gradients_4/GAIL_model_1/gail_d_hidden_2/Mul_grad/Reshape_1C^gradients_4/GAIL_model_1/gail_d_hidden_2/Mul_grad/tuple/group_deps*
T0*N
_classD
B@loc:@gradients_4/GAIL_model_1/gail_d_hidden_2/Mul_grad/Reshape_1
¤
4gradients_4/gradients_3/AddN_1_grad/tuple/group_depsNoOpf^gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/MatMul_grad/MatMul_grad/tuple/control_dependency
×
<gradients_4/gradients_3/AddN_1_grad/tuple/control_dependencyIdentityegradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/MatMul_grad/MatMul_grad/tuple/control_dependency5^gradients_4/gradients_3/AddN_1_grad/tuple/group_deps*
T0*f
_class\
ZXloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/MatMul_grad/MatMul_grad/MatMul
Ł
>gradients_4/gradients_3/AddN_1_grad/tuple/control_dependency_1Identityegradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/MatMul_grad/MatMul_grad/tuple/control_dependency5^gradients_4/gradients_3/AddN_1_grad/tuple/group_deps*
T0*f
_class\
ZXloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/MatMul_grad/MatMul_grad/MatMul

gradients_4/AddNAddNHgradients_4/GAIL_model/gail_d_hidden_2/Mul_grad/tuple/control_dependency?gradients_4/GAIL_model/gail_d_hidden_2/Sigmoid_grad/SigmoidGrad*
T0*J
_class@
><loc:@gradients_4/GAIL_model/gail_d_hidden_2/Mul_grad/Reshape*
N

?gradients_4/GAIL_model/gail_d_hidden_2/BiasAdd_grad/BiasAddGradBiasAddGradgradients_4/AddN*
T0*
data_formatNHWC
”
Dgradients_4/GAIL_model/gail_d_hidden_2/BiasAdd_grad/tuple/group_depsNoOp^gradients_4/AddN@^gradients_4/GAIL_model/gail_d_hidden_2/BiasAdd_grad/BiasAddGrad

Lgradients_4/GAIL_model/gail_d_hidden_2/BiasAdd_grad/tuple/control_dependencyIdentitygradients_4/AddNE^gradients_4/GAIL_model/gail_d_hidden_2/BiasAdd_grad/tuple/group_deps*
T0*J
_class@
><loc:@gradients_4/GAIL_model/gail_d_hidden_2/Mul_grad/Reshape
æ
Ngradients_4/GAIL_model/gail_d_hidden_2/BiasAdd_grad/tuple/control_dependency_1Identity?gradients_4/GAIL_model/gail_d_hidden_2/BiasAdd_grad/BiasAddGradE^gradients_4/GAIL_model/gail_d_hidden_2/BiasAdd_grad/tuple/group_deps*
T0*R
_classH
FDloc:@gradients_4/GAIL_model/gail_d_hidden_2/BiasAdd_grad/BiasAddGrad
Ķ
Agradients_4/GAIL_model_1/gail_d_hidden_2/Sigmoid_grad/SigmoidGradSigmoidGrad$GAIL_model_1/gail_d_hidden_2/SigmoidLgradients_4/GAIL_model_1/gail_d_hidden_2/Mul_grad/tuple/control_dependency_1*
T0
©
Pgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Reshape_grad/ShapeShape5gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum*
T0*
out_type0

Rgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Reshape_grad/ReshapeReshape<gradients_4/gradients_3/AddN_1_grad/tuple/control_dependencyPgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Reshape_grad/Shape*
T0*
Tshape0
ć
Vgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Sigmoid_grad/SigmoidGrad_grad/mulMul>gradients_4/gradients_3/AddN_1_grad/tuple/control_dependency_1;gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Reshape_1*
T0
Č
Zgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Sigmoid_grad/SigmoidGrad_grad/mul_1/xConst?^gradients_4/gradients_3/AddN_1_grad/tuple/control_dependency_1*
valueB
 *   @*
dtype0

Xgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Sigmoid_grad/SigmoidGrad_grad/mul_1MulZgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Sigmoid_grad/SigmoidGrad_grad/mul_1/xVgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Sigmoid_grad/SigmoidGrad_grad/mul*
T0
č
Xgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Sigmoid_grad/SigmoidGrad_grad/mul_2MulXgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Sigmoid_grad/SigmoidGrad_grad/mul_1$GAIL_model_2/gail_d_hidden_1/Sigmoid*
T0

Vgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Sigmoid_grad/SigmoidGrad_grad/subSubVgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Sigmoid_grad/SigmoidGrad_grad/mulXgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Sigmoid_grad/SigmoidGrad_grad/mul_2*
T0
Ü
^gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Sigmoid_grad/SigmoidGrad_grad/SigmoidGradSigmoidGrad$GAIL_model_2/gail_d_hidden_1/Sigmoid>gradients_4/gradients_3/AddN_1_grad/tuple/control_dependency_1*
T0
„
cgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Sigmoid_grad/SigmoidGrad_grad/tuple/group_depsNoOp_^gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Sigmoid_grad/SigmoidGrad_grad/SigmoidGradW^gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Sigmoid_grad/SigmoidGrad_grad/sub
©
kgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Sigmoid_grad/SigmoidGrad_grad/tuple/control_dependencyIdentityVgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Sigmoid_grad/SigmoidGrad_grad/subd^gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Sigmoid_grad/SigmoidGrad_grad/tuple/group_deps*
T0*i
_class_
][loc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Sigmoid_grad/SigmoidGrad_grad/sub
»
mgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Sigmoid_grad/SigmoidGrad_grad/tuple/control_dependency_1Identity^gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Sigmoid_grad/SigmoidGrad_grad/SigmoidGradd^gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Sigmoid_grad/SigmoidGrad_grad/tuple/group_deps*
T0*q
_classg
ecloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Sigmoid_grad/SigmoidGrad_grad/SigmoidGrad
č
9gradients_4/GAIL_model/gail_d_hidden_2/MatMul_grad/MatMulMatMulLgradients_4/GAIL_model/gail_d_hidden_2/BiasAdd_grad/tuple/control_dependency&GAIL_model/gail_d_hidden_2/kernel/read*
transpose_b(*
T0*
transpose_a( 
ā
;gradients_4/GAIL_model/gail_d_hidden_2/MatMul_grad/MatMul_1MatMulGAIL_model/gail_d_hidden_1/MulLgradients_4/GAIL_model/gail_d_hidden_2/BiasAdd_grad/tuple/control_dependency*
transpose_b( *
T0*
transpose_a(
Å
Cgradients_4/GAIL_model/gail_d_hidden_2/MatMul_grad/tuple/group_depsNoOp:^gradients_4/GAIL_model/gail_d_hidden_2/MatMul_grad/MatMul<^gradients_4/GAIL_model/gail_d_hidden_2/MatMul_grad/MatMul_1
Æ
Kgradients_4/GAIL_model/gail_d_hidden_2/MatMul_grad/tuple/control_dependencyIdentity9gradients_4/GAIL_model/gail_d_hidden_2/MatMul_grad/MatMulD^gradients_4/GAIL_model/gail_d_hidden_2/MatMul_grad/tuple/group_deps*
T0*L
_classB
@>loc:@gradients_4/GAIL_model/gail_d_hidden_2/MatMul_grad/MatMul
µ
Mgradients_4/GAIL_model/gail_d_hidden_2/MatMul_grad/tuple/control_dependency_1Identity;gradients_4/GAIL_model/gail_d_hidden_2/MatMul_grad/MatMul_1D^gradients_4/GAIL_model/gail_d_hidden_2/MatMul_grad/tuple/group_deps*
T0*N
_classD
B@loc:@gradients_4/GAIL_model/gail_d_hidden_2/MatMul_grad/MatMul_1

gradients_4/AddN_1AddNJgradients_4/GAIL_model_1/gail_d_hidden_2/Mul_grad/tuple/control_dependencyAgradients_4/GAIL_model_1/gail_d_hidden_2/Sigmoid_grad/SigmoidGrad*
T0*L
_classB
@>loc:@gradients_4/GAIL_model_1/gail_d_hidden_2/Mul_grad/Reshape*
N

Agradients_4/GAIL_model_1/gail_d_hidden_2/BiasAdd_grad/BiasAddGradBiasAddGradgradients_4/AddN_1*
T0*
data_formatNHWC
§
Fgradients_4/GAIL_model_1/gail_d_hidden_2/BiasAdd_grad/tuple/group_depsNoOp^gradients_4/AddN_1B^gradients_4/GAIL_model_1/gail_d_hidden_2/BiasAdd_grad/BiasAddGrad

Ngradients_4/GAIL_model_1/gail_d_hidden_2/BiasAdd_grad/tuple/control_dependencyIdentitygradients_4/AddN_1G^gradients_4/GAIL_model_1/gail_d_hidden_2/BiasAdd_grad/tuple/group_deps*
T0*L
_classB
@>loc:@gradients_4/GAIL_model_1/gail_d_hidden_2/Mul_grad/Reshape
Ē
Pgradients_4/GAIL_model_1/gail_d_hidden_2/BiasAdd_grad/tuple/control_dependency_1IdentityAgradients_4/GAIL_model_1/gail_d_hidden_2/BiasAdd_grad/BiasAddGradG^gradients_4/GAIL_model_1/gail_d_hidden_2/BiasAdd_grad/tuple/group_deps*
T0*T
_classJ
HFloc:@gradients_4/GAIL_model_1/gail_d_hidden_2/BiasAdd_grad/BiasAddGrad
„
Lgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/ShapeShape5gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul*
T0*
out_type0
Ö
Kgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/SizeConst*_
_classU
SQloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/Shape*
value	B :*
dtype0
Ó
Jgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/addAddV2Ggradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/BroadcastGradientArgsKgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/Size*
T0*_
_classU
SQloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/Shape
Ł
Jgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/modFloorModJgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/addKgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/Size*
T0*_
_classU
SQloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/Shape

Ngradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/Shape_1ShapeJgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/mod*
T0*_
_classU
SQloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/Shape*
out_type0
Ż
Rgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/range/startConst*_
_classU
SQloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/Shape*
value	B : *
dtype0
Ż
Rgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/range/deltaConst*_
_classU
SQloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/Shape*
value	B :*
dtype0
·
Lgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/rangeRangeRgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/range/startKgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/SizeRgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/range/delta*

Tidx0*_
_classU
SQloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/Shape
Ü
Qgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/Fill/valueConst*_
_classU
SQloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/Shape*
value	B :*
dtype0
ņ
Kgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/FillFillNgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/Shape_1Qgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/Fill/value*
T0*_
_classU
SQloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/Shape*

index_type0

Tgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/DynamicStitchDynamicStitchLgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/rangeJgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/modLgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/ShapeKgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/Fill*
T0*_
_classU
SQloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/Shape*
N
Ū
Pgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/Maximum/yConst*_
_classU
SQloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/Shape*
value	B :*
dtype0
ė
Ngradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/MaximumMaximumTgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/DynamicStitchPgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/Maximum/y*
T0*_
_classU
SQloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/Shape
ć
Ogradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/floordivFloorDivLgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/ShapeNgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/Maximum*
T0*_
_classU
SQloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/Shape

Ngradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/ReshapeReshapeRgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Reshape_grad/ReshapeTgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/DynamicStitch*
T0*
Tshape0

Kgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/TileTileNgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/ReshapeOgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/floordiv*

Tmultiples0*
T0
­
Rgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Reshape_1_grad/ShapeShape7gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1*
T0*
out_type0
¹
Tgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Reshape_1_grad/ReshapeReshapemgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Sigmoid_grad/SigmoidGrad_grad/tuple/control_dependency_1Rgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Reshape_1_grad/Shape*
T0*
Tshape0
{
5gradients_4/GAIL_model/gail_d_hidden_1/Mul_grad/ShapeShape"GAIL_model/gail_d_hidden_1/BiasAdd*
T0*
out_type0
}
7gradients_4/GAIL_model/gail_d_hidden_1/Mul_grad/Shape_1Shape"GAIL_model/gail_d_hidden_1/Sigmoid*
T0*
out_type0
×
Egradients_4/GAIL_model/gail_d_hidden_1/Mul_grad/BroadcastGradientArgsBroadcastGradientArgs5gradients_4/GAIL_model/gail_d_hidden_1/Mul_grad/Shape7gradients_4/GAIL_model/gail_d_hidden_1/Mul_grad/Shape_1*
T0
“
3gradients_4/GAIL_model/gail_d_hidden_1/Mul_grad/MulMulKgradients_4/GAIL_model/gail_d_hidden_2/MatMul_grad/tuple/control_dependency"GAIL_model/gail_d_hidden_1/Sigmoid*
T0
Ü
3gradients_4/GAIL_model/gail_d_hidden_1/Mul_grad/SumSum3gradients_4/GAIL_model/gail_d_hidden_1/Mul_grad/MulEgradients_4/GAIL_model/gail_d_hidden_1/Mul_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0
Å
7gradients_4/GAIL_model/gail_d_hidden_1/Mul_grad/ReshapeReshape3gradients_4/GAIL_model/gail_d_hidden_1/Mul_grad/Sum5gradients_4/GAIL_model/gail_d_hidden_1/Mul_grad/Shape*
T0*
Tshape0
¶
5gradients_4/GAIL_model/gail_d_hidden_1/Mul_grad/Mul_1Mul"GAIL_model/gail_d_hidden_1/BiasAddKgradients_4/GAIL_model/gail_d_hidden_2/MatMul_grad/tuple/control_dependency*
T0
ā
5gradients_4/GAIL_model/gail_d_hidden_1/Mul_grad/Sum_1Sum5gradients_4/GAIL_model/gail_d_hidden_1/Mul_grad/Mul_1Ggradients_4/GAIL_model/gail_d_hidden_1/Mul_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0
Ė
9gradients_4/GAIL_model/gail_d_hidden_1/Mul_grad/Reshape_1Reshape5gradients_4/GAIL_model/gail_d_hidden_1/Mul_grad/Sum_17gradients_4/GAIL_model/gail_d_hidden_1/Mul_grad/Shape_1*
T0*
Tshape0
¾
@gradients_4/GAIL_model/gail_d_hidden_1/Mul_grad/tuple/group_depsNoOp8^gradients_4/GAIL_model/gail_d_hidden_1/Mul_grad/Reshape:^gradients_4/GAIL_model/gail_d_hidden_1/Mul_grad/Reshape_1
„
Hgradients_4/GAIL_model/gail_d_hidden_1/Mul_grad/tuple/control_dependencyIdentity7gradients_4/GAIL_model/gail_d_hidden_1/Mul_grad/ReshapeA^gradients_4/GAIL_model/gail_d_hidden_1/Mul_grad/tuple/group_deps*
T0*J
_class@
><loc:@gradients_4/GAIL_model/gail_d_hidden_1/Mul_grad/Reshape
«
Jgradients_4/GAIL_model/gail_d_hidden_1/Mul_grad/tuple/control_dependency_1Identity9gradients_4/GAIL_model/gail_d_hidden_1/Mul_grad/Reshape_1A^gradients_4/GAIL_model/gail_d_hidden_1/Mul_grad/tuple/group_deps*
T0*L
_classB
@>loc:@gradients_4/GAIL_model/gail_d_hidden_1/Mul_grad/Reshape_1
ģ
;gradients_4/GAIL_model_1/gail_d_hidden_2/MatMul_grad/MatMulMatMulNgradients_4/GAIL_model_1/gail_d_hidden_2/BiasAdd_grad/tuple/control_dependency&GAIL_model/gail_d_hidden_2/kernel/read*
transpose_b(*
T0*
transpose_a( 
č
=gradients_4/GAIL_model_1/gail_d_hidden_2/MatMul_grad/MatMul_1MatMul GAIL_model_1/gail_d_hidden_1/MulNgradients_4/GAIL_model_1/gail_d_hidden_2/BiasAdd_grad/tuple/control_dependency*
transpose_b( *
T0*
transpose_a(
Ė
Egradients_4/GAIL_model_1/gail_d_hidden_2/MatMul_grad/tuple/group_depsNoOp<^gradients_4/GAIL_model_1/gail_d_hidden_2/MatMul_grad/MatMul>^gradients_4/GAIL_model_1/gail_d_hidden_2/MatMul_grad/MatMul_1
·
Mgradients_4/GAIL_model_1/gail_d_hidden_2/MatMul_grad/tuple/control_dependencyIdentity;gradients_4/GAIL_model_1/gail_d_hidden_2/MatMul_grad/MatMulF^gradients_4/GAIL_model_1/gail_d_hidden_2/MatMul_grad/tuple/group_deps*
T0*N
_classD
B@loc:@gradients_4/GAIL_model_1/gail_d_hidden_2/MatMul_grad/MatMul
½
Ogradients_4/GAIL_model_1/gail_d_hidden_2/MatMul_grad/tuple/control_dependency_1Identity=gradients_4/GAIL_model_1/gail_d_hidden_2/MatMul_grad/MatMul_1F^gradients_4/GAIL_model_1/gail_d_hidden_2/MatMul_grad/tuple/group_deps*
T0*P
_classF
DBloc:@gradients_4/GAIL_model_1/gail_d_hidden_2/MatMul_grad/MatMul_1
«
Lgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_grad/ShapeShape;gradients_3/GAIL_model_2/gail_d_hidden_2/MatMul_grad/MatMul*
T0*
out_type0

Ngradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_grad/Shape_1Shape$GAIL_model_2/gail_d_hidden_1/Sigmoid*
T0*
out_type0

\gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_grad/BroadcastGradientArgsBroadcastGradientArgsLgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_grad/ShapeNgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_grad/Shape_1*
T0
Ķ
Jgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_grad/MulMulKgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/Tile$GAIL_model_2/gail_d_hidden_1/Sigmoid*
T0
”
Jgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_grad/SumSumJgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_grad/Mul\gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0

Ngradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_grad/ReshapeReshapeJgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_grad/SumLgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_grad/Shape*
T0*
Tshape0
ę
Lgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_grad/Mul_1Mul;gradients_3/GAIL_model_2/gail_d_hidden_2/MatMul_grad/MatMulKgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_grad/Tile*
T0
§
Lgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_grad/Sum_1SumLgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_grad/Mul_1^gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0

Pgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_grad/Reshape_1ReshapeLgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_grad/Sum_1Ngradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_grad/Shape_1*
T0*
Tshape0

Wgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_grad/tuple/group_depsNoOpO^gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_grad/ReshapeQ^gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_grad/Reshape_1

_gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_grad/tuple/control_dependencyIdentityNgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_grad/ReshapeX^gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_grad/tuple/group_deps*
T0*a
_classW
USloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_grad/Reshape

agradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_grad/tuple/control_dependency_1IdentityPgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_grad/Reshape_1X^gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_grad/tuple/group_deps*
T0*c
_classY
WUloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_grad/Reshape_1
©
Ngradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/ShapeShape7gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_1*
T0*
out_type0
Ś
Mgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/SizeConst*a
_classW
USloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/Shape*
value	B :*
dtype0
Ū
Lgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/addAddV2Igradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/BroadcastGradientArgs:1Mgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/Size*
T0*a
_classW
USloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/Shape
į
Lgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/modFloorModLgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/addMgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/Size*
T0*a
_classW
USloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/Shape
£
Pgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/Shape_1ShapeLgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/mod*
T0*a
_classW
USloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/Shape*
out_type0
į
Tgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/range/startConst*a
_classW
USloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/Shape*
value	B : *
dtype0
į
Tgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/range/deltaConst*a
_classW
USloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/Shape*
value	B :*
dtype0
Į
Ngradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/rangeRangeTgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/range/startMgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/SizeTgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/range/delta*

Tidx0*a
_classW
USloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/Shape
ą
Sgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/Fill/valueConst*a
_classW
USloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/Shape*
value	B :*
dtype0
ś
Mgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/FillFillPgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/Shape_1Sgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/Fill/value*
T0*a
_classW
USloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/Shape*

index_type0

Vgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/DynamicStitchDynamicStitchNgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/rangeLgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/modNgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/ShapeMgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/Fill*
T0*a
_classW
USloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/Shape*
N
ß
Rgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/Maximum/yConst*a
_classW
USloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/Shape*
value	B :*
dtype0
ó
Pgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/MaximumMaximumVgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/DynamicStitchRgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/Maximum/y*
T0*a
_classW
USloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/Shape
ė
Qgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/floordivFloorDivNgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/ShapePgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/Maximum*
T0*a
_classW
USloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/Shape
 
Pgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/ReshapeReshapeTgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Reshape_1_grad/ReshapeVgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/DynamicStitch*
T0*
Tshape0

Mgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/TileTilePgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/ReshapeQgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/floordiv*

Tmultiples0*
T0
Ē
?gradients_4/GAIL_model/gail_d_hidden_1/Sigmoid_grad/SigmoidGradSigmoidGrad"GAIL_model/gail_d_hidden_1/SigmoidJgradients_4/GAIL_model/gail_d_hidden_1/Mul_grad/tuple/control_dependency_1*
T0

7gradients_4/GAIL_model_1/gail_d_hidden_1/Mul_grad/ShapeShape$GAIL_model_1/gail_d_hidden_1/BiasAdd*
T0*
out_type0

9gradients_4/GAIL_model_1/gail_d_hidden_1/Mul_grad/Shape_1Shape$GAIL_model_1/gail_d_hidden_1/Sigmoid*
T0*
out_type0
Ż
Ggradients_4/GAIL_model_1/gail_d_hidden_1/Mul_grad/BroadcastGradientArgsBroadcastGradientArgs7gradients_4/GAIL_model_1/gail_d_hidden_1/Mul_grad/Shape9gradients_4/GAIL_model_1/gail_d_hidden_1/Mul_grad/Shape_1*
T0
ŗ
5gradients_4/GAIL_model_1/gail_d_hidden_1/Mul_grad/MulMulMgradients_4/GAIL_model_1/gail_d_hidden_2/MatMul_grad/tuple/control_dependency$GAIL_model_1/gail_d_hidden_1/Sigmoid*
T0
ā
5gradients_4/GAIL_model_1/gail_d_hidden_1/Mul_grad/SumSum5gradients_4/GAIL_model_1/gail_d_hidden_1/Mul_grad/MulGgradients_4/GAIL_model_1/gail_d_hidden_1/Mul_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0
Ė
9gradients_4/GAIL_model_1/gail_d_hidden_1/Mul_grad/ReshapeReshape5gradients_4/GAIL_model_1/gail_d_hidden_1/Mul_grad/Sum7gradients_4/GAIL_model_1/gail_d_hidden_1/Mul_grad/Shape*
T0*
Tshape0
¼
7gradients_4/GAIL_model_1/gail_d_hidden_1/Mul_grad/Mul_1Mul$GAIL_model_1/gail_d_hidden_1/BiasAddMgradients_4/GAIL_model_1/gail_d_hidden_2/MatMul_grad/tuple/control_dependency*
T0
č
7gradients_4/GAIL_model_1/gail_d_hidden_1/Mul_grad/Sum_1Sum7gradients_4/GAIL_model_1/gail_d_hidden_1/Mul_grad/Mul_1Igradients_4/GAIL_model_1/gail_d_hidden_1/Mul_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0
Ń
;gradients_4/GAIL_model_1/gail_d_hidden_1/Mul_grad/Reshape_1Reshape7gradients_4/GAIL_model_1/gail_d_hidden_1/Mul_grad/Sum_19gradients_4/GAIL_model_1/gail_d_hidden_1/Mul_grad/Shape_1*
T0*
Tshape0
Ä
Bgradients_4/GAIL_model_1/gail_d_hidden_1/Mul_grad/tuple/group_depsNoOp:^gradients_4/GAIL_model_1/gail_d_hidden_1/Mul_grad/Reshape<^gradients_4/GAIL_model_1/gail_d_hidden_1/Mul_grad/Reshape_1
­
Jgradients_4/GAIL_model_1/gail_d_hidden_1/Mul_grad/tuple/control_dependencyIdentity9gradients_4/GAIL_model_1/gail_d_hidden_1/Mul_grad/ReshapeC^gradients_4/GAIL_model_1/gail_d_hidden_1/Mul_grad/tuple/group_deps*
T0*L
_classB
@>loc:@gradients_4/GAIL_model_1/gail_d_hidden_1/Mul_grad/Reshape
³
Lgradients_4/GAIL_model_1/gail_d_hidden_1/Mul_grad/tuple/control_dependency_1Identity;gradients_4/GAIL_model_1/gail_d_hidden_1/Mul_grad/Reshape_1C^gradients_4/GAIL_model_1/gail_d_hidden_1/Mul_grad/tuple/group_deps*
T0*N
_classD
B@loc:@gradients_4/GAIL_model_1/gail_d_hidden_1/Mul_grad/Reshape_1

Ngradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_1_grad/ShapeShape$GAIL_model_2/gail_d_hidden_1/BiasAdd*
T0*
out_type0
Æ
Pgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_1_grad/Shape_1Shape;gradients_3/GAIL_model_2/gail_d_hidden_2/MatMul_grad/MatMul*
T0*
out_type0
¢
^gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_1_grad/BroadcastGradientArgsBroadcastGradientArgsNgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_1_grad/ShapePgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_1_grad/Shape_1*
T0
č
Lgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_1_grad/MulMulMgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/Tile;gradients_3/GAIL_model_2/gail_d_hidden_2/MatMul_grad/MatMul*
T0
§
Lgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_1_grad/SumSumLgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_1_grad/Mul^gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_1_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0

Pgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_1_grad/ReshapeReshapeLgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_1_grad/SumNgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_1_grad/Shape*
T0*
Tshape0
Ó
Ngradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_1_grad/Mul_1Mul$GAIL_model_2/gail_d_hidden_1/BiasAddMgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1_grad/Tile*
T0
­
Ngradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_1_grad/Sum_1SumNgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_1_grad/Mul_1`gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_1_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0

Rgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_1_grad/Reshape_1ReshapeNgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_1_grad/Sum_1Pgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_1_grad/Shape_1*
T0*
Tshape0

Ygradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_1_grad/tuple/group_depsNoOpQ^gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_1_grad/ReshapeS^gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_1_grad/Reshape_1

agradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_1_grad/tuple/control_dependencyIdentityPgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_1_grad/ReshapeZ^gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_1_grad/tuple/group_deps*
T0*c
_classY
WUloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_1_grad/Reshape

cgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_1_grad/tuple/control_dependency_1IdentityRgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_1_grad/Reshape_1Z^gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_1_grad/tuple/group_deps*
T0*e
_class[
YWloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_1_grad/Reshape_1

gradients_4/AddN_2AddNHgradients_4/GAIL_model/gail_d_hidden_1/Mul_grad/tuple/control_dependency?gradients_4/GAIL_model/gail_d_hidden_1/Sigmoid_grad/SigmoidGrad*
T0*J
_class@
><loc:@gradients_4/GAIL_model/gail_d_hidden_1/Mul_grad/Reshape*
N

?gradients_4/GAIL_model/gail_d_hidden_1/BiasAdd_grad/BiasAddGradBiasAddGradgradients_4/AddN_2*
T0*
data_formatNHWC
£
Dgradients_4/GAIL_model/gail_d_hidden_1/BiasAdd_grad/tuple/group_depsNoOp^gradients_4/AddN_2@^gradients_4/GAIL_model/gail_d_hidden_1/BiasAdd_grad/BiasAddGrad

Lgradients_4/GAIL_model/gail_d_hidden_1/BiasAdd_grad/tuple/control_dependencyIdentitygradients_4/AddN_2E^gradients_4/GAIL_model/gail_d_hidden_1/BiasAdd_grad/tuple/group_deps*
T0*J
_class@
><loc:@gradients_4/GAIL_model/gail_d_hidden_1/Mul_grad/Reshape
æ
Ngradients_4/GAIL_model/gail_d_hidden_1/BiasAdd_grad/tuple/control_dependency_1Identity?gradients_4/GAIL_model/gail_d_hidden_1/BiasAdd_grad/BiasAddGradE^gradients_4/GAIL_model/gail_d_hidden_1/BiasAdd_grad/tuple/group_deps*
T0*R
_classH
FDloc:@gradients_4/GAIL_model/gail_d_hidden_1/BiasAdd_grad/BiasAddGrad
Ķ
Agradients_4/GAIL_model_1/gail_d_hidden_1/Sigmoid_grad/SigmoidGradSigmoidGrad$GAIL_model_1/gail_d_hidden_1/SigmoidLgradients_4/GAIL_model_1/gail_d_hidden_1/Mul_grad/tuple/control_dependency_1*
T0
Õ
gradients_4/AddN_3AddN_gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_grad/tuple/control_dependencycgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_1_grad/tuple/control_dependency_1*
T0*a
_classW
USloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_grad/Reshape*
N
Č
Sgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/MatMul_grad/MatMul_grad/MatMulMatMulgradients_4/AddN_3&GAIL_model/gail_d_hidden_2/kernel/read*
transpose_b( *
T0*
transpose_a( 
“
Ugradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/MatMul_grad/MatMul_grad/MatMul_1MatMulgradients_4/AddN_3gradients_3/AddN*
transpose_b( *
T0*
transpose_a(

]gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/MatMul_grad/MatMul_grad/tuple/group_depsNoOpT^gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/MatMul_grad/MatMul_grad/MatMulV^gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/MatMul_grad/MatMul_grad/MatMul_1

egradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/MatMul_grad/MatMul_grad/tuple/control_dependencyIdentitySgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/MatMul_grad/MatMul_grad/MatMul^^gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/MatMul_grad/MatMul_grad/tuple/group_deps*
T0*f
_class\
ZXloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/MatMul_grad/MatMul_grad/MatMul

ggradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/MatMul_grad/MatMul_grad/tuple/control_dependency_1IdentityUgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/MatMul_grad/MatMul_grad/MatMul_1^^gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/MatMul_grad/MatMul_grad/tuple/group_deps*
T0*h
_class^
\Zloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/MatMul_grad/MatMul_grad/MatMul_1
č
9gradients_4/GAIL_model/gail_d_hidden_1/MatMul_grad/MatMulMatMulLgradients_4/GAIL_model/gail_d_hidden_1/BiasAdd_grad/tuple/control_dependency&GAIL_model/gail_d_hidden_1/kernel/read*
transpose_b(*
T0*
transpose_a( 
Õ
;gradients_4/GAIL_model/gail_d_hidden_1/MatMul_grad/MatMul_1MatMulGAIL_model/concatLgradients_4/GAIL_model/gail_d_hidden_1/BiasAdd_grad/tuple/control_dependency*
transpose_b( *
T0*
transpose_a(
Å
Cgradients_4/GAIL_model/gail_d_hidden_1/MatMul_grad/tuple/group_depsNoOp:^gradients_4/GAIL_model/gail_d_hidden_1/MatMul_grad/MatMul<^gradients_4/GAIL_model/gail_d_hidden_1/MatMul_grad/MatMul_1
Æ
Kgradients_4/GAIL_model/gail_d_hidden_1/MatMul_grad/tuple/control_dependencyIdentity9gradients_4/GAIL_model/gail_d_hidden_1/MatMul_grad/MatMulD^gradients_4/GAIL_model/gail_d_hidden_1/MatMul_grad/tuple/group_deps*
T0*L
_classB
@>loc:@gradients_4/GAIL_model/gail_d_hidden_1/MatMul_grad/MatMul
µ
Mgradients_4/GAIL_model/gail_d_hidden_1/MatMul_grad/tuple/control_dependency_1Identity;gradients_4/GAIL_model/gail_d_hidden_1/MatMul_grad/MatMul_1D^gradients_4/GAIL_model/gail_d_hidden_1/MatMul_grad/tuple/group_deps*
T0*N
_classD
B@loc:@gradients_4/GAIL_model/gail_d_hidden_1/MatMul_grad/MatMul_1

gradients_4/AddN_4AddNJgradients_4/GAIL_model_1/gail_d_hidden_1/Mul_grad/tuple/control_dependencyAgradients_4/GAIL_model_1/gail_d_hidden_1/Sigmoid_grad/SigmoidGrad*
T0*L
_classB
@>loc:@gradients_4/GAIL_model_1/gail_d_hidden_1/Mul_grad/Reshape*
N

Agradients_4/GAIL_model_1/gail_d_hidden_1/BiasAdd_grad/BiasAddGradBiasAddGradgradients_4/AddN_4*
T0*
data_formatNHWC
§
Fgradients_4/GAIL_model_1/gail_d_hidden_1/BiasAdd_grad/tuple/group_depsNoOp^gradients_4/AddN_4B^gradients_4/GAIL_model_1/gail_d_hidden_1/BiasAdd_grad/BiasAddGrad

Ngradients_4/GAIL_model_1/gail_d_hidden_1/BiasAdd_grad/tuple/control_dependencyIdentitygradients_4/AddN_4G^gradients_4/GAIL_model_1/gail_d_hidden_1/BiasAdd_grad/tuple/group_deps*
T0*L
_classB
@>loc:@gradients_4/GAIL_model_1/gail_d_hidden_1/Mul_grad/Reshape
Ē
Pgradients_4/GAIL_model_1/gail_d_hidden_1/BiasAdd_grad/tuple/control_dependency_1IdentityAgradients_4/GAIL_model_1/gail_d_hidden_1/BiasAdd_grad/BiasAddGradG^gradients_4/GAIL_model_1/gail_d_hidden_1/BiasAdd_grad/tuple/group_deps*
T0*T
_classJ
HFloc:@gradients_4/GAIL_model_1/gail_d_hidden_1/BiasAdd_grad/BiasAddGrad
¢
2gradients_4/gradients_3/AddN_grad/tuple/group_depsNoOpf^gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/MatMul_grad/MatMul_grad/tuple/control_dependency
Ó
:gradients_4/gradients_3/AddN_grad/tuple/control_dependencyIdentityegradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/MatMul_grad/MatMul_grad/tuple/control_dependency3^gradients_4/gradients_3/AddN_grad/tuple/group_deps*
T0*f
_class\
ZXloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/MatMul_grad/MatMul_grad/MatMul
Õ
<gradients_4/gradients_3/AddN_grad/tuple/control_dependency_1Identityegradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/MatMul_grad/MatMul_grad/tuple/control_dependency3^gradients_4/gradients_3/AddN_grad/tuple/group_deps*
T0*f
_class\
ZXloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/MatMul_grad/MatMul_grad/MatMul
ģ
;gradients_4/GAIL_model_1/gail_d_hidden_1/MatMul_grad/MatMulMatMulNgradients_4/GAIL_model_1/gail_d_hidden_1/BiasAdd_grad/tuple/control_dependency&GAIL_model/gail_d_hidden_1/kernel/read*
transpose_b(*
T0*
transpose_a( 
Ū
=gradients_4/GAIL_model_1/gail_d_hidden_1/MatMul_grad/MatMul_1MatMulGAIL_model_1/concatNgradients_4/GAIL_model_1/gail_d_hidden_1/BiasAdd_grad/tuple/control_dependency*
transpose_b( *
T0*
transpose_a(
Ė
Egradients_4/GAIL_model_1/gail_d_hidden_1/MatMul_grad/tuple/group_depsNoOp<^gradients_4/GAIL_model_1/gail_d_hidden_1/MatMul_grad/MatMul>^gradients_4/GAIL_model_1/gail_d_hidden_1/MatMul_grad/MatMul_1
·
Mgradients_4/GAIL_model_1/gail_d_hidden_1/MatMul_grad/tuple/control_dependencyIdentity;gradients_4/GAIL_model_1/gail_d_hidden_1/MatMul_grad/MatMulF^gradients_4/GAIL_model_1/gail_d_hidden_1/MatMul_grad/tuple/group_deps*
T0*N
_classD
B@loc:@gradients_4/GAIL_model_1/gail_d_hidden_1/MatMul_grad/MatMul
½
Ogradients_4/GAIL_model_1/gail_d_hidden_1/MatMul_grad/tuple/control_dependency_1Identity=gradients_4/GAIL_model_1/gail_d_hidden_1/MatMul_grad/MatMul_1F^gradients_4/GAIL_model_1/gail_d_hidden_1/MatMul_grad/tuple/group_deps*
T0*P
_classF
DBloc:@gradients_4/GAIL_model_1/gail_d_hidden_1/MatMul_grad/MatMul_1
©
Pgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Reshape_grad/ShapeShape5gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum*
T0*
out_type0

Rgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Reshape_grad/ReshapeReshape:gradients_4/gradients_3/AddN_grad/tuple/control_dependencyPgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Reshape_grad/Shape*
T0*
Tshape0
į
Vgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Sigmoid_grad/SigmoidGrad_grad/mulMul<gradients_4/gradients_3/AddN_grad/tuple/control_dependency_1;gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Reshape_1*
T0
Ę
Zgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Sigmoid_grad/SigmoidGrad_grad/mul_1/xConst=^gradients_4/gradients_3/AddN_grad/tuple/control_dependency_1*
valueB
 *   @*
dtype0

Xgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Sigmoid_grad/SigmoidGrad_grad/mul_1MulZgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Sigmoid_grad/SigmoidGrad_grad/mul_1/xVgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Sigmoid_grad/SigmoidGrad_grad/mul*
T0
č
Xgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Sigmoid_grad/SigmoidGrad_grad/mul_2MulXgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Sigmoid_grad/SigmoidGrad_grad/mul_1$GAIL_model_2/gail_d_hidden_2/Sigmoid*
T0

Vgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Sigmoid_grad/SigmoidGrad_grad/subSubVgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Sigmoid_grad/SigmoidGrad_grad/mulXgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Sigmoid_grad/SigmoidGrad_grad/mul_2*
T0
Ś
^gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Sigmoid_grad/SigmoidGrad_grad/SigmoidGradSigmoidGrad$GAIL_model_2/gail_d_hidden_2/Sigmoid<gradients_4/gradients_3/AddN_grad/tuple/control_dependency_1*
T0
„
cgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Sigmoid_grad/SigmoidGrad_grad/tuple/group_depsNoOp_^gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Sigmoid_grad/SigmoidGrad_grad/SigmoidGradW^gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Sigmoid_grad/SigmoidGrad_grad/sub
©
kgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Sigmoid_grad/SigmoidGrad_grad/tuple/control_dependencyIdentityVgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Sigmoid_grad/SigmoidGrad_grad/subd^gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Sigmoid_grad/SigmoidGrad_grad/tuple/group_deps*
T0*i
_class_
][loc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Sigmoid_grad/SigmoidGrad_grad/sub
»
mgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Sigmoid_grad/SigmoidGrad_grad/tuple/control_dependency_1Identity^gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Sigmoid_grad/SigmoidGrad_grad/SigmoidGradd^gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Sigmoid_grad/SigmoidGrad_grad/tuple/group_deps*
T0*q
_classg
ecloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Sigmoid_grad/SigmoidGrad_grad/SigmoidGrad
„
Lgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/ShapeShape5gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul*
T0*
out_type0
Ö
Kgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/SizeConst*_
_classU
SQloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/Shape*
value	B :*
dtype0
Ó
Jgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/addAddV2Ggradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/BroadcastGradientArgsKgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/Size*
T0*_
_classU
SQloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/Shape
Ł
Jgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/modFloorModJgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/addKgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/Size*
T0*_
_classU
SQloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/Shape

Ngradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/Shape_1ShapeJgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/mod*
T0*_
_classU
SQloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/Shape*
out_type0
Ż
Rgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/range/startConst*_
_classU
SQloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/Shape*
value	B : *
dtype0
Ż
Rgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/range/deltaConst*_
_classU
SQloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/Shape*
value	B :*
dtype0
·
Lgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/rangeRangeRgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/range/startKgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/SizeRgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/range/delta*

Tidx0*_
_classU
SQloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/Shape
Ü
Qgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/Fill/valueConst*_
_classU
SQloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/Shape*
value	B :*
dtype0
ņ
Kgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/FillFillNgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/Shape_1Qgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/Fill/value*
T0*_
_classU
SQloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/Shape*

index_type0

Tgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/DynamicStitchDynamicStitchLgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/rangeJgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/modLgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/ShapeKgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/Fill*
T0*_
_classU
SQloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/Shape*
N
Ū
Pgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/Maximum/yConst*_
_classU
SQloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/Shape*
value	B :*
dtype0
ė
Ngradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/MaximumMaximumTgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/DynamicStitchPgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/Maximum/y*
T0*_
_classU
SQloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/Shape
ć
Ogradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/floordivFloorDivLgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/ShapeNgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/Maximum*
T0*_
_classU
SQloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/Shape

Ngradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/ReshapeReshapeRgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Reshape_grad/ReshapeTgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/DynamicStitch*
T0*
Tshape0

Kgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/TileTileNgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/ReshapeOgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/floordiv*

Tmultiples0*
T0
­
Rgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Reshape_1_grad/ShapeShape7gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1*
T0*
out_type0
¹
Tgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Reshape_1_grad/ReshapeReshapemgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Sigmoid_grad/SigmoidGrad_grad/tuple/control_dependency_1Rgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Reshape_1_grad/Shape*
T0*
Tshape0
«
Lgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_grad/ShapeShape;gradients_3/GAIL_model_2/gail_d_estimate/MatMul_grad/MatMul*
T0*
out_type0

Ngradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_grad/Shape_1Shape$GAIL_model_2/gail_d_hidden_2/Sigmoid*
T0*
out_type0

\gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_grad/BroadcastGradientArgsBroadcastGradientArgsLgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_grad/ShapeNgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_grad/Shape_1*
T0
Ķ
Jgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_grad/MulMulKgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/Tile$GAIL_model_2/gail_d_hidden_2/Sigmoid*
T0
”
Jgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_grad/SumSumJgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_grad/Mul\gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0

Ngradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_grad/ReshapeReshapeJgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_grad/SumLgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_grad/Shape*
T0*
Tshape0
ę
Lgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_grad/Mul_1Mul;gradients_3/GAIL_model_2/gail_d_estimate/MatMul_grad/MatMulKgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_grad/Tile*
T0
§
Lgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_grad/Sum_1SumLgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_grad/Mul_1^gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0

Pgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_grad/Reshape_1ReshapeLgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_grad/Sum_1Ngradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_grad/Shape_1*
T0*
Tshape0

Wgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_grad/tuple/group_depsNoOpO^gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_grad/ReshapeQ^gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_grad/Reshape_1

_gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_grad/tuple/control_dependencyIdentityNgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_grad/ReshapeX^gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_grad/tuple/group_deps*
T0*a
_classW
USloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_grad/Reshape

agradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_grad/tuple/control_dependency_1IdentityPgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_grad/Reshape_1X^gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_grad/tuple/group_deps*
T0*c
_classY
WUloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_grad/Reshape_1
©
Ngradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/ShapeShape7gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_1*
T0*
out_type0
Ś
Mgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/SizeConst*a
_classW
USloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/Shape*
value	B :*
dtype0
Ū
Lgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/addAddV2Igradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/BroadcastGradientArgs:1Mgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/Size*
T0*a
_classW
USloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/Shape
į
Lgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/modFloorModLgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/addMgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/Size*
T0*a
_classW
USloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/Shape
£
Pgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/Shape_1ShapeLgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/mod*
T0*a
_classW
USloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/Shape*
out_type0
į
Tgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/range/startConst*a
_classW
USloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/Shape*
value	B : *
dtype0
į
Tgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/range/deltaConst*a
_classW
USloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/Shape*
value	B :*
dtype0
Į
Ngradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/rangeRangeTgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/range/startMgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/SizeTgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/range/delta*

Tidx0*a
_classW
USloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/Shape
ą
Sgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/Fill/valueConst*a
_classW
USloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/Shape*
value	B :*
dtype0
ś
Mgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/FillFillPgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/Shape_1Sgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/Fill/value*
T0*a
_classW
USloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/Shape*

index_type0

Vgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/DynamicStitchDynamicStitchNgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/rangeLgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/modNgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/ShapeMgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/Fill*
T0*a
_classW
USloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/Shape*
N
ß
Rgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/Maximum/yConst*a
_classW
USloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/Shape*
value	B :*
dtype0
ó
Pgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/MaximumMaximumVgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/DynamicStitchRgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/Maximum/y*
T0*a
_classW
USloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/Shape
ė
Qgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/floordivFloorDivNgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/ShapePgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/Maximum*
T0*a
_classW
USloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/Shape
 
Pgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/ReshapeReshapeTgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Reshape_1_grad/ReshapeVgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/DynamicStitch*
T0*
Tshape0

Mgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/TileTilePgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/ReshapeQgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/floordiv*

Tmultiples0*
T0

Ngradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_1_grad/ShapeShape$GAIL_model_2/gail_d_hidden_2/BiasAdd*
T0*
out_type0
Æ
Pgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_1_grad/Shape_1Shape;gradients_3/GAIL_model_2/gail_d_estimate/MatMul_grad/MatMul*
T0*
out_type0
¢
^gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_1_grad/BroadcastGradientArgsBroadcastGradientArgsNgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_1_grad/ShapePgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_1_grad/Shape_1*
T0
č
Lgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_1_grad/MulMulMgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/Tile;gradients_3/GAIL_model_2/gail_d_estimate/MatMul_grad/MatMul*
T0
§
Lgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_1_grad/SumSumLgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_1_grad/Mul^gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_1_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0

Pgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_1_grad/ReshapeReshapeLgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_1_grad/SumNgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_1_grad/Shape*
T0*
Tshape0
Ó
Ngradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_1_grad/Mul_1Mul$GAIL_model_2/gail_d_hidden_2/BiasAddMgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1_grad/Tile*
T0
­
Ngradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_1_grad/Sum_1SumNgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_1_grad/Mul_1`gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_1_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0

Rgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_1_grad/Reshape_1ReshapeNgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_1_grad/Sum_1Pgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_1_grad/Shape_1*
T0*
Tshape0

Ygradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_1_grad/tuple/group_depsNoOpQ^gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_1_grad/ReshapeS^gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_1_grad/Reshape_1

agradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_1_grad/tuple/control_dependencyIdentityPgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_1_grad/ReshapeZ^gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_1_grad/tuple/group_deps*
T0*c
_classY
WUloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_1_grad/Reshape

cgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_1_grad/tuple/control_dependency_1IdentityRgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_1_grad/Reshape_1Z^gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_1_grad/tuple/group_deps*
T0*e
_class[
YWloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_1_grad/Reshape_1
Õ
gradients_4/AddN_5AddN_gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_grad/tuple/control_dependencycgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_1_grad/tuple/control_dependency_1*
T0*a
_classW
USloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_grad/Reshape*
N
Č
Sgradients_4/gradients_3/GAIL_model_2/gail_d_estimate/MatMul_grad/MatMul_grad/MatMulMatMulgradients_4/AddN_5&GAIL_model/gail_d_estimate/kernel/read*
transpose_b( *
T0*
transpose_a( 
å
Ugradients_4/gradients_3/GAIL_model_2/gail_d_estimate/MatMul_grad/MatMul_grad/MatMul_1MatMulgradients_4/AddN_5Agradients_3/GAIL_model_2/gail_d_estimate/Sigmoid_grad/SigmoidGrad*
transpose_b( *
T0*
transpose_a(

]gradients_4/gradients_3/GAIL_model_2/gail_d_estimate/MatMul_grad/MatMul_grad/tuple/group_depsNoOpT^gradients_4/gradients_3/GAIL_model_2/gail_d_estimate/MatMul_grad/MatMul_grad/MatMulV^gradients_4/gradients_3/GAIL_model_2/gail_d_estimate/MatMul_grad/MatMul_grad/MatMul_1

egradients_4/gradients_3/GAIL_model_2/gail_d_estimate/MatMul_grad/MatMul_grad/tuple/control_dependencyIdentitySgradients_4/gradients_3/GAIL_model_2/gail_d_estimate/MatMul_grad/MatMul_grad/MatMul^^gradients_4/gradients_3/GAIL_model_2/gail_d_estimate/MatMul_grad/MatMul_grad/tuple/group_deps*
T0*f
_class\
ZXloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_estimate/MatMul_grad/MatMul_grad/MatMul

ggradients_4/gradients_3/GAIL_model_2/gail_d_estimate/MatMul_grad/MatMul_grad/tuple/control_dependency_1IdentityUgradients_4/gradients_3/GAIL_model_2/gail_d_estimate/MatMul_grad/MatMul_grad/MatMul_1^^gradients_4/gradients_3/GAIL_model_2/gail_d_estimate/MatMul_grad/MatMul_grad/tuple/group_deps*
T0*h
_class^
\Zloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_estimate/MatMul_grad/MatMul_grad/MatMul_1
ß
Vgradients_4/gradients_3/GAIL_model_2/gail_d_estimate/Sigmoid_grad/SigmoidGrad_grad/mulMulegradients_4/gradients_3/GAIL_model_2/gail_d_estimate/MatMul_grad/MatMul_grad/tuple/control_dependencygradients_3/Fill*
T0
ļ
Zgradients_4/gradients_3/GAIL_model_2/gail_d_estimate/Sigmoid_grad/SigmoidGrad_grad/mul_1/xConstf^gradients_4/gradients_3/GAIL_model_2/gail_d_estimate/MatMul_grad/MatMul_grad/tuple/control_dependency*
valueB
 *   @*
dtype0

Xgradients_4/gradients_3/GAIL_model_2/gail_d_estimate/Sigmoid_grad/SigmoidGrad_grad/mul_1MulZgradients_4/gradients_3/GAIL_model_2/gail_d_estimate/Sigmoid_grad/SigmoidGrad_grad/mul_1/xVgradients_4/gradients_3/GAIL_model_2/gail_d_estimate/Sigmoid_grad/SigmoidGrad_grad/mul*
T0
č
Xgradients_4/gradients_3/GAIL_model_2/gail_d_estimate/Sigmoid_grad/SigmoidGrad_grad/mul_2MulXgradients_4/gradients_3/GAIL_model_2/gail_d_estimate/Sigmoid_grad/SigmoidGrad_grad/mul_1$GAIL_model_2/gail_d_estimate/Sigmoid*
T0

Vgradients_4/gradients_3/GAIL_model_2/gail_d_estimate/Sigmoid_grad/SigmoidGrad_grad/subSubVgradients_4/gradients_3/GAIL_model_2/gail_d_estimate/Sigmoid_grad/SigmoidGrad_grad/mulXgradients_4/gradients_3/GAIL_model_2/gail_d_estimate/Sigmoid_grad/SigmoidGrad_grad/mul_2*
T0

^gradients_4/gradients_3/GAIL_model_2/gail_d_estimate/Sigmoid_grad/SigmoidGrad_grad/SigmoidGradSigmoidGrad$GAIL_model_2/gail_d_estimate/Sigmoidegradients_4/gradients_3/GAIL_model_2/gail_d_estimate/MatMul_grad/MatMul_grad/tuple/control_dependency*
T0
„
cgradients_4/gradients_3/GAIL_model_2/gail_d_estimate/Sigmoid_grad/SigmoidGrad_grad/tuple/group_depsNoOp_^gradients_4/gradients_3/GAIL_model_2/gail_d_estimate/Sigmoid_grad/SigmoidGrad_grad/SigmoidGradW^gradients_4/gradients_3/GAIL_model_2/gail_d_estimate/Sigmoid_grad/SigmoidGrad_grad/sub
©
kgradients_4/gradients_3/GAIL_model_2/gail_d_estimate/Sigmoid_grad/SigmoidGrad_grad/tuple/control_dependencyIdentityVgradients_4/gradients_3/GAIL_model_2/gail_d_estimate/Sigmoid_grad/SigmoidGrad_grad/subd^gradients_4/gradients_3/GAIL_model_2/gail_d_estimate/Sigmoid_grad/SigmoidGrad_grad/tuple/group_deps*
T0*i
_class_
][loc:@gradients_4/gradients_3/GAIL_model_2/gail_d_estimate/Sigmoid_grad/SigmoidGrad_grad/sub
»
mgradients_4/gradients_3/GAIL_model_2/gail_d_estimate/Sigmoid_grad/SigmoidGrad_grad/tuple/control_dependency_1Identity^gradients_4/gradients_3/GAIL_model_2/gail_d_estimate/Sigmoid_grad/SigmoidGrad_grad/SigmoidGradd^gradients_4/gradients_3/GAIL_model_2/gail_d_estimate/Sigmoid_grad/SigmoidGrad_grad/tuple/group_deps*
T0*q
_classg
ecloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_estimate/Sigmoid_grad/SigmoidGrad_grad/SigmoidGrad
ģ
Agradients_4/GAIL_model_2/gail_d_estimate/Sigmoid_grad/SigmoidGradSigmoidGrad$GAIL_model_2/gail_d_estimate/Sigmoidkgradients_4/gradients_3/GAIL_model_2/gail_d_estimate/Sigmoid_grad/SigmoidGrad_grad/tuple/control_dependency*
T0
³
Agradients_4/GAIL_model_2/gail_d_estimate/BiasAdd_grad/BiasAddGradBiasAddGradAgradients_4/GAIL_model_2/gail_d_estimate/Sigmoid_grad/SigmoidGrad*
T0*
data_formatNHWC
Ö
Fgradients_4/GAIL_model_2/gail_d_estimate/BiasAdd_grad/tuple/group_depsNoOpB^gradients_4/GAIL_model_2/gail_d_estimate/BiasAdd_grad/BiasAddGradB^gradients_4/GAIL_model_2/gail_d_estimate/Sigmoid_grad/SigmoidGrad
Å
Ngradients_4/GAIL_model_2/gail_d_estimate/BiasAdd_grad/tuple/control_dependencyIdentityAgradients_4/GAIL_model_2/gail_d_estimate/Sigmoid_grad/SigmoidGradG^gradients_4/GAIL_model_2/gail_d_estimate/BiasAdd_grad/tuple/group_deps*
T0*T
_classJ
HFloc:@gradients_4/GAIL_model_2/gail_d_estimate/Sigmoid_grad/SigmoidGrad
Ē
Pgradients_4/GAIL_model_2/gail_d_estimate/BiasAdd_grad/tuple/control_dependency_1IdentityAgradients_4/GAIL_model_2/gail_d_estimate/BiasAdd_grad/BiasAddGradG^gradients_4/GAIL_model_2/gail_d_estimate/BiasAdd_grad/tuple/group_deps*
T0*T
_classJ
HFloc:@gradients_4/GAIL_model_2/gail_d_estimate/BiasAdd_grad/BiasAddGrad
ģ
;gradients_4/GAIL_model_2/gail_d_estimate/MatMul_grad/MatMulMatMulNgradients_4/GAIL_model_2/gail_d_estimate/BiasAdd_grad/tuple/control_dependency&GAIL_model/gail_d_estimate/kernel/read*
transpose_b(*
T0*
transpose_a( 
č
=gradients_4/GAIL_model_2/gail_d_estimate/MatMul_grad/MatMul_1MatMul GAIL_model_2/gail_d_hidden_2/MulNgradients_4/GAIL_model_2/gail_d_estimate/BiasAdd_grad/tuple/control_dependency*
transpose_b( *
T0*
transpose_a(
Ė
Egradients_4/GAIL_model_2/gail_d_estimate/MatMul_grad/tuple/group_depsNoOp<^gradients_4/GAIL_model_2/gail_d_estimate/MatMul_grad/MatMul>^gradients_4/GAIL_model_2/gail_d_estimate/MatMul_grad/MatMul_1
·
Mgradients_4/GAIL_model_2/gail_d_estimate/MatMul_grad/tuple/control_dependencyIdentity;gradients_4/GAIL_model_2/gail_d_estimate/MatMul_grad/MatMulF^gradients_4/GAIL_model_2/gail_d_estimate/MatMul_grad/tuple/group_deps*
T0*N
_classD
B@loc:@gradients_4/GAIL_model_2/gail_d_estimate/MatMul_grad/MatMul
½
Ogradients_4/GAIL_model_2/gail_d_estimate/MatMul_grad/tuple/control_dependency_1Identity=gradients_4/GAIL_model_2/gail_d_estimate/MatMul_grad/MatMul_1F^gradients_4/GAIL_model_2/gail_d_estimate/MatMul_grad/tuple/group_deps*
T0*P
_classF
DBloc:@gradients_4/GAIL_model_2/gail_d_estimate/MatMul_grad/MatMul_1
ō
gradients_4/AddN_6AddNNgradients_4/GAIL_model/gail_d_estimate/BiasAdd_grad/tuple/control_dependency_1Pgradients_4/GAIL_model_1/gail_d_estimate/BiasAdd_grad/tuple/control_dependency_1Pgradients_4/GAIL_model_2/gail_d_estimate/BiasAdd_grad/tuple/control_dependency_1*
T0*R
_classH
FDloc:@gradients_4/GAIL_model/gail_d_estimate/BiasAdd_grad/BiasAddGrad*
N

7gradients_4/GAIL_model_2/gail_d_hidden_2/Mul_grad/ShapeShape$GAIL_model_2/gail_d_hidden_2/BiasAdd*
T0*
out_type0

9gradients_4/GAIL_model_2/gail_d_hidden_2/Mul_grad/Shape_1Shape$GAIL_model_2/gail_d_hidden_2/Sigmoid*
T0*
out_type0
Ż
Ggradients_4/GAIL_model_2/gail_d_hidden_2/Mul_grad/BroadcastGradientArgsBroadcastGradientArgs7gradients_4/GAIL_model_2/gail_d_hidden_2/Mul_grad/Shape9gradients_4/GAIL_model_2/gail_d_hidden_2/Mul_grad/Shape_1*
T0
ŗ
5gradients_4/GAIL_model_2/gail_d_hidden_2/Mul_grad/MulMulMgradients_4/GAIL_model_2/gail_d_estimate/MatMul_grad/tuple/control_dependency$GAIL_model_2/gail_d_hidden_2/Sigmoid*
T0
ā
5gradients_4/GAIL_model_2/gail_d_hidden_2/Mul_grad/SumSum5gradients_4/GAIL_model_2/gail_d_hidden_2/Mul_grad/MulGgradients_4/GAIL_model_2/gail_d_hidden_2/Mul_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0
Ė
9gradients_4/GAIL_model_2/gail_d_hidden_2/Mul_grad/ReshapeReshape5gradients_4/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum7gradients_4/GAIL_model_2/gail_d_hidden_2/Mul_grad/Shape*
T0*
Tshape0
¼
7gradients_4/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_1Mul$GAIL_model_2/gail_d_hidden_2/BiasAddMgradients_4/GAIL_model_2/gail_d_estimate/MatMul_grad/tuple/control_dependency*
T0
č
7gradients_4/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_1Sum7gradients_4/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_1Igradients_4/GAIL_model_2/gail_d_hidden_2/Mul_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0
Ń
;gradients_4/GAIL_model_2/gail_d_hidden_2/Mul_grad/Reshape_1Reshape7gradients_4/GAIL_model_2/gail_d_hidden_2/Mul_grad/Sum_19gradients_4/GAIL_model_2/gail_d_hidden_2/Mul_grad/Shape_1*
T0*
Tshape0
Ä
Bgradients_4/GAIL_model_2/gail_d_hidden_2/Mul_grad/tuple/group_depsNoOp:^gradients_4/GAIL_model_2/gail_d_hidden_2/Mul_grad/Reshape<^gradients_4/GAIL_model_2/gail_d_hidden_2/Mul_grad/Reshape_1
­
Jgradients_4/GAIL_model_2/gail_d_hidden_2/Mul_grad/tuple/control_dependencyIdentity9gradients_4/GAIL_model_2/gail_d_hidden_2/Mul_grad/ReshapeC^gradients_4/GAIL_model_2/gail_d_hidden_2/Mul_grad/tuple/group_deps*
T0*L
_classB
@>loc:@gradients_4/GAIL_model_2/gail_d_hidden_2/Mul_grad/Reshape
³
Lgradients_4/GAIL_model_2/gail_d_hidden_2/Mul_grad/tuple/control_dependency_1Identity;gradients_4/GAIL_model_2/gail_d_hidden_2/Mul_grad/Reshape_1C^gradients_4/GAIL_model_2/gail_d_hidden_2/Mul_grad/tuple/group_deps*
T0*N
_classD
B@loc:@gradients_4/GAIL_model_2/gail_d_hidden_2/Mul_grad/Reshape_1
Ö
gradients_4/AddN_7AddNMgradients_4/GAIL_model/gail_d_estimate/MatMul_grad/tuple/control_dependency_1Ogradients_4/GAIL_model_1/gail_d_estimate/MatMul_grad/tuple/control_dependency_1ggradients_4/gradients_3/GAIL_model_2/gail_d_estimate/MatMul_grad/MatMul_grad/tuple/control_dependency_1Ogradients_4/GAIL_model_2/gail_d_estimate/MatMul_grad/tuple/control_dependency_1*
T0*N
_classD
B@loc:@gradients_4/GAIL_model/gail_d_estimate/MatMul_grad/MatMul_1*
N
µ
gradients_4/AddN_8AddNkgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Sigmoid_grad/SigmoidGrad_grad/tuple/control_dependencyagradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_grad/tuple/control_dependency_1Lgradients_4/GAIL_model_2/gail_d_hidden_2/Mul_grad/tuple/control_dependency_1*
T0*i
_class_
][loc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Sigmoid_grad/SigmoidGrad_grad/sub*
N

Agradients_4/GAIL_model_2/gail_d_hidden_2/Sigmoid_grad/SigmoidGradSigmoidGrad$GAIL_model_2/gail_d_hidden_2/Sigmoidgradients_4/AddN_8*
T0

gradients_4/AddN_9AddNagradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_1_grad/tuple/control_dependencyJgradients_4/GAIL_model_2/gail_d_hidden_2/Mul_grad/tuple/control_dependencyAgradients_4/GAIL_model_2/gail_d_hidden_2/Sigmoid_grad/SigmoidGrad*
T0*c
_classY
WUloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_1_grad/Reshape*
N

Agradients_4/GAIL_model_2/gail_d_hidden_2/BiasAdd_grad/BiasAddGradBiasAddGradgradients_4/AddN_9*
T0*
data_formatNHWC
§
Fgradients_4/GAIL_model_2/gail_d_hidden_2/BiasAdd_grad/tuple/group_depsNoOp^gradients_4/AddN_9B^gradients_4/GAIL_model_2/gail_d_hidden_2/BiasAdd_grad/BiasAddGrad
„
Ngradients_4/GAIL_model_2/gail_d_hidden_2/BiasAdd_grad/tuple/control_dependencyIdentitygradients_4/AddN_9G^gradients_4/GAIL_model_2/gail_d_hidden_2/BiasAdd_grad/tuple/group_deps*
T0*c
_classY
WUloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/Mul_grad/Mul_1_grad/Reshape
Ē
Pgradients_4/GAIL_model_2/gail_d_hidden_2/BiasAdd_grad/tuple/control_dependency_1IdentityAgradients_4/GAIL_model_2/gail_d_hidden_2/BiasAdd_grad/BiasAddGradG^gradients_4/GAIL_model_2/gail_d_hidden_2/BiasAdd_grad/tuple/group_deps*
T0*T
_classJ
HFloc:@gradients_4/GAIL_model_2/gail_d_hidden_2/BiasAdd_grad/BiasAddGrad
ģ
;gradients_4/GAIL_model_2/gail_d_hidden_2/MatMul_grad/MatMulMatMulNgradients_4/GAIL_model_2/gail_d_hidden_2/BiasAdd_grad/tuple/control_dependency&GAIL_model/gail_d_hidden_2/kernel/read*
transpose_b(*
T0*
transpose_a( 
č
=gradients_4/GAIL_model_2/gail_d_hidden_2/MatMul_grad/MatMul_1MatMul GAIL_model_2/gail_d_hidden_1/MulNgradients_4/GAIL_model_2/gail_d_hidden_2/BiasAdd_grad/tuple/control_dependency*
transpose_b( *
T0*
transpose_a(
Ė
Egradients_4/GAIL_model_2/gail_d_hidden_2/MatMul_grad/tuple/group_depsNoOp<^gradients_4/GAIL_model_2/gail_d_hidden_2/MatMul_grad/MatMul>^gradients_4/GAIL_model_2/gail_d_hidden_2/MatMul_grad/MatMul_1
·
Mgradients_4/GAIL_model_2/gail_d_hidden_2/MatMul_grad/tuple/control_dependencyIdentity;gradients_4/GAIL_model_2/gail_d_hidden_2/MatMul_grad/MatMulF^gradients_4/GAIL_model_2/gail_d_hidden_2/MatMul_grad/tuple/group_deps*
T0*N
_classD
B@loc:@gradients_4/GAIL_model_2/gail_d_hidden_2/MatMul_grad/MatMul
½
Ogradients_4/GAIL_model_2/gail_d_hidden_2/MatMul_grad/tuple/control_dependency_1Identity=gradients_4/GAIL_model_2/gail_d_hidden_2/MatMul_grad/MatMul_1F^gradients_4/GAIL_model_2/gail_d_hidden_2/MatMul_grad/tuple/group_deps*
T0*P
_classF
DBloc:@gradients_4/GAIL_model_2/gail_d_hidden_2/MatMul_grad/MatMul_1
õ
gradients_4/AddN_10AddNNgradients_4/GAIL_model/gail_d_hidden_2/BiasAdd_grad/tuple/control_dependency_1Pgradients_4/GAIL_model_1/gail_d_hidden_2/BiasAdd_grad/tuple/control_dependency_1Pgradients_4/GAIL_model_2/gail_d_hidden_2/BiasAdd_grad/tuple/control_dependency_1*
T0*R
_classH
FDloc:@gradients_4/GAIL_model/gail_d_hidden_2/BiasAdd_grad/BiasAddGrad*
N

7gradients_4/GAIL_model_2/gail_d_hidden_1/Mul_grad/ShapeShape$GAIL_model_2/gail_d_hidden_1/BiasAdd*
T0*
out_type0

9gradients_4/GAIL_model_2/gail_d_hidden_1/Mul_grad/Shape_1Shape$GAIL_model_2/gail_d_hidden_1/Sigmoid*
T0*
out_type0
Ż
Ggradients_4/GAIL_model_2/gail_d_hidden_1/Mul_grad/BroadcastGradientArgsBroadcastGradientArgs7gradients_4/GAIL_model_2/gail_d_hidden_1/Mul_grad/Shape9gradients_4/GAIL_model_2/gail_d_hidden_1/Mul_grad/Shape_1*
T0
ŗ
5gradients_4/GAIL_model_2/gail_d_hidden_1/Mul_grad/MulMulMgradients_4/GAIL_model_2/gail_d_hidden_2/MatMul_grad/tuple/control_dependency$GAIL_model_2/gail_d_hidden_1/Sigmoid*
T0
ā
5gradients_4/GAIL_model_2/gail_d_hidden_1/Mul_grad/SumSum5gradients_4/GAIL_model_2/gail_d_hidden_1/Mul_grad/MulGgradients_4/GAIL_model_2/gail_d_hidden_1/Mul_grad/BroadcastGradientArgs*

Tidx0*
	keep_dims( *
T0
Ė
9gradients_4/GAIL_model_2/gail_d_hidden_1/Mul_grad/ReshapeReshape5gradients_4/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum7gradients_4/GAIL_model_2/gail_d_hidden_1/Mul_grad/Shape*
T0*
Tshape0
¼
7gradients_4/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_1Mul$GAIL_model_2/gail_d_hidden_1/BiasAddMgradients_4/GAIL_model_2/gail_d_hidden_2/MatMul_grad/tuple/control_dependency*
T0
č
7gradients_4/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_1Sum7gradients_4/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_1Igradients_4/GAIL_model_2/gail_d_hidden_1/Mul_grad/BroadcastGradientArgs:1*

Tidx0*
	keep_dims( *
T0
Ń
;gradients_4/GAIL_model_2/gail_d_hidden_1/Mul_grad/Reshape_1Reshape7gradients_4/GAIL_model_2/gail_d_hidden_1/Mul_grad/Sum_19gradients_4/GAIL_model_2/gail_d_hidden_1/Mul_grad/Shape_1*
T0*
Tshape0
Ä
Bgradients_4/GAIL_model_2/gail_d_hidden_1/Mul_grad/tuple/group_depsNoOp:^gradients_4/GAIL_model_2/gail_d_hidden_1/Mul_grad/Reshape<^gradients_4/GAIL_model_2/gail_d_hidden_1/Mul_grad/Reshape_1
­
Jgradients_4/GAIL_model_2/gail_d_hidden_1/Mul_grad/tuple/control_dependencyIdentity9gradients_4/GAIL_model_2/gail_d_hidden_1/Mul_grad/ReshapeC^gradients_4/GAIL_model_2/gail_d_hidden_1/Mul_grad/tuple/group_deps*
T0*L
_classB
@>loc:@gradients_4/GAIL_model_2/gail_d_hidden_1/Mul_grad/Reshape
³
Lgradients_4/GAIL_model_2/gail_d_hidden_1/Mul_grad/tuple/control_dependency_1Identity;gradients_4/GAIL_model_2/gail_d_hidden_1/Mul_grad/Reshape_1C^gradients_4/GAIL_model_2/gail_d_hidden_1/Mul_grad/tuple/group_deps*
T0*N
_classD
B@loc:@gradients_4/GAIL_model_2/gail_d_hidden_1/Mul_grad/Reshape_1
×
gradients_4/AddN_11AddNMgradients_4/GAIL_model/gail_d_hidden_2/MatMul_grad/tuple/control_dependency_1Ogradients_4/GAIL_model_1/gail_d_hidden_2/MatMul_grad/tuple/control_dependency_1ggradients_4/gradients_3/GAIL_model_2/gail_d_hidden_2/MatMul_grad/MatMul_grad/tuple/control_dependency_1Ogradients_4/GAIL_model_2/gail_d_hidden_2/MatMul_grad/tuple/control_dependency_1*
T0*N
_classD
B@loc:@gradients_4/GAIL_model/gail_d_hidden_2/MatMul_grad/MatMul_1*
N
¶
gradients_4/AddN_12AddNkgradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Sigmoid_grad/SigmoidGrad_grad/tuple/control_dependencyagradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_grad/tuple/control_dependency_1Lgradients_4/GAIL_model_2/gail_d_hidden_1/Mul_grad/tuple/control_dependency_1*
T0*i
_class_
][loc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Sigmoid_grad/SigmoidGrad_grad/sub*
N

Agradients_4/GAIL_model_2/gail_d_hidden_1/Sigmoid_grad/SigmoidGradSigmoidGrad$GAIL_model_2/gail_d_hidden_1/Sigmoidgradients_4/AddN_12*
T0

gradients_4/AddN_13AddNagradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_1_grad/tuple/control_dependencyJgradients_4/GAIL_model_2/gail_d_hidden_1/Mul_grad/tuple/control_dependencyAgradients_4/GAIL_model_2/gail_d_hidden_1/Sigmoid_grad/SigmoidGrad*
T0*c
_classY
WUloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_1_grad/Reshape*
N

Agradients_4/GAIL_model_2/gail_d_hidden_1/BiasAdd_grad/BiasAddGradBiasAddGradgradients_4/AddN_13*
T0*
data_formatNHWC
Ø
Fgradients_4/GAIL_model_2/gail_d_hidden_1/BiasAdd_grad/tuple/group_depsNoOp^gradients_4/AddN_13B^gradients_4/GAIL_model_2/gail_d_hidden_1/BiasAdd_grad/BiasAddGrad
¦
Ngradients_4/GAIL_model_2/gail_d_hidden_1/BiasAdd_grad/tuple/control_dependencyIdentitygradients_4/AddN_13G^gradients_4/GAIL_model_2/gail_d_hidden_1/BiasAdd_grad/tuple/group_deps*
T0*c
_classY
WUloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/Mul_grad/Mul_1_grad/Reshape
Ē
Pgradients_4/GAIL_model_2/gail_d_hidden_1/BiasAdd_grad/tuple/control_dependency_1IdentityAgradients_4/GAIL_model_2/gail_d_hidden_1/BiasAdd_grad/BiasAddGradG^gradients_4/GAIL_model_2/gail_d_hidden_1/BiasAdd_grad/tuple/group_deps*
T0*T
_classJ
HFloc:@gradients_4/GAIL_model_2/gail_d_hidden_1/BiasAdd_grad/BiasAddGrad
ģ
;gradients_4/GAIL_model_2/gail_d_hidden_1/MatMul_grad/MatMulMatMulNgradients_4/GAIL_model_2/gail_d_hidden_1/BiasAdd_grad/tuple/control_dependency&GAIL_model/gail_d_hidden_1/kernel/read*
transpose_b(*
T0*
transpose_a( 
Ū
=gradients_4/GAIL_model_2/gail_d_hidden_1/MatMul_grad/MatMul_1MatMulGAIL_model_2/concatNgradients_4/GAIL_model_2/gail_d_hidden_1/BiasAdd_grad/tuple/control_dependency*
transpose_b( *
T0*
transpose_a(
Ė
Egradients_4/GAIL_model_2/gail_d_hidden_1/MatMul_grad/tuple/group_depsNoOp<^gradients_4/GAIL_model_2/gail_d_hidden_1/MatMul_grad/MatMul>^gradients_4/GAIL_model_2/gail_d_hidden_1/MatMul_grad/MatMul_1
·
Mgradients_4/GAIL_model_2/gail_d_hidden_1/MatMul_grad/tuple/control_dependencyIdentity;gradients_4/GAIL_model_2/gail_d_hidden_1/MatMul_grad/MatMulF^gradients_4/GAIL_model_2/gail_d_hidden_1/MatMul_grad/tuple/group_deps*
T0*N
_classD
B@loc:@gradients_4/GAIL_model_2/gail_d_hidden_1/MatMul_grad/MatMul
½
Ogradients_4/GAIL_model_2/gail_d_hidden_1/MatMul_grad/tuple/control_dependency_1Identity=gradients_4/GAIL_model_2/gail_d_hidden_1/MatMul_grad/MatMul_1F^gradients_4/GAIL_model_2/gail_d_hidden_1/MatMul_grad/tuple/group_deps*
T0*P
_classF
DBloc:@gradients_4/GAIL_model_2/gail_d_hidden_1/MatMul_grad/MatMul_1
õ
gradients_4/AddN_14AddNNgradients_4/GAIL_model/gail_d_hidden_1/BiasAdd_grad/tuple/control_dependency_1Pgradients_4/GAIL_model_1/gail_d_hidden_1/BiasAdd_grad/tuple/control_dependency_1Pgradients_4/GAIL_model_2/gail_d_hidden_1/BiasAdd_grad/tuple/control_dependency_1*
T0*R
_classH
FDloc:@gradients_4/GAIL_model/gail_d_hidden_1/BiasAdd_grad/BiasAddGrad*
N
ń
gradients_4/AddN_15AddNggradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/MatMul_grad/MatMul_grad/tuple/control_dependency_1Mgradients_4/GAIL_model/gail_d_hidden_1/MatMul_grad/tuple/control_dependency_1Ogradients_4/GAIL_model_1/gail_d_hidden_1/MatMul_grad/tuple/control_dependency_1Ogradients_4/GAIL_model_2/gail_d_hidden_1/MatMul_grad/tuple/control_dependency_1*
T0*h
_class^
\Zloc:@gradients_4/gradients_3/GAIL_model_2/gail_d_hidden_1/MatMul_grad/MatMul_grad/MatMul_1*
N
|
beta1_power_3/initial_valueConst*2
_class(
&$loc:@GAIL_model/gail_d_estimate/bias*
valueB
 *fff?*
dtype0

beta1_power_3
VariableV2*
shape: *
shared_name *2
_class(
&$loc:@GAIL_model/gail_d_estimate/bias*
dtype0*
	container 
°
beta1_power_3/AssignAssignbeta1_power_3beta1_power_3/initial_value*
use_locking(*
T0*2
_class(
&$loc:@GAIL_model/gail_d_estimate/bias*
validate_shape(
j
beta1_power_3/readIdentitybeta1_power_3*
T0*2
_class(
&$loc:@GAIL_model/gail_d_estimate/bias
|
beta2_power_3/initial_valueConst*2
_class(
&$loc:@GAIL_model/gail_d_estimate/bias*
valueB
 *w¾?*
dtype0

beta2_power_3
VariableV2*
shape: *
shared_name *2
_class(
&$loc:@GAIL_model/gail_d_estimate/bias*
dtype0*
	container 
°
beta2_power_3/AssignAssignbeta2_power_3beta2_power_3/initial_value*
use_locking(*
T0*2
_class(
&$loc:@GAIL_model/gail_d_estimate/bias*
validate_shape(
j
beta2_power_3/readIdentitybeta2_power_3*
T0*2
_class(
&$loc:@GAIL_model/gail_d_estimate/bias
³
HGAIL_model/gail_d_hidden_1/kernel/Adam/Initializer/zeros/shape_as_tensorConst*
valueB"k     *4
_class*
(&loc:@GAIL_model/gail_d_hidden_1/kernel*
dtype0
”
>GAIL_model/gail_d_hidden_1/kernel/Adam/Initializer/zeros/ConstConst*
valueB
 *    *4
_class*
(&loc:@GAIL_model/gail_d_hidden_1/kernel*
dtype0

8GAIL_model/gail_d_hidden_1/kernel/Adam/Initializer/zerosFillHGAIL_model/gail_d_hidden_1/kernel/Adam/Initializer/zeros/shape_as_tensor>GAIL_model/gail_d_hidden_1/kernel/Adam/Initializer/zeros/Const*
T0*

index_type0*4
_class*
(&loc:@GAIL_model/gail_d_hidden_1/kernel
²
&GAIL_model/gail_d_hidden_1/kernel/Adam
VariableV2*
shape:
ė*
shared_name *4
_class*
(&loc:@GAIL_model/gail_d_hidden_1/kernel*
dtype0*
	container 

-GAIL_model/gail_d_hidden_1/kernel/Adam/AssignAssign&GAIL_model/gail_d_hidden_1/kernel/Adam8GAIL_model/gail_d_hidden_1/kernel/Adam/Initializer/zeros*
use_locking(*
T0*4
_class*
(&loc:@GAIL_model/gail_d_hidden_1/kernel*
validate_shape(

+GAIL_model/gail_d_hidden_1/kernel/Adam/readIdentity&GAIL_model/gail_d_hidden_1/kernel/Adam*
T0*4
_class*
(&loc:@GAIL_model/gail_d_hidden_1/kernel
µ
JGAIL_model/gail_d_hidden_1/kernel/Adam_1/Initializer/zeros/shape_as_tensorConst*
valueB"k     *4
_class*
(&loc:@GAIL_model/gail_d_hidden_1/kernel*
dtype0
£
@GAIL_model/gail_d_hidden_1/kernel/Adam_1/Initializer/zeros/ConstConst*
valueB
 *    *4
_class*
(&loc:@GAIL_model/gail_d_hidden_1/kernel*
dtype0
”
:GAIL_model/gail_d_hidden_1/kernel/Adam_1/Initializer/zerosFillJGAIL_model/gail_d_hidden_1/kernel/Adam_1/Initializer/zeros/shape_as_tensor@GAIL_model/gail_d_hidden_1/kernel/Adam_1/Initializer/zeros/Const*
T0*

index_type0*4
_class*
(&loc:@GAIL_model/gail_d_hidden_1/kernel
“
(GAIL_model/gail_d_hidden_1/kernel/Adam_1
VariableV2*
shape:
ė*
shared_name *4
_class*
(&loc:@GAIL_model/gail_d_hidden_1/kernel*
dtype0*
	container 

/GAIL_model/gail_d_hidden_1/kernel/Adam_1/AssignAssign(GAIL_model/gail_d_hidden_1/kernel/Adam_1:GAIL_model/gail_d_hidden_1/kernel/Adam_1/Initializer/zeros*
use_locking(*
T0*4
_class*
(&loc:@GAIL_model/gail_d_hidden_1/kernel*
validate_shape(
¢
-GAIL_model/gail_d_hidden_1/kernel/Adam_1/readIdentity(GAIL_model/gail_d_hidden_1/kernel/Adam_1*
T0*4
_class*
(&loc:@GAIL_model/gail_d_hidden_1/kernel

6GAIL_model/gail_d_hidden_1/bias/Adam/Initializer/zerosConst*
valueB*    *2
_class(
&$loc:@GAIL_model/gail_d_hidden_1/bias*
dtype0
©
$GAIL_model/gail_d_hidden_1/bias/Adam
VariableV2*
shape:*
shared_name *2
_class(
&$loc:@GAIL_model/gail_d_hidden_1/bias*
dtype0*
	container 
ł
+GAIL_model/gail_d_hidden_1/bias/Adam/AssignAssign$GAIL_model/gail_d_hidden_1/bias/Adam6GAIL_model/gail_d_hidden_1/bias/Adam/Initializer/zeros*
use_locking(*
T0*2
_class(
&$loc:@GAIL_model/gail_d_hidden_1/bias*
validate_shape(

)GAIL_model/gail_d_hidden_1/bias/Adam/readIdentity$GAIL_model/gail_d_hidden_1/bias/Adam*
T0*2
_class(
&$loc:@GAIL_model/gail_d_hidden_1/bias

8GAIL_model/gail_d_hidden_1/bias/Adam_1/Initializer/zerosConst*
valueB*    *2
_class(
&$loc:@GAIL_model/gail_d_hidden_1/bias*
dtype0
«
&GAIL_model/gail_d_hidden_1/bias/Adam_1
VariableV2*
shape:*
shared_name *2
_class(
&$loc:@GAIL_model/gail_d_hidden_1/bias*
dtype0*
	container 
’
-GAIL_model/gail_d_hidden_1/bias/Adam_1/AssignAssign&GAIL_model/gail_d_hidden_1/bias/Adam_18GAIL_model/gail_d_hidden_1/bias/Adam_1/Initializer/zeros*
use_locking(*
T0*2
_class(
&$loc:@GAIL_model/gail_d_hidden_1/bias*
validate_shape(

+GAIL_model/gail_d_hidden_1/bias/Adam_1/readIdentity&GAIL_model/gail_d_hidden_1/bias/Adam_1*
T0*2
_class(
&$loc:@GAIL_model/gail_d_hidden_1/bias
³
HGAIL_model/gail_d_hidden_2/kernel/Adam/Initializer/zeros/shape_as_tensorConst*
valueB"      *4
_class*
(&loc:@GAIL_model/gail_d_hidden_2/kernel*
dtype0
”
>GAIL_model/gail_d_hidden_2/kernel/Adam/Initializer/zeros/ConstConst*
valueB
 *    *4
_class*
(&loc:@GAIL_model/gail_d_hidden_2/kernel*
dtype0

8GAIL_model/gail_d_hidden_2/kernel/Adam/Initializer/zerosFillHGAIL_model/gail_d_hidden_2/kernel/Adam/Initializer/zeros/shape_as_tensor>GAIL_model/gail_d_hidden_2/kernel/Adam/Initializer/zeros/Const*
T0*

index_type0*4
_class*
(&loc:@GAIL_model/gail_d_hidden_2/kernel
²
&GAIL_model/gail_d_hidden_2/kernel/Adam
VariableV2*
shape:
*
shared_name *4
_class*
(&loc:@GAIL_model/gail_d_hidden_2/kernel*
dtype0*
	container 

-GAIL_model/gail_d_hidden_2/kernel/Adam/AssignAssign&GAIL_model/gail_d_hidden_2/kernel/Adam8GAIL_model/gail_d_hidden_2/kernel/Adam/Initializer/zeros*
use_locking(*
T0*4
_class*
(&loc:@GAIL_model/gail_d_hidden_2/kernel*
validate_shape(

+GAIL_model/gail_d_hidden_2/kernel/Adam/readIdentity&GAIL_model/gail_d_hidden_2/kernel/Adam*
T0*4
_class*
(&loc:@GAIL_model/gail_d_hidden_2/kernel
µ
JGAIL_model/gail_d_hidden_2/kernel/Adam_1/Initializer/zeros/shape_as_tensorConst*
valueB"      *4
_class*
(&loc:@GAIL_model/gail_d_hidden_2/kernel*
dtype0
£
@GAIL_model/gail_d_hidden_2/kernel/Adam_1/Initializer/zeros/ConstConst*
valueB
 *    *4
_class*
(&loc:@GAIL_model/gail_d_hidden_2/kernel*
dtype0
”
:GAIL_model/gail_d_hidden_2/kernel/Adam_1/Initializer/zerosFillJGAIL_model/gail_d_hidden_2/kernel/Adam_1/Initializer/zeros/shape_as_tensor@GAIL_model/gail_d_hidden_2/kernel/Adam_1/Initializer/zeros/Const*
T0*

index_type0*4
_class*
(&loc:@GAIL_model/gail_d_hidden_2/kernel
“
(GAIL_model/gail_d_hidden_2/kernel/Adam_1
VariableV2*
shape:
*
shared_name *4
_class*
(&loc:@GAIL_model/gail_d_hidden_2/kernel*
dtype0*
	container 

/GAIL_model/gail_d_hidden_2/kernel/Adam_1/AssignAssign(GAIL_model/gail_d_hidden_2/kernel/Adam_1:GAIL_model/gail_d_hidden_2/kernel/Adam_1/Initializer/zeros*
use_locking(*
T0*4
_class*
(&loc:@GAIL_model/gail_d_hidden_2/kernel*
validate_shape(
¢
-GAIL_model/gail_d_hidden_2/kernel/Adam_1/readIdentity(GAIL_model/gail_d_hidden_2/kernel/Adam_1*
T0*4
_class*
(&loc:@GAIL_model/gail_d_hidden_2/kernel

6GAIL_model/gail_d_hidden_2/bias/Adam/Initializer/zerosConst*
valueB*    *2
_class(
&$loc:@GAIL_model/gail_d_hidden_2/bias*
dtype0
©
$GAIL_model/gail_d_hidden_2/bias/Adam
VariableV2*
shape:*
shared_name *2
_class(
&$loc:@GAIL_model/gail_d_hidden_2/bias*
dtype0*
	container 
ł
+GAIL_model/gail_d_hidden_2/bias/Adam/AssignAssign$GAIL_model/gail_d_hidden_2/bias/Adam6GAIL_model/gail_d_hidden_2/bias/Adam/Initializer/zeros*
use_locking(*
T0*2
_class(
&$loc:@GAIL_model/gail_d_hidden_2/bias*
validate_shape(

)GAIL_model/gail_d_hidden_2/bias/Adam/readIdentity$GAIL_model/gail_d_hidden_2/bias/Adam*
T0*2
_class(
&$loc:@GAIL_model/gail_d_hidden_2/bias

8GAIL_model/gail_d_hidden_2/bias/Adam_1/Initializer/zerosConst*
valueB*    *2
_class(
&$loc:@GAIL_model/gail_d_hidden_2/bias*
dtype0
«
&GAIL_model/gail_d_hidden_2/bias/Adam_1
VariableV2*
shape:*
shared_name *2
_class(
&$loc:@GAIL_model/gail_d_hidden_2/bias*
dtype0*
	container 
’
-GAIL_model/gail_d_hidden_2/bias/Adam_1/AssignAssign&GAIL_model/gail_d_hidden_2/bias/Adam_18GAIL_model/gail_d_hidden_2/bias/Adam_1/Initializer/zeros*
use_locking(*
T0*2
_class(
&$loc:@GAIL_model/gail_d_hidden_2/bias*
validate_shape(

+GAIL_model/gail_d_hidden_2/bias/Adam_1/readIdentity&GAIL_model/gail_d_hidden_2/bias/Adam_1*
T0*2
_class(
&$loc:@GAIL_model/gail_d_hidden_2/bias
¤
8GAIL_model/gail_d_estimate/kernel/Adam/Initializer/zerosConst*
valueB	*    *4
_class*
(&loc:@GAIL_model/gail_d_estimate/kernel*
dtype0
±
&GAIL_model/gail_d_estimate/kernel/Adam
VariableV2*
shape:	*
shared_name *4
_class*
(&loc:@GAIL_model/gail_d_estimate/kernel*
dtype0*
	container 

-GAIL_model/gail_d_estimate/kernel/Adam/AssignAssign&GAIL_model/gail_d_estimate/kernel/Adam8GAIL_model/gail_d_estimate/kernel/Adam/Initializer/zeros*
use_locking(*
T0*4
_class*
(&loc:@GAIL_model/gail_d_estimate/kernel*
validate_shape(

+GAIL_model/gail_d_estimate/kernel/Adam/readIdentity&GAIL_model/gail_d_estimate/kernel/Adam*
T0*4
_class*
(&loc:@GAIL_model/gail_d_estimate/kernel
¦
:GAIL_model/gail_d_estimate/kernel/Adam_1/Initializer/zerosConst*
valueB	*    *4
_class*
(&loc:@GAIL_model/gail_d_estimate/kernel*
dtype0
³
(GAIL_model/gail_d_estimate/kernel/Adam_1
VariableV2*
shape:	*
shared_name *4
_class*
(&loc:@GAIL_model/gail_d_estimate/kernel*
dtype0*
	container 

/GAIL_model/gail_d_estimate/kernel/Adam_1/AssignAssign(GAIL_model/gail_d_estimate/kernel/Adam_1:GAIL_model/gail_d_estimate/kernel/Adam_1/Initializer/zeros*
use_locking(*
T0*4
_class*
(&loc:@GAIL_model/gail_d_estimate/kernel*
validate_shape(
¢
-GAIL_model/gail_d_estimate/kernel/Adam_1/readIdentity(GAIL_model/gail_d_estimate/kernel/Adam_1*
T0*4
_class*
(&loc:@GAIL_model/gail_d_estimate/kernel

6GAIL_model/gail_d_estimate/bias/Adam/Initializer/zerosConst*
valueB*    *2
_class(
&$loc:@GAIL_model/gail_d_estimate/bias*
dtype0
Ø
$GAIL_model/gail_d_estimate/bias/Adam
VariableV2*
shape:*
shared_name *2
_class(
&$loc:@GAIL_model/gail_d_estimate/bias*
dtype0*
	container 
ł
+GAIL_model/gail_d_estimate/bias/Adam/AssignAssign$GAIL_model/gail_d_estimate/bias/Adam6GAIL_model/gail_d_estimate/bias/Adam/Initializer/zeros*
use_locking(*
T0*2
_class(
&$loc:@GAIL_model/gail_d_estimate/bias*
validate_shape(

)GAIL_model/gail_d_estimate/bias/Adam/readIdentity$GAIL_model/gail_d_estimate/bias/Adam*
T0*2
_class(
&$loc:@GAIL_model/gail_d_estimate/bias

8GAIL_model/gail_d_estimate/bias/Adam_1/Initializer/zerosConst*
valueB*    *2
_class(
&$loc:@GAIL_model/gail_d_estimate/bias*
dtype0
Ŗ
&GAIL_model/gail_d_estimate/bias/Adam_1
VariableV2*
shape:*
shared_name *2
_class(
&$loc:@GAIL_model/gail_d_estimate/bias*
dtype0*
	container 
’
-GAIL_model/gail_d_estimate/bias/Adam_1/AssignAssign&GAIL_model/gail_d_estimate/bias/Adam_18GAIL_model/gail_d_estimate/bias/Adam_1/Initializer/zeros*
use_locking(*
T0*2
_class(
&$loc:@GAIL_model/gail_d_estimate/bias*
validate_shape(

+GAIL_model/gail_d_estimate/bias/Adam_1/readIdentity&GAIL_model/gail_d_estimate/bias/Adam_1*
T0*2
_class(
&$loc:@GAIL_model/gail_d_estimate/bias
A
Adam_3/learning_rateConst*
valueB
 *RI9*
dtype0
9
Adam_3/beta1Const*
valueB
 *fff?*
dtype0
9
Adam_3/beta2Const*
valueB
 *w¾?*
dtype0
;
Adam_3/epsilonConst*
valueB
 *wĢ+2*
dtype0
 
9Adam_3/update_GAIL_model/gail_d_hidden_1/kernel/ApplyAdam	ApplyAdam!GAIL_model/gail_d_hidden_1/kernel&GAIL_model/gail_d_hidden_1/kernel/Adam(GAIL_model/gail_d_hidden_1/kernel/Adam_1beta1_power_3/readbeta2_power_3/readAdam_3/learning_rateAdam_3/beta1Adam_3/beta2Adam_3/epsilongradients_4/AddN_15*
use_locking( *
T0*4
_class*
(&loc:@GAIL_model/gail_d_hidden_1/kernel*
use_nesterov( 

7Adam_3/update_GAIL_model/gail_d_hidden_1/bias/ApplyAdam	ApplyAdamGAIL_model/gail_d_hidden_1/bias$GAIL_model/gail_d_hidden_1/bias/Adam&GAIL_model/gail_d_hidden_1/bias/Adam_1beta1_power_3/readbeta2_power_3/readAdam_3/learning_rateAdam_3/beta1Adam_3/beta2Adam_3/epsilongradients_4/AddN_14*
use_locking( *
T0*2
_class(
&$loc:@GAIL_model/gail_d_hidden_1/bias*
use_nesterov( 
 
9Adam_3/update_GAIL_model/gail_d_hidden_2/kernel/ApplyAdam	ApplyAdam!GAIL_model/gail_d_hidden_2/kernel&GAIL_model/gail_d_hidden_2/kernel/Adam(GAIL_model/gail_d_hidden_2/kernel/Adam_1beta1_power_3/readbeta2_power_3/readAdam_3/learning_rateAdam_3/beta1Adam_3/beta2Adam_3/epsilongradients_4/AddN_11*
use_locking( *
T0*4
_class*
(&loc:@GAIL_model/gail_d_hidden_2/kernel*
use_nesterov( 

7Adam_3/update_GAIL_model/gail_d_hidden_2/bias/ApplyAdam	ApplyAdamGAIL_model/gail_d_hidden_2/bias$GAIL_model/gail_d_hidden_2/bias/Adam&GAIL_model/gail_d_hidden_2/bias/Adam_1beta1_power_3/readbeta2_power_3/readAdam_3/learning_rateAdam_3/beta1Adam_3/beta2Adam_3/epsilongradients_4/AddN_10*
use_locking( *
T0*2
_class(
&$loc:@GAIL_model/gail_d_hidden_2/bias*
use_nesterov( 

9Adam_3/update_GAIL_model/gail_d_estimate/kernel/ApplyAdam	ApplyAdam!GAIL_model/gail_d_estimate/kernel&GAIL_model/gail_d_estimate/kernel/Adam(GAIL_model/gail_d_estimate/kernel/Adam_1beta1_power_3/readbeta2_power_3/readAdam_3/learning_rateAdam_3/beta1Adam_3/beta2Adam_3/epsilongradients_4/AddN_7*
use_locking( *
T0*4
_class*
(&loc:@GAIL_model/gail_d_estimate/kernel*
use_nesterov( 

7Adam_3/update_GAIL_model/gail_d_estimate/bias/ApplyAdam	ApplyAdamGAIL_model/gail_d_estimate/bias$GAIL_model/gail_d_estimate/bias/Adam&GAIL_model/gail_d_estimate/bias/Adam_1beta1_power_3/readbeta2_power_3/readAdam_3/learning_rateAdam_3/beta1Adam_3/beta2Adam_3/epsilongradients_4/AddN_6*
use_locking( *
T0*2
_class(
&$loc:@GAIL_model/gail_d_estimate/bias*
use_nesterov( 
Ņ

Adam_3/mulMulbeta1_power_3/readAdam_3/beta18^Adam_3/update_GAIL_model/gail_d_estimate/bias/ApplyAdam:^Adam_3/update_GAIL_model/gail_d_estimate/kernel/ApplyAdam8^Adam_3/update_GAIL_model/gail_d_hidden_1/bias/ApplyAdam:^Adam_3/update_GAIL_model/gail_d_hidden_1/kernel/ApplyAdam8^Adam_3/update_GAIL_model/gail_d_hidden_2/bias/ApplyAdam:^Adam_3/update_GAIL_model/gail_d_hidden_2/kernel/ApplyAdam*
T0*2
_class(
&$loc:@GAIL_model/gail_d_estimate/bias

Adam_3/AssignAssignbeta1_power_3
Adam_3/mul*
use_locking( *
T0*2
_class(
&$loc:@GAIL_model/gail_d_estimate/bias*
validate_shape(
Ō
Adam_3/mul_1Mulbeta2_power_3/readAdam_3/beta28^Adam_3/update_GAIL_model/gail_d_estimate/bias/ApplyAdam:^Adam_3/update_GAIL_model/gail_d_estimate/kernel/ApplyAdam8^Adam_3/update_GAIL_model/gail_d_hidden_1/bias/ApplyAdam:^Adam_3/update_GAIL_model/gail_d_hidden_1/kernel/ApplyAdam8^Adam_3/update_GAIL_model/gail_d_hidden_2/bias/ApplyAdam:^Adam_3/update_GAIL_model/gail_d_hidden_2/kernel/ApplyAdam*
T0*2
_class(
&$loc:@GAIL_model/gail_d_estimate/bias

Adam_3/Assign_1Assignbeta2_power_3Adam_3/mul_1*
use_locking( *
T0*2
_class(
&$loc:@GAIL_model/gail_d_estimate/bias*
validate_shape(

Adam_3NoOp^Adam_3/Assign^Adam_3/Assign_18^Adam_3/update_GAIL_model/gail_d_estimate/bias/ApplyAdam:^Adam_3/update_GAIL_model/gail_d_estimate/kernel/ApplyAdam8^Adam_3/update_GAIL_model/gail_d_hidden_1/bias/ApplyAdam:^Adam_3/update_GAIL_model/gail_d_hidden_1/kernel/ApplyAdam8^Adam_3/update_GAIL_model/gail_d_hidden_2/bias/ApplyAdam:^Adam_3/update_GAIL_model/gail_d_hidden_2/kernel/ApplyAdam
A
save/filename/inputConst*
valueB Bmodel*
dtype0
V
save/filenamePlaceholderWithDefaultsave/filename/input*
shape: *
dtype0
M

save/ConstPlaceholderWithDefaultsave/filename*
shape: *
dtype0
+
save/SaveV2/tensor_namesConst*Ń*
valueĒ*BÄ*BGAIL_model/gail_d_estimate/biasB$GAIL_model/gail_d_estimate/bias/AdamB&GAIL_model/gail_d_estimate/bias/Adam_1B!GAIL_model/gail_d_estimate/kernelB&GAIL_model/gail_d_estimate/kernel/AdamB(GAIL_model/gail_d_estimate/kernel/Adam_1BGAIL_model/gail_d_hidden_1/biasB$GAIL_model/gail_d_hidden_1/bias/AdamB&GAIL_model/gail_d_hidden_1/bias/Adam_1B!GAIL_model/gail_d_hidden_1/kernelB&GAIL_model/gail_d_hidden_1/kernel/AdamB(GAIL_model/gail_d_hidden_1/kernel/Adam_1BGAIL_model/gail_d_hidden_2/biasB$GAIL_model/gail_d_hidden_2/bias/AdamB&GAIL_model/gail_d_hidden_2/bias/Adam_1B!GAIL_model/gail_d_hidden_2/kernelB&GAIL_model/gail_d_hidden_2/kernel/AdamB(GAIL_model/gail_d_hidden_2/kernel/Adam_1BVariableB
Variable_1B
Variable_2Baction_output_shapeBaction_output_shape_1Baction_output_shape_2Bbeta1_powerBbeta1_power_1Bbeta1_power_2Bbeta1_power_3Bbeta2_powerBbeta2_power_1Bbeta2_power_2Bbeta2_power_3B&critic/q/q1_encoding/extrinsic_q1/biasB+critic/q/q1_encoding/extrinsic_q1/bias/AdamB-critic/q/q1_encoding/extrinsic_q1/bias/Adam_1B(critic/q/q1_encoding/extrinsic_q1/kernelB-critic/q/q1_encoding/extrinsic_q1/kernel/AdamB/critic/q/q1_encoding/extrinsic_q1/kernel/Adam_1B!critic/q/q1_encoding/gail_q1/biasB&critic/q/q1_encoding/gail_q1/bias/AdamB(critic/q/q1_encoding/gail_q1/bias/Adam_1B#critic/q/q1_encoding/gail_q1/kernelB(critic/q/q1_encoding/gail_q1/kernel/AdamB*critic/q/q1_encoding/gail_q1/kernel/Adam_1B-critic/q/q1_encoding/q1_encoder/hidden_0/biasB2critic/q/q1_encoding/q1_encoder/hidden_0/bias/AdamB4critic/q/q1_encoding/q1_encoder/hidden_0/bias/Adam_1B/critic/q/q1_encoding/q1_encoder/hidden_0/kernelB4critic/q/q1_encoding/q1_encoder/hidden_0/kernel/AdamB6critic/q/q1_encoding/q1_encoder/hidden_0/kernel/Adam_1B-critic/q/q1_encoding/q1_encoder/hidden_1/biasB2critic/q/q1_encoding/q1_encoder/hidden_1/bias/AdamB4critic/q/q1_encoding/q1_encoder/hidden_1/bias/Adam_1B/critic/q/q1_encoding/q1_encoder/hidden_1/kernelB4critic/q/q1_encoding/q1_encoder/hidden_1/kernel/AdamB6critic/q/q1_encoding/q1_encoder/hidden_1/kernel/Adam_1B&critic/q/q2_encoding/extrinsic_q2/biasB+critic/q/q2_encoding/extrinsic_q2/bias/AdamB-critic/q/q2_encoding/extrinsic_q2/bias/Adam_1B(critic/q/q2_encoding/extrinsic_q2/kernelB-critic/q/q2_encoding/extrinsic_q2/kernel/AdamB/critic/q/q2_encoding/extrinsic_q2/kernel/Adam_1B!critic/q/q2_encoding/gail_q2/biasB&critic/q/q2_encoding/gail_q2/bias/AdamB(critic/q/q2_encoding/gail_q2/bias/Adam_1B#critic/q/q2_encoding/gail_q2/kernelB(critic/q/q2_encoding/gail_q2/kernel/AdamB*critic/q/q2_encoding/gail_q2/kernel/Adam_1B-critic/q/q2_encoding/q2_encoder/hidden_0/biasB2critic/q/q2_encoding/q2_encoder/hidden_0/bias/AdamB4critic/q/q2_encoding/q2_encoder/hidden_0/bias/Adam_1B/critic/q/q2_encoding/q2_encoder/hidden_0/kernelB4critic/q/q2_encoding/q2_encoder/hidden_0/kernel/AdamB6critic/q/q2_encoding/q2_encoder/hidden_0/kernel/Adam_1B-critic/q/q2_encoding/q2_encoder/hidden_1/biasB2critic/q/q2_encoding/q2_encoder/hidden_1/bias/AdamB4critic/q/q2_encoding/q2_encoder/hidden_1/bias/Adam_1B/critic/q/q2_encoding/q2_encoder/hidden_1/kernelB4critic/q/q2_encoding/q2_encoder/hidden_1/kernel/AdamB6critic/q/q2_encoding/q2_encoder/hidden_1/kernel/Adam_1B"critic/value/encoder/hidden_0/biasB'critic/value/encoder/hidden_0/bias/AdamB)critic/value/encoder/hidden_0/bias/Adam_1B$critic/value/encoder/hidden_0/kernelB)critic/value/encoder/hidden_0/kernel/AdamB+critic/value/encoder/hidden_0/kernel/Adam_1B"critic/value/encoder/hidden_1/biasB'critic/value/encoder/hidden_1/bias/AdamB)critic/value/encoder/hidden_1/bias/Adam_1B$critic/value/encoder/hidden_1/kernelB)critic/value/encoder/hidden_1/kernel/AdamB+critic/value/encoder/hidden_1/kernel/Adam_1B!critic/value/extrinsic_value/biasB&critic/value/extrinsic_value/bias/AdamB(critic/value/extrinsic_value/bias/Adam_1B#critic/value/extrinsic_value/kernelB(critic/value/extrinsic_value/kernel/AdamB*critic/value/extrinsic_value/kernel/Adam_1Bcritic/value/gail_value/biasB!critic/value/gail_value/bias/AdamB#critic/value/gail_value/bias/Adam_1Bcritic/value/gail_value/kernelB#critic/value/gail_value/kernel/AdamB%critic/value/gail_value/kernel/Adam_1Bglobal_stepBglobal_step_1Bglobal_step_2Bis_continuous_controlBis_continuous_control_1Bis_continuous_control_2Blog_ent_coefBlog_ent_coef/AdamBlog_ent_coef/Adam_1Bmemory_sizeBmemory_size_1Bmemory_size_2Bpolicy/dense/kernelBpolicy/dense/kernel/AdamBpolicy/dense/kernel/Adam_1Bpolicy/dense_1/kernelBpolicy/dense_1/kernel/AdamBpolicy/dense_1/kernel/Adam_1Bpolicy/dense_2/kernelBpolicy/dense_2/kernel/AdamBpolicy/dense_2/kernel/Adam_1Bpolicy/encoder/hidden_0/biasB!policy/encoder/hidden_0/bias/AdamB#policy/encoder/hidden_0/bias/Adam_1Bpolicy/encoder/hidden_0/kernelB#policy/encoder/hidden_0/kernel/AdamB%policy/encoder/hidden_0/kernel/Adam_1Bpolicy/encoder/hidden_1/biasB!policy/encoder/hidden_1/bias/AdamB#policy/encoder/hidden_1/bias/Adam_1Bpolicy/encoder/hidden_1/kernelB#policy/encoder/hidden_1/kernel/AdamB%policy/encoder/hidden_1/kernel/Adam_1B1target_network/critic/value/encoder/hidden_0/biasB3target_network/critic/value/encoder/hidden_0/kernelB1target_network/critic/value/encoder/hidden_1/biasB3target_network/critic/value/encoder/hidden_1/kernelB0target_network/critic/value/extrinsic_value/biasB2target_network/critic/value/extrinsic_value/kernelB+target_network/critic/value/gail_value/biasB-target_network/critic/value/gail_value/kernelBversion_numberBversion_number_1Bversion_number_2*
dtype0
ó
save/SaveV2/shape_and_slicesConst*¾
value“B±B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B *
dtype0
¼,
save/SaveV2SaveV2
save/Constsave/SaveV2/tensor_namessave/SaveV2/shape_and_slicesGAIL_model/gail_d_estimate/bias$GAIL_model/gail_d_estimate/bias/Adam&GAIL_model/gail_d_estimate/bias/Adam_1!GAIL_model/gail_d_estimate/kernel&GAIL_model/gail_d_estimate/kernel/Adam(GAIL_model/gail_d_estimate/kernel/Adam_1GAIL_model/gail_d_hidden_1/bias$GAIL_model/gail_d_hidden_1/bias/Adam&GAIL_model/gail_d_hidden_1/bias/Adam_1!GAIL_model/gail_d_hidden_1/kernel&GAIL_model/gail_d_hidden_1/kernel/Adam(GAIL_model/gail_d_hidden_1/kernel/Adam_1GAIL_model/gail_d_hidden_2/bias$GAIL_model/gail_d_hidden_2/bias/Adam&GAIL_model/gail_d_hidden_2/bias/Adam_1!GAIL_model/gail_d_hidden_2/kernel&GAIL_model/gail_d_hidden_2/kernel/Adam(GAIL_model/gail_d_hidden_2/kernel/Adam_1Variable
Variable_1
Variable_2action_output_shapeaction_output_shape_1action_output_shape_2beta1_powerbeta1_power_1beta1_power_2beta1_power_3beta2_powerbeta2_power_1beta2_power_2beta2_power_3&critic/q/q1_encoding/extrinsic_q1/bias+critic/q/q1_encoding/extrinsic_q1/bias/Adam-critic/q/q1_encoding/extrinsic_q1/bias/Adam_1(critic/q/q1_encoding/extrinsic_q1/kernel-critic/q/q1_encoding/extrinsic_q1/kernel/Adam/critic/q/q1_encoding/extrinsic_q1/kernel/Adam_1!critic/q/q1_encoding/gail_q1/bias&critic/q/q1_encoding/gail_q1/bias/Adam(critic/q/q1_encoding/gail_q1/bias/Adam_1#critic/q/q1_encoding/gail_q1/kernel(critic/q/q1_encoding/gail_q1/kernel/Adam*critic/q/q1_encoding/gail_q1/kernel/Adam_1-critic/q/q1_encoding/q1_encoder/hidden_0/bias2critic/q/q1_encoding/q1_encoder/hidden_0/bias/Adam4critic/q/q1_encoding/q1_encoder/hidden_0/bias/Adam_1/critic/q/q1_encoding/q1_encoder/hidden_0/kernel4critic/q/q1_encoding/q1_encoder/hidden_0/kernel/Adam6critic/q/q1_encoding/q1_encoder/hidden_0/kernel/Adam_1-critic/q/q1_encoding/q1_encoder/hidden_1/bias2critic/q/q1_encoding/q1_encoder/hidden_1/bias/Adam4critic/q/q1_encoding/q1_encoder/hidden_1/bias/Adam_1/critic/q/q1_encoding/q1_encoder/hidden_1/kernel4critic/q/q1_encoding/q1_encoder/hidden_1/kernel/Adam6critic/q/q1_encoding/q1_encoder/hidden_1/kernel/Adam_1&critic/q/q2_encoding/extrinsic_q2/bias+critic/q/q2_encoding/extrinsic_q2/bias/Adam-critic/q/q2_encoding/extrinsic_q2/bias/Adam_1(critic/q/q2_encoding/extrinsic_q2/kernel-critic/q/q2_encoding/extrinsic_q2/kernel/Adam/critic/q/q2_encoding/extrinsic_q2/kernel/Adam_1!critic/q/q2_encoding/gail_q2/bias&critic/q/q2_encoding/gail_q2/bias/Adam(critic/q/q2_encoding/gail_q2/bias/Adam_1#critic/q/q2_encoding/gail_q2/kernel(critic/q/q2_encoding/gail_q2/kernel/Adam*critic/q/q2_encoding/gail_q2/kernel/Adam_1-critic/q/q2_encoding/q2_encoder/hidden_0/bias2critic/q/q2_encoding/q2_encoder/hidden_0/bias/Adam4critic/q/q2_encoding/q2_encoder/hidden_0/bias/Adam_1/critic/q/q2_encoding/q2_encoder/hidden_0/kernel4critic/q/q2_encoding/q2_encoder/hidden_0/kernel/Adam6critic/q/q2_encoding/q2_encoder/hidden_0/kernel/Adam_1-critic/q/q2_encoding/q2_encoder/hidden_1/bias2critic/q/q2_encoding/q2_encoder/hidden_1/bias/Adam4critic/q/q2_encoding/q2_encoder/hidden_1/bias/Adam_1/critic/q/q2_encoding/q2_encoder/hidden_1/kernel4critic/q/q2_encoding/q2_encoder/hidden_1/kernel/Adam6critic/q/q2_encoding/q2_encoder/hidden_1/kernel/Adam_1"critic/value/encoder/hidden_0/bias'critic/value/encoder/hidden_0/bias/Adam)critic/value/encoder/hidden_0/bias/Adam_1$critic/value/encoder/hidden_0/kernel)critic/value/encoder/hidden_0/kernel/Adam+critic/value/encoder/hidden_0/kernel/Adam_1"critic/value/encoder/hidden_1/bias'critic/value/encoder/hidden_1/bias/Adam)critic/value/encoder/hidden_1/bias/Adam_1$critic/value/encoder/hidden_1/kernel)critic/value/encoder/hidden_1/kernel/Adam+critic/value/encoder/hidden_1/kernel/Adam_1!critic/value/extrinsic_value/bias&critic/value/extrinsic_value/bias/Adam(critic/value/extrinsic_value/bias/Adam_1#critic/value/extrinsic_value/kernel(critic/value/extrinsic_value/kernel/Adam*critic/value/extrinsic_value/kernel/Adam_1critic/value/gail_value/bias!critic/value/gail_value/bias/Adam#critic/value/gail_value/bias/Adam_1critic/value/gail_value/kernel#critic/value/gail_value/kernel/Adam%critic/value/gail_value/kernel/Adam_1global_stepglobal_step_1global_step_2is_continuous_controlis_continuous_control_1is_continuous_control_2log_ent_coeflog_ent_coef/Adamlog_ent_coef/Adam_1memory_sizememory_size_1memory_size_2policy/dense/kernelpolicy/dense/kernel/Adampolicy/dense/kernel/Adam_1policy/dense_1/kernelpolicy/dense_1/kernel/Adampolicy/dense_1/kernel/Adam_1policy/dense_2/kernelpolicy/dense_2/kernel/Adampolicy/dense_2/kernel/Adam_1policy/encoder/hidden_0/bias!policy/encoder/hidden_0/bias/Adam#policy/encoder/hidden_0/bias/Adam_1policy/encoder/hidden_0/kernel#policy/encoder/hidden_0/kernel/Adam%policy/encoder/hidden_0/kernel/Adam_1policy/encoder/hidden_1/bias!policy/encoder/hidden_1/bias/Adam#policy/encoder/hidden_1/bias/Adam_1policy/encoder/hidden_1/kernel#policy/encoder/hidden_1/kernel/Adam%policy/encoder/hidden_1/kernel/Adam_11target_network/critic/value/encoder/hidden_0/bias3target_network/critic/value/encoder/hidden_0/kernel1target_network/critic/value/encoder/hidden_1/bias3target_network/critic/value/encoder/hidden_1/kernel0target_network/critic/value/extrinsic_value/bias2target_network/critic/value/extrinsic_value/kernel+target_network/critic/value/gail_value/bias-target_network/critic/value/gail_value/kernelversion_numberversion_number_1version_number_2*„
dtypes
2
e
save/control_dependencyIdentity
save/Const^save/SaveV2*
T0*
_class
loc:@save/Const
+
save/RestoreV2/tensor_namesConst"/device:CPU:0*Ń*
valueĒ*BÄ*BGAIL_model/gail_d_estimate/biasB$GAIL_model/gail_d_estimate/bias/AdamB&GAIL_model/gail_d_estimate/bias/Adam_1B!GAIL_model/gail_d_estimate/kernelB&GAIL_model/gail_d_estimate/kernel/AdamB(GAIL_model/gail_d_estimate/kernel/Adam_1BGAIL_model/gail_d_hidden_1/biasB$GAIL_model/gail_d_hidden_1/bias/AdamB&GAIL_model/gail_d_hidden_1/bias/Adam_1B!GAIL_model/gail_d_hidden_1/kernelB&GAIL_model/gail_d_hidden_1/kernel/AdamB(GAIL_model/gail_d_hidden_1/kernel/Adam_1BGAIL_model/gail_d_hidden_2/biasB$GAIL_model/gail_d_hidden_2/bias/AdamB&GAIL_model/gail_d_hidden_2/bias/Adam_1B!GAIL_model/gail_d_hidden_2/kernelB&GAIL_model/gail_d_hidden_2/kernel/AdamB(GAIL_model/gail_d_hidden_2/kernel/Adam_1BVariableB
Variable_1B
Variable_2Baction_output_shapeBaction_output_shape_1Baction_output_shape_2Bbeta1_powerBbeta1_power_1Bbeta1_power_2Bbeta1_power_3Bbeta2_powerBbeta2_power_1Bbeta2_power_2Bbeta2_power_3B&critic/q/q1_encoding/extrinsic_q1/biasB+critic/q/q1_encoding/extrinsic_q1/bias/AdamB-critic/q/q1_encoding/extrinsic_q1/bias/Adam_1B(critic/q/q1_encoding/extrinsic_q1/kernelB-critic/q/q1_encoding/extrinsic_q1/kernel/AdamB/critic/q/q1_encoding/extrinsic_q1/kernel/Adam_1B!critic/q/q1_encoding/gail_q1/biasB&critic/q/q1_encoding/gail_q1/bias/AdamB(critic/q/q1_encoding/gail_q1/bias/Adam_1B#critic/q/q1_encoding/gail_q1/kernelB(critic/q/q1_encoding/gail_q1/kernel/AdamB*critic/q/q1_encoding/gail_q1/kernel/Adam_1B-critic/q/q1_encoding/q1_encoder/hidden_0/biasB2critic/q/q1_encoding/q1_encoder/hidden_0/bias/AdamB4critic/q/q1_encoding/q1_encoder/hidden_0/bias/Adam_1B/critic/q/q1_encoding/q1_encoder/hidden_0/kernelB4critic/q/q1_encoding/q1_encoder/hidden_0/kernel/AdamB6critic/q/q1_encoding/q1_encoder/hidden_0/kernel/Adam_1B-critic/q/q1_encoding/q1_encoder/hidden_1/biasB2critic/q/q1_encoding/q1_encoder/hidden_1/bias/AdamB4critic/q/q1_encoding/q1_encoder/hidden_1/bias/Adam_1B/critic/q/q1_encoding/q1_encoder/hidden_1/kernelB4critic/q/q1_encoding/q1_encoder/hidden_1/kernel/AdamB6critic/q/q1_encoding/q1_encoder/hidden_1/kernel/Adam_1B&critic/q/q2_encoding/extrinsic_q2/biasB+critic/q/q2_encoding/extrinsic_q2/bias/AdamB-critic/q/q2_encoding/extrinsic_q2/bias/Adam_1B(critic/q/q2_encoding/extrinsic_q2/kernelB-critic/q/q2_encoding/extrinsic_q2/kernel/AdamB/critic/q/q2_encoding/extrinsic_q2/kernel/Adam_1B!critic/q/q2_encoding/gail_q2/biasB&critic/q/q2_encoding/gail_q2/bias/AdamB(critic/q/q2_encoding/gail_q2/bias/Adam_1B#critic/q/q2_encoding/gail_q2/kernelB(critic/q/q2_encoding/gail_q2/kernel/AdamB*critic/q/q2_encoding/gail_q2/kernel/Adam_1B-critic/q/q2_encoding/q2_encoder/hidden_0/biasB2critic/q/q2_encoding/q2_encoder/hidden_0/bias/AdamB4critic/q/q2_encoding/q2_encoder/hidden_0/bias/Adam_1B/critic/q/q2_encoding/q2_encoder/hidden_0/kernelB4critic/q/q2_encoding/q2_encoder/hidden_0/kernel/AdamB6critic/q/q2_encoding/q2_encoder/hidden_0/kernel/Adam_1B-critic/q/q2_encoding/q2_encoder/hidden_1/biasB2critic/q/q2_encoding/q2_encoder/hidden_1/bias/AdamB4critic/q/q2_encoding/q2_encoder/hidden_1/bias/Adam_1B/critic/q/q2_encoding/q2_encoder/hidden_1/kernelB4critic/q/q2_encoding/q2_encoder/hidden_1/kernel/AdamB6critic/q/q2_encoding/q2_encoder/hidden_1/kernel/Adam_1B"critic/value/encoder/hidden_0/biasB'critic/value/encoder/hidden_0/bias/AdamB)critic/value/encoder/hidden_0/bias/Adam_1B$critic/value/encoder/hidden_0/kernelB)critic/value/encoder/hidden_0/kernel/AdamB+critic/value/encoder/hidden_0/kernel/Adam_1B"critic/value/encoder/hidden_1/biasB'critic/value/encoder/hidden_1/bias/AdamB)critic/value/encoder/hidden_1/bias/Adam_1B$critic/value/encoder/hidden_1/kernelB)critic/value/encoder/hidden_1/kernel/AdamB+critic/value/encoder/hidden_1/kernel/Adam_1B!critic/value/extrinsic_value/biasB&critic/value/extrinsic_value/bias/AdamB(critic/value/extrinsic_value/bias/Adam_1B#critic/value/extrinsic_value/kernelB(critic/value/extrinsic_value/kernel/AdamB*critic/value/extrinsic_value/kernel/Adam_1Bcritic/value/gail_value/biasB!critic/value/gail_value/bias/AdamB#critic/value/gail_value/bias/Adam_1Bcritic/value/gail_value/kernelB#critic/value/gail_value/kernel/AdamB%critic/value/gail_value/kernel/Adam_1Bglobal_stepBglobal_step_1Bglobal_step_2Bis_continuous_controlBis_continuous_control_1Bis_continuous_control_2Blog_ent_coefBlog_ent_coef/AdamBlog_ent_coef/Adam_1Bmemory_sizeBmemory_size_1Bmemory_size_2Bpolicy/dense/kernelBpolicy/dense/kernel/AdamBpolicy/dense/kernel/Adam_1Bpolicy/dense_1/kernelBpolicy/dense_1/kernel/AdamBpolicy/dense_1/kernel/Adam_1Bpolicy/dense_2/kernelBpolicy/dense_2/kernel/AdamBpolicy/dense_2/kernel/Adam_1Bpolicy/encoder/hidden_0/biasB!policy/encoder/hidden_0/bias/AdamB#policy/encoder/hidden_0/bias/Adam_1Bpolicy/encoder/hidden_0/kernelB#policy/encoder/hidden_0/kernel/AdamB%policy/encoder/hidden_0/kernel/Adam_1Bpolicy/encoder/hidden_1/biasB!policy/encoder/hidden_1/bias/AdamB#policy/encoder/hidden_1/bias/Adam_1Bpolicy/encoder/hidden_1/kernelB#policy/encoder/hidden_1/kernel/AdamB%policy/encoder/hidden_1/kernel/Adam_1B1target_network/critic/value/encoder/hidden_0/biasB3target_network/critic/value/encoder/hidden_0/kernelB1target_network/critic/value/encoder/hidden_1/biasB3target_network/critic/value/encoder/hidden_1/kernelB0target_network/critic/value/extrinsic_value/biasB2target_network/critic/value/extrinsic_value/kernelB+target_network/critic/value/gail_value/biasB-target_network/critic/value/gail_value/kernelBversion_numberBversion_number_1Bversion_number_2*
dtype0

save/RestoreV2/shape_and_slicesConst"/device:CPU:0*¾
value“B±B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B B *
dtype0

save/RestoreV2	RestoreV2
save/Constsave/RestoreV2/tensor_namessave/RestoreV2/shape_and_slices"/device:CPU:0*„
dtypes
2
¬
save/AssignAssignGAIL_model/gail_d_estimate/biassave/RestoreV2*
use_locking(*
T0*2
_class(
&$loc:@GAIL_model/gail_d_estimate/bias*
validate_shape(
µ
save/Assign_1Assign$GAIL_model/gail_d_estimate/bias/Adamsave/RestoreV2:1*
use_locking(*
T0*2
_class(
&$loc:@GAIL_model/gail_d_estimate/bias*
validate_shape(
·
save/Assign_2Assign&GAIL_model/gail_d_estimate/bias/Adam_1save/RestoreV2:2*
use_locking(*
T0*2
_class(
&$loc:@GAIL_model/gail_d_estimate/bias*
validate_shape(
“
save/Assign_3Assign!GAIL_model/gail_d_estimate/kernelsave/RestoreV2:3*
use_locking(*
T0*4
_class*
(&loc:@GAIL_model/gail_d_estimate/kernel*
validate_shape(
¹
save/Assign_4Assign&GAIL_model/gail_d_estimate/kernel/Adamsave/RestoreV2:4*
use_locking(*
T0*4
_class*
(&loc:@GAIL_model/gail_d_estimate/kernel*
validate_shape(
»
save/Assign_5Assign(GAIL_model/gail_d_estimate/kernel/Adam_1save/RestoreV2:5*
use_locking(*
T0*4
_class*
(&loc:@GAIL_model/gail_d_estimate/kernel*
validate_shape(
°
save/Assign_6AssignGAIL_model/gail_d_hidden_1/biassave/RestoreV2:6*
use_locking(*
T0*2
_class(
&$loc:@GAIL_model/gail_d_hidden_1/bias*
validate_shape(
µ
save/Assign_7Assign$GAIL_model/gail_d_hidden_1/bias/Adamsave/RestoreV2:7*
use_locking(*
T0*2
_class(
&$loc:@GAIL_model/gail_d_hidden_1/bias*
validate_shape(
·
save/Assign_8Assign&GAIL_model/gail_d_hidden_1/bias/Adam_1save/RestoreV2:8*
use_locking(*
T0*2
_class(
&$loc:@GAIL_model/gail_d_hidden_1/bias*
validate_shape(
“
save/Assign_9Assign!GAIL_model/gail_d_hidden_1/kernelsave/RestoreV2:9*
use_locking(*
T0*4
_class*
(&loc:@GAIL_model/gail_d_hidden_1/kernel*
validate_shape(
»
save/Assign_10Assign&GAIL_model/gail_d_hidden_1/kernel/Adamsave/RestoreV2:10*
use_locking(*
T0*4
_class*
(&loc:@GAIL_model/gail_d_hidden_1/kernel*
validate_shape(
½
save/Assign_11Assign(GAIL_model/gail_d_hidden_1/kernel/Adam_1save/RestoreV2:11*
use_locking(*
T0*4
_class*
(&loc:@GAIL_model/gail_d_hidden_1/kernel*
validate_shape(
²
save/Assign_12AssignGAIL_model/gail_d_hidden_2/biassave/RestoreV2:12*
use_locking(*
T0*2
_class(
&$loc:@GAIL_model/gail_d_hidden_2/bias*
validate_shape(
·
save/Assign_13Assign$GAIL_model/gail_d_hidden_2/bias/Adamsave/RestoreV2:13*
use_locking(*
T0*2
_class(
&$loc:@GAIL_model/gail_d_hidden_2/bias*
validate_shape(
¹
save/Assign_14Assign&GAIL_model/gail_d_hidden_2/bias/Adam_1save/RestoreV2:14*
use_locking(*
T0*2
_class(
&$loc:@GAIL_model/gail_d_hidden_2/bias*
validate_shape(
¶
save/Assign_15Assign!GAIL_model/gail_d_hidden_2/kernelsave/RestoreV2:15*
use_locking(*
T0*4
_class*
(&loc:@GAIL_model/gail_d_hidden_2/kernel*
validate_shape(
»
save/Assign_16Assign&GAIL_model/gail_d_hidden_2/kernel/Adamsave/RestoreV2:16*
use_locking(*
T0*4
_class*
(&loc:@GAIL_model/gail_d_hidden_2/kernel*
validate_shape(
½
save/Assign_17Assign(GAIL_model/gail_d_hidden_2/kernel/Adam_1save/RestoreV2:17*
use_locking(*
T0*4
_class*
(&loc:@GAIL_model/gail_d_hidden_2/kernel*
validate_shape(

save/Assign_18AssignVariablesave/RestoreV2:18*
use_locking(*
T0*
_class
loc:@Variable*
validate_shape(

save/Assign_19Assign
Variable_1save/RestoreV2:19*
use_locking(*
T0*
_class
loc:@Variable_1*
validate_shape(

save/Assign_20Assign
Variable_2save/RestoreV2:20*
use_locking(*
T0*
_class
loc:@Variable_2*
validate_shape(

save/Assign_21Assignaction_output_shapesave/RestoreV2:21*
use_locking(*
T0*&
_class
loc:@action_output_shape*
validate_shape(

save/Assign_22Assignaction_output_shape_1save/RestoreV2:22*
use_locking(*
T0*(
_class
loc:@action_output_shape_1*
validate_shape(

save/Assign_23Assignaction_output_shape_2save/RestoreV2:23*
use_locking(*
T0*(
_class
loc:@action_output_shape_2*
validate_shape(

save/Assign_24Assignbeta1_powersave/RestoreV2:24*
use_locking(*
T0*&
_class
loc:@policy/dense/kernel*
validate_shape(
§
save/Assign_25Assignbeta1_power_1save/RestoreV2:25*
use_locking(*
T0*9
_class/
-+loc:@critic/q/q1_encoding/extrinsic_q1/bias*
validate_shape(

save/Assign_26Assignbeta1_power_2save/RestoreV2:26*
use_locking(*
T0*
_class
loc:@log_ent_coef*
validate_shape(
 
save/Assign_27Assignbeta1_power_3save/RestoreV2:27*
use_locking(*
T0*2
_class(
&$loc:@GAIL_model/gail_d_estimate/bias*
validate_shape(

save/Assign_28Assignbeta2_powersave/RestoreV2:28*
use_locking(*
T0*&
_class
loc:@policy/dense/kernel*
validate_shape(
§
save/Assign_29Assignbeta2_power_1save/RestoreV2:29*
use_locking(*
T0*9
_class/
-+loc:@critic/q/q1_encoding/extrinsic_q1/bias*
validate_shape(

save/Assign_30Assignbeta2_power_2save/RestoreV2:30*
use_locking(*
T0*
_class
loc:@log_ent_coef*
validate_shape(
 
save/Assign_31Assignbeta2_power_3save/RestoreV2:31*
use_locking(*
T0*2
_class(
&$loc:@GAIL_model/gail_d_estimate/bias*
validate_shape(
Ą
save/Assign_32Assign&critic/q/q1_encoding/extrinsic_q1/biassave/RestoreV2:32*
use_locking(*
T0*9
_class/
-+loc:@critic/q/q1_encoding/extrinsic_q1/bias*
validate_shape(
Å
save/Assign_33Assign+critic/q/q1_encoding/extrinsic_q1/bias/Adamsave/RestoreV2:33*
use_locking(*
T0*9
_class/
-+loc:@critic/q/q1_encoding/extrinsic_q1/bias*
validate_shape(
Ē
save/Assign_34Assign-critic/q/q1_encoding/extrinsic_q1/bias/Adam_1save/RestoreV2:34*
use_locking(*
T0*9
_class/
-+loc:@critic/q/q1_encoding/extrinsic_q1/bias*
validate_shape(
Ä
save/Assign_35Assign(critic/q/q1_encoding/extrinsic_q1/kernelsave/RestoreV2:35*
use_locking(*
T0*;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel*
validate_shape(
É
save/Assign_36Assign-critic/q/q1_encoding/extrinsic_q1/kernel/Adamsave/RestoreV2:36*
use_locking(*
T0*;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel*
validate_shape(
Ė
save/Assign_37Assign/critic/q/q1_encoding/extrinsic_q1/kernel/Adam_1save/RestoreV2:37*
use_locking(*
T0*;
_class1
/-loc:@critic/q/q1_encoding/extrinsic_q1/kernel*
validate_shape(
¶
save/Assign_38Assign!critic/q/q1_encoding/gail_q1/biassave/RestoreV2:38*
use_locking(*
T0*4
_class*
(&loc:@critic/q/q1_encoding/gail_q1/bias*
validate_shape(
»
save/Assign_39Assign&critic/q/q1_encoding/gail_q1/bias/Adamsave/RestoreV2:39*
use_locking(*
T0*4
_class*
(&loc:@critic/q/q1_encoding/gail_q1/bias*
validate_shape(
½
save/Assign_40Assign(critic/q/q1_encoding/gail_q1/bias/Adam_1save/RestoreV2:40*
use_locking(*
T0*4
_class*
(&loc:@critic/q/q1_encoding/gail_q1/bias*
validate_shape(
ŗ
save/Assign_41Assign#critic/q/q1_encoding/gail_q1/kernelsave/RestoreV2:41*
use_locking(*
T0*6
_class,
*(loc:@critic/q/q1_encoding/gail_q1/kernel*
validate_shape(
æ
save/Assign_42Assign(critic/q/q1_encoding/gail_q1/kernel/Adamsave/RestoreV2:42*
use_locking(*
T0*6
_class,
*(loc:@critic/q/q1_encoding/gail_q1/kernel*
validate_shape(
Į
save/Assign_43Assign*critic/q/q1_encoding/gail_q1/kernel/Adam_1save/RestoreV2:43*
use_locking(*
T0*6
_class,
*(loc:@critic/q/q1_encoding/gail_q1/kernel*
validate_shape(
Ī
save/Assign_44Assign-critic/q/q1_encoding/q1_encoder/hidden_0/biassave/RestoreV2:44*
use_locking(*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_0/bias*
validate_shape(
Ó
save/Assign_45Assign2critic/q/q1_encoding/q1_encoder/hidden_0/bias/Adamsave/RestoreV2:45*
use_locking(*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_0/bias*
validate_shape(
Õ
save/Assign_46Assign4critic/q/q1_encoding/q1_encoder/hidden_0/bias/Adam_1save/RestoreV2:46*
use_locking(*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_0/bias*
validate_shape(
Ņ
save/Assign_47Assign/critic/q/q1_encoding/q1_encoder/hidden_0/kernelsave/RestoreV2:47*
use_locking(*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel*
validate_shape(
×
save/Assign_48Assign4critic/q/q1_encoding/q1_encoder/hidden_0/kernel/Adamsave/RestoreV2:48*
use_locking(*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel*
validate_shape(
Ł
save/Assign_49Assign6critic/q/q1_encoding/q1_encoder/hidden_0/kernel/Adam_1save/RestoreV2:49*
use_locking(*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_0/kernel*
validate_shape(
Ī
save/Assign_50Assign-critic/q/q1_encoding/q1_encoder/hidden_1/biassave/RestoreV2:50*
use_locking(*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_1/bias*
validate_shape(
Ó
save/Assign_51Assign2critic/q/q1_encoding/q1_encoder/hidden_1/bias/Adamsave/RestoreV2:51*
use_locking(*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_1/bias*
validate_shape(
Õ
save/Assign_52Assign4critic/q/q1_encoding/q1_encoder/hidden_1/bias/Adam_1save/RestoreV2:52*
use_locking(*
T0*@
_class6
42loc:@critic/q/q1_encoding/q1_encoder/hidden_1/bias*
validate_shape(
Ņ
save/Assign_53Assign/critic/q/q1_encoding/q1_encoder/hidden_1/kernelsave/RestoreV2:53*
use_locking(*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel*
validate_shape(
×
save/Assign_54Assign4critic/q/q1_encoding/q1_encoder/hidden_1/kernel/Adamsave/RestoreV2:54*
use_locking(*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel*
validate_shape(
Ł
save/Assign_55Assign6critic/q/q1_encoding/q1_encoder/hidden_1/kernel/Adam_1save/RestoreV2:55*
use_locking(*
T0*B
_class8
64loc:@critic/q/q1_encoding/q1_encoder/hidden_1/kernel*
validate_shape(
Ą
save/Assign_56Assign&critic/q/q2_encoding/extrinsic_q2/biassave/RestoreV2:56*
use_locking(*
T0*9
_class/
-+loc:@critic/q/q2_encoding/extrinsic_q2/bias*
validate_shape(
Å
save/Assign_57Assign+critic/q/q2_encoding/extrinsic_q2/bias/Adamsave/RestoreV2:57*
use_locking(*
T0*9
_class/
-+loc:@critic/q/q2_encoding/extrinsic_q2/bias*
validate_shape(
Ē
save/Assign_58Assign-critic/q/q2_encoding/extrinsic_q2/bias/Adam_1save/RestoreV2:58*
use_locking(*
T0*9
_class/
-+loc:@critic/q/q2_encoding/extrinsic_q2/bias*
validate_shape(
Ä
save/Assign_59Assign(critic/q/q2_encoding/extrinsic_q2/kernelsave/RestoreV2:59*
use_locking(*
T0*;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel*
validate_shape(
É
save/Assign_60Assign-critic/q/q2_encoding/extrinsic_q2/kernel/Adamsave/RestoreV2:60*
use_locking(*
T0*;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel*
validate_shape(
Ė
save/Assign_61Assign/critic/q/q2_encoding/extrinsic_q2/kernel/Adam_1save/RestoreV2:61*
use_locking(*
T0*;
_class1
/-loc:@critic/q/q2_encoding/extrinsic_q2/kernel*
validate_shape(
¶
save/Assign_62Assign!critic/q/q2_encoding/gail_q2/biassave/RestoreV2:62*
use_locking(*
T0*4
_class*
(&loc:@critic/q/q2_encoding/gail_q2/bias*
validate_shape(
»
save/Assign_63Assign&critic/q/q2_encoding/gail_q2/bias/Adamsave/RestoreV2:63*
use_locking(*
T0*4
_class*
(&loc:@critic/q/q2_encoding/gail_q2/bias*
validate_shape(
½
save/Assign_64Assign(critic/q/q2_encoding/gail_q2/bias/Adam_1save/RestoreV2:64*
use_locking(*
T0*4
_class*
(&loc:@critic/q/q2_encoding/gail_q2/bias*
validate_shape(
ŗ
save/Assign_65Assign#critic/q/q2_encoding/gail_q2/kernelsave/RestoreV2:65*
use_locking(*
T0*6
_class,
*(loc:@critic/q/q2_encoding/gail_q2/kernel*
validate_shape(
æ
save/Assign_66Assign(critic/q/q2_encoding/gail_q2/kernel/Adamsave/RestoreV2:66*
use_locking(*
T0*6
_class,
*(loc:@critic/q/q2_encoding/gail_q2/kernel*
validate_shape(
Į
save/Assign_67Assign*critic/q/q2_encoding/gail_q2/kernel/Adam_1save/RestoreV2:67*
use_locking(*
T0*6
_class,
*(loc:@critic/q/q2_encoding/gail_q2/kernel*
validate_shape(
Ī
save/Assign_68Assign-critic/q/q2_encoding/q2_encoder/hidden_0/biassave/RestoreV2:68*
use_locking(*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_0/bias*
validate_shape(
Ó
save/Assign_69Assign2critic/q/q2_encoding/q2_encoder/hidden_0/bias/Adamsave/RestoreV2:69*
use_locking(*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_0/bias*
validate_shape(
Õ
save/Assign_70Assign4critic/q/q2_encoding/q2_encoder/hidden_0/bias/Adam_1save/RestoreV2:70*
use_locking(*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_0/bias*
validate_shape(
Ņ
save/Assign_71Assign/critic/q/q2_encoding/q2_encoder/hidden_0/kernelsave/RestoreV2:71*
use_locking(*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel*
validate_shape(
×
save/Assign_72Assign4critic/q/q2_encoding/q2_encoder/hidden_0/kernel/Adamsave/RestoreV2:72*
use_locking(*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel*
validate_shape(
Ł
save/Assign_73Assign6critic/q/q2_encoding/q2_encoder/hidden_0/kernel/Adam_1save/RestoreV2:73*
use_locking(*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_0/kernel*
validate_shape(
Ī
save/Assign_74Assign-critic/q/q2_encoding/q2_encoder/hidden_1/biassave/RestoreV2:74*
use_locking(*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_1/bias*
validate_shape(
Ó
save/Assign_75Assign2critic/q/q2_encoding/q2_encoder/hidden_1/bias/Adamsave/RestoreV2:75*
use_locking(*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_1/bias*
validate_shape(
Õ
save/Assign_76Assign4critic/q/q2_encoding/q2_encoder/hidden_1/bias/Adam_1save/RestoreV2:76*
use_locking(*
T0*@
_class6
42loc:@critic/q/q2_encoding/q2_encoder/hidden_1/bias*
validate_shape(
Ņ
save/Assign_77Assign/critic/q/q2_encoding/q2_encoder/hidden_1/kernelsave/RestoreV2:77*
use_locking(*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel*
validate_shape(
×
save/Assign_78Assign4critic/q/q2_encoding/q2_encoder/hidden_1/kernel/Adamsave/RestoreV2:78*
use_locking(*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel*
validate_shape(
Ł
save/Assign_79Assign6critic/q/q2_encoding/q2_encoder/hidden_1/kernel/Adam_1save/RestoreV2:79*
use_locking(*
T0*B
_class8
64loc:@critic/q/q2_encoding/q2_encoder/hidden_1/kernel*
validate_shape(
ø
save/Assign_80Assign"critic/value/encoder/hidden_0/biassave/RestoreV2:80*
use_locking(*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_0/bias*
validate_shape(
½
save/Assign_81Assign'critic/value/encoder/hidden_0/bias/Adamsave/RestoreV2:81*
use_locking(*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_0/bias*
validate_shape(
æ
save/Assign_82Assign)critic/value/encoder/hidden_0/bias/Adam_1save/RestoreV2:82*
use_locking(*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_0/bias*
validate_shape(
¼
save/Assign_83Assign$critic/value/encoder/hidden_0/kernelsave/RestoreV2:83*
use_locking(*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel*
validate_shape(
Į
save/Assign_84Assign)critic/value/encoder/hidden_0/kernel/Adamsave/RestoreV2:84*
use_locking(*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel*
validate_shape(
Ć
save/Assign_85Assign+critic/value/encoder/hidden_0/kernel/Adam_1save/RestoreV2:85*
use_locking(*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_0/kernel*
validate_shape(
ø
save/Assign_86Assign"critic/value/encoder/hidden_1/biassave/RestoreV2:86*
use_locking(*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_1/bias*
validate_shape(
½
save/Assign_87Assign'critic/value/encoder/hidden_1/bias/Adamsave/RestoreV2:87*
use_locking(*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_1/bias*
validate_shape(
æ
save/Assign_88Assign)critic/value/encoder/hidden_1/bias/Adam_1save/RestoreV2:88*
use_locking(*
T0*5
_class+
)'loc:@critic/value/encoder/hidden_1/bias*
validate_shape(
¼
save/Assign_89Assign$critic/value/encoder/hidden_1/kernelsave/RestoreV2:89*
use_locking(*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel*
validate_shape(
Į
save/Assign_90Assign)critic/value/encoder/hidden_1/kernel/Adamsave/RestoreV2:90*
use_locking(*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel*
validate_shape(
Ć
save/Assign_91Assign+critic/value/encoder/hidden_1/kernel/Adam_1save/RestoreV2:91*
use_locking(*
T0*7
_class-
+)loc:@critic/value/encoder/hidden_1/kernel*
validate_shape(
¶
save/Assign_92Assign!critic/value/extrinsic_value/biassave/RestoreV2:92*
use_locking(*
T0*4
_class*
(&loc:@critic/value/extrinsic_value/bias*
validate_shape(
»
save/Assign_93Assign&critic/value/extrinsic_value/bias/Adamsave/RestoreV2:93*
use_locking(*
T0*4
_class*
(&loc:@critic/value/extrinsic_value/bias*
validate_shape(
½
save/Assign_94Assign(critic/value/extrinsic_value/bias/Adam_1save/RestoreV2:94*
use_locking(*
T0*4
_class*
(&loc:@critic/value/extrinsic_value/bias*
validate_shape(
ŗ
save/Assign_95Assign#critic/value/extrinsic_value/kernelsave/RestoreV2:95*
use_locking(*
T0*6
_class,
*(loc:@critic/value/extrinsic_value/kernel*
validate_shape(
æ
save/Assign_96Assign(critic/value/extrinsic_value/kernel/Adamsave/RestoreV2:96*
use_locking(*
T0*6
_class,
*(loc:@critic/value/extrinsic_value/kernel*
validate_shape(
Į
save/Assign_97Assign*critic/value/extrinsic_value/kernel/Adam_1save/RestoreV2:97*
use_locking(*
T0*6
_class,
*(loc:@critic/value/extrinsic_value/kernel*
validate_shape(
¬
save/Assign_98Assigncritic/value/gail_value/biassave/RestoreV2:98*
use_locking(*
T0*/
_class%
#!loc:@critic/value/gail_value/bias*
validate_shape(
±
save/Assign_99Assign!critic/value/gail_value/bias/Adamsave/RestoreV2:99*
use_locking(*
T0*/
_class%
#!loc:@critic/value/gail_value/bias*
validate_shape(
µ
save/Assign_100Assign#critic/value/gail_value/bias/Adam_1save/RestoreV2:100*
use_locking(*
T0*/
_class%
#!loc:@critic/value/gail_value/bias*
validate_shape(
²
save/Assign_101Assigncritic/value/gail_value/kernelsave/RestoreV2:101*
use_locking(*
T0*1
_class'
%#loc:@critic/value/gail_value/kernel*
validate_shape(
·
save/Assign_102Assign#critic/value/gail_value/kernel/Adamsave/RestoreV2:102*
use_locking(*
T0*1
_class'
%#loc:@critic/value/gail_value/kernel*
validate_shape(
¹
save/Assign_103Assign%critic/value/gail_value/kernel/Adam_1save/RestoreV2:103*
use_locking(*
T0*1
_class'
%#loc:@critic/value/gail_value/kernel*
validate_shape(

save/Assign_104Assignglobal_stepsave/RestoreV2:104*
use_locking(*
T0*
_class
loc:@global_step*
validate_shape(

save/Assign_105Assignglobal_step_1save/RestoreV2:105*
use_locking(*
T0* 
_class
loc:@global_step_1*
validate_shape(

save/Assign_106Assignglobal_step_2save/RestoreV2:106*
use_locking(*
T0* 
_class
loc:@global_step_2*
validate_shape(
 
save/Assign_107Assignis_continuous_controlsave/RestoreV2:107*
use_locking(*
T0*(
_class
loc:@is_continuous_control*
validate_shape(
¤
save/Assign_108Assignis_continuous_control_1save/RestoreV2:108*
use_locking(*
T0**
_class 
loc:@is_continuous_control_1*
validate_shape(
¤
save/Assign_109Assignis_continuous_control_2save/RestoreV2:109*
use_locking(*
T0**
_class 
loc:@is_continuous_control_2*
validate_shape(

save/Assign_110Assignlog_ent_coefsave/RestoreV2:110*
use_locking(*
T0*
_class
loc:@log_ent_coef*
validate_shape(

save/Assign_111Assignlog_ent_coef/Adamsave/RestoreV2:111*
use_locking(*
T0*
_class
loc:@log_ent_coef*
validate_shape(

save/Assign_112Assignlog_ent_coef/Adam_1save/RestoreV2:112*
use_locking(*
T0*
_class
loc:@log_ent_coef*
validate_shape(

save/Assign_113Assignmemory_sizesave/RestoreV2:113*
use_locking(*
T0*
_class
loc:@memory_size*
validate_shape(

save/Assign_114Assignmemory_size_1save/RestoreV2:114*
use_locking(*
T0* 
_class
loc:@memory_size_1*
validate_shape(

save/Assign_115Assignmemory_size_2save/RestoreV2:115*
use_locking(*
T0* 
_class
loc:@memory_size_2*
validate_shape(

save/Assign_116Assignpolicy/dense/kernelsave/RestoreV2:116*
use_locking(*
T0*&
_class
loc:@policy/dense/kernel*
validate_shape(
”
save/Assign_117Assignpolicy/dense/kernel/Adamsave/RestoreV2:117*
use_locking(*
T0*&
_class
loc:@policy/dense/kernel*
validate_shape(
£
save/Assign_118Assignpolicy/dense/kernel/Adam_1save/RestoreV2:118*
use_locking(*
T0*&
_class
loc:@policy/dense/kernel*
validate_shape(
 
save/Assign_119Assignpolicy/dense_1/kernelsave/RestoreV2:119*
use_locking(*
T0*(
_class
loc:@policy/dense_1/kernel*
validate_shape(
„
save/Assign_120Assignpolicy/dense_1/kernel/Adamsave/RestoreV2:120*
use_locking(*
T0*(
_class
loc:@policy/dense_1/kernel*
validate_shape(
§
save/Assign_121Assignpolicy/dense_1/kernel/Adam_1save/RestoreV2:121*
use_locking(*
T0*(
_class
loc:@policy/dense_1/kernel*
validate_shape(
 
save/Assign_122Assignpolicy/dense_2/kernelsave/RestoreV2:122*
use_locking(*
T0*(
_class
loc:@policy/dense_2/kernel*
validate_shape(
„
save/Assign_123Assignpolicy/dense_2/kernel/Adamsave/RestoreV2:123*
use_locking(*
T0*(
_class
loc:@policy/dense_2/kernel*
validate_shape(
§
save/Assign_124Assignpolicy/dense_2/kernel/Adam_1save/RestoreV2:124*
use_locking(*
T0*(
_class
loc:@policy/dense_2/kernel*
validate_shape(
®
save/Assign_125Assignpolicy/encoder/hidden_0/biassave/RestoreV2:125*
use_locking(*
T0*/
_class%
#!loc:@policy/encoder/hidden_0/bias*
validate_shape(
³
save/Assign_126Assign!policy/encoder/hidden_0/bias/Adamsave/RestoreV2:126*
use_locking(*
T0*/
_class%
#!loc:@policy/encoder/hidden_0/bias*
validate_shape(
µ
save/Assign_127Assign#policy/encoder/hidden_0/bias/Adam_1save/RestoreV2:127*
use_locking(*
T0*/
_class%
#!loc:@policy/encoder/hidden_0/bias*
validate_shape(
²
save/Assign_128Assignpolicy/encoder/hidden_0/kernelsave/RestoreV2:128*
use_locking(*
T0*1
_class'
%#loc:@policy/encoder/hidden_0/kernel*
validate_shape(
·
save/Assign_129Assign#policy/encoder/hidden_0/kernel/Adamsave/RestoreV2:129*
use_locking(*
T0*1
_class'
%#loc:@policy/encoder/hidden_0/kernel*
validate_shape(
¹
save/Assign_130Assign%policy/encoder/hidden_0/kernel/Adam_1save/RestoreV2:130*
use_locking(*
T0*1
_class'
%#loc:@policy/encoder/hidden_0/kernel*
validate_shape(
®
save/Assign_131Assignpolicy/encoder/hidden_1/biassave/RestoreV2:131*
use_locking(*
T0*/
_class%
#!loc:@policy/encoder/hidden_1/bias*
validate_shape(
³
save/Assign_132Assign!policy/encoder/hidden_1/bias/Adamsave/RestoreV2:132*
use_locking(*
T0*/
_class%
#!loc:@policy/encoder/hidden_1/bias*
validate_shape(
µ
save/Assign_133Assign#policy/encoder/hidden_1/bias/Adam_1save/RestoreV2:133*
use_locking(*
T0*/
_class%
#!loc:@policy/encoder/hidden_1/bias*
validate_shape(
²
save/Assign_134Assignpolicy/encoder/hidden_1/kernelsave/RestoreV2:134*
use_locking(*
T0*1
_class'
%#loc:@policy/encoder/hidden_1/kernel*
validate_shape(
·
save/Assign_135Assign#policy/encoder/hidden_1/kernel/Adamsave/RestoreV2:135*
use_locking(*
T0*1
_class'
%#loc:@policy/encoder/hidden_1/kernel*
validate_shape(
¹
save/Assign_136Assign%policy/encoder/hidden_1/kernel/Adam_1save/RestoreV2:136*
use_locking(*
T0*1
_class'
%#loc:@policy/encoder/hidden_1/kernel*
validate_shape(
Ų
save/Assign_137Assign1target_network/critic/value/encoder/hidden_0/biassave/RestoreV2:137*
use_locking(*
T0*D
_class:
86loc:@target_network/critic/value/encoder/hidden_0/bias*
validate_shape(
Ü
save/Assign_138Assign3target_network/critic/value/encoder/hidden_0/kernelsave/RestoreV2:138*
use_locking(*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_0/kernel*
validate_shape(
Ų
save/Assign_139Assign1target_network/critic/value/encoder/hidden_1/biassave/RestoreV2:139*
use_locking(*
T0*D
_class:
86loc:@target_network/critic/value/encoder/hidden_1/bias*
validate_shape(
Ü
save/Assign_140Assign3target_network/critic/value/encoder/hidden_1/kernelsave/RestoreV2:140*
use_locking(*
T0*F
_class<
:8loc:@target_network/critic/value/encoder/hidden_1/kernel*
validate_shape(
Ö
save/Assign_141Assign0target_network/critic/value/extrinsic_value/biassave/RestoreV2:141*
use_locking(*
T0*C
_class9
75loc:@target_network/critic/value/extrinsic_value/bias*
validate_shape(
Ś
save/Assign_142Assign2target_network/critic/value/extrinsic_value/kernelsave/RestoreV2:142*
use_locking(*
T0*E
_class;
97loc:@target_network/critic/value/extrinsic_value/kernel*
validate_shape(
Ģ
save/Assign_143Assign+target_network/critic/value/gail_value/biassave/RestoreV2:143*
use_locking(*
T0*>
_class4
20loc:@target_network/critic/value/gail_value/bias*
validate_shape(
Š
save/Assign_144Assign-target_network/critic/value/gail_value/kernelsave/RestoreV2:144*
use_locking(*
T0*@
_class6
42loc:@target_network/critic/value/gail_value/kernel*
validate_shape(

save/Assign_145Assignversion_numbersave/RestoreV2:145*
use_locking(*
T0*!
_class
loc:@version_number*
validate_shape(

save/Assign_146Assignversion_number_1save/RestoreV2:146*
use_locking(*
T0*#
_class
loc:@version_number_1*
validate_shape(

save/Assign_147Assignversion_number_2save/RestoreV2:147*
use_locking(*
T0*#
_class
loc:@version_number_2*
validate_shape(

save/restore_allNoOp^save/Assign^save/Assign_1^save/Assign_10^save/Assign_100^save/Assign_101^save/Assign_102^save/Assign_103^save/Assign_104^save/Assign_105^save/Assign_106^save/Assign_107^save/Assign_108^save/Assign_109^save/Assign_11^save/Assign_110^save/Assign_111^save/Assign_112^save/Assign_113^save/Assign_114^save/Assign_115^save/Assign_116^save/Assign_117^save/Assign_118^save/Assign_119^save/Assign_12^save/Assign_120^save/Assign_121^save/Assign_122^save/Assign_123^save/Assign_124^save/Assign_125^save/Assign_126^save/Assign_127^save/Assign_128^save/Assign_129^save/Assign_13^save/Assign_130^save/Assign_131^save/Assign_132^save/Assign_133^save/Assign_134^save/Assign_135^save/Assign_136^save/Assign_137^save/Assign_138^save/Assign_139^save/Assign_14^save/Assign_140^save/Assign_141^save/Assign_142^save/Assign_143^save/Assign_144^save/Assign_145^save/Assign_146^save/Assign_147^save/Assign_15^save/Assign_16^save/Assign_17^save/Assign_18^save/Assign_19^save/Assign_2^save/Assign_20^save/Assign_21^save/Assign_22^save/Assign_23^save/Assign_24^save/Assign_25^save/Assign_26^save/Assign_27^save/Assign_28^save/Assign_29^save/Assign_3^save/Assign_30^save/Assign_31^save/Assign_32^save/Assign_33^save/Assign_34^save/Assign_35^save/Assign_36^save/Assign_37^save/Assign_38^save/Assign_39^save/Assign_4^save/Assign_40^save/Assign_41^save/Assign_42^save/Assign_43^save/Assign_44^save/Assign_45^save/Assign_46^save/Assign_47^save/Assign_48^save/Assign_49^save/Assign_5^save/Assign_50^save/Assign_51^save/Assign_52^save/Assign_53^save/Assign_54^save/Assign_55^save/Assign_56^save/Assign_57^save/Assign_58^save/Assign_59^save/Assign_6^save/Assign_60^save/Assign_61^save/Assign_62^save/Assign_63^save/Assign_64^save/Assign_65^save/Assign_66^save/Assign_67^save/Assign_68^save/Assign_69^save/Assign_7^save/Assign_70^save/Assign_71^save/Assign_72^save/Assign_73^save/Assign_74^save/Assign_75^save/Assign_76^save/Assign_77^save/Assign_78^save/Assign_79^save/Assign_8^save/Assign_80^save/Assign_81^save/Assign_82^save/Assign_83^save/Assign_84^save/Assign_85^save/Assign_86^save/Assign_87^save/Assign_88^save/Assign_89^save/Assign_9^save/Assign_90^save/Assign_91^save/Assign_92^save/Assign_93^save/Assign_94^save/Assign_95^save/Assign_96^save/Assign_97^save/Assign_98^save/Assign_99
ē3
initNoOp,^GAIL_model/gail_d_estimate/bias/Adam/Assign.^GAIL_model/gail_d_estimate/bias/Adam_1/Assign'^GAIL_model/gail_d_estimate/bias/Assign.^GAIL_model/gail_d_estimate/kernel/Adam/Assign0^GAIL_model/gail_d_estimate/kernel/Adam_1/Assign)^GAIL_model/gail_d_estimate/kernel/Assign,^GAIL_model/gail_d_hidden_1/bias/Adam/Assign.^GAIL_model/gail_d_hidden_1/bias/Adam_1/Assign'^GAIL_model/gail_d_hidden_1/bias/Assign.^GAIL_model/gail_d_hidden_1/kernel/Adam/Assign0^GAIL_model/gail_d_hidden_1/kernel/Adam_1/Assign)^GAIL_model/gail_d_hidden_1/kernel/Assign,^GAIL_model/gail_d_hidden_2/bias/Adam/Assign.^GAIL_model/gail_d_hidden_2/bias/Adam_1/Assign'^GAIL_model/gail_d_hidden_2/bias/Assign.^GAIL_model/gail_d_hidden_2/kernel/Adam/Assign0^GAIL_model/gail_d_hidden_2/kernel/Adam_1/Assign)^GAIL_model/gail_d_hidden_2/kernel/Assign^Variable/Assign^Variable_1/Assign^Variable_2/Assign^action_output_shape/Assign^action_output_shape_1/Assign^action_output_shape_2/Assign^beta1_power/Assign^beta1_power_1/Assign^beta1_power_2/Assign^beta1_power_3/Assign^beta2_power/Assign^beta2_power_1/Assign^beta2_power_2/Assign^beta2_power_3/Assign3^critic/q/q1_encoding/extrinsic_q1/bias/Adam/Assign5^critic/q/q1_encoding/extrinsic_q1/bias/Adam_1/Assign.^critic/q/q1_encoding/extrinsic_q1/bias/Assign5^critic/q/q1_encoding/extrinsic_q1/kernel/Adam/Assign7^critic/q/q1_encoding/extrinsic_q1/kernel/Adam_1/Assign0^critic/q/q1_encoding/extrinsic_q1/kernel/Assign.^critic/q/q1_encoding/gail_q1/bias/Adam/Assign0^critic/q/q1_encoding/gail_q1/bias/Adam_1/Assign)^critic/q/q1_encoding/gail_q1/bias/Assign0^critic/q/q1_encoding/gail_q1/kernel/Adam/Assign2^critic/q/q1_encoding/gail_q1/kernel/Adam_1/Assign+^critic/q/q1_encoding/gail_q1/kernel/Assign:^critic/q/q1_encoding/q1_encoder/hidden_0/bias/Adam/Assign<^critic/q/q1_encoding/q1_encoder/hidden_0/bias/Adam_1/Assign5^critic/q/q1_encoding/q1_encoder/hidden_0/bias/Assign<^critic/q/q1_encoding/q1_encoder/hidden_0/kernel/Adam/Assign>^critic/q/q1_encoding/q1_encoder/hidden_0/kernel/Adam_1/Assign7^critic/q/q1_encoding/q1_encoder/hidden_0/kernel/Assign:^critic/q/q1_encoding/q1_encoder/hidden_1/bias/Adam/Assign<^critic/q/q1_encoding/q1_encoder/hidden_1/bias/Adam_1/Assign5^critic/q/q1_encoding/q1_encoder/hidden_1/bias/Assign<^critic/q/q1_encoding/q1_encoder/hidden_1/kernel/Adam/Assign>^critic/q/q1_encoding/q1_encoder/hidden_1/kernel/Adam_1/Assign7^critic/q/q1_encoding/q1_encoder/hidden_1/kernel/Assign3^critic/q/q2_encoding/extrinsic_q2/bias/Adam/Assign5^critic/q/q2_encoding/extrinsic_q2/bias/Adam_1/Assign.^critic/q/q2_encoding/extrinsic_q2/bias/Assign5^critic/q/q2_encoding/extrinsic_q2/kernel/Adam/Assign7^critic/q/q2_encoding/extrinsic_q2/kernel/Adam_1/Assign0^critic/q/q2_encoding/extrinsic_q2/kernel/Assign.^critic/q/q2_encoding/gail_q2/bias/Adam/Assign0^critic/q/q2_encoding/gail_q2/bias/Adam_1/Assign)^critic/q/q2_encoding/gail_q2/bias/Assign0^critic/q/q2_encoding/gail_q2/kernel/Adam/Assign2^critic/q/q2_encoding/gail_q2/kernel/Adam_1/Assign+^critic/q/q2_encoding/gail_q2/kernel/Assign:^critic/q/q2_encoding/q2_encoder/hidden_0/bias/Adam/Assign<^critic/q/q2_encoding/q2_encoder/hidden_0/bias/Adam_1/Assign5^critic/q/q2_encoding/q2_encoder/hidden_0/bias/Assign<^critic/q/q2_encoding/q2_encoder/hidden_0/kernel/Adam/Assign>^critic/q/q2_encoding/q2_encoder/hidden_0/kernel/Adam_1/Assign7^critic/q/q2_encoding/q2_encoder/hidden_0/kernel/Assign:^critic/q/q2_encoding/q2_encoder/hidden_1/bias/Adam/Assign<^critic/q/q2_encoding/q2_encoder/hidden_1/bias/Adam_1/Assign5^critic/q/q2_encoding/q2_encoder/hidden_1/bias/Assign<^critic/q/q2_encoding/q2_encoder/hidden_1/kernel/Adam/Assign>^critic/q/q2_encoding/q2_encoder/hidden_1/kernel/Adam_1/Assign7^critic/q/q2_encoding/q2_encoder/hidden_1/kernel/Assign/^critic/value/encoder/hidden_0/bias/Adam/Assign1^critic/value/encoder/hidden_0/bias/Adam_1/Assign*^critic/value/encoder/hidden_0/bias/Assign1^critic/value/encoder/hidden_0/kernel/Adam/Assign3^critic/value/encoder/hidden_0/kernel/Adam_1/Assign,^critic/value/encoder/hidden_0/kernel/Assign/^critic/value/encoder/hidden_1/bias/Adam/Assign1^critic/value/encoder/hidden_1/bias/Adam_1/Assign*^critic/value/encoder/hidden_1/bias/Assign1^critic/value/encoder/hidden_1/kernel/Adam/Assign3^critic/value/encoder/hidden_1/kernel/Adam_1/Assign,^critic/value/encoder/hidden_1/kernel/Assign.^critic/value/extrinsic_value/bias/Adam/Assign0^critic/value/extrinsic_value/bias/Adam_1/Assign)^critic/value/extrinsic_value/bias/Assign0^critic/value/extrinsic_value/kernel/Adam/Assign2^critic/value/extrinsic_value/kernel/Adam_1/Assign+^critic/value/extrinsic_value/kernel/Assign)^critic/value/gail_value/bias/Adam/Assign+^critic/value/gail_value/bias/Adam_1/Assign$^critic/value/gail_value/bias/Assign+^critic/value/gail_value/kernel/Adam/Assign-^critic/value/gail_value/kernel/Adam_1/Assign&^critic/value/gail_value/kernel/Assign^global_step/Assign^global_step_1/Assign^global_step_2/Assign^is_continuous_control/Assign^is_continuous_control_1/Assign^is_continuous_control_2/Assign^log_ent_coef/Adam/Assign^log_ent_coef/Adam_1/Assign^log_ent_coef/Assign^memory_size/Assign^memory_size_1/Assign^memory_size_2/Assign ^policy/dense/kernel/Adam/Assign"^policy/dense/kernel/Adam_1/Assign^policy/dense/kernel/Assign"^policy/dense_1/kernel/Adam/Assign$^policy/dense_1/kernel/Adam_1/Assign^policy/dense_1/kernel/Assign"^policy/dense_2/kernel/Adam/Assign$^policy/dense_2/kernel/Adam_1/Assign^policy/dense_2/kernel/Assign)^policy/encoder/hidden_0/bias/Adam/Assign+^policy/encoder/hidden_0/bias/Adam_1/Assign$^policy/encoder/hidden_0/bias/Assign+^policy/encoder/hidden_0/kernel/Adam/Assign-^policy/encoder/hidden_0/kernel/Adam_1/Assign&^policy/encoder/hidden_0/kernel/Assign)^policy/encoder/hidden_1/bias/Adam/Assign+^policy/encoder/hidden_1/bias/Adam_1/Assign$^policy/encoder/hidden_1/bias/Assign+^policy/encoder/hidden_1/kernel/Adam/Assign-^policy/encoder/hidden_1/kernel/Adam_1/Assign&^policy/encoder/hidden_1/kernel/Assign9^target_network/critic/value/encoder/hidden_0/bias/Assign;^target_network/critic/value/encoder/hidden_0/kernel/Assign9^target_network/critic/value/encoder/hidden_1/bias/Assign;^target_network/critic/value/encoder/hidden_1/kernel/Assign8^target_network/critic/value/extrinsic_value/bias/Assign:^target_network/critic/value/extrinsic_value/kernel/Assign3^target_network/critic/value/gail_value/bias/Assign5^target_network/critic/value/gail_value/kernel/Assign^version_number/Assign^version_number_1/Assign^version_number_2/Assign"w