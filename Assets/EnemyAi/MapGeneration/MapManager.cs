﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapManager : MonoBehaviour
{
    public GameObject obstaclePrefab;
    public GameObject trailObstaclePrefab;
    public GameObject motorObstaclePrefab;
    public GameObject borderPrefab;
    public Transform mapPlane;

    public float mapScale;

    public Transform[] players;

    private Transform[] obstacles;

    private const float CENTER_SIZE_HALF = 5f;

    private const int NUMBER_OF_OBSTACLES = 240;
    private const int NUMBER_OF_TRAIL_OBSTACLES = 300;
    private const int NUMBER_OF_MOTOR_OBSTACLES = 60;
    private const int TOTAL_OBSTACLES = NUMBER_OF_OBSTACLES + NUMBER_OF_MOTOR_OBSTACLES + NUMBER_OF_TRAIL_OBSTACLES;

    private const float OFFSET = 10f;

    void Start()
    {
        obstacles = new Transform[TOTAL_OBSTACLES];
        //set map scale
        mapPlane.localScale = new Vector3(mapScale / 2f, mapScale / 2f, mapScale / 2f);
        mapPlane.localPosition = new Vector3(0, -mapScale / 2f, 0);
        //create obstacles and borders
        initBorders();
        initObstacles();

        ResetMap();
    }

    private void initObstacles()
    {
        Transform parent = new GameObject().GetComponent<Transform>();
        parent.name = "Obstacles";
        parent.parent = transform.parent;
        parent.localPosition = Vector3.zero;

        for (int i = 0; i < NUMBER_OF_OBSTACLES; i++) 
        {
            obstacles[i] = Instantiate(obstaclePrefab).GetComponent<Transform>();
            obstacles[i].parent = parent;
        }
        for (int i = NUMBER_OF_OBSTACLES; i < NUMBER_OF_OBSTACLES + NUMBER_OF_MOTOR_OBSTACLES; i++) 
        {
            obstacles[i] = Instantiate(motorObstaclePrefab).GetComponent<Transform>();
            obstacles[i].parent = parent;
        }
        for (int i = NUMBER_OF_OBSTACLES + NUMBER_OF_MOTOR_OBSTACLES; i < NUMBER_OF_OBSTACLES + NUMBER_OF_MOTOR_OBSTACLES + NUMBER_OF_TRAIL_OBSTACLES; i++) 
        {
            obstacles[i] = Instantiate(trailObstaclePrefab).GetComponent<Transform>();
            obstacles[i].parent = parent;
        }
    }

    private void initBorders()
    {
        Transform parent = new GameObject().GetComponent<Transform>();
        parent.name = "MapBorders";
        parent.parent = transform.parent;
        parent.localPosition = Vector3.zero;

        Transform up = Instantiate(borderPrefab, parent).GetComponent<Transform>();
        up.name = "UpBorder";
        Transform down = Instantiate(borderPrefab, parent).GetComponent<Transform>();
        down.name = "DownBorder";
        Transform left = Instantiate(borderPrefab, parent).GetComponent<Transform>();
        left.name = "LeftBorder";
        Transform right = Instantiate(borderPrefab, parent).GetComponent<Transform>();
        right.name = "DownBorder";

        Vector3 upPosition = new Vector3(mapScale / 2f, 0, 0);
        Vector3 downPosition = new Vector3(-mapScale / 2f, 0, 0);
        Vector3 leftPosition = new Vector3(0, 0, mapScale / 2f);
        Vector3 rightPosition = new Vector3(0, 0, -mapScale / 2f);

        up.localPosition = upPosition;
        down.localPosition = downPosition;
        left.localPosition = leftPosition;
        right.localPosition = rightPosition;

        Vector3 upScale = new Vector3(10f, mapScale, mapScale);
        Vector3 downScale = new Vector3(10f, mapScale, mapScale);
        Vector3 leftScale = new Vector3(mapScale, mapScale, 10f);
        Vector3 rightScale = new Vector3(mapScale, mapScale, 10f);

        up.localScale = upScale;
        down.localScale = downScale;
        left.localScale = leftScale;
        right.localScale = rightScale;
    }

    public void ResetMap()
    {
        mapPlane.GetComponent<Planet>().GeneratePlanet();
        //randomize obstacle positions
        randomizeObstacles();
    }

    private void randomizeObstacles()
    {
        DeactivateObstacles();

        for (int i = 0; i < TOTAL_OBSTACLES; i++)
        {
            float newX = 0f;
            float newZ = 0f;

            //x
            if (Random.value > 0.5f)
            {
                newX = Random.Range(-mapScale / 2f + OFFSET, -CENTER_SIZE_HALF * 2);
            }
            else
            {
                newX = Random.Range(CENTER_SIZE_HALF * 2, mapScale / 2f - OFFSET);
            }
            //z
            if (Random.value > 0.5f)
            {
                newZ = Random.Range(-mapScale / 2f + OFFSET, -CENTER_SIZE_HALF * 2);
            }
            else{
                newZ = Random.Range(CENTER_SIZE_HALF * 2, mapScale / 2f - OFFSET);
            }
            
            float newY = 0f;

            obstacles[i].localPosition = new Vector3(newX, newY, newZ);
        }

        //randomize agent positions while obstacles are inactive
        randomizePlayers();

        ActivateObstacles();
    }

    private void randomizePlayers()
    {
        for (int i = 0; i < players.Length; i++)
        {
            float newX = Random.Range(-CENTER_SIZE_HALF / 2f, CENTER_SIZE_HALF / 2f);
            float newZ = Random.Range(-CENTER_SIZE_HALF / 2f, CENTER_SIZE_HALF / 2f);
            float newY = 100f;

            RaycastHit hit;
            Physics.Raycast(new Vector3(newX, mapScale * 2, newZ), Vector3.down, out hit);
            if (hit.collider != null)
            {
                newY = hit.point.y + OFFSET / 2f;
                //Debug.Log(newY);
            }

            Rigidbody rb = players[i].GetComponentInChildren<Rigidbody>();
            rb.ResetInertiaTensor();
            rb.velocity = Vector3.zero;
            rb.transform.localRotation = Quaternion.Euler(Vector3.zero);

            players[i].localPosition = new Vector3(newX, newY, newZ);
        }
    }

    private void DeactivateObstacles()
    {
        for (int i = 0; i < NUMBER_OF_OBSTACLES; i++)
        {
            obstacles[i].gameObject.SetActive(false);
        }
    }
    private void ActivateObstacles()
    {
        for (int i = 0; i < NUMBER_OF_OBSTACLES; i++)
        {
            obstacles[i].gameObject.SetActive(true);
        }
    }
}
