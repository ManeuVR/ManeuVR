﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Persistent object to hold the shape(noise layers) data for one planet type
 */
[CreateAssetMenu()]
public class ShapeSettings : ScriptableObject
{
    public float planetRadius = 1f;
    public NoiseLayer[] noiseLayers;

    [System.Serializable]
    public class NoiseLayer
    {
        public bool enabled = true;
        public bool useFirstLayerAsMask;
        public bool useFirstLayerAsInverseMask;
        public NoiseSettings noiseSettings;
    }
}
