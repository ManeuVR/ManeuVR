﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Implements simple noise
 */
public class SimpleNoiseFilter : INoiseFilter
{
    NoiseSettings.SimpleNoiseSettings settings;
    Noise noise;

    public SimpleNoiseFilter(NoiseSettings.SimpleNoiseSettings settings, int seed)
    {
        noise = new Noise(seed);
        this.settings = settings;
    }

    public float Evaluate(Vector3 point)
    {
        float noiseValue = 0;
        float frequency = settings.baseRoughness;
        float amplitude = 1f;

        for(int i=0; i<settings.numLayers; i++)
        {
            float v = noise.Evaluate(point * frequency + settings.center);
            noiseValue += (v + 1) * 0.5f * amplitude;
            frequency *= settings.roughness;
            amplitude *= settings.persistence;
        }

        noiseValue = noiseValue - settings.minValue;
        return noiseValue * settings.strength;
    }
}
