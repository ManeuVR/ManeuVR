﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Initializes everything needed to build a planet
 */
public class Planet : MonoBehaviour
{
    public Material planetMaterial;
    public bool randomSeed = false;
    public int seed = 0;

    [Range(2, 256)]
    public int resolution = 10;
    public bool autoUpdate = true;

    public ShapeSettings shapeSettings;
    [HideInInspector]
    public bool shapeSettingsFoldout;

    ShapeGenerator shapeGenerator;

    [SerializeField, HideInInspector]
    MeshFilter[] meshFilters;
    [SerializeField, HideInInspector]
    TerrainFace[] terrainFaces;
    [SerializeField, HideInInspector]
    MeshCollider[] meshColliders;
    [SerializeField, HideInInspector]
    GameObject[] faceObjects;

    public void Awake()
    {
        GeneratePlanet();
    }

    /**
     * Sets seeds for all generators using noise
     * Generates 6 cube faces
     */
    void Initialize()
    {
        if (randomSeed)
        {
            seed = Random.Range(0, 100000);
        }
        shapeGenerator = new ShapeGenerator(seed);

        shapeGenerator.UpdateSettings(shapeSettings);

        if(faceObjects==null || faceObjects.Length == 0)
        {
            faceObjects = new GameObject[1];
        }
        if (meshFilters == null || meshFilters.Length == 0)
        {
            meshFilters = new MeshFilter[1];
        }
        if(meshColliders == null || meshColliders.Length == 0)
        {
            meshColliders = new MeshCollider[1];
        }
        terrainFaces = new TerrainFace[1];
        

        Vector3[] directions = { Vector3.up, Vector3.down, Vector3.left,
            Vector3.right, Vector3.forward, Vector3.back };
        
        for(int i = 0; i < 1; i++)
        {
            if(faceObjects[i] == null)
            {
                faceObjects[i] = new GameObject("face" + i.ToString());
                faceObjects[i].transform.parent = transform;
                faceObjects[i].AddComponent<MeshRenderer>();
            }
            if (meshFilters[i] == null)
            {
                meshFilters[i] = faceObjects[i].AddComponent<MeshFilter>();
                meshFilters[i].mesh = new Mesh();
                faceObjects[i].GetComponent<MeshRenderer>().material = planetMaterial;
                
            }
            if(meshColliders[i] == null)
            {
                meshColliders[i] = faceObjects[i].AddComponent<MeshCollider>();
                //meshColliders[i] = new MeshCollider();
            }

            terrainFaces[i] = new TerrainFace(shapeGenerator, meshFilters[i].mesh, resolution, directions[i], meshColliders[i]);
        }
    }

    public void GeneratePlanet()
    {
        Initialize();
        GenerateMesh();
    }

    public void OnShapeSettingsUpdated()
    {
        if (autoUpdate)
        {
            GeneratePlanet();
        }
    }

    public void OnColorSettingsUpdated()
    {
        if (autoUpdate)
        {
            Initialize();
        }
    }

    void GenerateMesh()
    {
        foreach(TerrainFace face in terrainFaces)
        {
            face.ConstructMesh();
        }

    }
}
