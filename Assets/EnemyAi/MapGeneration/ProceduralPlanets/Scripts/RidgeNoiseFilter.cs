﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RidgeNoiseFilter : INoiseFilter
{
    NoiseSettings.RidgeNoiseSettings settings;
    Noise noise;

    public RidgeNoiseFilter(NoiseSettings.RidgeNoiseSettings settings, int seed)
    {
        noise = new Noise(seed);
        this.settings = settings;
    }

    public float Evaluate(Vector3 point)
    {
        float noiseValue = 0;
        float frequency = settings.baseRoughness;
        float amplitude = 1f;
        float weight = 1f;

        for (int i = 0; i < settings.numLayers; i++)
        {
            float v = 1f - Mathf.Abs(noise.Evaluate(point * frequency + settings.center));
            v *= v;
            v *= weight;
            weight = Mathf.Clamp01(v * settings.weigthMultiplier);

            noiseValue += v * amplitude;
            frequency *= settings.roughness;
            amplitude *= settings.persistence;
        }

        noiseValue = noiseValue - settings.minValue;
        return noiseValue * settings.strength;
    }
}
