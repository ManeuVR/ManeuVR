﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Holds the min and max height for one planet
 */
public class MinMaxPoint
{
    public float Min { get; private set; }
    public float Max { get; private set; }

    public MinMaxPoint()
    {
        Min = float.MaxValue;
        Max = float.MinValue;
    }

    public void AddValue(float v)
    {
        if (v < Min)
        {
            Min = v;
        }
        if (v > Max)
        {
            Max = v;
        }
    }
}
