﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShapeGenerator
{
    int seed;

    ShapeSettings settings;
    INoiseFilter[] noiseFilters;
    public MinMaxPoint elevationMinMax;

    public ShapeGenerator(int seed)
    {
        this.seed = seed;
    }

    public void UpdateSettings(ShapeSettings settings)
    {
        this.settings = settings;
        noiseFilters = new INoiseFilter[settings.noiseLayers.Length];
        for(int i=0; i<noiseFilters.Length; i++)
        {
            noiseFilters[i] = NoiseFilterFactory.CreateNoiseFilter(settings.noiseLayers[i].noiseSettings, seed);
        }
        elevationMinMax = new MinMaxPoint();
    }

    public float CalculateUnscaledElevation(Vector3 pointOnUnitSphere)
    {
        float firstLayerValue = 0;
        float elevation = 0;

        if (noiseFilters.Length > 0)
        {
            firstLayerValue = noiseFilters[0].Evaluate(pointOnUnitSphere);
            if (settings.noiseLayers[0].enabled)
            {
                elevation = firstLayerValue;
            }
        }

        for(int i = 1; i < noiseFilters.Length; i++)
        {
            if (settings.noiseLayers[i].enabled)
            {
                float mask = 1f;
                if (settings.noiseLayers[i].useFirstLayerAsMask) mask = firstLayerValue;
                if (settings.noiseLayers[i].useFirstLayerAsInverseMask && firstLayerValue > 0) mask = 0;

                elevation += noiseFilters[i].Evaluate(pointOnUnitSphere) * mask;
                
            }
        }
        elevationMinMax.AddValue(elevation);

        return elevation;
    }

    public float GetScaledElevation(float unscaledElevation)
    {
        float elevation = Mathf.Max(0f, unscaledElevation);
        elevation = settings.planetRadius * (1 + elevation);

        return elevation;
    }
}
