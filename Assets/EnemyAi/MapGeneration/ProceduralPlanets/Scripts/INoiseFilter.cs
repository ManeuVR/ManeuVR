﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Requires Evaluate implementation
 */
public interface INoiseFilter
{
    float Evaluate(Vector3 point);
}
