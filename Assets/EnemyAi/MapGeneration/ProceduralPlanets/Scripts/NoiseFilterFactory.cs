﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Design pattern to simplify using multiple noise types
 */
public static class NoiseFilterFactory 
{
    public static INoiseFilter CreateNoiseFilter(NoiseSettings settings, int seed)
    {
        switch (settings.filterType)
        {
            case NoiseSettings.FilterType.Simple:
                return new SimpleNoiseFilter(settings.simpleNoiseSettings, seed);
            case NoiseSettings.FilterType.Ridge:
                return new RidgeNoiseFilter(settings.ridgeNoiseSettings, seed);
        }
        return null;
    }
}
