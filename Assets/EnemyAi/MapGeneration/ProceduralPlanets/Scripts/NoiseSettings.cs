﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Holds all noise used for a single planet and their parameters
 */
[System.Serializable]
public class NoiseSettings
{
    public enum FilterType {Simple, Ridge};
    public FilterType filterType;

    [ConditionalHide("filterType",0)]
    public SimpleNoiseSettings simpleNoiseSettings;
    [ConditionalHide("filterType", 1)]
    public RidgeNoiseSettings ridgeNoiseSettings;

    [System.Serializable]
    public class SimpleNoiseSettings
    {
        public float strength = 1f;
        [Range(1, 8)]
        public int numLayers = 1;
        public float baseRoughness = 1f;
        public float roughness = 2f;
        public float persistence = 0.5f;
        public Vector3 center;
        public float minValue;
    }

    [System.Serializable]
    public class RidgeNoiseSettings : SimpleNoiseSettings
    {
        public float weigthMultiplier = 0.8f;
    }

}
