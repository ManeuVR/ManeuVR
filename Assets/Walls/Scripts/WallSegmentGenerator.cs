﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TargetFollow))]
public class WallSegmentGenerator : MonoBehaviour
{

    public Transform bikeRearPoint;
    public GameObject wallSegmentPrefab;
    public GameObject trailPrefab;

    public float nonPausedLen = 10f;
    public float pausedLen = 2f;
    private float totalDistance = 0f;

    private Transform trailParent;
    private GameObject currentTrail;

    private Transform wallSegmentParent;
    private Vector3 lastSegmentPosition;

    private float minDistance;

    private bool isTracing = true;
    private bool isAllowed = false;
    private bool isPaused = false;

    //use for status active indicator
    public bool statusActive { get; set; }

    [SerializeField]
    private RacerController motorController;

    private float currentDistance;
    private Vector3 currentTargetPosition;
    private Vector3 lastTargetPosition;
    private GameObject segment;

    private bool isFirstSegment = true;
    private bool isSecondSegment = false;

    [SerializeField]
    private float toggleCooldown = 2f;
    public float currentToggleCooldown { get; set; } = 0f;

    private void Start()
    {
        GetComponent<TargetFollow>().SetTarget(bikeRearPoint);

        minDistance = wallSegmentPrefab.transform.localScale.z;
        wallSegmentParent = new GameObject().GetComponent<Transform>();
        wallSegmentParent.transform.name = "Walls";
        trailParent = new GameObject().GetComponent<Transform>();
        trailParent.transform.name = "Trails";

        //offset = trailPrefab.transform.localScale.z;

        motorController.goingForwardsEvent += GoingForwardsHandle;
        motorController.goingBackwardsEvent += GoingBackwardsHandle;
        motorController.trailToggleEvent += ToggleTrail;
    }

    void Update()
    {
        statusActive = false;
        if (currentToggleCooldown >= 0) currentToggleCooldown -= Time.deltaTime;

        if (!isAllowed || !isTracing) return;

        CalculateDistances();

        if(isPaused && totalDistance >= pausedLen)
        {
            isPaused = !isPaused;
            totalDistance = 0f;
            if(isTracing && isAllowed) StartTrailing();
        }
        else if(!isPaused && totalDistance >= nonPausedLen)
        {
            isPaused = !isPaused;
            totalDistance = 0f;
            StopTrailing();
            currentDistance = (currentTargetPosition - lastSegmentPosition).magnitude;
        }

        if (!isPaused && currentDistance >= minDistance / 2f)  
        {
            statusActive = true;
            PlaceSegment();
        }
        if (isFirstSegment)
        {
            if (segment != null) Destroy(segment);
            isFirstSegment = false;
            isSecondSegment = true;
        }
        else if (isSecondSegment)
        {
            isSecondSegment = false;
        }
    }

    private void CalculateDistances()
    {
        currentTargetPosition = bikeRearPoint.position;
        currentDistance = (currentTargetPosition - lastSegmentPosition).magnitude;
        if (isTracing) totalDistance += (currentTargetPosition - lastTargetPosition).magnitude;
        lastTargetPosition = currentTargetPosition;
    }

    private void PlaceSegment()
    {
        segment = GameObject.Instantiate(wallSegmentPrefab, wallSegmentParent);

        Vector3 newPosition;
        if (isSecondSegment)
        {
            newPosition = Vector3.MoveTowards(lastSegmentPosition, bikeRearPoint.position, minDistance / 2f);
        }
        else
        {
            newPosition = Vector3.MoveTowards(lastSegmentPosition, bikeRearPoint.position, minDistance / 2f);
        }

        segment.transform.position = newPosition;

        if(!isFirstSegment) segment.transform.forward = (bikeRearPoint.position - lastSegmentPosition);

        lastSegmentPosition = segment.transform.position;
    }

    public void GoingForwardsHandle()
    {
        isAllowed = true;
        if(isTracing) StartTrailing();
    }

    public void GoingBackwardsHandle()
    {
        isAllowed = false;
        if(isTracing) StopTrailing();
    }

    public void StopTrailing()
    {
        FinishCurrentTrail();
    }

    public void StartTrailing()
    {
        StopTrailing();
        lastSegmentPosition = bikeRearPoint.position;
        isFirstSegment = true;
        isSecondSegment = false;
        CreateNewTrail();
    }

    private void CreateNewTrail()
    {
        //Debug.Log("Create new trail");
        currentTrail = GameObject.Instantiate(trailPrefab, trailParent);
        currentTrail.GetComponent<ICustomTrail>().SetTarget(bikeRearPoint);
    }

    private void FinishCurrentTrail()
    {
        //Debug.Log("Finish current trail");
        if (currentTrail == null) return;

        currentTrail.GetComponent<ICustomTrail>().emitting = false;
        currentTrail = null;
    }

    private void OnEnable()
    {
        motorController.goingForwardsEvent += GoingForwardsHandle;
        motorController.goingBackwardsEvent += GoingBackwardsHandle;
        motorController.trailToggleEvent += ToggleTrail;

    }

    private void OnDisable()
    {
        motorController.goingForwardsEvent -= GoingForwardsHandle;
        motorController.goingBackwardsEvent -= GoingBackwardsHandle;
        motorController.trailToggleEvent -= ToggleTrail;

        StopTrailing();
    }

    private void ToggleTrail()
    {
        if (!isAllowed || currentToggleCooldown > 0) return;

        currentToggleCooldown = toggleCooldown;

        isTracing = !isTracing;
        totalDistance = 0f;
        isPaused = false;
        if (isTracing)
        {
            StartTrailing();
        }
        else
        {
            StopTrailing();
        }
    }
}
