﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class CustomTrailRotated : MonoBehaviour, ICustomTrail
{
    private bool _emitting = true;
    public bool emitting { 
        get => _emitting;
        set
        {
            _emitting = value;
            if (!_emitting && currentPointIndex > 1) AddSegment();
        }
    }

    public float minStep = 0.1f;
    public float height = 0.5f;
    public Material[] materials;

    private Mesh mesh;
    private MeshFilter meshFilter;
    private MeshRenderer meshRenderer;

    private Transform target;
    private Vector3 lastPosition;
    private Vector3 currentPosition;
    private float currentDistance = 0f;
    private int currentPointIndex = 0;

    private bool isInit = false;

    private void Start()
    {
        meshFilter = GetComponent<MeshFilter>();
        meshRenderer = GetComponent<MeshRenderer>();
        mesh = new Mesh();
        meshFilter.mesh = mesh;

        meshRenderer.materials = materials;

        if (emitting) InitSegment();
    }

    private void Update()
    {
        if (!emitting) return;

        if (!isInit)
        {
            InitSegment();
            return;
        }

        currentPosition = target.transform.position;
        currentDistance += (currentPosition - lastPosition).magnitude;
        if (currentDistance >= minStep)
        {
            AddSegment();
        }
        lastPosition = currentPosition;
    }

    private void InitSegment()
    {
        currentPosition = target.transform.position;
        lastPosition = currentPosition;

        Vector3 offset = target.up.normalized * height;
        Vector3 pointUp = currentPosition + offset;
        Vector3 pointDown = currentPosition - offset;
        Vector3[] newVertices = new Vector3[2];

        newVertices[0] = pointDown;
        newVertices[1] = pointUp;
        mesh.vertices = newVertices;

        currentDistance = 0;
        currentPointIndex++;
        isInit = true;
    }

    private void AddSegment()
    {
        Vector3 offset = target.up.normalized * height;
        Vector3 pointUp = currentPosition + offset;
        Vector3 pointDown = currentPosition - offset;
        Vector3[] oldVertices = mesh.vertices;
        int[] oldTriangles = mesh.triangles;
        int oldVerticesLength = oldVertices.Length;
        int oldTrianglesLength = oldTriangles.Length;

        Vector3[] newVertices = new Vector3[oldVertices.Length + 2];
        int[] newTriangles = new int[oldTriangles.Length + 6];
        for (int i = 0; i < oldVerticesLength; i++)
        {
            newVertices[i] = oldVertices[i];
        }
        for (int i = 0; i < oldTrianglesLength; i++)
        {
            newTriangles[i] = oldTriangles[i];
        }

        newVertices[oldVerticesLength] = pointDown;
        newVertices[oldVerticesLength + 1] = pointUp;

        int index = 2 * currentPointIndex;

        newTriangles[oldTrianglesLength] = index - 2;
        newTriangles[oldTrianglesLength + 1] = index;
        newTriangles[oldTrianglesLength + 2] = index + 1;

        newTriangles[oldTrianglesLength + 3] = index - 2;
        newTriangles[oldTrianglesLength + 4] = index + 1;
        newTriangles[oldTrianglesLength + 5] = index - 1;

        mesh.vertices = newVertices;
        mesh.triangles = newTriangles;
        mesh.RecalculateNormals();

        currentDistance = 0;
        currentPointIndex++;
    }

    public void SetTarget(Transform target)
    {
        this.target = target;
    }
}
