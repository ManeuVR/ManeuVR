﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICustomTrail 
{
    bool emitting { get; set; }
    void SetTarget(Transform target);
}
