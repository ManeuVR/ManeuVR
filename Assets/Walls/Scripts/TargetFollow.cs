﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetFollow : MonoBehaviour
{
    private Transform target;

    public TargetFollow(Transform target)
    {
        this.target = target;
    }

    void Update()
    {
        this.transform.position = target.transform.position;
    }

    public void SetTarget(Transform target)
    {
        this.target = target;
    }
}
