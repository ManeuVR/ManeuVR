﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

[RequireComponent(typeof(Slider))]
public class MixerSlider : MonoBehaviour
{
    public AudioMixer mixer;
    public string mixerParameter;
    public float maxAttenuation = 0.0f;
    public float minAttenuation = -80.0f;

    private Slider slider;

    void Awake()
    {
        slider = GetComponent<Slider>();
        mixer.GetFloat(mixerParameter, out var value);
        slider.value = (value - minAttenuation) / (maxAttenuation - minAttenuation);
        slider.onValueChanged.AddListener(SliderValueChange);
    }

    
    void SliderValueChange(float value)
    {
        mixer.SetFloat(mixerParameter, minAttenuation + value * (maxAttenuation - minAttenuation));
    }
}
