﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour
{
    [SerializeField]
    private GameObject playerSpawnEffect;

    public GameObject SpawnPlayer(GameObject player)
    {
        GameObject instPlayer = Instantiate(player);
        instPlayer.transform.position = transform.position;
        //random rotation
        instPlayer.transform.rotation = Quaternion.Euler(0, Random.value * 360, 0);

        if (playerSpawnEffect != null)
        {
            Instantiate(playerSpawnEffect).transform.position = transform.position;
        }
        return instPlayer;
    }
}
