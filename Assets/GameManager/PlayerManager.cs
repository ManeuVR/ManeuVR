﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    [SerializeField]
    GameObject disableTarget;
    public bool isHuman = false;

    CollisionHandling collisionHandling;
    GameManager gameManager;

    private void Start()
    {
        gameManager = GameManager.singleton;
        collisionHandling = GetComponentInChildren<CollisionHandling>();
        collisionHandling.deathCollisionEvent += PlayerDied;
    }

    private void PlayerDied()
    {
        if (isHuman)
        {
            gameManager.HumanDied();
        }
        else
        {
            gameManager.BotDied();
        }
        GetComponent<MovementObserverGenerator>().enabled = false;
        disableTarget.SetActive(false);
    }

    private void OnDisable()
    {
        collisionHandling.deathCollisionEvent -= PlayerDied;
    }
}
