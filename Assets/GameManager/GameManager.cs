﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Valve.VR;

public class GameManager : MonoBehaviour
{
    public bool isVR = true;
    [SerializeField]
    public GameObject vrHumanPrefab;
    [SerializeField]
    public GameObject keyboardHumanPrefab;
    [SerializeField]
    private GameObject[] botPrefabs;

    [SerializeField]
    private GameObject winEffect;

    private SpawnPoint[] spawnPoints;

    [Range(1,7)]
    public int numberOfBots = 4;
    private int numOfPlayers = 0;

    private GameObject humanPlayer;
    private Vector3 lastPosition;

    public static GameManager singleton;

    private int[] randomShuffle;
    private bool gameOver = false;
    private void Awake()
    {
        if (singleton == null)
        {
            singleton = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(this);
        }
    }

    public void BotDied()
    {
        numOfPlayers--;
        Debug.Log("Bots alive: " + (numOfPlayers - 1));
        if (numOfPlayers <= 1 && !gameOver)
        {
            gameOver = true;
            HumanWon();
        }
    }

    public void HumanDied()
    {
        if (gameOver) return;
        gameOver = true;
        Debug.Log("Human player lost. The bots have taken over the island.");
        DisableHuman();
        GameOver();
    }

    private void HumanWon()
    {
        if (winEffect != null)
        {
            GameObject fireworks = Instantiate(winEffect);
            fireworks.transform.position = lastPosition;
        }
        Debug.Log("The human won and saved us from judgement day");
        DisableHuman();
        GameOver();
    }

    private void FixedUpdate()
    {
        if (humanPlayer != null) lastPosition = humanPlayer.transform.position;
    }

    private void DisableHuman()
    {
        humanPlayer.GetComponentInChildren<CollisionHandling>().enabled = false;
        humanPlayer.GetComponentInChildren<RacerController>().enabled = false;
        humanPlayer.GetComponentInChildren<Rigidbody>().isKinematic = true;
    }

    private void GameOver()
    {
        numOfPlayers = 0;

        DisableRigidbodies();

        StartCoroutine(StopGameScene());
    }

    private void DisableRigidbodies()
    {
        Rigidbody[] rbodies = FindObjectsOfType<Rigidbody>();
        CollisionHandling[] collisionHandlers = FindObjectsOfType<CollisionHandling>();
        RacerController[] racerControllers = FindObjectsOfType<RacerController>();

        foreach(Rigidbody r in rbodies)
        {
            r.isKinematic = true;
        }
        foreach(CollisionHandling h in collisionHandlers)
        {
            h.enabled = false;
        }
        foreach(RacerController c in racerControllers)
        {
            c.enabled = false;
        }

        MovementObserverGenerator[] movementObservers = FindObjectsOfType<MovementObserverGenerator>();
        foreach(MovementObserverGenerator m in movementObservers)
        {
            m.enabled = false;
        }
        PlayerMovementObserver[] playerMovements = FindObjectsOfType<PlayerMovementObserver>();
        foreach(PlayerMovementObserver p in playerMovements)
        {
            p.enabled = false;
        }
    }

    IEnumerator StopGameScene()
    {
        //TODO
        //secondary camera on player position
        Debug.Log("Finishing up...");
        yield return new WaitForSeconds(6f);
        //go to menu
        SteamVR_LoadLevel.Begin("Main Menu");
        //SceneManager.LoadScene(0, LoadSceneMode.Single);
    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        //Debug.Log("OnSceneLoaded: " + scene.name);
        //Debug.Log(mode);
        gameOver = false;

        switch (scene.name.ToLower())
        {
            case "easterisland":
                numOfPlayers = numberOfBots + 1;
                StartCoroutine(SpawnPlayers());
                break;
            case "main menu":
                spawnPoints = null;
                humanPlayer = null;
                break;
        }
    }

    IEnumerator SpawnPlayers()
    {
        Debug.Log("Spawning players...");
        spawnPoints = FindObjectsOfType<SpawnPoint>();
        Shuffle();
        SpawnHuman();
        SpawnBots();
        yield return null;
    }

    private void Shuffle()
    {
        randomShuffle = new int[spawnPoints.Length];
        for(int i=0; i < spawnPoints.Length; i++)
        {
            randomShuffle[i] = i;
        }

        for (int i = 0; i < spawnPoints.Length; i++) 
        {
            int index = Random.Range(i, spawnPoints.Length);
            int temp = randomShuffle[i];
            randomShuffle[i] = randomShuffle[index];
            randomShuffle[index] = temp;
        }
    }

    private void SpawnHuman()
    {
        if (isVR)
        {
            humanPlayer = spawnPoints[randomShuffle[0]].SpawnPlayer(vrHumanPrefab);
        }
        else
        {
            humanPlayer = spawnPoints[randomShuffle[0]].SpawnPlayer(keyboardHumanPrefab);
        }
    }

    private void SpawnBots()
    {
        for(int i = 1; i <= numberOfBots; i++)
        {
            spawnPoints[randomShuffle[i]].SpawnPlayer(botPrefabs[Random.Range(0, 3)]);
        }
    }
}
