# Project repository for course "Software Design Project" @ FER

ManeuVR is a computer game designed for VR.  
The player takes the role of one motorcyclist on Easter Island.
While maneuvering around the island, motorcycles leave trails behind them.
If any player touches that trail, they die.  
Last player standing wins.  

\
![](./Screenshots/PrettyScene.png)
\
![](./Screenshots/PrettySea.png)
